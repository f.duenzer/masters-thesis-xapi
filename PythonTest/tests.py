import unittest
from xapi.core.xapi_actor import xAPI_Actor
from xapi.core.xapi_body import xAPI_Body
from xapi.definitions.xapi_definitions import xAPI_Definitions
from xapi.core.xapi_statements import xAPI_Statements
from xapi.definitions.xapi_activities_lms import xAPI_Activities_Lms
from xapi.definitions.xapi_activities_tagformance import xAPI_Activities_Tagformance
from xapi.definitions.xapi_activities_observation import xAPI_Activities_Observation
from xapi.definitions.xapi_verbs_projectjupyter import xAPI_Verbs_ProjectJupyter
from xapi.definitions.xapi_verbs_uhfreader import xAPI_Verbs_UhfReader
from xapi.definitions.xapi_verbs_gestures import xAPI_Verbs_Gestures
from xapi.definitions.xapi_context_eyetracking_extensions import xAPI_Context_EyeTracking_Extensions
from xapi.definitions.xapi_context_gestures_extensions import xAPI_Context_Gestures_Extensions
import tincan

class Tests(unittest.TestCase):
    def test_activities(self):
        self.assertEqual('A chapter of a book.',
                         xAPI_Activities_Lms().book_chapter.descriptions['en-US'])
        self.assertEqual('tagformance',
                         xAPI_Activities_Tagformance().experiment.context)
        self.assertEqual('behält stehts den Überblick',
                         xAPI_Activities_Observation().subscores.behaelt_stehts_den_Ueberblick.names['de-DE'])

    def test_verbs(self):
        self.assertEqual('Indicates, that an actor run some programming context. Typically a Jupyter Notebook Code Cell.',
                         xAPI_Verbs_ProjectJupyter().run.descriptions['en-US'])
        self.assertEqual('uhfReader',
                         xAPI_Verbs_UhfReader().contextName)
        self.assertEqual('schüttelte',
                         xAPI_Verbs_Gestures().shaked.names['de-DE'])

    def test_extensions(self):
        exts = xAPI_Context_EyeTracking_Extensions().result.duration(5).numberOfBlinking(2)
        self.assertEqual(2, len(exts.extensions))
        self.assertEqual(5, exts.extensions[0][1])
        self.assertEqual('Hand', xAPI_Context_Gestures_Extensions().activity.hand('right').extensions[0][0].names['de-DE'])

    def test_definitions(self):
        self.assertIsNotNone(xAPI_Definitions.virtualReality.activities.touchpad)
        self.assertEqual('German', xAPI_Definitions.systemControl.extensions.result.language('German').extensions[0][1])
        self.assertEqual('An actor dragged and pulled an object.', xAPI_Definitions.vrRfidChamber.verbs.dragPull.descriptions['en-US'])

    def test_statement(self):
        statement = xAPI_Statements.createStatementExt(
            'http://example.com',
            xAPI_Actor('Name', 'Email@example.com'),
            xAPI_Definitions.generic.verbs.clicked,
            xAPI_Definitions.generic.activities.mouse,
            xAPI_Definitions.generic.extensions.activity.mouseButton('left').mousePosition('100,100'),
            tincan.Score(), True, True)
        
        self.assertEqual('mailto:Email@example.com', statement.actor.mbox)
        self.assertEqual('http://example.com/generic/verbs/clicked', statement.verb.id)
        self.assertEqual('A mouse to use UI or interact with the game in another way.',
                         statement.object.definition.description['en-US'])
        self.assertEqual(True, statement.result.success)
        self.assertEqual('{\'http://example.com/generic/extensions/activity/mouseButton\': \'left\','
                         + ' \'http://example.com/generic/extensions/activity/mousePosition\': \'100,100\'}',
                         str(statement.object.definition.extensions))

if __name__ == '__main__':
    unittest.main()
