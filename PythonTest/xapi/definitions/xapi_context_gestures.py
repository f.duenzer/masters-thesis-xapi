from ..core.xapi_context import xAPI_Context
from .xapi_verbs_gestures import xAPI_Verbs_Gestures
from .xapi_activities_gestures import xAPI_Activities_Gestures
from .xapi_context_gestures_extensions import xAPI_Context_Gestures_Extensions

"""Provides the definitions of the context gestures as public properties."""
class xAPI_Context_Gestures(xAPI_Context):
    verbs = xAPI_Verbs_Gestures()

    activities = xAPI_Activities_Gestures()

    extensions = xAPI_Context_Gestures_Extensions()

    def __init__(self):
        super().__init__("gestures")
