from ..core.xapi_context import xAPI_Context
from .xapi_verbs_observation import xAPI_Verbs_Observation
from .xapi_activities_observation import xAPI_Activities_Observation
from .xapi_context_observation_extensions import xAPI_Context_Observation_Extensions

"""Provides the definitions of the context observation as public properties."""
class xAPI_Context_Observation(xAPI_Context):
    verbs = xAPI_Verbs_Observation()

    activities = xAPI_Activities_Observation()

    extensions = xAPI_Context_Observation_Extensions()

    def __init__(self):
        super().__init__("observation")
