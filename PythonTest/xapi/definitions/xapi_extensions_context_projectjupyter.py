from .xapi_extensions_context import xAPI_Extensions_Context
from ..core.xapi_extension import xAPI_Extension

"""Provides all extensions of the context projectJupyter of type context as public properties."""
class xAPI_Extensions_Context_ProjectJupyter(xAPI_Extensions_Context):

    def __init__(self):
        super().__init__("projectJupyter")

    def profilename(self, value):
        self.add(xAPI_Extension(
                self.context,
                self.extensionType,
                "profilename",
                {
                    "en-US": "profilename",
                    "de-DE": "Profilname"
                },
                {
                    "en-US": "Name of the currently loaded JupyterLab profile (within a JupyterHub).",
                    "de-DE": "Name des aktuell geladenen JupyterLab-Profils (in einem JupyterHub)."
                }),
            value);
        return self;
