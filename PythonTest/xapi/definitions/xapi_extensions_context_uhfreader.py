from .xapi_extensions_context import xAPI_Extensions_Context
from ..core.xapi_extension import xAPI_Extension

"""Provides all extensions of the context uhfReader of type context as public properties."""
class xAPI_Extensions_Context_UhfReader(xAPI_Extensions_Context):

    def __init__(self):
        super().__init__("uhfReader")

    def actionName(self, value):
        self.add(xAPI_Extension(
                self.context,
                self.extensionType,
                "actionName",
                {
                    "en-US": "action name"
                },
                {
                    "en-US": "Name of an UI element action, which the user interacted with. For example the read value checkbox has the name 'readValue'."
                }),
            value);
        return self;

    def evaluationType(self, value):
        self.add(xAPI_Extension(
                self.context,
                self.extensionType,
                "evaluationType",
                {
                    "en-US": "evaluation type"
                },
                {
                    "en-US": "An evaluation can have a type. For example a transponder can be evaluated externally using google by copying the id and pasting it externally."
                }),
            value);
        return self;

    def readingHead(self, value):
        self.add(xAPI_Extension(
                self.context,
                self.extensionType,
                "readingHead",
                {
                    "en-US": "reading head"
                },
                {
                    "en-US": "The head that is selected to read the TID."
                }),
            value);
        return self;

    def readTid(self, value):
        self.add(xAPI_Extension(
                self.context,
                self.extensionType,
                "readTid",
                {
                    "en-US": "read Tid"
                },
                {
                    "en-US": "The selected element to read for the transponder."
                }),
            value);
        return self;
