from ..core.xapi_context import xAPI_Context
from .xapi_verbs_lms import xAPI_Verbs_Lms
from .xapi_activities_lms import xAPI_Activities_Lms
from .xapi_context_lms_extensions import xAPI_Context_Lms_Extensions

"""Provides the definitions of the context lms as public properties."""
class xAPI_Context_Lms(xAPI_Context):
    verbs = xAPI_Verbs_Lms()

    activities = xAPI_Activities_Lms()

    extensions = xAPI_Context_Lms_Extensions()

    def __init__(self):
        super().__init__("lms")
