from .xapi_extensions_activity_gestures import xAPI_Extensions_Activity_Gestures
from .xapi_extensions_result_gestures import xAPI_Extensions_Result_Gestures

"""Provides the extensions of the context gestures as public properties."""
class xAPI_Context_Gestures_Extensions:

    def __init__(self):
        pass

    @property
    def activity(self):
        return xAPI_Extensions_Activity_Gestures();

    @property
    def result(self):
        return xAPI_Extensions_Result_Gestures();
