from ..core.xapi_activity import xAPI_Activity
from ..core.xapi_activities import xAPI_Activities

"""Provides the xAPI_Activities of the context eyeTracking as public properties."""
class xAPI_Activities_EyeTracking(xAPI_Activities):
    eye = xAPI_Activity(
        "eyeTracking",
        "eye",
        {
            "en-US": "eye",
            "de-DE": "Auge"
        },
        {
            "en-US": "Organ of the visual system. Actors physical (left or right) eye.",
            "de-DE": "Sinnesorgan zur Wahrnehmung von Lichtreizen, in diesem Fall physikalisches Auge des Akteurs (links oder rechts)."
        })
    """Organ of the visual system. Actors physical (left or right) eye."""

    def __init__(self):
        super().__init__("eyeTracking")
