from .xapi_extensions_result import xAPI_Extensions_Result
from ..core.xapi_extension import xAPI_Extension

"""Provides all extensions of the context gestures of type result as public properties."""
class xAPI_Extensions_Result_Gestures(xAPI_Extensions_Result):

    def __init__(self):
        super().__init__("gestures")

    def numberOfGestures(self, value):
        self.add(xAPI_Extension(
                self.context,
                self.extensionType,
                "numberOfGestures",
                {
                    "en-US": "number of gestures",
                    "de-DE": "Gestenanzahl"
                },
                {
                    "en-US": "The number of actors gestures. Has to be an integer.",
                    "de-DE": "Die Anzahl an Gesten, die die Akteurin tätigt. Muss ein Integer sein."
                }),
            value);
        return self;

    def primaryHand(self, value):
        self.add(xAPI_Extension(
                self.context,
                self.extensionType,
                "primaryHand",
                {
                    "en-US": "primary hand",
                    "de-DE": "Primärhand"
                },
                {
                    "en-US": "Setting, which hand is currently the primary hand.",
                    "de-DE": "Einstellung, welche Hand die Primärhand ist."
                }),
            value);
        return self;
