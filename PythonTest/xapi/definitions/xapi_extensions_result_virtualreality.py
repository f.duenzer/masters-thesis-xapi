from .xapi_extensions_result import xAPI_Extensions_Result
from ..core.xapi_extension import xAPI_Extension

"""Provides all extensions of the context virtualReality of type result as public properties."""
class xAPI_Extensions_Result_VirtualReality(xAPI_Extensions_Result):

    def __init__(self):
        super().__init__("virtualReality")

    def position(self, value):
        self.add(xAPI_Extension(
                self.context,
                self.extensionType,
                "position",
                {
                    "en-US": "position",
                    "de-DE": "Position"
                },
                {
                    "en-US": "Position in scene.",
                    "de-DE": "Position in der Szene."
                }),
            value);
        return self;

    def rotation(self, value):
        self.add(xAPI_Extension(
                self.context,
                self.extensionType,
                "rotation",
                {
                    "en-US": "rotation",
                    "de-DE": "Rotation"
                },
                {
                    "en-US": "Rotation in scene.",
                    "de-DE": "Rotation in der Szene."
                }),
            value);
        return self;

    def scale(self, value):
        self.add(xAPI_Extension(
                self.context,
                self.extensionType,
                "scale",
                {
                    "en-US": "scale",
                    "de-DE": "Skalierung"
                },
                {
                    "en-US": "Scale in scene.",
                    "de-DE": "Skalierung in der Szene."
                }),
            value);
        return self;
