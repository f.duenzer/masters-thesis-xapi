from .xapi_extensions_result import xAPI_Extensions_Result
from ..core.xapi_extension import xAPI_Extension

"""Provides all extensions of the context eyeTracking of type result as public properties."""
class xAPI_Extensions_Result_EyeTracking(xAPI_Extensions_Result):

    def __init__(self):
        super().__init__("eyeTracking")

    def duration(self, value):
        self.add(xAPI_Extension(
                self.context,
                self.extensionType,
                "duration",
                {
                    "en-US": "fixation duration",
                    "de-DE": "Fixationsdauer"
                },
                {
                    "en-US": "The fixation duration is the measure how long the actor spend looking at a particular location.",
                    "de-DE": "Die Fixationsdauer beschreibt die Zeit, die ein Akteur oder eine Akteurin den Blick auf einem bestimmten Ort hat verweilen lassen."
                }),
            value);
        return self;

    def numberOfBlinking(self, value):
        self.add(xAPI_Extension(
                self.context,
                self.extensionType,
                "numberOfBlinking",
                {
                    "en-US": "number of blinking",
                    "de-DE": "Zwinker Anzahl"
                },
                {
                    "en-US": "The number of actors eye blinking within a focus sequence. Has to be an integer.",
                    "de-DE": "Die Anzahl an Zwinker, die die Akteurin während einer Fokus Sequence getätigt hat. Muss ein Integer sein."
                }),
            value);
        return self;
