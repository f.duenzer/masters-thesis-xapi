from .xapi_extensions_activity_systemcontrol import xAPI_Extensions_Activity_SystemControl
from .xapi_extensions_result_systemcontrol import xAPI_Extensions_Result_SystemControl

"""Provides the extensions of the context systemControl as public properties."""
class xAPI_Context_SystemControl_Extensions:

    def __init__(self):
        pass

    @property
    def activity(self):
        return xAPI_Extensions_Activity_SystemControl();

    @property
    def result(self):
        return xAPI_Extensions_Result_SystemControl();
