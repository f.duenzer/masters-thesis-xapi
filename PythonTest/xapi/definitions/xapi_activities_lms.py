from ..core.xapi_activity import xAPI_Activity
from ..core.xapi_activities import xAPI_Activities

"""Provides the xAPI_Activities of the context lms as public properties."""
class xAPI_Activities_Lms(xAPI_Activities):
    assign = xAPI_Activity(
        "lms",
        "assign",
        {
            "en-US": "Assignment",
            "de-DE": "Aufgabe"
        },
        {
            "en-US": "An assignment object. Provides the students with a task description and further needed materials and allows to upload a submission.",
            "de-DE": "Eine Aufgabe. Gibt eine Aufgabenstellung vor und weiteres benötigtes Material und ermöglicht das Hochladen einer Abgabe."
        })
    """An assignment object. Provides the students with a task description and further needed materials and allows to upload a submission."""

    book = xAPI_Activity(
        "lms",
        "book",
        {
            "en-US": "Book",
            "de-DE": "Buch"
        },
        {
            "en-US": "A book, containing multiple texts and images, structured as chapters.",
            "de-DE": "Ein Buch welches Texte und Bilder in einzelnen Kapiteln darstellt."
        })
    """A book, containing multiple texts and images, structured as chapters."""

    book_chapter = xAPI_Activity(
        "lms",
        "book_chapter",
        {
            "en-US": "Book Chapter",
            "de-DE": "Buchkapitel"
        },
        {
            "en-US": "A chapter of a book.",
            "de-DE": "Ein Kapitel in einem Buch."
        })
    """A chapter of a book."""

    course = xAPI_Activity(
        "lms",
        "course",
        {
            "en-US": "Course",
            "de-DE": "Kurs"
        },
        {
            "en-US": "A course within an LMS. Contains learning materials and activities",
            "de-DE": "Ein Kurs in einem LMS. Enthält Lernmaterialien und -aktivitäten"
        })
    """A course within an LMS. Contains learning materials and activities"""

    forum = xAPI_Activity(
        "lms",
        "forum",
        {
            "en-US": "Forum",
            "de-DE": "Forum"
        },
        {
            "en-US": "A forum for discussion.",
            "de-DE": "Ein Forum für Diskussionen."
        })
    """A forum for discussion."""

    lms = xAPI_Activity(
        "lms",
        "lms",
        {
            "en-US": "LMS",
            "de-DE": "LMS"
        },
        {
            "en-US": "A Learning Management System (LMS)",
            "de-DE": "Ein Lernmanagementsystem (LMS)"
        })
    """A Learning Management System (LMS)"""

    page = xAPI_Activity(
        "lms",
        "page",
        {
            "en-US": "Page",
            "de-DE": "Seite"
        },
        {
            "en-US": "A page within a course displaying custom content like text, images, videos and more.",
            "de-DE": "Eine Seite im Kurs, die verschiedene Inhalte wie Text, Bilder, Videos und andere darstellt."
        })
    """A page within a course displaying custom content like text, images, videos and more."""

    resource = xAPI_Activity(
        "lms",
        "resource",
        {
            "en-US": "Resource",
            "de-DE": "Ressource"
        },
        {
            "en-US": "A course resource as a single file (e.g. PDF).",
            "de-DE": "Eine Kursressource als einzelne Datei (z. B. PDF)."
        })
    """A course resource as a single file (e.g. PDF)."""

    def __init__(self):
        super().__init__("lms")
