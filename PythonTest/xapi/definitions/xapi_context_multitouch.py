from ..core.xapi_context import xAPI_Context
from .xapi_verbs_multitouch import xAPI_Verbs_Multitouch
from .xapi_activities_multitouch import xAPI_Activities_Multitouch
from .xapi_context_multitouch_extensions import xAPI_Context_Multitouch_Extensions

"""Provides the definitions of the context multitouch as public properties."""
class xAPI_Context_Multitouch(xAPI_Context):
    verbs = xAPI_Verbs_Multitouch()

    activities = xAPI_Activities_Multitouch()

    extensions = xAPI_Context_Multitouch_Extensions()

    def __init__(self):
        super().__init__("multitouch")
