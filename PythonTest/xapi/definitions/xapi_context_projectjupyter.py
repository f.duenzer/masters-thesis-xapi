from ..core.xapi_context import xAPI_Context
from .xapi_verbs_projectjupyter import xAPI_Verbs_ProjectJupyter
from .xapi_activities_projectjupyter import xAPI_Activities_ProjectJupyter
from .xapi_context_projectjupyter_extensions import xAPI_Context_ProjectJupyter_Extensions

"""Provides the definitions of the context projectJupyter as public properties."""
class xAPI_Context_ProjectJupyter(xAPI_Context):
    verbs = xAPI_Verbs_ProjectJupyter()

    activities = xAPI_Activities_ProjectJupyter()

    extensions = xAPI_Context_ProjectJupyter_Extensions()

    def __init__(self):
        super().__init__("projectJupyter")
