from ..core.xapi_verb import xAPI_Verb
from ..core.xapi_verbs import xAPI_Verbs

"""Provides the xAPI_Verbs of the context projectJupyter as public properties."""
class xAPI_Verbs_ProjectJupyter(xAPI_Verbs):
    run = xAPI_Verb(
        "projectJupyter",
        "run",
        {
            "en-US": "run",
            "de-DE": "führte aus"
        },
        {
            "en-US": "Indicates, that an actor run some programming context. Typically a Jupyter Notebook Code Cell.",
            "de-DE": "Signalisiert, dass ein Akteur ein Programmierkonstrukt ausführte. Typischerweise eine Jupyter Notebook Codezelle."
        })
    """Indicates, that an actor run some programming context. Typically a Jupyter Notebook Code Cell."""

    def __init__(self):
        super().__init__("projectJupyter")
