from ..core.xapi_context import xAPI_Context
from .xapi_verbs_tagformance import xAPI_Verbs_Tagformance
from .xapi_activities_tagformance import xAPI_Activities_Tagformance
from .xapi_context_tagformance_extensions import xAPI_Context_Tagformance_Extensions

"""Provides the definitions of the context tagformance as public properties."""
class xAPI_Context_Tagformance(xAPI_Context):
    verbs = xAPI_Verbs_Tagformance()

    activities = xAPI_Activities_Tagformance()

    extensions = xAPI_Context_Tagformance_Extensions()

    def __init__(self):
        super().__init__("tagformance")
