from ..core.xapi_verb import xAPI_Verb
from ..core.xapi_verbs import xAPI_Verbs

"""Provides the xAPI_Verbs of the context virtualReality as public properties."""
class xAPI_Verbs_VirtualReality(xAPI_Verbs):
    created = xAPI_Verb(
        "virtualReality",
        "created",
        {
            "en-US": "created",
            "de-DE": "erstellte"
        },
        {
            "en-US": "An actor has created a virtual object in the VR environment.",
            "de-DE": "Eine Akteurin hat ein virtuelles Objekt in der VR Umgebung erstellt."
        })
    """An actor has created a virtual object in the VR environment."""

    interacted = xAPI_Verb(
        "virtualReality",
        "interacted",
        {
            "en-US": "interacted",
            "de-DE": "interagierte"
        },
        {
            "en-US": "An actor interacted with the VR object in the VR environment.",
            "de-DE": "Eine Akteurin interagierte einem VR Objekt in der VR Umgebung."
        })
    """An actor interacted with the VR object in the VR environment."""

    moved = xAPI_Verb(
        "virtualReality",
        "moved",
        {
            "en-US": "moved",
            "de-DE": "bewegte"
        },
        {
            "en-US": "An actor has changed a virtual object's position in VR environment.",
            "de-DE": "Eine Akteurin hat die Position eines virtuellen Objekts verändert."
        })
    """An actor has changed a virtual object's position in VR environment."""

    placed = xAPI_Verb(
        "virtualReality",
        "placed",
        {
            "en-US": "placed",
            "de-DE": "platzierte"
        },
        {
            "en-US": "An actor has placed a virtual object in the VR environment.",
            "de-DE": "Eine Akteurin hat ein virtuelles Objekt in der VR Umgebung platziert."
        })
    """An actor has placed a virtual object in the VR environment."""

    pointed = xAPI_Verb(
        "virtualReality",
        "pointed",
        {
            "en-US": "pointed",
            "de-DE": "zeigte"
        },
        {
            "en-US": "An actor has pointed on an VR object.",
            "de-DE": "Eine Akteurin zeigte auf ein VR Objekt."
        })
    """An actor has pointed on an VR object."""

    pressed = xAPI_Verb(
        "virtualReality",
        "pressed",
        {
            "en-US": "pressed",
            "de-DE": "drückte"
        },
        {
            "en-US": "The actor has pressed a controller button.",
            "de-DE": "Eine Akteurin drückte einen Controller Taste."
        })
    """The actor has pressed a controller button."""

    released = xAPI_Verb(
        "virtualReality",
        "released",
        {
            "en-US": "released",
            "de-DE": "löste"
        },
        {
            "en-US": "The actor has released a controller button.",
            "de-DE": "Eine Akteurin löste einen Controller Taste."
        })
    """The actor has released a controller button."""

    removed = xAPI_Verb(
        "virtualReality",
        "removed",
        {
            "en-US": "removed",
            "de-DE": "entfernte"
        },
        {
            "en-US": "An actor has removed a virtual object from the VR environment.",
            "de-DE": "Ein Akteur hat ein virtuelles Objekt aus der VR Umgebung entfernt."
        })
    """An actor has removed a virtual object from the VR environment."""

    rotated = xAPI_Verb(
        "virtualReality",
        "rotated",
        {
            "en-US": "rotated",
            "de-DE": "rotierte"
        },
        {
            "en-US": "An actor has changed a virtual object's rotation in VR environment.",
            "de-DE": "Ein Akteur hat die Rotation eines virtuellen Objekts verändert."
        })
    """An actor has changed a virtual object's rotation in VR environment."""

    scaled = xAPI_Verb(
        "virtualReality",
        "scaled",
        {
            "en-US": "scaled",
            "de-DE": "skalierte"
        },
        {
            "en-US": "An actor has changed a virtual object's scale in VR environment.",
            "de-DE": "Eine Akteurin hat die Skalierung eines virtuellen Objekts verändert."
        })
    """An actor has changed a virtual object's scale in VR environment."""

    teleported = xAPI_Verb(
        "virtualReality",
        "teleported",
        {
            "en-US": "teleported",
            "de-DE": "teleportierte"
        },
        {
            "en-US": "An actor has changed her own position in VR environment by teleport.",
            "de-DE": "Eine Akteurin hat die eigene Position in der VR Umgebung durch Teleportation verändert."
        })
    """An actor has changed her own position in VR environment by teleport."""

    touched = xAPI_Verb(
        "virtualReality",
        "touched",
        {
            "en-US": "touched",
            "de-DE": "berührte"
        },
        {
            "en-US": "An actor has touched a virtual object in VR environment.",
            "de-DE": "Eine Akteurin hat ein virtuelles Objekt berührt."
        })
    """An actor has touched a virtual object in VR environment."""

    def __init__(self):
        super().__init__("virtualReality")
