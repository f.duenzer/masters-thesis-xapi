from ..core.xapi_verb import xAPI_Verb
from ..core.xapi_verbs import xAPI_Verbs

"""Provides the xAPI_Verbs of the context multitouch as public properties."""
class xAPI_Verbs_Multitouch(xAPI_Verbs):
    clicked = xAPI_Verb(
        "multitouch",
        "clicked",
        {
            "en-US": "clicked",
            "de-DE": "klickte"
        },
        {
            "en-US": "An actor clicked on something on the screen.",
            "de-DE": "Ein Akteur klickte auf ein Objekt auf dem Bildschirm."
        })
    """An actor clicked on something on the screen."""

    doubleclicked = xAPI_Verb(
        "multitouch",
        "doubleclicked",
        {
            "en-US": "doubleclicked",
            "de-DE": "doppelklickte"
        },
        {
            "en-US": "An actor clicked two times at the same spot in a short time period. Can also bve done with a finger or tangible.",
            "de-DE": "Ein Akteur klickte zwei mal auf den selben Punkt kurz hintereinander. Kann auch mit dem Finger oder einem Tangible gemacht werden."
        })
    """An actor clicked two times at the same spot in a short time period. Can also bve done with a finger or tangible."""

    dragged = xAPI_Verb(
        "multitouch",
        "dragged",
        {
            "en-US": "dragged",
            "de-DE": "zog"
        },
        {
            "en-US": "An actor dragged a draggable object across the screen.",
            "de-DE": "Ein Akteur zog ein ziehbares Objekt über den Bildschirm."
        })
    """An actor dragged a draggable object across the screen."""

    flicked = xAPI_Verb(
        "multitouch",
        "flicked",
        {
            "en-US": "flicked",
            "de-DE": "schnippte"
        },
        {
            "en-US": "An actor dragged a draggable across the screen and let it go of further without guidence.",
            "de-DE": "Ein Akteur zog ein ziehbares Objekt über den Bildschirm und lies es in der Bewegung los was dazu führte, dass das Objekt sich weiter bewegte."
        })
    """An actor dragged a draggable across the screen and let it go of further without guidence."""

    interacted = xAPI_Verb(
        "multitouch",
        "interacted",
        {
            "en-US": "interacted",
            "de-DE": "interagierte"
        },
        {
            "en-US": "An actor interacted with the game or an game object.",
            "de-DE": "Ein Akteur interagierte mit dem Spiel oder einem Objekt im Spiel."
        })
    """An actor interacted with the game or an game object."""

    longpressed = xAPI_Verb(
        "multitouch",
        "longpressed",
        {
            "en-US": "longpressed",
            "de-DE": "hielt gedrückt"
        },
        {
            "en-US": "An actor held down the mousebutton, a finger or a tangible at one spot. How much time has to pass until a press becomes a longpress is up to the user.",
            "de-DE": "Ein Akteur hielt eine Maustaste, einen Finger oder ein Tangible länger auf einem Punkt gedrückt. Wie lange es dauert bis ein drücken ein gedrückt halten wird ist dem Nutzer überlassen."
        })
    """An actor held down the mousebutton, a finger or a tangible at one spot. How much time has to pass until a press becomes a longpress is up to the user."""

    mirrored = xAPI_Verb(
        "multitouch",
        "mirrored",
        {
            "en-US": "mirrored",
            "de-DE": "spiegelte"
        },
        {
            "en-US": "An actor mirrored some game object.",
            "de-DE": "Ein Akteur spiegelte ein Spiel Objekt."
        })
    """An actor mirrored some game object."""

    pressed = xAPI_Verb(
        "multitouch",
        "pressed",
        {
            "en-US": "pressed",
            "de-DE": "drückte"
        },
        {
            "en-US": "An actor activated a button.",
            "de-DE": "Ein Akteur aktivierte einen Knopf."
        })
    """An actor activated a button."""

    rotated = xAPI_Verb(
        "multitouch",
        "rotated",
        {
            "en-US": "rotated",
            "de-DE": "rotierte"
        },
        {
            "en-US": "An actor rotated a game object.",
            "de-DE": "Ein Akteur rotierte ein Spiel Objekt."
        })
    """An actor rotated a game object."""

    touched = xAPI_Verb(
        "multitouch",
        "touched",
        {
            "en-US": "touched",
            "de-DE": "berührte"
        },
        {
            "en-US": "An actor touched the screen or something on the screen with a finger or tangible.",
            "de-DE": "Ein Akteur berührte den Bildschirm oder etwas auf dem Bildschirm mit einem Finger oder Tangible."
        })
    """An actor touched the screen or something on the screen with a finger or tangible."""

    zoomedIn = xAPI_Verb(
        "multitouch",
        "zoomedIn",
        {
            "en-US": "zoomed in",
            "de-DE": "vergrößerte"
        },
        {
            "en-US": "An actor zoomed into some visual part of the game.",
            "de-DE": "Ein Akteur vergrößerte einen sichtbaren Teil des Spiels."
        })
    """An actor zoomed into some visual part of the game."""

    zoomedOut = xAPI_Verb(
        "multitouch",
        "zoomedOut",
        {
            "en-US": "zoomed out",
            "de-DE": "verkleinerte"
        },
        {
            "en-US": "An actor zoomed out of some visual part of the game.",
            "de-DE": "Ein Akteur verkleinerte einen sichtbaren Teil des Spiels."
        })
    """An actor zoomed out of some visual part of the game."""

    def __init__(self):
        super().__init__("multitouch")
