from ..core.xapi_verb import xAPI_Verb
from ..core.xapi_verbs import xAPI_Verbs

"""Provides the xAPI_Verbs of the context systemControl as public properties."""
class xAPI_Verbs_SystemControl(xAPI_Verbs):
    canceled = xAPI_Verb(
        "systemControl",
        "canceled",
        {
            "en-US": "canceled",
            "de-DE": "brach ab"
        },
        {
            "en-US": "An actor ended a session, game or level without completing it.",
            "de-DE": "Ein Akteur brach eine Spiel Instanz, ein Spiel oder ein Level ab ohne es zu vollenden."
        })
    """An actor ended a session, game or level without completing it."""

    ended = xAPI_Verb(
        "systemControl",
        "ended",
        {
            "en-US": "ended",
            "de-DE": "beendete"
        },
        {
            "en-US": "An actor ended a game session. This verb is only used with the gamesession activity for better session tracking.",
            "de-DE": "Ein Akteur beendete eine Spiel Instanz. Dieses Verb wird nur mit der gamesession Aktivität verwendet, um Spiel Instanzen besser verfolgen zu können."
        })
    """An actor ended a game session. This verb is only used with the gamesession activity for better session tracking."""

    finished = xAPI_Verb(
        "systemControl",
        "finished",
        {
            "en-US": "finished",
            "de-DE": "schloss ab"
        },
        {
            "en-US": "A player finished an activity in a game session. This could be the game itself, a level or stage for example.",
            "de-DE": "Ein Spieler schloss eine Aktivität in einer Spielinstanz ab. Dies könnte zum Beispiel ein Spiel, ein Level oder ein Spielabschnitt sein."
        })
    """A player finished an activity in a game session. This could be the game itself, a level or stage for example."""

    initialized = xAPI_Verb(
        "systemControl",
        "initialized",
        {
            "en-US": "initialized",
            "de-DE": "initialisierte"
        },
        {
            "en-US": "An actor initialized a game session. This verb is only used with the gamesession activity for better session tracking.",
            "de-DE": "Ein Akteur initialisierte eine Spielinstanz. Dieses Verb wird nur mit der gamesession Aktivität verwendet, um Spielinstanzen besser verfolgen zu können."
        })
    """An actor initialized a game session. This verb is only used with the gamesession activity for better session tracking."""

    loaded = xAPI_Verb(
        "systemControl",
        "loaded",
        {
            "en-US": "loaded",
            "de-DE": "lud"
        },
        {
            "en-US": "An actor loaded something in the game (a level or menu for example).",
            "de-DE": "Ein Akteur hat etwas im Spiel geladen. Dies kann zum Beispiel ein Menü oder Level sein."
        })
    """An actor loaded something in the game (a level or menu for example)."""

    loggedIn = xAPI_Verb(
        "systemControl",
        "loggedIn",
        {
            "en-US": "logged In",
            "de-DE": "loggte sich ein"
        },
        {
            "en-US": "A player logged into a session.",
            "de-DE": "Ein Spieler loggte sich in eine Spiel Instanz ein."
        })
    """A player logged into a session."""

    loggedOut = xAPI_Verb(
        "systemControl",
        "loggedOut",
        {
            "en-US": "logged out",
            "de-DE": "loggte sich aus"
        },
        {
            "en-US": "A player logged out of a session.",
            "de-DE": "Ein Spieler loggte sich aus einer Spiel Instanz aus."
        })
    """A player logged out of a session."""

    paused = xAPI_Verb(
        "systemControl",
        "paused",
        {
            "en-US": "paused",
            "de-DE": "pausierte"
        },
        {
            "en-US": "An actor paused an activity in a game session.",
            "de-DE": "Ein Akteur pausierte eine Aktivität in einer Spiel Instanz."
        })
    """An actor paused an activity in a game session."""

    resumed = xAPI_Verb(
        "systemControl",
        "resumed",
        {
            "en-US": "resumed",
            "de-DE": "setzte fort"
        },
        {
            "en-US": "An actor resumed an activity in a game session.",
            "de-DE": "Ein Akteur setzte eine Aktivität in einer Spiel Instanz fort."
        })
    """An actor resumed an activity in a game session."""

    started = xAPI_Verb(
        "systemControl",
        "started",
        {
            "en-US": "started",
            "de-DE": "startete"
        },
        {
            "en-US": "A player started an activity in a game session. This could be the game itself, a level or a stage for example.",
            "de-DE": "Ein Spieler startete eine Aktivität in einer Spielinstanz. Dies könnte zum Beispiel ein Spiel selber sein, ein Level oder ein Spielabschnitt."
        })
    """A player started an activity in a game session. This could be the game itself, a level or a stage for example."""

    def __init__(self):
        super().__init__("systemControl")
