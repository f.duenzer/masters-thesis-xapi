from ..core.xapi_verb import xAPI_Verb
from ..core.xapi_verbs import xAPI_Verbs

"""Provides the xAPI_Verbs of the context vrRfidChamber as public properties."""
class xAPI_Verbs_VrRfidChamber(xAPI_Verbs):
    clicked = xAPI_Verb(
        "vrRfidChamber",
        "clicked",
        {
            "en-US": "clicked"
        },
        {
            "en-US": "An actor clicked something on the screen."
        })
    """An actor clicked something on the screen."""

    closed = xAPI_Verb(
        "vrRfidChamber",
        "closed",
        {
            "en-US": "closed"
        },
        {
            "en-US": "An actor closed an the door of the RFID chamber."
        })
    """An actor closed an the door of the RFID chamber."""

    completed = xAPI_Verb(
        "vrRfidChamber",
        "completed",
        {
            "en-US": "completed"
        },
        {
            "en-US": "The system scan is completed to check the RFID output."
        })
    """The system scan is completed to check the RFID output."""

    dragged = xAPI_Verb(
        "vrRfidChamber",
        "dragged",
        {
            "en-US": "dragged"
        },
        {
            "en-US": "An actor has dragged an object across the screen."
        })
    """An actor has dragged an object across the screen."""

    dragPull = xAPI_Verb(
        "vrRfidChamber",
        "dragPull",
        {
            "en-US": "dragPull"
        },
        {
            "en-US": "An actor dragged and pulled an object."
        })
    """An actor dragged and pulled an object."""

    dragPush = xAPI_Verb(
        "vrRfidChamber",
        "dragPush",
        {
            "en-US": "dragPush"
        },
        {
            "en-US": "An actor dragged and pushed an object on the screen."
        })
    """An actor dragged and pushed an object on the screen."""

    focussed = xAPI_Verb(
        "vrRfidChamber",
        "focussed",
        {
            "en-US": "focused"
        },
        {
            "en-US": "An actor has focused on the RFID tag."
        })
    """An actor has focused on the RFID tag."""

    interacted = xAPI_Verb(
        "vrRfidChamber",
        "interacted",
        {
            "en-US": "interacted"
        },
        {
            "en-US": "An actor has interacted with the object."
        })
    """An actor has interacted with the object."""

    opened = xAPI_Verb(
        "vrRfidChamber",
        "opened",
        {
            "en-US": "opened"
        },
        {
            "en-US": "An actor has opened the object."
        })
    """An actor has opened the object."""

    panned = xAPI_Verb(
        "vrRfidChamber",
        "panned",
        {
            "en-US": "panned"
        },
        {
            "en-US": "An actor panned the screen."
        })
    """An actor panned the screen."""

    pressed = xAPI_Verb(
        "vrRfidChamber",
        "pressed",
        {
            "en-US": "pressed"
        },
        {
            "en-US": "An actor has pressed the keyboard."
        })
    """An actor has pressed the keyboard."""

    released = xAPI_Verb(
        "vrRfidChamber",
        "released",
        {
            "en-US": "released"
        },
        {
            "en-US": "An actor has released the object."
        })
    """An actor has released the object."""

    removed = xAPI_Verb(
        "vrRfidChamber",
        "removed",
        {
            "en-US": "removed"
        },
        {
            "en-US": "An actor has removed an object from the target."
        })
    """An actor has removed an object from the target."""

    rotated = xAPI_Verb(
        "vrRfidChamber",
        "rotated",
        {
            "en-US": "rotated"
        },
        {
            "en-US": "An actor rotated a lab object."
        })
    """An actor rotated a lab object."""

    started = xAPI_Verb(
        "vrRfidChamber",
        "started",
        {
            "en-US": "started"
        },
        {
            "en-US": "An actor has started the object."
        })
    """An actor has started the object."""

    terminated = xAPI_Verb(
        "vrRfidChamber",
        "terminated",
        {
            "en-US": "terminated"
        },
        {
            "en-US": "An actor has terminated the operation."
        })
    """An actor has terminated the operation."""

    unfocussed = xAPI_Verb(
        "vrRfidChamber",
        "unfocussed",
        {
            "en-US": "unfocused"
        },
        {
            "en-US": "An actor unforcused the object"
        })
    """An actor unforcused the object"""

    viewed = xAPI_Verb(
        "vrRfidChamber",
        "viewed",
        {
            "en-US": "viewed"
        },
        {
            "en-US": "An actor has viewed the object details"
        })
    """An actor has viewed the object details"""

    visited = xAPI_Verb(
        "vrRfidChamber",
        "visited",
        {
            "en-US": "visited"
        },
        {
            "en-US": "An actor has visited the virtual lab."
        })
    """An actor has visited the virtual lab."""

    watched = xAPI_Verb(
        "vrRfidChamber",
        "watched",
        {
            "en-US": "Watched"
        },
        {
            "en-US": "An actor has watched the formation of the graph."
        })
    """An actor has watched the formation of the graph."""

    zoomedIn = xAPI_Verb(
        "vrRfidChamber",
        "zoomedIn",
        {
            "en-US": "zoomed in"
        },
        {
            "en-US": "An actor zoomed into some part of the screen."
        })
    """An actor zoomed into some part of the screen."""

    zoomedOut = xAPI_Verb(
        "vrRfidChamber",
        "zoomedOut",
        {
            "en-US": "zoomed out"
        },
        {
            "en-US": "An actor zoomed out of some part of the screen."
        })
    """An actor zoomed out of some part of the screen."""

    def __init__(self):
        super().__init__("vrRfidChamber")
