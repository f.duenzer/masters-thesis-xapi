from ..core.xapi_activity import xAPI_Activity
from ..core.xapi_activities import xAPI_Activities

"""Provides the xAPI_Activities of the context projectJupyter as public properties."""
class xAPI_Activities_ProjectJupyter(xAPI_Activities):
    codeCell = xAPI_Activity(
        "projectJupyter",
        "codeCell",
        {
            "en-US": "Code Cell",
            "de-DE": "Codezelle"
        },
        {
            "en-US": "A Jupyter Notebook Cell of type 'code cell'. Code Cells store programming code, that can be executed by a user. Code within a Code Cell is typically conceptually coherent.",
            "de-DE": "Eine Jupyter Notebook Zelle mit dem Typ 'code cell'. Codezellen enthalten programmier code, welcher von einem user ausgeführt werden kann. Der Code einer Zelle ist meist konzeptionell zusammenhängend."
        })
    """A Jupyter Notebook Cell of type 'code cell'. Code Cells store programming code, that can be executed by a user. Code within a Code Cell is typically conceptually coherent."""

    jupyterLab = xAPI_Activity(
        "projectJupyter",
        "jupyterLab",
        {
            "en-US": "JupyterLab",
            "de-DE": "JupyterLab"
        },
        {
            "en-US": "A web-based user interface for Project Jupyter. See https://jupyterlab.readthedocs.io",
            "de-DE": "Ein webbasiertes Benutzerinterface für Project Jupyter. Mehr informationen hier: https://jupyterlab.readthedocs.io"
        })
    """A web-based user interface for Project Jupyter. See https://jupyterlab.readthedocs.io"""

    jupyterNotebook = xAPI_Activity(
        "projectJupyter",
        "jupyterNotebook",
        {
            "en-US": "Jupyter Notebook Document",
            "de-DE": "Jupyter Notebook Dokument"
        },
        {
            "en-US": "This activity refers to Jupyter Notebook Document (the document type), in contrast to the Jupyter Notebook (a document editor). Notebook documents are json-files ending in '.ipynb'. ",
            "de-DE": "Diese Aktivität bezieht sich auf das Jupyter Notebook Dokument (den Dateityp), im Gegensatz zu dem Jupyter Notebook (einem Dateieditor). Dokumente eines Notebooks sind json-files mit einer '.ipynb' endung."
        })
    """This activity refers to Jupyter Notebook Document (the document type), in contrast to the Jupyter Notebook (a document editor). Notebook documents are json-files ending in '.ipynb'. """

    markdownCell = xAPI_Activity(
        "projectJupyter",
        "markdownCell",
        {
            "en-US": "Markdown Cell",
            "de-DE": "Markdownzelle"
        },
        {
            "en-US": "A Jupyter Notebook Cell of type 'markdown cell'. Markdonw Cells store markdown formatted text. The text is meant to be read by a user.",
            "de-DE": "Eine Jupyter Notebook Zelle mit dem Typ 'markdown cell'. Markdownzellen enthalten Markdown formatierten Text. Der Text einer Markdownzelle ist bestimmt um von einem Benutzer gelesen zu werden."
        })
    """A Jupyter Notebook Cell of type 'markdown cell'. Markdonw Cells store markdown formatted text. The text is meant to be read by a user."""

    rawCell = xAPI_Activity(
        "projectJupyter",
        "rawCell",
        {
            "en-US": "Raw Cell",
            "de-DE": "Raw Cell"
        },
        {
            "en-US": "A Jupyter Notebook Cell of type 'raw cell'. Raw Cells provide storage options for data other than programming code and markdown text. They can contain any type of data.",
            "de-DE": "Eine Jupyter Notebook Zelle mit dem Typ 'raw cell'. Raw Cells bieten die Möglichkeit daten zu speichern, welche weder programmier code noch markdown text sind. Sie können alle arten von daten enthalten"
        })
    """A Jupyter Notebook Cell of type 'raw cell'. Raw Cells provide storage options for data other than programming code and markdown text. They can contain any type of data."""

    def __init__(self):
        super().__init__("projectJupyter")
