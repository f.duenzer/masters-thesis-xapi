from ..core.xapi_context import xAPI_Context
from .xapi_verbs_virtualreality import xAPI_Verbs_VirtualReality
from .xapi_activities_virtualreality import xAPI_Activities_VirtualReality
from .xapi_context_virtualreality_extensions import xAPI_Context_VirtualReality_Extensions

"""Provides the definitions of the context virtualReality as public properties."""
class xAPI_Context_VirtualReality(xAPI_Context):
    verbs = xAPI_Verbs_VirtualReality()

    activities = xAPI_Activities_VirtualReality()

    extensions = xAPI_Context_VirtualReality_Extensions()

    def __init__(self):
        super().__init__("virtualReality")
