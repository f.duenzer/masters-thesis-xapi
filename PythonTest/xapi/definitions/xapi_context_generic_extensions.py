from .xapi_extensions_activity_generic import xAPI_Extensions_Activity_Generic
from .xapi_extensions_result_generic import xAPI_Extensions_Result_Generic

"""Provides the extensions of the context generic as public properties."""
class xAPI_Context_Generic_Extensions:

    def __init__(self):
        pass

    @property
    def activity(self):
        return xAPI_Extensions_Activity_Generic();

    @property
    def result(self):
        return xAPI_Extensions_Result_Generic();
