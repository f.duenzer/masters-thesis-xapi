from ..core.xapi_activity import xAPI_Activity
from ..core.xapi_activities import xAPI_Activities

"""Provides the xAPI_Activities of the context vrRfidChamber as public properties."""
class xAPI_Activities_VrRfidChamber(xAPI_Activities):
    button = xAPI_Activity(
        "vrRfidChamber",
        "button",
        {
            "en-US": "button"
        },
        {
            "en-US": "Something an actor can press, which then triggers an action in the lab."
        })
    """Something an actor can press, which then triggers an action in the lab."""

    comment = xAPI_Activity(
        "vrRfidChamber",
        "comment",
        {
            "en-US": "comment"
        },
        {
            "en-US": "Some kind of written text to explain an action"
        })
    """Some kind of written text to explain an action"""

    device = xAPI_Activity(
        "vrRfidChamber",
        "device",
        {
            "en-US": "device "
        },
        {
            "en-US": "Represents the RFID chamber."
        })
    """Represents the RFID chamber."""

    door = xAPI_Activity(
        "vrRfidChamber",
        "door",
        {
            "en-US": "door"
        },
        {
            "en-US": "An actor opened or closed the door"
        })
    """An actor opened or closed the door"""

    draggable = xAPI_Activity(
        "vrRfidChamber",
        "draggable",
        {
            "en-US": "drag"
        },
        {
            "en-US": "Clicking an item, holding the click and moving it around the lab."
        })
    """Clicking an item, holding the click and moving it around the lab."""

    keyboard = xAPI_Activity(
        "vrRfidChamber",
        "keyboard",
        {
            "en-US": "keyboard"
        },
        {
            "en-US": "A keyboard used to input text or interact with the lab."
        })
    """A keyboard used to input text or interact with the lab."""

    labAudio = xAPI_Activity(
        "vrRfidChamber",
        "labAudio",
        {
            "en-US": "lab audio"
        },
        {
            "en-US": "Some audio content to feel the rela atmosphere of the lab. This can be switched on or off"
        })
    """Some audio content to feel the rela atmosphere of the lab. This can be switched on or off"""

    mouse = xAPI_Activity(
        "vrRfidChamber",
        "mouse",
        {
            "en-US": "mouse"
        },
        {
            "en-US": "A mouse used to click on buttons or drag items in the lab."
        })
    """A mouse used to click on buttons or drag items in the lab."""

    screen = xAPI_Activity(
        "vrRfidChamber",
        "screen",
        {
            "en-US": "screen"
        },
        {
            "en-US": "The screen showing the virtual lab and can be clicked or touched on for interaction with the game."
        })
    """The screen showing the virtual lab and can be clicked or touched on for interaction with the game."""

    selection = xAPI_Activity(
        "vrRfidChamber",
        "selection",
        {
            "en-US": "Select"
        },
        {
            "en-US": "Some kind of selection is represented by this. It is given by user."
        })
    """Some kind of selection is represented by this. It is given by user."""

    def __init__(self):
        super().__init__("vrRfidChamber")
