from ..core.xapi_context import xAPI_Context
from .xapi_verbs_systemcontrol import xAPI_Verbs_SystemControl
from .xapi_activities_systemcontrol import xAPI_Activities_SystemControl
from .xapi_context_systemcontrol_extensions import xAPI_Context_SystemControl_Extensions

"""Provides the definitions of the context systemControl as public properties."""
class xAPI_Context_SystemControl(xAPI_Context):
    verbs = xAPI_Verbs_SystemControl()

    activities = xAPI_Activities_SystemControl()

    extensions = xAPI_Context_SystemControl_Extensions()

    def __init__(self):
        super().__init__("systemControl")
