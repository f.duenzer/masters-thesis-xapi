from ..core.xapi_verb import xAPI_Verb
from ..core.xapi_verbs import xAPI_Verbs

"""Provides the xAPI_Verbs of the context tagformance as public properties."""
class xAPI_Verbs_Tagformance(xAPI_Verbs):
    changed = xAPI_Verb(
        "tagformance",
        "changed",
        {
            "en-US": "selected"
        },
        {
            "en-US": "An actor changed a value of an ui element."
        })
    """An actor changed a value of an ui element."""

    clicked = xAPI_Verb(
        "tagformance",
        "clicked",
        {
            "en-US": "clicked"
        },
        {
            "en-US": "An actor clicked on something on the screen."
        })
    """An actor clicked on something on the screen."""

    commented = xAPI_Verb(
        "tagformance",
        "commented",
        {
            "en-US": "commented"
        },
        {
            "en-US": "An actor has written down the his/her thought."
        })
    """An actor has written down the his/her thought."""

    deleted = xAPI_Verb(
        "tagformance",
        "deleted",
        {
            "en-US": "deleted"
        },
        {
            "en-US": "An actor has deleted a digital artefact from the software."
        })
    """An actor has deleted a digital artefact from the software."""

    exported = xAPI_Verb(
        "tagformance",
        "exported",
        {
            "en-US": "exported"
        },
        {
            "en-US": "An actor has exported a digital artefact from the software."
        })
    """An actor has exported a digital artefact from the software."""

    generated = xAPI_Verb(
        "tagformance",
        "generated",
        {
            "en-US": "generated"
        },
        {
            "en-US": "An actor has generated a digital artefact from the software."
        })
    """An actor has generated a digital artefact from the software."""

    loggedIn = xAPI_Verb(
        "tagformance",
        "loggedIn",
        {
            "en-US": "Log In"
        },
        {
            "en-US": "An actor logged in to the portal."
        })
    """An actor logged in to the portal."""

    loggedOut = xAPI_Verb(
        "tagformance",
        "loggedOut",
        {
            "en-US": "Log Out"
        },
        {
            "en-US": "An actor logged out of the portal."
        })
    """An actor logged out of the portal."""

    saved = xAPI_Verb(
        "tagformance",
        "saved",
        {
            "en-US": "saved"
        },
        {
            "en-US": "An actor has saved a digital artefact from the software."
        })
    """An actor has saved a digital artefact from the software."""

    started = xAPI_Verb(
        "tagformance",
        "started",
        {
            "en-US": "started"
        },
        {
            "en-US": "An actor has started the measurements."
        })
    """An actor has started the measurements."""

    zoomedIn = xAPI_Verb(
        "tagformance",
        "zoomedIn",
        {
            "en-US": "zoomed in"
        },
        {
            "en-US": "An actor zoomed into the visual element like the graph generated."
        })
    """An actor zoomed into the visual element like the graph generated."""

    zoomedOut = xAPI_Verb(
        "tagformance",
        "zoomedOut",
        {
            "en-US": "zoomed out"
        },
        {
            "en-US": "An actor zoomed out of a visual element like the graph generated."
        })
    """An actor zoomed out of a visual element like the graph generated."""

    def __init__(self):
        super().__init__("tagformance")
