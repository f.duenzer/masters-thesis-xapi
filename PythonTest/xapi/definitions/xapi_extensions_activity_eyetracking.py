from .xapi_extensions_activity import xAPI_Extensions_Activity
from ..core.xapi_extension import xAPI_Extension

"""Provides all extensions of the context eyeTracking of type activity as public properties."""
class xAPI_Extensions_Activity_EyeTracking(xAPI_Extensions_Activity):

    def __init__(self):
        super().__init__("eyeTracking")

    def eye(self, value):
        self.add(xAPI_Extension(
                self.context,
                self.extensionType,
                "eye",
                {
                    "en-US": "eye",
                    "de-DE": "Auge"
                },
                {
                    "en-US": "Organ of the visual system. Actors physical (left or right) eye.",
                    "de-DE": "Sinnesorgan zur Wahrnehmung von Lichtreizen, in diesem Fall physikalisches Auge des Akteurs (links oder rechts)."
                }),
            value);
        return self;
