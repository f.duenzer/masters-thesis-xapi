from .xapi_extensions_activity import xAPI_Extensions_Activity
from ..core.xapi_extension import xAPI_Extension

"""Provides all extensions of the context seriousgames of type activity as public properties."""
class xAPI_Extensions_Activity_Seriousgames(xAPI_Extensions_Activity):

    def __init__(self):
        super().__init__("seriousgames")

    def buttontype(self, value):
        self.add(xAPI_Extension(
                self.context,
                self.extensionType,
                "buttontype",
                {
                    "en-US": "button type",
                    "de-DE": "Knopf Typ"
                },
                {
                    "en-US": "The type of button which was pressed. Has to be String.",
                    "de-DE": "Der Typ des Knopfes der gedrückt wurde. Muss ein String sein."
                }),
            value);
        return self;
