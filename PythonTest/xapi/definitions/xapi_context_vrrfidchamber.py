from ..core.xapi_context import xAPI_Context
from .xapi_verbs_vrrfidchamber import xAPI_Verbs_VrRfidChamber
from .xapi_activities_vrrfidchamber import xAPI_Activities_VrRfidChamber
from .xapi_context_vrrfidchamber_extensions import xAPI_Context_VrRfidChamber_Extensions

"""Provides the definitions of the context vrRfidChamber as public properties."""
class xAPI_Context_VrRfidChamber(xAPI_Context):
    verbs = xAPI_Verbs_VrRfidChamber()

    activities = xAPI_Activities_VrRfidChamber()

    extensions = xAPI_Context_VrRfidChamber_Extensions()

    def __init__(self):
        super().__init__("vrRfidChamber")
