from .xapi_extensions_activity_virtualreality import xAPI_Extensions_Activity_VirtualReality
from .xapi_extensions_result_virtualreality import xAPI_Extensions_Result_VirtualReality

"""Provides the extensions of the context virtualReality as public properties."""
class xAPI_Context_VirtualReality_Extensions:

    def __init__(self):
        pass

    @property
    def activity(self):
        return xAPI_Extensions_Activity_VirtualReality();

    @property
    def result(self):
        return xAPI_Extensions_Result_VirtualReality();
