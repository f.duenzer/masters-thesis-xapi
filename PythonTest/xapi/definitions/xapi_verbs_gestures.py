from ..core.xapi_verb import xAPI_Verb
from ..core.xapi_verbs import xAPI_Verbs

"""Provides the xAPI_Verbs of the context gestures as public properties."""
class xAPI_Verbs_Gestures(xAPI_Verbs):
    gazed = xAPI_Verb(
        "gestures",
        "gazed",
        {
            "en-US": "gazed",
            "de-DE": "richtete aus"
        },
        {
            "en-US": "An actor has gazed a part of her body in a direction.",
            "de-DE": "Eine Akteurin richtete ihr Körperteil in eine Richtung aus."
        })
    """An actor has gazed a part of her body in a direction."""

    gestured = xAPI_Verb(
        "gestures",
        "gestured",
        {
            "en-US": "gestured",
            "de-DE": "gestikulierte"
        },
        {
            "en-US": "An actor has gestured with part of her body (e.g. hands, feets, ...).",
            "de-DE": "Eine Akteurin gestikulierte mit einem ihrer Körperteile (z.B. Hände, Füße, ...)."
        })
    """An actor has gestured with part of her body (e.g. hands, feets, ...)."""

    nodded = xAPI_Verb(
        "gestures",
        "nodded",
        {
            "en-US": "nodding",
            "de-DE": "nickte"
        },
        {
            "en-US": "An actor has nodded with her head. Could be collected by computation of headset movement or a webcam or by hand annotation.",
            "de-DE": "Eine Akteurin nickte mit ihrem Kopf. Könnte zum Beispiel erkannt worden sein, indem Headset Daten ausgewertet wurden, eine Erkennung in einem Video geschehen ist oder durch eine händische Annotation."
        })
    """An actor has nodded with her head. Could be collected by computation of headset movement or a webcam or by hand annotation."""

    performed = xAPI_Verb(
        "gestures",
        "performed",
        {
            "en-US": "performed",
            "de-DE": "verrichtete"
        },
        {
            "en-US": "An actor performed something, e.g. a gesture",
            "de-DE": "Ein Akteur hat etwas getan, z.B. eine Geste verrichtet."
        })
    """An actor performed something, e.g. a gesture"""

    shaked = xAPI_Verb(
        "gestures",
        "shaked",
        {
            "en-US": "shaked",
            "de-DE": "schüttelte"
        },
        {
            "en-US": "An actor has shaked something, for example a part of her body.",
            "de-DE": "Eine Akteurin schüttelte etwas, zum Beispiel einen Körperteil wie ihren Kopf."
        })
    """An actor has shaked something, for example a part of her body."""

    def __init__(self):
        super().__init__("gestures")
