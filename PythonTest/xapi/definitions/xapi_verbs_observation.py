from ..core.xapi_verb import xAPI_Verb
from ..core.xapi_verbs import xAPI_Verbs

"""Provides the xAPI_Verbs of the context observation as public properties."""
class xAPI_Verbs_Observation(xAPI_Verbs):
    beschaeftigt = xAPI_Verb(
        "observation",
        "beschaeftigt",
        {
            "de-DE": "beschäftigt"
        },
        {
            "de-DE": "Ein Akteur beschäftigt sich mit etwas"
        })
    """Ein Akteur beschäftigt sich mit etwas"""

    erklaert = xAPI_Verb(
        "observation",
        "erklaert",
        {
            "de-DE": "erklärt"
        },
        {
            "de-DE": "Ein Akteur erklärt jemand anderen etwas verbal."
        })
    """Ein Akteur erklärt jemand anderen etwas verbal."""

    erzaehlt = xAPI_Verb(
        "observation",
        "erzaehlt",
        {
            "de-DE": "erzählt"
        },
        {
            "de-DE": "Ein Akteur erzählt etwas verbal"
        })
    """Ein Akteur erzählt etwas verbal"""

    fragt = xAPI_Verb(
        "observation",
        "fragt",
        {
            "de-DE": "fragt"
        },
        {
            "de-DE": "Ein Akteur fragt etwas"
        })
    """Ein Akteur fragt etwas"""

    gibt = xAPI_Verb(
        "observation",
        "gibt",
        {
            "de-DE": "gibt"
        },
        {
            "de-DE": "Ein Akteur gibt etwas"
        })
    """Ein Akteur gibt etwas"""

    motiviert = xAPI_Verb(
        "observation",
        "motiviert",
        {
            "de-DE": "motiviert"
        },
        {
            "de-DE": "Ein Akteur motiviert jemanden"
        })
    """Ein Akteur motiviert jemanden"""

    nimmt = xAPI_Verb(
        "observation",
        "nimmt",
        {
            "de-DE": "nimmt"
        },
        {
            "de-DE": "Ein Akteur nimmt etwas"
        })
    """Ein Akteur nimmt etwas"""

    sieht = xAPI_Verb(
        "observation",
        "sieht",
        {
            "de-DE": "sieht"
        },
        {
            "de-DE": "Ein Akteur sieht etwas"
        })
    """Ein Akteur sieht etwas"""

    unterbricht = xAPI_Verb(
        "observation",
        "unterbricht",
        {
            "de-DE": "unterbricht"
        },
        {
            "de-DE": "Ein Akteur unterbricht jemand oder etwas anderen verbal."
        })
    """Ein Akteur unterbricht jemand oder etwas anderen verbal."""

    def __init__(self):
        super().__init__("observation")
