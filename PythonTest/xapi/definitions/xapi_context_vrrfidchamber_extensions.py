from .xapi_extensions_context_vrrfidchamber import xAPI_Extensions_Context_VrRfidChamber
from .xapi_extensions_result_vrrfidchamber import xAPI_Extensions_Result_VrRfidChamber

"""Provides the extensions of the context vrRfidChamber as public properties."""
class xAPI_Context_VrRfidChamber_Extensions:

    def __init__(self):
        pass

    @property
    def context(self):
        return xAPI_Extensions_Context_VrRfidChamber();

    @property
    def result(self):
        return xAPI_Extensions_Result_VrRfidChamber();
