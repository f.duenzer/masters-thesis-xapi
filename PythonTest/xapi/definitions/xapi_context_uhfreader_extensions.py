from .xapi_extensions_context_uhfreader import xAPI_Extensions_Context_UhfReader
from .xapi_extensions_result_uhfreader import xAPI_Extensions_Result_UhfReader

"""Provides the extensions of the context uhfReader as public properties."""
class xAPI_Context_UhfReader_Extensions:

    def __init__(self):
        pass

    @property
    def context(self):
        return xAPI_Extensions_Context_UhfReader();

    @property
    def result(self):
        return xAPI_Extensions_Result_UhfReader();
