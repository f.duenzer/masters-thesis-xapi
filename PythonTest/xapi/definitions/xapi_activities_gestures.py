from ..core.xapi_activity import xAPI_Activity
from ..core.xapi_activities import xAPI_Activities

"""Provides the xAPI_Activities of the context gestures as public properties."""
class xAPI_Activities_Gestures(xAPI_Activities):
    hand = xAPI_Activity(
        "gestures",
        "hand",
        {
            "en-US": "hand",
            "de-DE": "Hand"
        },
        {
            "en-US": "Actors physical (left or right) hand.",
            "de-DE": "Physikalische (linke oder rechte) Hand vom Akteur."
        })
    """Actors physical (left or right) hand."""

    head = xAPI_Activity(
        "gestures",
        "head",
        {
            "en-US": "head",
            "de-DE": "Kopf"
        },
        {
            "en-US": "Actors physical head.",
            "de-DE": "Physikalische Kopf vom Akteur."
        })
    """Actors physical head."""

    def __init__(self):
        super().__init__("gestures")
