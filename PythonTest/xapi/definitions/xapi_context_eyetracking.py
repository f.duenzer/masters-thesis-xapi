from ..core.xapi_context import xAPI_Context
from .xapi_verbs_eyetracking import xAPI_Verbs_EyeTracking
from .xapi_activities_eyetracking import xAPI_Activities_EyeTracking
from .xapi_context_eyetracking_extensions import xAPI_Context_EyeTracking_Extensions

"""Provides the definitions of the context eyeTracking as public properties."""
class xAPI_Context_EyeTracking(xAPI_Context):
    verbs = xAPI_Verbs_EyeTracking()

    activities = xAPI_Activities_EyeTracking()

    extensions = xAPI_Context_EyeTracking_Extensions()

    def __init__(self):
        super().__init__("eyeTracking")
