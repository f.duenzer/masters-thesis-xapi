from ..core.xapi_extensions import xAPI_Extensions

class xAPI_Extensions_Result(xAPI_Extensions):

    def __init__(self, context = None):
        super().__init__("result", context)
