from .xapi_extensions_activity_seriousgames import xAPI_Extensions_Activity_Seriousgames
from .xapi_extensions_context_seriousgames import xAPI_Extensions_Context_Seriousgames
from .xapi_extensions_result_seriousgames import xAPI_Extensions_Result_Seriousgames

"""Provides the extensions of the context seriousgames as public properties."""
class xAPI_Context_Seriousgames_Extensions:

    def __init__(self):
        pass

    @property
    def activity(self):
        return xAPI_Extensions_Activity_Seriousgames();

    @property
    def context(self):
        return xAPI_Extensions_Context_Seriousgames();

    @property
    def result(self):
        return xAPI_Extensions_Result_Seriousgames();
