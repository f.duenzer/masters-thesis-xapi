from ..core.xapi_context import xAPI_Context
from .xapi_verbs_generic import xAPI_Verbs_Generic
from .xapi_activities_generic import xAPI_Activities_Generic
from .xapi_context_generic_extensions import xAPI_Context_Generic_Extensions

"""Provides the definitions of the context generic as public properties."""
class xAPI_Context_Generic(xAPI_Context):
    verbs = xAPI_Verbs_Generic()

    activities = xAPI_Activities_Generic()

    extensions = xAPI_Context_Generic_Extensions()

    def __init__(self):
        super().__init__("generic")
