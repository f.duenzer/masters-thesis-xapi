from ..core.xapi_context import xAPI_Context
from .xapi_verbs_seriousgames import xAPI_Verbs_Seriousgames
from .xapi_activities_seriousgames import xAPI_Activities_Seriousgames
from .xapi_context_seriousgames_extensions import xAPI_Context_Seriousgames_Extensions

"""Provides the definitions of the context seriousgames as public properties."""
class xAPI_Context_Seriousgames(xAPI_Context):
    verbs = xAPI_Verbs_Seriousgames()

    activities = xAPI_Activities_Seriousgames()

    extensions = xAPI_Context_Seriousgames_Extensions()

    def __init__(self):
        super().__init__("seriousgames")
