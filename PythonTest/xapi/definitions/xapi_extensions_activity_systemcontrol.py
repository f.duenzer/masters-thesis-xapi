from .xapi_extensions_activity import xAPI_Extensions_Activity
from ..core.xapi_extension import xAPI_Extension

"""Provides all extensions of the context systemControl of type activity as public properties."""
class xAPI_Extensions_Activity_SystemControl(xAPI_Extensions_Activity):

    def __init__(self):
        super().__init__("systemControl")

    def name(self, value):
        self.add(xAPI_Extension(
                self.context,
                self.extensionType,
                "name",
                {
                    "en-US": "System control activity name",
                    "de-DE": "System Control Activity Name"
                },
                {
                    "en-US": "Name of a system control activity. Must be a string.",
                    "de-DE": "Namen von einer Aktivität von des Systemkontrollers. Muss ein String sein."
                }),
            value);
        return self;
