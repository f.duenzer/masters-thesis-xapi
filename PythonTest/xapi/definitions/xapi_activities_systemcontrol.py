from ..core.xapi_activity import xAPI_Activity
from ..core.xapi_activities import xAPI_Activities

"""Provides the xAPI_Activities of the context systemControl as public properties."""
class xAPI_Activities_SystemControl(xAPI_Activities):
    game = xAPI_Activity(
        "systemControl",
        "game",
        {
            "en-US": "game",
            "de-DE": "Spiel"
        },
        {
            "en-US": "Represents a game or competition of any kind.",
            "de-DE": "Repräsentiert jede Art von Spiel."
        })
    """Represents a game or competition of any kind."""

    level = xAPI_Activity(
        "systemControl",
        "level",
        {
            "en-US": "level",
            "de-DE": "Level"
        },
        {
            "en-US": "A level is some, in itself closed, part of the game which the game itself calls level.",
            "de-DE": "Ein Level ist ein in sich selbst geschlossener Teil eines Spiels, den das Spiel selber Level nennt."
        })
    """A level is some, in itself closed, part of the game which the game itself calls level."""

    session = xAPI_Activity(
        "systemControl",
        "session",
        {
            "en-US": "session",
            "de-DE": "Spiel Instanz"
        },
        {
            "en-US": "A session is one execution of a game from booting the game to exiting it.",
            "de-DE": "Eine Spiel Instanz ist ein Durchlauf eines Spiels vom Initalisieren des Spiels bis zum Beenden."
        })
    """A session is one execution of a game from booting the game to exiting it."""

    stage = xAPI_Activity(
        "systemControl",
        "stage",
        {
            "en-US": "stage",
            "de-DE": "Spielabschnitt"
        },
        {
            "en-US": "A stage is a part of a level.",
            "de-DE": "Ein Spielabschnitt ist ein Teil eines Levels."
        })
    """A stage is a part of a level."""

    def __init__(self):
        super().__init__("systemControl")
