from .xapi_extensions_result import xAPI_Extensions_Result
from ..core.xapi_extension import xAPI_Extension

"""Provides all extensions of the context tagformance of type result as public properties."""
class xAPI_Extensions_Result_Tagformance(xAPI_Extensions_Result):

    def __init__(self):
        super().__init__("tagformance")

    def duration(self, value):
        self.add(xAPI_Extension(
                self.context,
                self.extensionType,
                "duration",
                {
                    "en-US": "duration"
                },
                {
                    "en-US": "The time taken to complete the experiment. This is captured from the system time of the local machine."
                }),
            value);
        return self;

    def empty(self, value):
        self.add(xAPI_Extension(
                self.context,
                self.extensionType,
                "empty",
                {
                    "en-US": "empty"
                },
                {
                    "en-US": "Boolean value which is true when something is empty."
                }),
            value);
        return self;
