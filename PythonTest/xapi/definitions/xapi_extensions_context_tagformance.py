from .xapi_extensions_context import xAPI_Extensions_Context
from ..core.xapi_extension import xAPI_Extension

"""Provides all extensions of the context tagformance of type context as public properties."""
class xAPI_Extensions_Context_Tagformance(xAPI_Extensions_Context):

    def __init__(self):
        super().__init__("tagformance")

    def energyMeasure(self, value):
        self.add(xAPI_Extension(
                self.context,
                self.extensionType,
                "energyMeasure",
                {
                    "en-US": "energy measure"
                },
                {
                    "en-US": "Energy Measure is an experiment mode as per the lab instruction."
                }),
            value);
        return self;

    def frequencyStep(self, value):
        self.add(xAPI_Extension(
                self.context,
                self.extensionType,
                "frequencyStep",
                {
                    "en-US": "frequency step"
                },
                {
                    "en-US": "An actor selects a frequency step for the measurement of the graph."
                }),
            value);
        return self;

    def measurementUnit(self, value):
        self.add(xAPI_Extension(
                self.context,
                self.extensionType,
                "measurementUnit",
                {
                    "en-US": "measurement unit dropdown"
                },
                {
                    "en-US": "An actor selects a unit in the Y-axis of the graph that is measured."
                }),
            value);
        return self;

    def orientationMeasure(self, value):
        self.add(xAPI_Extension(
                self.context,
                self.extensionType,
                "orientationMeasure",
                {
                    "en-US": "orientation measure"
                },
                {
                    "en-US": "This is an experiment mode as per lab instruction."
                }),
            value);
        return self;

    def powerStep(self, value):
        self.add(xAPI_Extension(
                self.context,
                self.extensionType,
                "powerStep",
                {
                    "en-US": "power step"
                },
                {
                    "en-US": "The power step selected for the measurement."
                }),
            value);
        return self;

    def protocol(self, value):
        self.add(xAPI_Extension(
                self.context,
                self.extensionType,
                "protocol",
                {
                    "en-US": "protocol"
                },
                {
                    "en-US": "The protocol used is ISO 18000-6c Query."
                }),
            value);
        return self;

    def selectTag(self, value):
        self.add(xAPI_Extension(
                self.context,
                self.extensionType,
                "selectTag",
                {
                    "en-US": "select tag dropdown"
                },
                {
                    "en-US": "The RFID tag selected for measurement."
                }),
            value);
        return self;

    def startFrequency(self, value):
        self.add(xAPI_Extension(
                self.context,
                self.extensionType,
                "startFrequency",
                {
                    "en-US": "start frequency"
                },
                {
                    "en-US": "The selected frequency to start the measurement."
                }),
            value);
        return self;

    def stopFrequency(self, value):
        self.add(xAPI_Extension(
                self.context,
                self.extensionType,
                "stopFrequency",
                {
                    "en-US": "stop frequency"
                },
                {
                    "en-US": "The selected frequency to stop the measurement."
                }),
            value);
        return self;

    def theoreticalReadingRange(self, value):
        self.add(xAPI_Extension(
                self.context,
                self.extensionType,
                "theoreticalReadingRange",
                {
                    "en-US": "theoretical reading range"
                },
                {
                    "en-US": "The Y-axis unit in the graph for measuring the reading range of transponder."
                }),
            value);
        return self;

    def transmittedPower(self, value):
        self.add(xAPI_Extension(
                self.context,
                self.extensionType,
                "transmittedPower",
                {
                    "en-US": "transmitted power"
                },
                {
                    "en-US": "The Y-axis unit in dBm for measurement."
                }),
            value);
        return self;
