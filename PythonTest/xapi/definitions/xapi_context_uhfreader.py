from ..core.xapi_context import xAPI_Context
from .xapi_verbs_uhfreader import xAPI_Verbs_UhfReader
from .xapi_activities_uhfreader import xAPI_Activities_UhfReader
from .xapi_context_uhfreader_extensions import xAPI_Context_UhfReader_Extensions

"""Provides the definitions of the context uhfReader as public properties."""
class xAPI_Context_UhfReader(xAPI_Context):
    verbs = xAPI_Verbs_UhfReader()

    activities = xAPI_Activities_UhfReader()

    extensions = xAPI_Context_UhfReader_Extensions()

    def __init__(self):
        super().__init__("uhfReader")
