from .xapi_extensions_activity import xAPI_Extensions_Activity
from ..core.xapi_extension import xAPI_Extension

"""Provides all extensions of the context generic of type activity as public properties."""
class xAPI_Extensions_Activity_Generic(xAPI_Extensions_Activity):

    def __init__(self):
        super().__init__("generic")

    def color(self, value):
        self.add(xAPI_Extension(
                self.context,
                self.extensionType,
                "color",
                {
                    "en-US": "color",
                    "de-DE": "Color"
                },
                {
                    "en-US": "The color of an object.",
                    "de-DE": "Das Farbe eines Objekts."
                }),
            value);
        return self;

    def filepath(self, value):
        self.add(xAPI_Extension(
                self.context,
                self.extensionType,
                "filepath",
                {
                    "en-US": "file path",
                    "de-DE": "Dateipfad"
                },
                {
                    "en-US": "A relative or absolute file path of a file. Can be used to provide the filepath for a file-activity.",
                    "de-DE": "Ein relativer oder absoluter Dateipfad. Kann genutzt werden um den Dateipfad eine Dateiaktivität anzugeben."
                }),
            value);
        return self;

    def keyboardButton(self, value):
        self.add(xAPI_Extension(
                self.context,
                self.extensionType,
                "keyboardButton",
                {
                    "en-US": "keyboard button",
                    "de-DE": "Tastatur Taste"
                },
                {
                    "en-US": "The button of the keyboard.",
                    "de-DE": "Eine Taste der Tastatur."
                }),
            value);
        return self;

    def mouseButton(self, value):
        self.add(xAPI_Extension(
                self.context,
                self.extensionType,
                "mouseButton",
                {
                    "en-US": "mouse button",
                    "de-DE": "Maus Taste"
                },
                {
                    "en-US": "The button of a mouse.",
                    "de-DE": "Die Taste einer Maus."
                }),
            value);
        return self;

    def mousePosition(self, value):
        self.add(xAPI_Extension(
                self.context,
                self.extensionType,
                "mousePosition",
                {
                    "en-US": "mouse position",
                    "de-DE": "Maus Position"
                },
                {
                    "en-US": "The position of the mouse.",
                    "de-DE": "Die Position der Maus."
                }),
            value);
        return self;

    def position(self, value):
        self.add(xAPI_Extension(
                self.context,
                self.extensionType,
                "position",
                {
                    "en-US": "position",
                    "de-DE": "Position"
                },
                {
                    "en-US": "An number or other identifier (e.g. 'top', 'bottom') to identify the position of an element inside a colletion such as an list of elements.",
                    "de-DE": "Eine Nummer oder ein anderer Kennzeichnung einer Position eines Elementes einer Sammlung wie zum Beispliel einer Liste von Elementen."
                }),
            value);
        return self;
