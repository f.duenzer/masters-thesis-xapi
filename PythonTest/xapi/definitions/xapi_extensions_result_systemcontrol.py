from .xapi_extensions_result import xAPI_Extensions_Result
from ..core.xapi_extension import xAPI_Extension

"""Provides all extensions of the context systemControl of type result as public properties."""
class xAPI_Extensions_Result_SystemControl(xAPI_Extensions_Result):

    def __init__(self):
        super().__init__("systemControl")

    def language(self, value):
        self.add(xAPI_Extension(
                self.context,
                self.extensionType,
                "language",
                {
                    "en-US": "language",
                    "de-DE": "Sprache"
                },
                {
                    "en-US": "Language of the system.",
                    "de-DE": "Sprache des Systems."
                }),
            value);
        return self;
