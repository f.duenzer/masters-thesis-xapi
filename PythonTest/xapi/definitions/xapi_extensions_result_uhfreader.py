from .xapi_extensions_result import xAPI_Extensions_Result
from ..core.xapi_extension import xAPI_Extension

"""Provides all extensions of the context uhfReader of type result as public properties."""
class xAPI_Extensions_Result_UhfReader(xAPI_Extensions_Result):

    def __init__(self):
        super().__init__("uhfReader")

    def transponderState(self, value):
        self.add(xAPI_Extension(
                self.context,
                self.extensionType,
                "transponderState",
                {
                    "en-US": "transponderState"
                },
                {
                    "en-US": "State of the transpnder to the physical reader."
                }),
            value);
        return self;
