from .xapi_extensions_activity import xAPI_Extensions_Activity
from ..core.xapi_extension import xAPI_Extension

"""Provides all extensions of the context gestures of type activity as public properties."""
class xAPI_Extensions_Activity_Gestures(xAPI_Extensions_Activity):

    def __init__(self):
        super().__init__("gestures")

    def hand(self, value):
        self.add(xAPI_Extension(
                self.context,
                self.extensionType,
                "hand",
                {
                    "en-US": "hand",
                    "de-DE": "Hand"
                },
                {
                    "en-US": "Hand which is currently active.",
                    "de-DE": "Die Hand, die gerade verwendet wird."
                }),
            value);
        return self;
