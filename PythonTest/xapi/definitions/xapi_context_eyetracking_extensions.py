from .xapi_extensions_activity_eyetracking import xAPI_Extensions_Activity_EyeTracking
from .xapi_extensions_result_eyetracking import xAPI_Extensions_Result_EyeTracking

"""Provides the extensions of the context eyeTracking as public properties."""
class xAPI_Context_EyeTracking_Extensions:

    def __init__(self):
        pass

    @property
    def activity(self):
        return xAPI_Extensions_Activity_EyeTracking();

    @property
    def result(self):
        return xAPI_Extensions_Result_EyeTracking();
