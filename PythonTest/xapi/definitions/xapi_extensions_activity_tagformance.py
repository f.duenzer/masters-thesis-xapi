from .xapi_extensions_activity import xAPI_Extensions_Activity
from ..core.xapi_extension import xAPI_Extension

"""Provides all extensions of the context tagformance of type activity as public properties."""
class xAPI_Extensions_Activity_Tagformance(xAPI_Extensions_Activity):

    def __init__(self):
        super().__init__("tagformance")

    def experimentMode(self, value):
        self.add(xAPI_Extension(
                self.context,
                self.extensionType,
                "experimentMode",
                {
                    "en-US": "experiment mode"
                },
                {
                    "en-US": "An experiment mode can be for example 'Energy Measurement' or 'Orientation Measurement'"
                }),
            value);
        return self;

    def graphName(self, value):
        self.add(xAPI_Extension(
                self.context,
                self.extensionType,
                "graphName",
                {
                    "en-US": "graphName"
                },
                {
                    "en-US": "An actor has authored the name of the graph or entered the username."
                }),
            value);
        return self;

    def mouseButton(self, value):
        self.add(xAPI_Extension(
                self.context,
                self.extensionType,
                "mouseButton",
                {
                    "en-US": "mouse button"
                },
                {
                    "en-US": "Name of the mouse button the actor has used (e.g. 'right', 'left')."
                }),
            value);
        return self;

    def uiElementValue(self, value):
        self.add(xAPI_Extension(
                self.context,
                self.extensionType,
                "uiElementValue",
                {
                    "en-US": "ui element value"
                },
                {
                    "en-US": "A value of an ui element (checkbox, dropdown, etc.)."
                }),
            value);
        return self;
