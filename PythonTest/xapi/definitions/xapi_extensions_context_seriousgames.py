from .xapi_extensions_context import xAPI_Extensions_Context
from ..core.xapi_extension import xAPI_Extension

"""Provides all extensions of the context seriousgames of type context as public properties."""
class xAPI_Extensions_Context_Seriousgames(xAPI_Extensions_Context):

    def __init__(self):
        super().__init__("seriousgames")

    def game(self, value):
        self.add(xAPI_Extension(
                self.context,
                self.extensionType,
                "game",
                {
                    "en-US": "game",
                    "de-DE": "Spiel"
                },
                {
                    "en-US": "The identifier for the game in which the event happened. Can be a string or integer.",
                    "de-DE": "Der Identifikator dafür welches Spiel gespielt wird. Kann ein String oder Integer sein."
                }),
            value);
        return self;

    def gamemode(self, value):
        self.add(xAPI_Extension(
                self.context,
                self.extensionType,
                "gamemode",
                {
                    "en-US": "gamemode",
                    "de-DE": "Spielmodus"
                },
                {
                    "en-US": "The gamemode in which the game is played. Can be a string or integer.",
                    "de-DE": "Der Spielmodus in welchem das Spiel gespielt wird. Kann ein String oder ein Integer sein."
                }),
            value);
        return self;

    def level(self, value):
        self.add(xAPI_Extension(
                self.context,
                self.extensionType,
                "level",
                {
                    "en-US": "level",
                    "de-DE": "Level"
                },
                {
                    "en-US": "The level in which or for which something happened. Can be an integer or a string.",
                    "de-DE": "Das Level in welchem ein Ereignis stattfand. Kann ein String oder Integer sein."
                }),
            value);
        return self;

    def numberOfPlayers(self, value):
        self.add(xAPI_Extension(
                self.context,
                self.extensionType,
                "numberOfPlayers",
                {
                    "en-US": "number of players",
                    "de-DE": "Spieleranzahl"
                },
                {
                    "en-US": "The number of players participating at the game. Has to be an integer.",
                    "de-DE": "Die Anzahl an Spielern welche am Spiel teilnehmen. Muss ein Integer sein."
                }),
            value);
        return self;
