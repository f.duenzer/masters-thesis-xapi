from abc import ABC
from abc import abstractmethod

class xAPI_Definition(ABC):
    @property
    def context(self):
        return self.__context

    @property
    def key(self):
        return self.__key

    @property
    def names(self):
        return self.__names

    @property
    def descriptions(self):
        return self.__descriptions

    @abstractmethod
    def __init__(self, context: str, key: str, names: dict, descriptions: dict):
        self.__context = context;
        self.__key = key
        self.__names = names
        self.__descriptions = descriptions

    def getName(self, language: dict):
        if not (language in self.__names):
            raise Error(f'There is no name for the language {language}.')
        return self.__names[language]

    def getDescription(self, language: dict):
        if not (language in self.__description):
            raise Error(f'There is no description for the language {language}.')
        return self.__description[language]

    def getNameDescription(self, language: dict):
        namesContainLang = language in this.__names
        descsContainLang = language in this.__descriptions

        if not namesContainLang:
            if not descsContainLang:
                raise Error(f'There is no name and no description for the language {language}.')
            raise Error(f'There is no name for the language {language}.')
        elif not descsContainLang:
            raise Error(f'There is no description for the language {language}.')
        return tuple(self.__names[language], self.__descriptions[language])

    def getLanguages(self):
        return [*list(self.__names), *filter(lambda l: not(l in self.__descriptions.keys), *list(self.__names))]

    def getPath(self):
        return self.__key

    def createValidId(self, uri: str):
        path = self.getPath()
        if uri[len(uri) - 1] == '/':
            uri = uri[0:-1]
        if path[0] == '/':
            path = path[1:]
        return f'{uri}/{path}'

    def __str__(self):
        return f'[type(self).__name__: name=[{str(this.__names)}], description=[{str(this.__descriptions)}]]'