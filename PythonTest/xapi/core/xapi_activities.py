from abc import ABC
from abc import abstractmethod

class xAPI_Activities(ABC):
    @property
    def contextName(self):
        return self.__contextName

    @abstractmethod
    def __init__(self, contextName: str):
        self.__contextName = contextName;