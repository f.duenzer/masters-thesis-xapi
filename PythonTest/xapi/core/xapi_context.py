from abc import ABC
from abc import abstractmethod

class xAPI_Context(ABC):
    def getActivity(self, name):
        return getattr(getattr(self, 'activities'), name)

    def getActivities(self, name):
        return getattr(getattr(self, 'activities'), name)

    def getVerb(self, name):
        return getattr(getattr(self, 'verbs'), name)

    def getVerbs(self, name):
        return getattr(getattr(self, 'verbs'), name)

    @abstractmethod
    def __init__(self, name: str):
        self.__name = name