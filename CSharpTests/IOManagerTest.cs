using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using xAPI_Definitions_Parser.IO;

namespace CSharpTests {
    [TestClass]
    public class IOManagerTest {
        private string definitionsPath = Path.Combine(Directory.GetParent(Environment.CurrentDirectory)
            .Parent.Parent.FullName, "definitions");

        private int CountDefinitions(List<Context> contexts) {
            var count = 0;

            foreach (var context in contexts) {
                count += CountDefinitions(context.Activities);
                foreach (var extensionType in context.Extensions) {
                    count += CountDefinitions(extensionType.Value);
                }
                count += CountDefinitions(context.Verbs);
            }

            return count;
        }

        private int CountDefinitions(List<FileMeta> metas) {
            var count = 0;

            foreach (var meta in metas) {
                count += meta.IsDirectory ? CountDefinitions(meta.DirContent) : 1;
            }

            return count;
        }

        [TestMethod]
        public void TestLocalListContextsFull() {
            var reader = new FileManager();
            var contextsList = reader.ListContextsFull(definitionsPath);

            Assert.AreEqual(13, contextsList.Count);
            Assert.AreEqual(438, CountDefinitions(contextsList));
            Assert.AreEqual("head", contextsList[2].Activities[1].Name);
            Assert.AreEqual(Path.Combine(definitionsPath, "observation", "verbs", "gibt.json"), contextsList[5].Verbs[4].Path);
            Assert.AreEqual("language", contextsList[8].Extensions["result"][0].Name);
            Assert.AreEqual(Path.Combine(definitionsPath, "virtualReality", "extensions", "activity", "triangle.json"), contextsList[11].Extensions["activity"][1].Path);
        }

        [TestMethod]
        public void TestLocalReadContexts() {
            var reader = new FileManager();
            var contexts = reader.ReadContexts(definitionsPath);

            Assert.AreEqual(13, contexts.Count);
            Assert.AreEqual(438, CountDefinitions(contexts));
            Assert.AreEqual(15, contexts[5].Activities.Count, 15);
            Assert.AreEqual(11, contexts[5].Activities[0].DirContent.Count, 11);
            Assert.AreEqual(hand, contexts[2].Activities[0].FileContent.Replace("\n", Environment.NewLine));
            Assert.AreEqual(erzaehlt, contexts[5].Verbs[2].FileContent.Replace("\n", Environment.NewLine));

        }

        [TestMethod]
        public void TestGitListContextsFull() {
            var reader = new GitLabManager("16349193", "glpat-x-z-38tnYLDYeNnTaP5Z");
            var path = "definitions";
            var contextsList = reader.ListContextsFull(path);

            Assert.AreEqual(13, contextsList.Count);
            Assert.AreEqual(438, CountDefinitions(contextsList));
            Assert.AreEqual("head", contextsList[2].Activities[1].Name);
            Assert.AreEqual(Path.Combine(path, "observation", "verbs", "gibt.json").Replace("\\", "/"), contextsList[5].Verbs[4].Path);
            Assert.AreEqual("language", contextsList[8].Extensions["result"][0].Name);
            Assert.AreEqual(Path.Combine(path, "virtualReality", "extensions", "activity", "triangle.json").Replace("\\", "/"), contextsList[11].Extensions["activity"][1].Path);
        }

        [TestMethod]
        public void TestGitReadContexts() {
            var reader = new GitLabManager("16349193", "glpat-x-z-38tnYLDYeNnTaP5Z");
            var path = "definitions";
            var contexts = reader.ReadContextsAsync(path).Result;

            Assert.AreEqual(13, contexts.Count);
            Assert.AreEqual(438, CountDefinitions(contexts));
            Assert.AreEqual(15, contexts[5].Activities.Count, 15);
            Assert.AreEqual(11, contexts[5].Activities[0].DirContent.Count, 11);
            Assert.AreEqual(hand, contexts[2].Activities[0].FileContent.Replace("\n", Environment.NewLine));
            Assert.AreEqual(erzaehlt, contexts[5].Verbs[2].FileContent.Replace("\n", Environment.NewLine));
        }

        private const string hand = @"{
    ""name"": {
        ""en-US"": ""hand"",
        ""de-DE"": ""Hand""
    },
    ""description"": {
        ""en-US"": ""Actors physical (left or right) hand."",
        ""de-DE"": ""Physikalische (linke oder rechte) Hand vom Akteur.""
    }
}";
        private const string erzaehlt = @"{
    ""name"": {
        ""de-DE"": ""erz�hlt""
    },
    ""description"": {
        ""de-DE"": ""Ein Akteur erz�hlt etwas verbal""
    },
    ""choices"": [
        {
            ""exactMatch"": [""https://xapi.elearn.rwth-aachen.de/definitions/observation/activities/seine Idee""],
            ""rules"" : [
                {
                    ""exactMatch"": [""https://xapi.elearn.rwth-aachen.de/definitions/observation/activities/subscore/entwickelt eigene Ideen""],
                    ""result"":{
                        ""score"":{
                          ""raw"": 5,
                          ""min"": 1,
                          ""max"": 5
                        }
                    }
                },
                {
                    ""exactMatch"": [""https://xapi.elearn.rwth-aachen.de/definitions/observation/activities/subscore/setzt geeignete Mittel ein/f�hrt geeignete Handlungen aus, um das Ziel zu erreichen""],
                    ""result"":{
                        ""score"":{
                          ""raw"": 5,
                          ""min"": 1,
                          ""max"": 5
                        }
                    }
                },
                {
                    ""exactMatch"": [""https://xapi.elearn.rwth-aachen.de/definitions/observation/activities/subscore/vertritt eigene Meinungen und zeigt sich dabei kompromissbereit""],
                    ""result"":{
                        ""score"":{
                          ""raw"": 5,
                          ""min"": 1,
                          ""max"": 5
                        }
                    }
                }
            ]
        }
    ]
}";
    }
}