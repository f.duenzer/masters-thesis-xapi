﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using xAPI_Definitions_Parser_CLI;

namespace CSharpTests {
    [TestClass]
    public class ConsoleTest {
        [TestMethod]
        public void TestArguments() {
            var args = new string[] { "-source", "local", "-path", @"D:\test\xapi-master\definitions", "-lang", "CSharp",
                "-namespace", "xAPI", "-output", @"D:\test\xAPI 2\output\CSharp", "-compile", @"D:\test\xAPI.dll", "-dotnet", "5.0", "-refpath", "" };

            var manager = new ArgumentManager(args);

            Assert.AreEqual("local", manager.GetValue("source"));
            Assert.AreEqual("CSharp", manager.GetValue("lang"));
            Assert.AreEqual("xAPI", manager.GetValue("namespace"));
            Assert.AreEqual("5.0", manager.GetValue("dotnet"));
        }
    }
}