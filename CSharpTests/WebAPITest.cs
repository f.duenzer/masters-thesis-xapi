﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using xAPI_Definitions_Parser.Definitions;
using xAPI_Definitions_Parser.Deserialization;
using xAPI_Definitions_Parser.IO;
using xAPI_WebAPI_Methods;

namespace CSharpTests {
    [TestClass]
    public class WebAPITest {
        private int CountDefinitions(List<Context> contexts) {
            var count = 0;

            foreach (var context in contexts) {
                count += CountDefinitions(context.Activities);
                foreach (var extensionType in context.Extensions) {
                    count += CountDefinitions(extensionType.Value);
                }
                count += CountDefinitions(context.Verbs);
            }

            return count;
        }

        private int CountDefinitions(List<FileMeta> metas) {
            var count = 0;

            foreach (var meta in metas) {
                count += meta.IsDirectory ? CountDefinitions(meta.DirContent) : 1;
            }

            return count;
        }

        private int CountDefinitions(List<xAPIContext> contexts) {
            var count = 0;

            foreach (var context in contexts) {
                count += CountDefinitions(context.Activities);
                foreach (var extensionType in context.Extensions) {
                    count += CountDefinitions(extensionType.Value);
                }
                count += CountDefinitions(context.Verbs);
            }

            return count;
        }

        private int CountDefinitions<T>(xAPIDefinitionsList<T> definitions) where T : xAPIDefinition {
            var count = definitions.Definitions.Count;

            foreach (var subdefinitions in definitions.SubDefinitions) {
                count += CountDefinitions(subdefinitions.Value);
            }

            return count;
        }

        [TestMethod]
        public void TestListDefs() {
            var contexts = HttpMethods.GetAPIListAsync().Result;

            Assert.AreEqual(13, contexts.Count);
            Assert.AreEqual(438, CountDefinitions(contexts));
        }

        [TestMethod]
        public void TestReadDefs() {
            var contexts = HttpMethods.GetAPIListAsync().Result;
            var readContexts = HttpMethods.GetAPIDefinitionsAsync(contexts).Result;

            Assert.AreEqual(13, readContexts.Count);
            Assert.AreEqual(438, CountDefinitions(readContexts));
        }

        [TestMethod]
        public void TestFilesContexts() {
            string definitionsPath = Path.Combine(Directory.GetParent(Environment.CurrentDirectory)
                .Parent.Parent.FullName, "definitions");
            var reader = new FileManager();
            var contexts = reader.ReadContexts(definitionsPath);
            var deserializedContexts = Deserializer.DeserializeContexts(contexts);
            var localContexts = HttpMethods.GetLocalContextsAsync(definitionsPath).Result;

            Assert.AreEqual(13, localContexts.Count);
            Assert.AreEqual(438, CountDefinitions(localContexts));
        }

        [TestMethod]
        public void TestFilesCode() {
            string definitionsPath = Path.Combine(Directory.GetParent(Environment.CurrentDirectory)
                .Parent.Parent.FullName, "definitions");
            var localCode = HttpMethods.GetLocalCodeAsync(xAPI_Definitions_Parser.CodeLanguage.CSharp, "xAPI", "", "", definitionsPath).Result;

            Assert.AreEqual(92, localCode.Count);
        }

        [TestMethod]
        public void TestGitlabContexts() {
            var gitlabContexts = HttpMethods.GetGitlabContextsAsync("definitions", "16349193", "glpat-x-z-38tnYLDYeNnTaP5Z").Result;

            Assert.AreEqual(13, gitlabContexts.Count);
            Assert.AreEqual(438, CountDefinitions(gitlabContexts));
        }

        [TestMethod]
        public void TestGitlabCode() {
            var gitlabCode = HttpMethods.GetGitlabCodeAsync(xAPI_Definitions_Parser.CodeLanguage.JavaScript, "xAPI", "", "", "definitions", "16349193", "glpat-x-z-38tnYLDYeNnTaP5Z").Result;

            Assert.AreEqual(92, gitlabCode.Count);
        }

        [TestMethod]
        public void TestContextsToCode() {
            string definitionsPath = Path.Combine(Directory.GetParent(Environment.CurrentDirectory)
                .Parent.Parent.FullName, "definitions");
            var localContexts = HttpMethods.GetLocalContextsAsync(definitionsPath).Result;
            var code = HttpMethods.GetCodeAsync(localContexts, xAPI_Definitions_Parser.CodeLanguage.Python, "", "", "").Result;

            Assert.AreEqual(94, code.Count);
        }
    }
}