﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using xAPI_Definitions_Parser;
using xAPI_Definitions_Parser.Code.CSharp;
using xAPI_Definitions_Parser.Code.JavaScript;
using xAPI_Definitions_Parser.Code.Python;
using xAPI_Definitions_Parser.IO;

namespace CSharpTests {
    [TestClass]
    public class GeneratorTest {
        [TestMethod]
        public void TestArguments() {
            var fileManager = Generator.CreateManager(SourceType.Local);
            var gitlabManager = Generator.CreateManager(SourceType.GitLab);
            var cSharpGenerator = Generator.CreateGenerator(CodeLanguage.CSharp);
            var jsGenerator = Generator.CreateGenerator(CodeLanguage.JavaScript);
            var pyGenerator = Generator.CreateGenerator(CodeLanguage.Python);

            Assert.AreEqual(typeof(FileManager), fileManager.GetType());
            Assert.AreEqual(typeof(GitLabManager), gitlabManager.GetType());
            Assert.AreEqual(typeof(CSharpGenerator), cSharpGenerator.GetType());
            Assert.AreEqual(typeof(JavaScriptGenerator), jsGenerator.GetType());
            Assert.AreEqual(typeof(PythonGenerator), pyGenerator.GetType());
        }
    }
}