﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using xAPI_Definitions_Parser.Definitions;
using xAPI_Definitions_Parser.Deserialization;
using xAPI_Definitions_Parser.IO;

namespace CSharpTests {
    [TestClass]
    public class DeserializerTest {
        private int CountDefinitions(List<xAPIContext> contexts) {
            var count = 0;

            foreach (var context in contexts) {
                count += CountDefinitions(context.Activities);
                foreach (var extensionType in context.Extensions) {
                    count += CountDefinitions(extensionType.Value);
                }
                count += CountDefinitions(context.Verbs);
            }

            return count;
        }

        private int CountDefinitions<T>(xAPIDefinitionsList<T> definitions) where T : xAPIDefinition {
            var count = definitions.Definitions.Count;

            foreach (var subdefinitions in definitions.SubDefinitions) {
                count += CountDefinitions(subdefinitions.Value);
            }

            return count;
        }

        [TestMethod]
        public void TestDeserializeContexts() {
            string definitionsPath = Path.Combine(Directory.GetParent(Environment.CurrentDirectory)
                .Parent.Parent.FullName, "definitions");
            var reader = new FileManager();
            var contextsList = reader.ReadContexts(definitionsPath);

            var deserialized = Deserializer.DeserializeContexts(contextsList);

            Assert.AreEqual(13, deserialized.Count);
            Assert.AreEqual(438, CountDefinitions(deserialized));
            Assert.AreEqual(contextsList[1].Activities[9].Name, deserialized[1].Activities.Definitions[9].Names["en-US"]);
            Assert.AreEqual("Ein Akteur rotierte ein Spiel Objekt.", deserialized[4].Verbs.Definitions[8].GetDescription("de-DE"));
            Assert.AreEqual(contextsList[7].Extensions["context"][1].Name, deserialized[7].Extensions["context"].Definitions[1].DefName);
            Assert.AreEqual("A progress to identify ", deserialized[10].Activities.Definitions[2].Descriptions["en-US"]);
        }
    }
}