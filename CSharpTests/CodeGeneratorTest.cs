﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using System.Linq;
using xAPI_Definitions_Parser.Code.CSharp;
using xAPI_Definitions_Parser.Code.JavaScript;
using xAPI_Definitions_Parser.Code.Python;
using xAPI_Definitions_Parser.Deserialization;
using xAPI_Definitions_Parser.IO;

namespace CSharpTests {
    [TestClass]
    public class CodeGeneratorTest {
        [TestMethod]
        public void TestGenerateContextsCSharp() {
            string definitionsPath = Path.Combine(Directory.GetParent(Environment.CurrentDirectory)
                .Parent.Parent.FullName, "definitions");
            var reader = new FileManager();
            var contextsList = reader.ReadContexts(definitionsPath);

            var deserialized = Deserializer.DeserializeContexts(contextsList);

            var generator = new CSharpGenerator("xAPI");
            var files = generator.GenerateContexts(deserialized);

            Assert.AreEqual(81, files.Count);
        }

        [TestMethod]
        public void TestGenerateContextsPython() {
            string definitionsPath = Path.Combine(Directory.GetParent(Environment.CurrentDirectory)
                .Parent.Parent.FullName, "definitions");
            var reader = new FileManager();
            var contextsList = reader.ReadContexts(definitionsPath);

            var deserialized = Deserializer.DeserializeContexts(contextsList);

            var generator = new PythonGenerator();
            var files = generator.GenerateContexts(deserialized);

            Assert.AreEqual(82, files.Count);
        }

        [TestMethod]
        public void TestGenerateContextsJavaScript() {
            string definitionsPath = Path.Combine(Directory.GetParent(Environment.CurrentDirectory)
                .Parent.Parent.FullName, "definitions");
            var reader = new FileManager();
            var contextsList = reader.ReadContexts(definitionsPath);

            var deserialized = Deserializer.DeserializeContexts(contextsList);

            var generator = new JavaScriptGenerator();
            var files = generator.GenerateContexts(deserialized);

            Assert.AreEqual(81, files.Count);
        }

        [TestMethod]
        public void TestGenerateCoreFilesCSharp() {
            string definitionsPath = Path.Combine(Directory.GetParent(Environment.CurrentDirectory)
                .Parent.Parent.FullName, "definitions");
            var reader = new FileManager();
            var contextsList = reader.ReadContexts(definitionsPath);

            var deserialized = Deserializer.DeserializeContexts(contextsList);

            var generator = new CSharpGenerator("xAPI");
            var files = generator.GenerateCoreFiles();

            Assert.AreEqual(11, files.Count);
            Assert.IsTrue(files.Any(f => f.Name == "xAPI_Activities.cs"));
            Assert.IsTrue(files.Any(f => f.Name == "xAPI_Activity.cs"));
            Assert.IsTrue(files.Any(f => f.Name == "xAPI_Actor.cs"));
            Assert.IsTrue(files.Any(f => f.Name == "xAPI_Body.cs"));
            Assert.IsTrue(files.Any(f => f.Name == "xAPI_Context.cs"));
            Assert.IsTrue(files.Any(f => f.Name == "xAPI_Definition.cs"));
            Assert.IsTrue(files.Any(f => f.Name == "xAPI_Extension.cs"));
            Assert.IsTrue(files.Any(f => f.Name == "xAPI_Extensions.cs"));
            Assert.IsTrue(files.Any(f => f.Name == "xAPI_Statements.cs"));
            Assert.IsTrue(files.Any(f => f.Name == "xAPI_Verb.cs"));
            Assert.IsTrue(files.Any(f => f.Name == "xAPI_Verbs.cs"));
        }

        [TestMethod]
        public void TestGenerateCoreFilesPython() {
            string definitionsPath = Path.Combine(Directory.GetParent(Environment.CurrentDirectory)
                .Parent.Parent.FullName, "definitions");
            var reader = new FileManager();
            var contextsList = reader.ReadContexts(definitionsPath);

            var deserialized = Deserializer.DeserializeContexts(contextsList);

            var generator = new PythonGenerator();
            var files = generator.GenerateCoreFiles();

            Assert.AreEqual(12, files.Count);
            Assert.IsTrue(files.Any(f => f.Name == "__init__.py"));
            Assert.IsTrue(files.Any(f => f.Name == "xapi_activities.py"));
            Assert.IsTrue(files.Any(f => f.Name == "xapi_activity.py"));
            Assert.IsTrue(files.Any(f => f.Name == "xapi_actor.py"));
            Assert.IsTrue(files.Any(f => f.Name == "xapi_body.py"));
            Assert.IsTrue(files.Any(f => f.Name == "xapi_context.py"));
            Assert.IsTrue(files.Any(f => f.Name == "xapi_definition.py"));
            Assert.IsTrue(files.Any(f => f.Name == "xapi_extension.py"));
            Assert.IsTrue(files.Any(f => f.Name == "xapi_extensions.py"));
            Assert.IsTrue(files.Any(f => f.Name == "xapi_statements.py"));
            Assert.IsTrue(files.Any(f => f.Name == "xapi_verb.py"));
            Assert.IsTrue(files.Any(f => f.Name == "xapi_verbs.py"));
        }

        [TestMethod]
        public void TestGenerateCoreFilesJavaScript() {
            string definitionsPath = Path.Combine(Directory.GetParent(Environment.CurrentDirectory)
                .Parent.Parent.FullName, "definitions");
            var reader = new FileManager();
            var contextsList = reader.ReadContexts(definitionsPath);

            var deserialized = Deserializer.DeserializeContexts(contextsList);

            var generator = new JavaScriptGenerator();
            var files = generator.GenerateCoreFiles();

            Assert.AreEqual(11, files.Count);
            Assert.IsTrue(files.Any(f => f.Name == "xapi_activities.js"));
            Assert.IsTrue(files.Any(f => f.Name == "xapi_activity.js"));
            Assert.IsTrue(files.Any(f => f.Name == "xapi_actor.js"));
            Assert.IsTrue(files.Any(f => f.Name == "xapi_body.js"));
            Assert.IsTrue(files.Any(f => f.Name == "xapi_context.js"));
            Assert.IsTrue(files.Any(f => f.Name == "xapi_definition.js"));
            Assert.IsTrue(files.Any(f => f.Name == "xapi_extension.js"));
            Assert.IsTrue(files.Any(f => f.Name == "xapi_extensions.js"));
            Assert.IsTrue(files.Any(f => f.Name == "xapi_statements.js"));
            Assert.IsTrue(files.Any(f => f.Name == "xapi_verb.js"));
            Assert.IsTrue(files.Any(f => f.Name == "xapi_verbs.js"));
        }
    }
}