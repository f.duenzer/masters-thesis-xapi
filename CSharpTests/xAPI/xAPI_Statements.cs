namespace xAPI.Core {
    using global::xAPI.Definitions;
    using Newtonsoft.Json.Linq;
    using System;
    using TinCan;
    
    public static class xAPI_Statements {
        private static Agent CreateAgent(xAPI_Actor actor) {
            return new Agent {
                //account = new AgentAccount(new Uri("https://learninglocker.net"), name),
                name = actor.Name,
                mbox = "mailto:" + actor.Email
            };
        }
    
        private static Verb CreateVerb(string uri, xAPI_Verb verb) {
            var v = new Verb {
                id = new Uri(verb.CreateValidId(uri)),
                display = new LanguageMap(verb.Names)
            };
    
            return v;
        }
    
        private static Activity CreateActivity(string uri, xAPI_Activity activity, xAPI_Extensions extensions = null) {
            var a = new Activity {
                id = activity.CreateValidId(uri),
                definition = activity.ToTinCanActivityDefinition()
            };
    
            a.definition.extensions = extensions?.ToTinCanExtensions(uri);
    
            return a;
        }
    
    
    
        public static (xAPI_Extensions_Activity, xAPI_Extensions_Context, xAPI_Extensions_Result) SplitExtensions(
            xAPI_Extensions extensions) {
            if (extensions == null)
                return (null, null, null);
    
            var activityExtensions = new xAPI_Extensions_Activity();
            var contextExtensions = new xAPI_Extensions_Context();
            var resultExtensions = new xAPI_Extensions_Result();
    
            foreach (var ext in extensions) {
                if (activityExtensions.ExtensionType == ext.Key.extensionType)
                    activityExtensions.Add(ext);
                else if (contextExtensions.ExtensionType == ext.Key.extensionType)
                    contextExtensions.Add(ext);
                else if (resultExtensions.ExtensionType == ext.Key.extensionType)
                    resultExtensions.Add(ext);
            }
    
            return (activityExtensions, contextExtensions, resultExtensions);
        }
    
        public static Statement CreateStatement(
            string uri,
            xAPI_Actor actor,
            xAPI_Verb verb,
            xAPI_Activity activity,
            xAPI_Extensions_Activity activityExtensions = null,
            xAPI_Extensions_Context contextExtensions = null,
            xAPI_Extensions_Result resultExtensions = null,
            Score score = null,
            bool completion = true,
            bool success = true,
            xAPI_Actor? instructor = null
        ) {
            var s = new Statement {
                actor = CreateAgent(actor),
                verb = CreateVerb(uri, verb),
                target = CreateActivity(uri, activity, activityExtensions),
                context = CreateContext(uri, contextExtensions, instructor),
                result = CreateResult(uri, score, completion, success, resultExtensions),
                //add timestamp to statement
                timestamp = DateTime.Now
            };
    
            return s;
        }
    
        private static Context CreateContext(string uri, xAPI_Extensions_Context extensions = null, xAPI_Actor? instructor = null) {
            return new Context() {
                instructor = instructor.HasValue ? CreateAgent(instructor.Value) : null,
                extensions = extensions?.ToTinCanExtensions(uri)
            };
        }
    
        public static Result CreateResult(string uri, Score score = null, bool? completion = null, bool? success = null, xAPI_Extensions_Result extensions = null) {
            return new Result {
                score = score,
                completion = completion,
                success = success,
                extensions = extensions?.ToTinCanExtensions(uri)
            };
        }
    
        public static Statement CreateStatement(string uri, xAPI_Actor actor,
            xAPI_Verb verb, xAPI_Activity activity, xAPI_Extensions extensions = null,
            Score score = null, bool completion = true, bool success = true) {
            var (activityExtensions, contextExtensions, resultExtensions) = SplitExtensions(extensions);
            return CreateStatement(uri, actor, verb, activity, activityExtensions, contextExtensions, resultExtensions,
                score, completion, success);
        }
        public static Statement CreateStatement(string uri, xAPI_Actor actor, xAPI_Body body) {
            var result = CreateResult(
                uri,
                completion: body.completion,
                success: body.success,
                score: new Score() {
                    max = body.maxScore,
                    min = body.minScore,
                    raw = body.rawScore,
                    scaled = body.scaledScore
                },
                extensions: (xAPI_Extensions_Result)body.resultExtensions
            );
            return new Statement {
                actor = CreateAgent(actor),
                verb = CreateVerb(uri, body.verb),
                target = CreateActivity(uri, body.activity, (xAPI_Extensions_Activity)body.activityExtensions),
                result = result,
                context = CreateContext(uri, (xAPI_Extensions_Context)body.contextExtensions),
                //add timestamp to statement
                timestamp = body.timestamp
            };
        }
    
        public static void AssignTo(this xAPI_Extensions extensions, JObject jObject, string uri) {
            // Skip if there are no extensions
            if (jObject == null)
                return;
            foreach (var ext in extensions) {
                var extension = ext.Key;
                var value = ext.Value;
                var id = extension.CreateValidId(uri);
                jObject.Add(id, value.ToString());
            }
        }
    
        public static TinCan.Extensions ToTinCanExtensions(this xAPI_Extensions extensions, System.Uri uri)
            => ToTinCanExtensions(extensions, uri.ToString());
        public static TinCan.Extensions ToTinCanExtensions(this xAPI_Extensions extensions, string uri) {
            var jObject = new JObject();
            extensions.AssignTo(jObject, uri);
    
            return new TinCan.Extensions(jObject);
        }
        public static TinCan.ActivityDefinition ToTinCanActivityDefinition(this xAPI_Definition definition) {
            return new ActivityDefinition {
                // translate xAPI name definitions to LanguageMap
                name = new LanguageMap(definition.Names),
                description = new LanguageMap(definition.Descriptions)
            };
        }
    
    }
}