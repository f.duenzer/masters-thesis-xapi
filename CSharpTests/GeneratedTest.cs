﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json.Linq;
using System;
using System.IO;

namespace CSharpTests {
    [TestClass]
    public class GeneratedTest {
        [TestMethod]
        public void TestActivities() {
            Assert.AreEqual("A chapter of a book.",
                new xAPI.Definitions.xAPI_Activities_Lms().book_chapter.Descriptions["en-US"]);
            Assert.AreEqual("tagformance",
                new xAPI.Definitions.xAPI_Activities_Tagformance().experiment.Context);
            Assert.AreEqual("behält stehts den Überblick",
                new xAPI.Definitions.xAPI_Activities_Observation().subscores.behaelt_stehts_den_Ueberblick.Names["de-DE"]);
        }

        [TestMethod]
        public void TestVerbs() {
            Assert.AreEqual("Indicates, that an actor run some programming context. Typically a Jupyter Notebook Code Cell.",
                new xAPI.Definitions.xAPI_Verbs_ProjectJupyter().run.Descriptions["en-US"]);
            Assert.AreEqual("uhfReader", new xAPI.Definitions.xAPI_Verbs_UhfReader().ContextName);
            Assert.AreEqual("schüttelte", new xAPI.Definitions.xAPI_Verbs_Gestures().shaked.Names["de-DE"]);
        }

        [TestMethod]
        public void TestExtensions() {
            var exts = new xAPI.Definitions.xAPI_Context_EyeTracking_Extensions().result.duration(5).numberOfBlinking(2);
            Assert.AreEqual(2, exts.Count);
            Assert.AreEqual(5, exts[0].Value);
            Assert.AreEqual("Hand", new xAPI.Definitions.xAPI_Context_Gestures_Extensions().activity.hand("right")[0].Key.Names["de-DE"]);
        }

        [TestMethod]
        public void TestDefinitions() {
            Assert.IsNotNull(xAPI.xAPI_Definitions.virtualReality.activities.touchpad);
            Assert.AreEqual("German", xAPI.xAPI_Definitions.systemControl.extensions.result.language("German")[0].Value);
            Assert.AreEqual("An actor dragged and pulled an object.", xAPI.xAPI_Definitions.vrRfidChamber.verbs.dragPull.Descriptions["en-US"]);
        }

        [TestMethod]
        public void TestStatement() {
            var statement = xAPI.Core.xAPI_Statements.CreateStatement(
                "http://example.com",
                new xAPI.Core.xAPI_Actor("Name", "Email@example.com"),
                xAPI.xAPI_Definitions.generic.verbs.clicked,
                xAPI.xAPI_Definitions.generic.activities.mouse,
                xAPI.xAPI_Definitions.generic.extensions.activity.mouseButton("left").mousePosition("100,100"),
                new TinCan.Score(), true, true);

            Assert.AreEqual("mailto:Email@example.com", statement.actor.mbox);
            Assert.AreEqual("http://example.com/generic/verbs/clicked", statement.verb.id.ToString());

            string path = Path.Combine(Directory.GetParent(Environment.CurrentDirectory)
                .Parent.Parent.FullName, "definitions", "generic", "activities", "mouse.json");
            var jObj = JObject.Parse(File.ReadAllText(path));
            Assert.AreEqual(jObj["description"].ToString(),
                (statement.target as TinCan.Activity).definition.description.ToJSON(true));
            Assert.AreEqual(true, statement.result.success);
            Assert.AreEqual(@"{
  ""http://example.com/generic/extensions/activity/mouseButton"": ""left"",
  ""http://example.com/generic/extensions/activity/mousePosition"": ""100,100""
}",
                (statement.target as TinCan.Activity).definition.extensions.ToJSON(true));
        }
    }
}