import { xAPI_Verb } from '../core/xapi_verb.js';
import { xAPI_Verbs } from '../core/xapi_verbs.js';

/**
 * Provides the xAPI_Verbs of the context lms as public properties.
 */
export class xAPI_Verbs_Lms extends xAPI_Verbs {
    /**
     * Actor linked an object/person to an object
     * @var {xAPI_Verb} linked
     */
    linked = new xAPI_Verb(
        "lms",
        "linked",
        {
            "en-US": "linked",
            "de-DE": "verknüpft"
        },
        {
            "en-US": "Actor linked an object/person to an object",
            "de-DE": "Akteur hat ein/e Objekt/Person mit einem Objekt verknüpft"
        });

    /**
     * Actor logged in (as him-/herself)
     * @var {xAPI_Verb} loggedIn
     */
    loggedIn = new xAPI_Verb(
        "lms",
        "loggedIn",
        {
            "en-US": "logged in",
            "de-DE": "meldet sich an"
        },
        {
            "en-US": "Actor logged in (as him-/herself)",
            "de-DE": "Akteur hat sich angemeldet (als er/sie selbst)"
        });

    /**
     * Actor started an activity
     * @var {xAPI_Verb} started
     */
    started = new xAPI_Verb(
        "lms",
        "started",
        {
            "en-US": "started",
            "de-DE": "gestartet"
        },
        {
            "en-US": "Actor started an activity",
            "de-DE": "Akteur hat eine Aktvität gestartet"
        });

    /**
     * Actor accessed/viewed a page, file, video, ...
     * @var {xAPI_Verb} accessed
     */
    accessed = new xAPI_Verb(
        "lms",
        "accessed",
        {
            "en-US": "accessed",
            "de-DE": "zugegriffen"
        },
        {
            "en-US": "Actor accessed/viewed a page, file, video, ...",
            "de-DE": "Akteur hat (auf) eine Seite, eine Datei, ein Video, ... zugegriffen/angesehen"
        });

    /**
     * Actor reviewed an object
     * @var {xAPI_Verb} reviewed
     */
    reviewed = new xAPI_Verb(
        "lms",
        "reviewed",
        {
            "en-US": "reviewed",
            "de-DE": "geprüft"
        },
        {
            "en-US": "Actor reviewed an object",
            "de-DE": "Akteur hat ein Objekt geprüft"
        });

    /**
     * Actor reopened an activity (assignment submission, forum)
     * @var {xAPI_Verb} reopened
     */
    reopened = new xAPI_Verb(
        "lms",
        "reopened",
        {
            "en-US": "reopened",
            "de-DE": "wiedereröffnet"
        },
        {
            "en-US": "Actor reopened an activity (assignment submission, forum)",
            "de-DE": "Akteuer hat eine Aktivität (Aufgabenabgabe, Forum) wiedereröffnet"
        });

    /**
     * Actor stopped an activity/process
     * @var {xAPI_Verb} stopped
     */
    stopped = new xAPI_Verb(
        "lms",
        "stopped",
        {
            "en-US": "stopped",
            "de-DE": "gestoppt"
        },
        {
            "en-US": "Actor stopped an activity/process",
            "de-DE": "Akteur hat eine/n Aktivität/Prozess gestoppt"
        });

    /**
     * Actor rated an object/person
     * @var {xAPI_Verb} graded
     */
    graded = new xAPI_Verb(
        "lms",
        "graded",
        {
            "en-US": "graded",
            "de-DE": "benotet"
        },
        {
            "en-US": "Actor rated an object/person",
            "de-DE": "Akteur hat ein/e Objekt/Person benotet"
        });

    /**
     * Actor sent a message to a person/group
     * @var {xAPI_Verb} sent
     */
    sent = new xAPI_Verb(
        "lms",
        "sent",
        {
            "en-US": "sent",
            "de-DE": "gesendet"
        },
        {
            "en-US": "Actor sent a message to a person/group",
            "de-DE": "Akteur hat eine Nachricht zu einer Person/Gruppe gesendet"
        });

    /**
     * Actor replaced an object/person
     * @var {xAPI_Verb} replaced
     */
    replaced = new xAPI_Verb(
        "lms",
        "replaced",
        {
            "en-US": "replaced",
            "de-DE": "ersetzt"
        },
        {
            "en-US": "Actor replaced an object/person",
            "de-DE": "Akteur hat ein/e Objekt/Person ersetzt"
        });

    /**
     * Actor restored a previous version of an object
     * @var {xAPI_Verb} restored
     */
    restored = new xAPI_Verb(
        "lms",
        "restored",
        {
            "en-US": "restored",
            "de-DE": "wiederhergestellt"
        },
        {
            "en-US": "Actor restored a previous version of an object",
            "de-DE": "Akteur hat eine vorherige Version eines Objektes wiederhergestellt"
        });

    /**
     * Actor logged in as another person
     * @var {xAPI_Verb} loggedInAs
     */
    loggedInAs = new xAPI_Verb(
        "lms",
        "loggedInAs",
        {
            "en-US": "logged in as",
            "de-DE": "angemeldet als"
        },
        {
            "en-US": "Actor logged in as another person",
            "de-DE": "Akteur hat sich als eine andere Person angemeldet"
        });

    /**
     * Actor failed an activity (quiz attempt, upload, log in)
     * @var {xAPI_Verb} failed
     */
    failed = new xAPI_Verb(
        "lms",
        "failed",
        {
            "en-US": "failed",
            "de-DE": "scheiterte"
        },
        {
            "en-US": "Actor failed an activity (quiz attempt, upload, log in)",
            "de-DE": "Akteur scheiterte an einer Aktivität (Quizversuch, Upload, Einloggen)"
        });

    /**
     * Actor imported an object/data
     * @var {xAPI_Verb} imported
     */
    imported = new xAPI_Verb(
        "lms",
        "imported",
        {
            "en-US": "imported",
            "de-DE": "importiert"
        },
        {
            "en-US": "Actor imported an object/data",
            "de-DE": "Akteur hat ein Objekt/Daten importiert"
        });

    /**
     * Actor exported an object/data
     * @var {xAPI_Verb} exported
     */
    exported = new xAPI_Verb(
        "lms",
        "exported",
        {
            "en-US": "exported",
            "de-DE": "exportiert"
        },
        {
            "en-US": "Actor exported an object/data",
            "de-DE": "Akteur hat ein Objekt/Daten exportiert"
        });

    /**
     * Actor unlocked an object
     * @var {xAPI_Verb} unlocked
     */
    unlocked = new xAPI_Verb(
        "lms",
        "unlocked",
        {
            "en-US": "unlocked",
            "de-DE": "freigegeben"
        },
        {
            "en-US": "Actor unlocked an object",
            "de-DE": "Akteur hat ein Objekt freigegeben"
        });

    /**
     * Actor added an object/person to a collection
     * @var {xAPI_Verb} added
     */
    added = new xAPI_Verb(
        "lms",
        "added",
        {
            "en-US": "added",
            "de-DE": "hinzugefügt"
        },
        {
            "en-US": "Actor added an object/person to a collection",
            "de-DE": "Akteur hat eine/n Person/Gegenstand zu einer Sammlung hinzugefügt"
        });

    /**
     * Actor unblocked a blocked person
     * @var {xAPI_Verb} unblocked
     */
    unblocked = new xAPI_Verb(
        "lms",
        "unblocked",
        {
            "en-US": "unblocked",
            "de-DE": "entblockt"
        },
        {
            "en-US": "Actor unblocked a blocked person",
            "de-DE": "Akteur hat eine blockierte Person entblockt"
        });

    /**
     * Actor moved an object/person from one group/area to another
     * @var {xAPI_Verb} moved
     */
    moved = new xAPI_Verb(
        "lms",
        "moved",
        {
            "en-US": "moved",
            "de-DE": "bewegt"
        },
        {
            "en-US": "Actor moved an object/person from one group/area to another",
            "de-DE": "Akteuer hat ein/e Objekt/Person von einer Gruppe/Region in eine andere bewegt"
        });

    /**
     * Actor logged in as another person
     * @var {xAPI_Verb} logged_in_as
     */
    logged_in_as = new xAPI_Verb(
        "lms",
        "logged_in_as",
        {
            "en-US": "logged in as",
            "de-DE": "angemeldet als"
        },
        {
            "en-US": "Actor logged in as another person",
            "de-DE": "Akteur hat sich als eine andere Person angemeldet"
        });

    /**
     * Actor assigned an object/task/activity to someone
     * @var {xAPI_Verb} assigned
     */
    assigned = new xAPI_Verb(
        "lms",
        "assigned",
        {
            "en-US": "assigned",
            "de-DE": "zugeteilt"
        },
        {
            "en-US": "Actor assigned an object/task/activity to someone",
            "de-DE": "Akteur wurde ein/e Objekt/Aufgabe/Aktivität zugeteilt"
        });

    /**
     * Actor logged out
     * @var {xAPI_Verb} loggedOut
     */
    loggedOut = new xAPI_Verb(
        "lms",
        "loggedOut",
        {
            "en-US": "logged out",
            "de-DE": "abgemeldet"
        },
        {
            "en-US": "Actor logged out",
            "de-DE": "Akteur hat sich abgemeldet"
        });

    /**
     * Actor deleted an object
     * @var {xAPI_Verb} deleted
     */
    deleted = new xAPI_Verb(
        "lms",
        "deleted",
        {
            "en-US": "deleted",
            "de-DE": "gelöscht"
        },
        {
            "en-US": "Actor deleted an object",
            "de-DE": "Akteur hat ein Objekt gelöscht"
        });

    /**
     * Actor resetted an object
     * @var {xAPI_Verb} resetted
     */
    resetted = new xAPI_Verb(
        "lms",
        "resetted",
        {
            "en-US": "resetted",
            "de-DE": "zurückgesetzt"
        },
        {
            "en-US": "Actor resetted an object",
            "de-DE": "Akteur hat ein Objekt zurückgesetzt"
        });

    /**
     * Actor logged out
     * @var {xAPI_Verb} logged_out
     */
    logged_out = new xAPI_Verb(
        "lms",
        "logged_out",
        {
            "en-US": "logged out",
            "de-DE": "abgemeldet"
        },
        {
            "en-US": "Actor logged out",
            "de-DE": "Akteur hat sich abgemeldet"
        });

    /**
     * Actor searched for something
     * @var {xAPI_Verb} searched
     */
    searched = new xAPI_Verb(
        "lms",
        "searched",
        {
            "en-US": "searched",
            "de-DE": "gesucht"
        },
        {
            "en-US": "Actor searched for something",
            "de-DE": "Akteur hat nach etwas gesucht"
        });

    /**
     * Actor unapproved an object (in Moodle "learning plan")
     * @var {xAPI_Verb} unapproved
     */
    unapproved = new xAPI_Verb(
        "lms",
        "unapproved",
        {
            "en-US": "unapproved",
            "de-DE": "nicht genehmigt"
        },
        {
            "en-US": "Actor unapproved an object (in Moodle \"learning plan\")",
            "de-DE": "Akteur hat ein Objekt (in Moodle \"learning plan\") nicht genehmigt"
        });

    /**
     * Actor removed the assignment of an object/task/activity to someone
     * @var {xAPI_Verb} withdrew
     */
    withdrew = new xAPI_Verb(
        "lms",
        "withdrew",
        {
            "en-US": "withdrew",
            "de-DE": "zog zurück"
        },
        {
            "en-US": "Actor removed the assignment of an object/task/activity to someone",
            "de-DE": "Akteur hat die Zuteilung eines/r Objektes/Aufgabe/Aktivität zurückgezogen"
        });

    /**
     * Actor removed an object/person from a collection
     * @var {xAPI_Verb} removed
     */
    removed = new xAPI_Verb(
        "lms",
        "removed",
        {
            "en-US": "removed",
            "de-DE": "entfernt"
        },
        {
            "en-US": "Actor removed an object/person from a collection",
            "de-DE": "Akteur hat ein/e Objekt/Person von einer Sammlung entfernt"
        });

    /**
     * Actor downloaded an object (e. g. a PDF file)
     * @var {xAPI_Verb} downloaded
     */
    downloaded = new xAPI_Verb(
        "lms",
        "downloaded",
        {
            "en-US": "downloaded",
            "de-DE": "heruntergeladen"
        },
        {
            "en-US": "Actor downloaded an object (e. g. a PDF file)",
            "de-DE": "Akteur hat ein Objekt (z. B. eine PDF Datei) heruntergeladen"
        });

    /**
     * Actor created an object
     * @var {xAPI_Verb} created
     */
    created = new xAPI_Verb(
        "lms",
        "created",
        {
            "en-US": "created",
            "de-DE": "erstellt"
        },
        {
            "en-US": "Actor created an object",
            "de-DE": "Akteur hat ein Objekt erstellt"
        });

    /**
     * Actor requested an object/access/information
     * @var {xAPI_Verb} requested
     */
    requested = new xAPI_Verb(
        "lms",
        "requested",
        {
            "en-US": "requested",
            "de-DE": "angefordert"
        },
        {
            "en-US": "Actor requested an object/access/information",
            "de-DE": "Akteur hat ein/en Objekt/Zugang/Informationen angefordert"
        });

    /**
     * Actor cancelled an activity, a meetin
     * @var {xAPI_Verb} cancelled
     */
    cancelled = new xAPI_Verb(
        "lms",
        "cancelled",
        {
            "en-US": "cancelled",
            "de-DE": "abgesagt"
        },
        {
            "en-US": "Actor cancelled an activity, a meetin",
            "de-DE": "Akteur hat eine Aktivität, ein Meeting abgesagt"
        });

    /**
     * Actor logged in (as him-/herself)
     * @var {xAPI_Verb} logged_in
     */
    logged_in = new xAPI_Verb(
        "lms",
        "logged_in",
        {
            "en-US": "logged in",
            "de-DE": "angemeldet"
        },
        {
            "en-US": "Actor logged in (as him-/herself)",
            "de-DE": "Akteur hat sich angemeldet (als er/sie selbst)"
        });

    /**
     * Actor blocked a person
     * @var {xAPI_Verb} blocked
     */
    blocked = new xAPI_Verb(
        "lms",
        "blocked",
        {
            "en-US": "blocked",
            "de-DE": "gesperrt"
        },
        {
            "en-US": "Actor blocked a person",
            "de-DE": "Akteur hat eine Person gesperrt"
        });

    /**
     * Actor closed an object (forum post)
     * @var {xAPI_Verb} closed
     */
    closed = new xAPI_Verb(
        "lms",
        "closed",
        {
            "en-US": "closed",
            "de-DE": "geschlossen"
        },
        {
            "en-US": "Actor closed an object (forum post)",
            "de-DE": "Akteur hat ein Objekt (Forumeintrag) geschlossen"
        });

    /**
     * Actor succeeded an activity
     * @var {xAPI_Verb} succeeded
     */
    succeeded = new xAPI_Verb(
        "lms",
        "succeeded",
        {
            "en-US": "succeeded",
            "de-DE": "gelang"
        },
        {
            "en-US": "Actor succeeded an activity",
            "de-DE": "Akteur ist eine Aktivität gelungen"
        });

    /**
     * Actor locked an object
     * @var {xAPI_Verb} locked
     */
    locked = new xAPI_Verb(
        "lms",
        "locked",
        {
            "en-US": "locked",
            "de-DE": "gesperrt"
        },
        {
            "en-US": "Actor locked an object",
            "de-DE": "Akteur hat ein Objekt gesperrt"
        });

    /**
     * Actor triggered a process
     * @var {xAPI_Verb} triggered
     */
    triggered = new xAPI_Verb(
        "lms",
        "triggered",
        {
            "en-US": "triggered",
            "de-DE": "ausgelöst"
        },
        {
            "en-US": "Actor triggered a process",
            "de-DE": "Akteur hat einen Prozess ausgelöst"
        });

    /**
     * Actor approved an object (in Moodle "learning plan")
     * @var {xAPI_Verb} approved
     */
    approved = new xAPI_Verb(
        "lms",
        "approved",
        {
            "en-US": "approved",
            "de-DE": "genehmigt"
        },
        {
            "en-US": "Actor approved an object (in Moodle \"learning plan\")",
            "de-DE": "Akteur genehmigte ein Objekt (in Moodle \"Lernplan\")"
        });

    /**
     * Actor unlinked an object/person from an object
     * @var {xAPI_Verb} unlinked
     */
    unlinked = new xAPI_Verb(
        "lms",
        "unlinked",
        {
            "en-US": "unlinked",
            "de-DE": "getrennt"
        },
        {
            "en-US": "Actor unlinked an object/person from an object",
            "de-DE": "Akteur hat ein/e Objekt/Person von einem Objekt getrennt"
        });

    /**
     * Actor completed an activity/task
     * @var {xAPI_Verb} completed
     */
    completed = new xAPI_Verb(
        "lms",
        "completed",
        {
            "en-US": "completed",
            "de-DE": "fertiggestellt"
        },
        {
            "en-US": "Actor completed an activity/task",
            "de-DE": "Akteur hat eine Aktivität/Aufgabe fertiggestellt"
        });

    /**
     * Actor updated the content of an object
     * @var {xAPI_Verb} updated
     */
    updated = new xAPI_Verb(
        "lms",
        "updated",
        {
            "en-US": "updated",
            "de-DE": "aktualisiert"
        },
        {
            "en-US": "Actor updated the content of an object",
            "de-DE": "Akteur aktualisierte den Inhalt eines Objekts."
        });

    constructor() {
        super("lms");
    }
}