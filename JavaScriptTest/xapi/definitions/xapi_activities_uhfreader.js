import { xAPI_Activity } from '../core/xapi_activity.js';
import { xAPI_Activities } from '../core/xapi_activities.js';

/**
 * Provides the xAPI_Activities of the context uhfReader as public properties.
 */
export class xAPI_Activities_UhfReader extends xAPI_Activities {
    /**
     * A box which can be checked or unchecked. For example in this case Read TID should be selected.
     * @var {xAPI_Activity} checkbox
     */
    checkbox = new xAPI_Activity(
        "uhfReader",
        "checkbox",
        {
            "en-US": "checkbox"
        },
        {
            "en-US": "A box which can be checked or unchecked. For example in this case Read TID should be selected."
        });

    /**
     * Represents the transpnder to the physical reader.
     * @var {xAPI_Activity} transponder
     */
    transponder = new xAPI_Activity(
        "uhfReader",
        "transponder",
        {
            "en-US": "transponder"
        },
        {
            "en-US": "Represents the transpnder to the physical reader."
        });

    /**
     * Represent the RFID tag. You can drag it as per your requirements
     * @var {xAPI_Activity} rfidTag
     */
    rfidTag = new xAPI_Activity(
        "uhfReader",
        "rfidTag",
        {
            "en-US": "rfidTag"
        },
        {
            "en-US": "Represent the RFID tag. You can drag it as per your requirements"
        });

    /**
     * A image of the reader is generated which is interactible. The reading head is selected in the image.
     * @var {xAPI_Activity} readerImage
     */
    readerImage = new xAPI_Activity(
        "uhfReader",
        "readerImage",
        {
            "en-US": "reader image"
        },
        {
            "en-US": "A image of the reader is generated which is interactible. The reading head is selected in the image."
        });

    /**
     * A mouse used to click on buttons in the software.
     * @var {xAPI_Activity} mouse
     */
    mouse = new xAPI_Activity(
        "uhfReader",
        "mouse",
        {
            "en-US": "mouse"
        },
        {
            "en-US": "A mouse used to click on buttons in the software."
        });

    /**
     * A progress to identify 
     * @var {xAPI_Activity} identification
     */
    identification = new xAPI_Activity(
        "uhfReader",
        "identification",
        {
            "en-US": "identification"
        },
        {
            "en-US": "A progress to identify "
        });

    /**
     * A field which can be click to fire an action. For example the 'start button'.
     * @var {xAPI_Activity} button
     */
    button = new xAPI_Activity(
        "uhfReader",
        "button",
        {
            "en-US": "button"
        },
        {
            "en-US": "A field which can be click to fire an action. For example the 'start button'."
        });

    constructor() {
        super("uhfReader");
    }
}