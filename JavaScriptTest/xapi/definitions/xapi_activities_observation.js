import { xAPI_Activity } from '../core/xapi_activity.js';
import { xAPI_Activities } from '../core/xapi_activities.js';
import { xAPI_Activities_Observation_Mainscores } from './xapi_activities_observation_mainscores.js';
import { xAPI_Activities_Observation_Subscores } from './xapi_activities_observation_subscores.js';

/**
 * Provides the xAPI_Activities of the context observation as public properties.
 */
export class xAPI_Activities_Observation extends xAPI_Activities {
    mainscores = new xAPI_Activities_Observation_Mainscores();

    subscores = new xAPI_Activities_Observation_Subscores();

    /**
     * Konkreter Gegenstand, der zum Aufgabenlösen zur Verfügung gestellt wurde
     * @var {xAPI_Activity} schere
     */
    schere = new xAPI_Activity(
        "observation",
        "schere",
        {
            "de-DE": "Schere"
        },
        {
            "de-DE": "Konkreter Gegenstand, der zum Aufgabenlösen zur Verfügung gestellt wurde"
        });

    /**
     * Verb bezieht sich auf Anweisungen
     * @var {xAPI_Activity} anweisungen
     */
    anweisungen = new xAPI_Activity(
        "observation",
        "anweisungen",
        {
            "de-DE": "Anweisungen"
        },
        {
            "de-DE": "Verb bezieht sich auf Anweisungen"
        });

    /**
     * Verb bezieht sich auf das Abgeben der Führung
     * @var {xAPI_Activity} fuehrung_ab
     */
    fuehrung_ab = new xAPI_Activity(
        "observation",
        "fuehrung_ab",
        {
            "de-DE": "Führung ab"
        },
        {
            "de-DE": "Verb bezieht sich auf das Abgeben der Führung"
        });

    /**
     * mit Betrachtung einiger Details
     * @var {xAPI_Activity} es_sich_genauer_an
     */
    es_sich_genauer_an = new xAPI_Activity(
        "observation",
        "es_sich_genauer_an",
        {
            "de-DE": "es sich genauer an"
        },
        {
            "de-DE": "mit Betrachtung einiger Details"
        });

    /**
     * Von Beobachtern durchgeführte Aufgabenerklärung
     * @var {xAPI_Activity} erklaerung
     */
    erklaerung = new xAPI_Activity(
        "observation",
        "erklaerung",
        {
            "de-DE": "Erklärung"
        },
        {
            "de-DE": "Von Beobachtern durchgeführte Aufgabenerklärung"
        });

    /**
     * Verb bezieht sich auf Objekte, die nichts mit der Aufgabenstellung zutun haben.
     * @var {xAPI_Activity} sich_mit_anderen_Dingen
     */
    sich_mit_anderen_Dingen = new xAPI_Activity(
        "observation",
        "sich_mit_anderen_Dingen",
        {
            "de-DE": "sich mit anderen Dingen"
        },
        {
            "de-DE": "Verb bezieht sich auf Objekte, die nichts mit der Aufgabenstellung zutun haben."
        });

    /**
     * genaure Informationen über Aufgaben
     * @var {xAPI_Activity} nach_Aufgaben_Details
     */
    nach_Aufgaben_Details = new xAPI_Activity(
        "observation",
        "nach_Aufgaben_Details",
        {
            "de-DE": "nach Aufgaben Details"
        },
        {
            "de-DE": "genaure Informationen über Aufgaben"
        });

    /**
     * aus dem Fenster
     * @var {xAPI_Activity} aus_dem_Fenster
     */
    aus_dem_Fenster = new xAPI_Activity(
        "observation",
        "aus_dem_Fenster",
        {
            "de-DE": "aus dem Fenster"
        },
        {
            "de-DE": "aus dem Fenster"
        });

    /**
     * Verb bezieht sich auf das Abgeben des Materials
     * @var {xAPI_Activity} material_ab
     */
    material_ab = new xAPI_Activity(
        "observation",
        "material_ab",
        {
            "de-DE": "Führung ab"
        },
        {
            "de-DE": "Verb bezieht sich auf das Abgeben des Materials"
        });

    /**
     * Verb bezieht sich auf andere Akteure
     * @var {xAPI_Activity} andere
     */
    andere = new xAPI_Activity(
        "observation",
        "andere",
        {
            "de-DE": "andere"
        },
        {
            "de-DE": "Verb bezieht sich auf andere Akteure"
        });

    /**
     * Gegnstände die zum Aufgabenlösen zur Verfügung gestellt werden
     * @var {xAPI_Activity} material
     */
    material = new xAPI_Activity(
        "observation",
        "material",
        {
            "de-DE": "Material"
        },
        {
            "de-DE": "Gegnstände die zum Aufgabenlösen zur Verfügung gestellt werden"
        });

    /**
     * Verb bezieht sich auf andere Mitschüler
     * @var {xAPI_Activity} mitschueler
     */
    mitschueler = new xAPI_Activity(
        "observation",
        "mitschueler",
        {
            "de-DE": "Mitschüler"
        },
        {
            "de-DE": "Verb bezieht sich auf andere Mitschüler"
        });

    /**
     * Von Akteur selbst erbrachte Denkleistung
     * @var {xAPI_Activity} seine_Idee
     */
    seine_Idee = new xAPI_Activity(
        "observation",
        "seine_Idee",
        {
            "de-DE": "seine Idee"
        },
        {
            "de-DE": "Von Akteur selbst erbrachte Denkleistung"
        });

    constructor() {
        super("observation");
    }
}