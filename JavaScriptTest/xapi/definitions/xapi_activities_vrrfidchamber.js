import { xAPI_Activity } from '../core/xapi_activity.js';
import { xAPI_Activities } from '../core/xapi_activities.js';

/**
 * Provides the xAPI_Activities of the context vrRfidChamber as public properties.
 */
export class xAPI_Activities_VrRfidChamber extends xAPI_Activities {
    /**
     * Some audio content to feel the rela atmosphere of the lab. This can be switched on or off
     * @var {xAPI_Activity} labAudio
     */
    labAudio = new xAPI_Activity(
        "vrRfidChamber",
        "labAudio",
        {
            "en-US": "lab audio"
        },
        {
            "en-US": "Some audio content to feel the rela atmosphere of the lab. This can be switched on or off"
        });

    /**
     * An actor opened or closed the door
     * @var {xAPI_Activity} door
     */
    door = new xAPI_Activity(
        "vrRfidChamber",
        "door",
        {
            "en-US": "door"
        },
        {
            "en-US": "An actor opened or closed the door"
        });

    /**
     * A mouse used to click on buttons or drag items in the lab.
     * @var {xAPI_Activity} mouse
     */
    mouse = new xAPI_Activity(
        "vrRfidChamber",
        "mouse",
        {
            "en-US": "mouse"
        },
        {
            "en-US": "A mouse used to click on buttons or drag items in the lab."
        });

    /**
     * Some kind of written text to explain an action
     * @var {xAPI_Activity} comment
     */
    comment = new xAPI_Activity(
        "vrRfidChamber",
        "comment",
        {
            "en-US": "comment"
        },
        {
            "en-US": "Some kind of written text to explain an action"
        });

    /**
     * Clicking an item, holding the click and moving it around the lab.
     * @var {xAPI_Activity} draggable
     */
    draggable = new xAPI_Activity(
        "vrRfidChamber",
        "draggable",
        {
            "en-US": "drag"
        },
        {
            "en-US": "Clicking an item, holding the click and moving it around the lab."
        });

    /**
     * The screen showing the virtual lab and can be clicked or touched on for interaction with the game.
     * @var {xAPI_Activity} screen
     */
    screen = new xAPI_Activity(
        "vrRfidChamber",
        "screen",
        {
            "en-US": "screen"
        },
        {
            "en-US": "The screen showing the virtual lab and can be clicked or touched on for interaction with the game."
        });

    /**
     * Represents the RFID chamber.
     * @var {xAPI_Activity} device
     */
    device = new xAPI_Activity(
        "vrRfidChamber",
        "device",
        {
            "en-US": "device "
        },
        {
            "en-US": "Represents the RFID chamber."
        });

    /**
     * Some kind of selection is represented by this. It is given by user.
     * @var {xAPI_Activity} selection
     */
    selection = new xAPI_Activity(
        "vrRfidChamber",
        "selection",
        {
            "en-US": "Select"
        },
        {
            "en-US": "Some kind of selection is represented by this. It is given by user."
        });

    /**
     * Something an actor can press, which then triggers an action in the lab.
     * @var {xAPI_Activity} button
     */
    button = new xAPI_Activity(
        "vrRfidChamber",
        "button",
        {
            "en-US": "button"
        },
        {
            "en-US": "Something an actor can press, which then triggers an action in the lab."
        });

    /**
     * A keyboard used to input text or interact with the lab.
     * @var {xAPI_Activity} keyboard
     */
    keyboard = new xAPI_Activity(
        "vrRfidChamber",
        "keyboard",
        {
            "en-US": "keyboard"
        },
        {
            "en-US": "A keyboard used to input text or interact with the lab."
        });

    constructor() {
        super("vrRfidChamber");
    }
}