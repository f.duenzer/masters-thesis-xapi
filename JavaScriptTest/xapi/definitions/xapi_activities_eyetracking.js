import { xAPI_Activity } from '../core/xapi_activity.js';
import { xAPI_Activities } from '../core/xapi_activities.js';

/**
 * Provides the xAPI_Activities of the context eyeTracking as public properties.
 */
export class xAPI_Activities_EyeTracking extends xAPI_Activities {
    /**
     * Organ of the visual system. Actors physical (left or right) eye.
     * @var {xAPI_Activity} eye
     */
    eye = new xAPI_Activity(
        "eyeTracking",
        "eye",
        {
            "en-US": "eye",
            "de-DE": "Auge"
        },
        {
            "en-US": "Organ of the visual system. Actors physical (left or right) eye.",
            "de-DE": "Sinnesorgan zur Wahrnehmung von Lichtreizen, in diesem Fall physikalisches Auge des Akteurs (links oder rechts)."
        });

    constructor() {
        super("eyeTracking");
    }
}