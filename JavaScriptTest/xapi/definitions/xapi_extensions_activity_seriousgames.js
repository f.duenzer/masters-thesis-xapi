import { xAPI_Extensions_Activity } from './xapi_extensions_activity.js';
import { xAPI_Extension } from '../core/xapi_extension.js';

/**
 * Provides all extensions of the context seriousgames of type activity as public properties.
 */
export class xAPI_Extensions_Activity_Seriousgames extends xAPI_Extensions_Activity {

    constructor() {
        super("seriousgames");
    }

    buttontype(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "buttontype",
                {
                    "en-US": "button type",
                    "de-DE": "Knopf Typ"
                },
                {
                    "en-US": "The type of button which was pressed. Has to be String.",
                    "de-DE": "Der Typ des Knopfes der gedrückt wurde. Muss ein String sein."
                }),
            value);
        return this;
    }
}