import { xAPI_Extensions } from '../core/xapi_extensions.js';

export class xAPI_Extensions_Context extends xAPI_Extensions {

    constructor(context = null) {
        super("context", context);
    }
}