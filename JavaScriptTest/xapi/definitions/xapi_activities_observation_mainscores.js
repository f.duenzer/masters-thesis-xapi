import { xAPI_Activity } from '../core/xapi_activity.js';
import { xAPI_Activities } from '../core/xapi_activities.js';

/**
 * Provides the xAPI_Activities of the context observation as public properties.
 */
export class xAPI_Activities_Observation_Mainscores extends xAPI_Activities {
    /**
     * Mainscore (5/5) wenn ein Akteuer Geduld zeigt
     * @var {xAPI_Activity} geduld
     */
    geduld = new xAPI_Activity(
        "observation",
        "geduld",
        {
            "de-DE": "Geduld"
        },
        {
            "de-DE": "Mainscore (5/5) wenn ein Akteuer Geduld zeigt"
        });

    /**
     * Mainscore (5/5) wenn ein Akteuer Sprachkompetenz zeigt
     * @var {xAPI_Activity} sprachkompetenz
     */
    sprachkompetenz = new xAPI_Activity(
        "observation",
        "sprachkompetenz",
        {
            "de-DE": "Sprachkompetenz"
        },
        {
            "de-DE": "Mainscore (5/5) wenn ein Akteuer Sprachkompetenz zeigt"
        });

    /**
     * Mainscore (5/5) wenn ein Akteuer Soziale Potenziale zeigt
     * @var {xAPI_Activity} soziale_Potenziale
     */
    soziale_Potenziale = new xAPI_Activity(
        "observation",
        "soziale_Potenziale",
        {
            "de-DE": "Soziale Potenziale"
        },
        {
            "de-DE": "Mainscore (5/5) wenn ein Akteuer Soziale Potenziale zeigt"
        });

    /**
     * Mainscore (5/5) wenn ein Akteuer Kommunikationsfähigkeiten zeigt
     * @var {xAPI_Activity} kommunikationsfaehigkeiten
     */
    kommunikationsfaehigkeiten = new xAPI_Activity(
        "observation",
        "kommunikationsfaehigkeiten",
        {
            "de-DE": "Kommunikationsfähigkeiten"
        },
        {
            "de-DE": "Mainscore (5/5) wenn ein Akteuer Kommunikationsfähigkeiten zeigt"
        });

    /**
     * Mainscore (5/5) wenn ein Akteuer Kreativität zeigt
     * @var {xAPI_Activity} kreativitaet
     */
    kreativitaet = new xAPI_Activity(
        "observation",
        "kreativitaet",
        {
            "de-DE": "Kreativität"
        },
        {
            "de-DE": "Mainscore (5/5) wenn ein Akteuer Kreativität zeigt"
        });

    /**
     * Mainscore (5/5) wenn ein Akteuer Problemlösent zeigt
     * @var {xAPI_Activity} problemloesen
     */
    problemloesen = new xAPI_Activity(
        "observation",
        "problemloesen",
        {
            "de-DE": "Problemlösen"
        },
        {
            "de-DE": "Mainscore (5/5) wenn ein Akteuer Problemlösent zeigt"
        });

    /**
     * Mainscore (5/5) wenn ein Akteuer Methodisches Potenzial zeigt
     * @var {xAPI_Activity} methodisches_Potenzial
     */
    methodisches_Potenzial = new xAPI_Activity(
        "observation",
        "methodisches_Potenzial",
        {
            "de-DE": "Methodisches Potenzial"
        },
        {
            "de-DE": "Mainscore (5/5) wenn ein Akteuer Methodisches Potenzial zeigt"
        });

    /**
     * Mainscore (5/5) wenn ein Akteuer Achtsamkeit zeigt
     * @var {xAPI_Activity} achtsamkeit
     */
    achtsamkeit = new xAPI_Activity(
        "observation",
        "achtsamkeit",
        {
            "de-DE": "Achtsamkeit"
        },
        {
            "de-DE": "Mainscore (5/5) wenn ein Akteuer Achtsamkeit zeigt"
        });

    /**
     * Mainscore (5/5) wenn ein Akteuer Praktisches Potenzial zeigt
     * @var {xAPI_Activity} praktisches_Potenzial
     */
    praktisches_Potenzial = new xAPI_Activity(
        "observation",
        "praktisches_Potenzial",
        {
            "de-DE": "Praktisches Potenzial"
        },
        {
            "de-DE": "Mainscore (5/5) wenn ein Akteuer Praktisches Potenzial zeigt"
        });

    /**
     * Mainscore (5/5) wenn ein Akteuer Räumliches Vorstellungsvermögen zeigt
     * @var {xAPI_Activity} raeumliches_Vorstellungsvermoegen
     */
    raeumliches_Vorstellungsvermoegen = new xAPI_Activity(
        "observation",
        "raeumliches_Vorstellungsvermoegen",
        {
            "de-DE": "Räumliches Vorstellungsvermögen"
        },
        {
            "de-DE": "Mainscore (5/5) wenn ein Akteuer Räumliches Vorstellungsvermögen zeigt"
        });

    /**
     * Mainscore (5/5) wenn ein Akteuer Motivation und Leistungsbereitschaft zeigt
     * @var {xAPI_Activity} motivation_und_Leistungsbereitschaft
     */
    motivation_und_Leistungsbereitschaft = new xAPI_Activity(
        "observation",
        "motivation_und_Leistungsbereitschaft",
        {
            "de-DE": "Motivation und Leistungsbereitschaft"
        },
        {
            "de-DE": "Mainscore (5/5) wenn ein Akteuer Motivation und Leistungsbereitschaft zeigt"
        });

    constructor() {
        super("observation");
    }
}