import { xAPI_Extensions_Result_SystemControl } from './xapi_extensions_result_systemcontrol.js';
import { xAPI_Extensions_Activity_SystemControl } from './xapi_extensions_activity_systemcontrol.js';

/**
 * Provides the extensions of the context systemControl as public properties.
 */
export class xAPI_Context_SystemControl_Extensions {

    constructor() {
    }

    get result() {
        return new xAPI_Extensions_Result_SystemControl();
    }

    get activity() {
        return new xAPI_Extensions_Activity_SystemControl();
    }
}