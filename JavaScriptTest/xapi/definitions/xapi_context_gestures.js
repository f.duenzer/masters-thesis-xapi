import { xAPI_Context } from '../core/xapi_context.js';
import { xAPI_Verbs_Gestures } from './xapi_verbs_gestures.js';
import { xAPI_Activities_Gestures } from './xapi_activities_gestures.js';
import { xAPI_Context_Gestures_Extensions } from './xapi_context_gestures_extensions.js';

/**
 * Provides the definitions of the context gestures as public properties.
 */
export class xAPI_Context_Gestures extends xAPI_Context {
    verbs = new xAPI_Verbs_Gestures();

    activities = new xAPI_Activities_Gestures();

    extensions = new xAPI_Context_Gestures_Extensions();

    constructor() {
        super("gestures");
    }
}