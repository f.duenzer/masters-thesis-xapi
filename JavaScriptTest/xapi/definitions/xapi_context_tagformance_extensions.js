import { xAPI_Extensions_Context_Tagformance } from './xapi_extensions_context_tagformance.js';
import { xAPI_Extensions_Result_Tagformance } from './xapi_extensions_result_tagformance.js';
import { xAPI_Extensions_Activity_Tagformance } from './xapi_extensions_activity_tagformance.js';

/**
 * Provides the extensions of the context tagformance as public properties.
 */
export class xAPI_Context_Tagformance_Extensions {

    constructor() {
    }

    get context() {
        return new xAPI_Extensions_Context_Tagformance();
    }

    get result() {
        return new xAPI_Extensions_Result_Tagformance();
    }

    get activity() {
        return new xAPI_Extensions_Activity_Tagformance();
    }
}