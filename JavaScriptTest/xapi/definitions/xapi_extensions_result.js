import { xAPI_Extensions } from '../core/xapi_extensions.js';

export class xAPI_Extensions_Result extends xAPI_Extensions {

    constructor(context = null) {
        super("result", context);
    }
}