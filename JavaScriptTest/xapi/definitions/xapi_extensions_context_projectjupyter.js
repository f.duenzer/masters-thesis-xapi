import { xAPI_Extensions_Context } from './xapi_extensions_context.js';
import { xAPI_Extension } from '../core/xapi_extension.js';

/**
 * Provides all extensions of the context projectJupyter of type context as public properties.
 */
export class xAPI_Extensions_Context_ProjectJupyter extends xAPI_Extensions_Context {

    constructor() {
        super("projectJupyter");
    }

    profilename(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "profilename",
                {
                    "en-US": "profilename",
                    "de-DE": "Profilname"
                },
                {
                    "en-US": "Name of the currently loaded JupyterLab profile (within a JupyterHub).",
                    "de-DE": "Name des aktuell geladenen JupyterLab-Profils (in einem JupyterHub)."
                }),
            value);
        return this;
    }
}