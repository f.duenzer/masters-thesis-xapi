import { xAPI_Context } from '../core/xapi_context.js';
import { xAPI_Verbs_EyeTracking } from './xapi_verbs_eyetracking.js';
import { xAPI_Activities_EyeTracking } from './xapi_activities_eyetracking.js';
import { xAPI_Context_EyeTracking_Extensions } from './xapi_context_eyetracking_extensions.js';

/**
 * Provides the definitions of the context eyeTracking as public properties.
 */
export class xAPI_Context_EyeTracking extends xAPI_Context {
    verbs = new xAPI_Verbs_EyeTracking();

    activities = new xAPI_Activities_EyeTracking();

    extensions = new xAPI_Context_EyeTracking_Extensions();

    constructor() {
        super("eyeTracking");
    }
}