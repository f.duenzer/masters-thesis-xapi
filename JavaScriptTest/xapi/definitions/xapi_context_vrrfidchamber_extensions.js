import { xAPI_Extensions_Context_VrRfidChamber } from './xapi_extensions_context_vrrfidchamber.js';
import { xAPI_Extensions_Result_VrRfidChamber } from './xapi_extensions_result_vrrfidchamber.js';

/**
 * Provides the extensions of the context vrRfidChamber as public properties.
 */
export class xAPI_Context_VrRfidChamber_Extensions {

    constructor() {
    }

    get context() {
        return new xAPI_Extensions_Context_VrRfidChamber();
    }

    get result() {
        return new xAPI_Extensions_Result_VrRfidChamber();
    }
}