import { xAPI_Context } from '../core/xapi_context.js';
import { xAPI_Verbs_Tagformance } from './xapi_verbs_tagformance.js';
import { xAPI_Activities_Tagformance } from './xapi_activities_tagformance.js';
import { xAPI_Context_Tagformance_Extensions } from './xapi_context_tagformance_extensions.js';

/**
 * Provides the definitions of the context tagformance as public properties.
 */
export class xAPI_Context_Tagformance extends xAPI_Context {
    verbs = new xAPI_Verbs_Tagformance();

    activities = new xAPI_Activities_Tagformance();

    extensions = new xAPI_Context_Tagformance_Extensions();

    constructor() {
        super("tagformance");
    }
}