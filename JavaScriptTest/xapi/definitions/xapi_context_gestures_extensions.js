import { xAPI_Extensions_Result_Gestures } from './xapi_extensions_result_gestures.js';
import { xAPI_Extensions_Activity_Gestures } from './xapi_extensions_activity_gestures.js';

/**
 * Provides the extensions of the context gestures as public properties.
 */
export class xAPI_Context_Gestures_Extensions {

    constructor() {
    }

    get result() {
        return new xAPI_Extensions_Result_Gestures();
    }

    get activity() {
        return new xAPI_Extensions_Activity_Gestures();
    }
}