import { xAPI_Context_EyeTracking } from './xapi_context_eyetracking.js';
import { xAPI_Context_Generic } from './xapi_context_generic.js';
import { xAPI_Context_Gestures } from './xapi_context_gestures.js';
import { xAPI_Context_Lms } from './xapi_context_lms.js';
import { xAPI_Context_Multitouch } from './xapi_context_multitouch.js';
import { xAPI_Context_Observation } from './xapi_context_observation.js';
import { xAPI_Context_ProjectJupyter } from './xapi_context_projectjupyter.js';
import { xAPI_Context_Seriousgames } from './xapi_context_seriousgames.js';
import { xAPI_Context_SystemControl } from './xapi_context_systemcontrol.js';
import { xAPI_Context_Tagformance } from './xapi_context_tagformance.js';
import { xAPI_Context_UhfReader } from './xapi_context_uhfreader.js';
import { xAPI_Context_VirtualReality } from './xapi_context_virtualreality.js';
import { xAPI_Context_VrRfidChamber } from './xapi_context_vrrfidchamber.js';

/**
 * Static class that provides all contexts as public properties.
 */
export class xAPI_Definitions {
    static eyeTracking = new xAPI_Context_EyeTracking();

    static generic = new xAPI_Context_Generic();

    static gestures = new xAPI_Context_Gestures();

    static lms = new xAPI_Context_Lms();

    static multitouch = new xAPI_Context_Multitouch();

    static observation = new xAPI_Context_Observation();

    static projectJupyter = new xAPI_Context_ProjectJupyter();

    static seriousgames = new xAPI_Context_Seriousgames();

    static systemControl = new xAPI_Context_SystemControl();

    static tagformance = new xAPI_Context_Tagformance();

    static uhfReader = new xAPI_Context_UhfReader();

    static virtualReality = new xAPI_Context_VirtualReality();

    static vrRfidChamber = new xAPI_Context_VrRfidChamber();
}