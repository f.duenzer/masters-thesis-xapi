import { xAPI_Context } from '../core/xapi_context.js';
import { xAPI_Verbs_SystemControl } from './xapi_verbs_systemcontrol.js';
import { xAPI_Activities_SystemControl } from './xapi_activities_systemcontrol.js';
import { xAPI_Context_SystemControl_Extensions } from './xapi_context_systemcontrol_extensions.js';

/**
 * Provides the definitions of the context systemControl as public properties.
 */
export class xAPI_Context_SystemControl extends xAPI_Context {
    verbs = new xAPI_Verbs_SystemControl();

    activities = new xAPI_Activities_SystemControl();

    extensions = new xAPI_Context_SystemControl_Extensions();

    constructor() {
        super("systemControl");
    }
}