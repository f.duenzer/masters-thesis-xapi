import { xAPI_Context } from '../core/xapi_context.js';
import { xAPI_Verbs_Observation } from './xapi_verbs_observation.js';
import { xAPI_Activities_Observation } from './xapi_activities_observation.js';
import { xAPI_Context_Observation_Extensions } from './xapi_context_observation_extensions.js';

/**
 * Provides the definitions of the context observation as public properties.
 */
export class xAPI_Context_Observation extends xAPI_Context {
    verbs = new xAPI_Verbs_Observation();

    activities = new xAPI_Activities_Observation();

    extensions = new xAPI_Context_Observation_Extensions();

    constructor() {
        super("observation");
    }
}