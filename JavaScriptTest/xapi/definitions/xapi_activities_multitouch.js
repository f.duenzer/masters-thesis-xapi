import { xAPI_Activity } from '../core/xapi_activity.js';
import { xAPI_Activities } from '../core/xapi_activities.js';

/**
 * Provides the xAPI_Activities of the context multitouch as public properties.
 */
export class xAPI_Activities_Multitouch extends xAPI_Activities {
    /**
     * Something a player can interact with in the game.
     * @var {xAPI_Activity} interactable
     */
    interactable = new xAPI_Activity(
        "multitouch",
        "interactable",
        {
            "en-US": "interactable",
            "de-DE": "interagierfähiges Objekt"
        },
        {
            "en-US": "Something a player can interact with in the game.",
            "de-DE": "Etwas womit der Spieler im Spiel interagieren kann."
        });

    /**
     * Something that can be dragged across the screen in the game.
     * @var {xAPI_Activity} draggable
     */
    draggable = new xAPI_Activity(
        "multitouch",
        "draggable",
        {
            "en-US": "draggable",
            "de-DE": "ziehbares Objekt"
        },
        {
            "en-US": "Something that can be dragged across the screen in the game.",
            "de-DE": "Etwas was im Spiel über den Bildschirm gezogen werden kann."
        });

    /**
     * The screen showing the game and being clicked or touched on for interaction with the game.
     * @var {xAPI_Activity} screen
     */
    screen = new xAPI_Activity(
        "multitouch",
        "screen",
        {
            "en-US": "screen",
            "de-DE": "Bildschirm"
        },
        {
            "en-US": "The screen showing the game and being clicked or touched on for interaction with the game.",
            "de-DE": "Der Bildschirm der das Spiel anzeigt und auf dem geklickt werden kann oder der berührt wird um mit dem Spiel zu interagieren."
        });

    /**
     * Something which can be collected inside the game to fullfill a task or goal.
     * @var {xAPI_Activity} collectable
     */
    collectable = new xAPI_Activity(
        "multitouch",
        "collectable",
        {
            "en-US": "collectable",
            "de-DE": "einsammelbares Objekt"
        },
        {
            "en-US": "Something which can be collected inside the game to fullfill a task or goal.",
            "de-DE": "Ein Objekt, welches im Spiel eingesammelt werden kann um ein Spielziel zu erreichen oder eine Aufgabe zu erfüllen."
        });

    /**
     * Something an actor can press, which then triggers an action in the game.
     * @var {xAPI_Activity} button
     */
    button = new xAPI_Activity(
        "multitouch",
        "button",
        {
            "en-US": "button",
            "de-DE": "Knopf"
        },
        {
            "en-US": "Something an actor can press, which then triggers an action in the game.",
            "de-DE": "Etwas was ein Akteur drücken kann, was dann eine Aktion im Spiel hervorruft."
        });

    constructor() {
        super("multitouch");
    }
}