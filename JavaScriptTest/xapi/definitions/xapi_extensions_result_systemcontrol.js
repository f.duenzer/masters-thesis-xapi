import { xAPI_Extensions_Result } from './xapi_extensions_result.js';
import { xAPI_Extension } from '../core/xapi_extension.js';

/**
 * Provides all extensions of the context systemControl of type result as public properties.
 */
export class xAPI_Extensions_Result_SystemControl extends xAPI_Extensions_Result {

    constructor() {
        super("systemControl");
    }

    language(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "language",
                {
                    "en-US": "language",
                    "de-DE": "Sprache"
                },
                {
                    "en-US": "Language of the system.",
                    "de-DE": "Sprache des Systems."
                }),
            value);
        return this;
    }
}