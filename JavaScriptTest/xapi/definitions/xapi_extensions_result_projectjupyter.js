import { xAPI_Extensions_Result } from './xapi_extensions_result.js';
import { xAPI_Extension } from '../core/xapi_extension.js';

/**
 * Provides all extensions of the context projectJupyter of type result as public properties.
 */
export class xAPI_Extensions_Result_ProjectJupyter extends xAPI_Extensions_Result {

    constructor() {
        super("projectJupyter");
    }

    errorMessage(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "errorMessage",
                {
                    "en-US": "Error message",
                    "de-DE": "Fehlermeldung"
                },
                {
                    "en-US": "A string describing an error. Used to provide the runtimeerror that might occur during the execution of ta JupyterLab notebook cell.",
                    "de-DE": "Die beschreibung eines Fehlers. Wird verwended um z.b. Laufzeitfehler während des Ausführens einer JupyterLab Notebook Zelle anzugeben."
                }),
            value);
        return this;
    }

    cellOutput(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "cellOutput",
                {
                    "en-US": "Cell Output",
                    "de-DE": "Zellenausgabe"
                },
                {
                    "en-US": "A string representing the cell outputs of a JupyterLab cell. Outputs can contain a.o. execution results or display data.",
                    "de-DE": "Ein String der die Ausgabe einer JupyterLab-Zelle beschreibt. Ausgaben können u.a. Ausführungsergebnisse oder Anzeigedaten enthalten."
                }),
            value);
        return this;
    }
}