import { xAPI_Activity } from '../core/xapi_activity.js';
import { xAPI_Activities } from '../core/xapi_activities.js';

/**
 * Provides the xAPI_Activities of the context virtualReality as public properties.
 */
export class xAPI_Activities_VirtualReality extends xAPI_Activities {
    /**
     * A VR controller touchpad the actor can use, appearance and possibilities depend on the specific conroller type.
     * @var {xAPI_Activity} touchpad
     */
    touchpad = new xAPI_Activity(
        "virtualReality",
        "touchpad",
        {
            "en-US": "controller touchpad",
            "de-DE": "Controller Touchpad"
        },
        {
            "en-US": "A VR controller touchpad the actor can use, appearance and possibilities depend on the specific conroller type.",
            "de-DE": "Ein VR Controller Touchpad, das die Akteurin benutzen kann. Aussehen und Möglichkeiten sind abhängig davon welcher Controller genutzt wurde."
        });

    /**
     * An UI element in VR the user can interact with (e.g. Panel, Button, Slider).
     * @var {xAPI_Activity} uiElement
     */
    uiElement = new xAPI_Activity(
        "virtualReality",
        "uiElement",
        {
            "en-US": "VR user interface element",
            "de-DE": "VR Benutzerflächenelement"
        },
        {
            "en-US": "An UI element in VR the user can interact with (e.g. Panel, Button, Slider).",
            "de-DE": "Ein Benutzerflächenelement mit dem der Benutzer interagieren kann (z.B. Panel, Button, Slider)."
        });

    /**
     * An object which can be pointed and highlighted with for example a laser starting from the controller.
     * @var {xAPI_Activity} pointable
     */
    pointable = new xAPI_Activity(
        "virtualReality",
        "pointable",
        {
            "en-US": "pointable object",
            "de-DE": "ein markierbares Objekt"
        },
        {
            "en-US": "An object which can be pointed and highlighted with for example a laser starting from the controller.",
            "de-DE": "Ein Objekt, welches z.B. mit einem Laser vom Controller markiert werden kann."
        });

    /**
     * An action the actor can trigger.
     * @var {xAPI_Activity} action
     */
    action = new xAPI_Activity(
        "virtualReality",
        "action",
        {
            "en-US": "action",
            "de-DE": "Aktion"
        },
        {
            "en-US": "An action the actor can trigger.",
            "de-DE": "Eine Aktion, die von der Akteurin ausgelöst wird."
        });

    /**
     * A virtual object in a VR environment.
     * @var {xAPI_Activity} vrObject
     */
    vrObject = new xAPI_Activity(
        "virtualReality",
        "vrObject",
        {
            "en-US": "VR object",
            "de-DE": "VR Objekt"
        },
        {
            "en-US": "A virtual object in a VR environment.",
            "de-DE": "Ein virtuelles Objekt in einer VR Umgebung."
        });

    /**
     * A controller button or gamepad button the actor can use. On–off controls that switch between different system states.
     * @var {xAPI_Activity} controllerButton
     */
    controllerButton = new xAPI_Activity(
        "virtualReality",
        "controllerButton",
        {
            "en-US": "controller button",
            "de-DE": "Controller Taste"
        },
        {
            "en-US": "A controller button or gamepad button the actor can use. On–off controls that switch between different system states.",
            "de-DE": "Eine Taste des Controllers oder des Gamepads, die die Akteurin benutzen kann. Die Betätigung der Taste führt zu einer Änderung des Systemstatus."
        });

    /**
     * A fixed teleport point in VR environment.
     * @var {xAPI_Activity} teleportPoint
     */
    teleportPoint = new xAPI_Activity(
        "virtualReality",
        "teleportPoint",
        {
            "en-US": "teleport point",
            "de-DE": "Teleport Punkt"
        },
        {
            "en-US": "A fixed teleport point in VR environment.",
            "de-DE": "Ein fester Teleport Punkt in der VR Umgebung."
        });

    constructor() {
        super("virtualReality");
    }
}