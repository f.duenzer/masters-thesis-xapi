import { xAPI_Context } from '../core/xapi_context.js';
import { xAPI_Verbs_ProjectJupyter } from './xapi_verbs_projectjupyter.js';
import { xAPI_Activities_ProjectJupyter } from './xapi_activities_projectjupyter.js';
import { xAPI_Context_ProjectJupyter_Extensions } from './xapi_context_projectjupyter_extensions.js';

/**
 * Provides the definitions of the context projectJupyter as public properties.
 */
export class xAPI_Context_ProjectJupyter extends xAPI_Context {
    verbs = new xAPI_Verbs_ProjectJupyter();

    activities = new xAPI_Activities_ProjectJupyter();

    extensions = new xAPI_Context_ProjectJupyter_Extensions();

    constructor() {
        super("projectJupyter");
    }
}