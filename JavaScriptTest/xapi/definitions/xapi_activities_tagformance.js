import { xAPI_Activity } from '../core/xapi_activity.js';
import { xAPI_Activities } from '../core/xapi_activities.js';

/**
 * Provides the xAPI_Activities of the context tagformance as public properties.
 */
export class xAPI_Activities_Tagformance extends xAPI_Activities {
    /**
     * A graph that is generated after clicking on Start Sweep button. This can be interactible.
     * @var {xAPI_Activity} graph
     */
    graph = new xAPI_Activity(
        "tagformance",
        "graph",
        {
            "en-US": "graph"
        },
        {
            "en-US": "A graph that is generated after clicking on Start Sweep button. This can be interactible."
        });

    /**
     * A box which can be checked to select an already generated graph. Then it can be deleted using the cross button.
     * @var {xAPI_Activity} checkbox
     */
    checkbox = new xAPI_Activity(
        "tagformance",
        "checkbox",
        {
            "en-US": "checkBox"
        },
        {
            "en-US": "A box which can be checked to select an already generated graph. Then it can be deleted using the cross button."
        });

    /**
     * A textbox, where the actor can fill text.
     * @var {xAPI_Activity} textBox
     */
    textBox = new xAPI_Activity(
        "tagformance",
        "textBox",
        {
            "en-US": "commentBox"
        },
        {
            "en-US": "A textbox, where the actor can fill text."
        });

    /**
     * A mouse used to click on buttons or drag items in the lab.
     * @var {xAPI_Activity} mouse
     */
    mouse = new xAPI_Activity(
        "tagformance",
        "mouse",
        {
            "en-US": "mouse"
        },
        {
            "en-US": "A mouse used to click on buttons or drag items in the lab."
        });

    /**
     * An actor selects an option through a dropdown menu.
     * @var {xAPI_Activity} dropdown
     */
    dropdown = new xAPI_Activity(
        "tagformance",
        "dropdown",
        {
            "en-US": "dropdown"
        },
        {
            "en-US": "An actor selects an option through a dropdown menu."
        });

    /**
     * A button the actor can interact with.
     * @var {xAPI_Activity} button
     */
    button = new xAPI_Activity(
        "tagformance",
        "button",
        {
            "en-US": "button"
        },
        {
            "en-US": "A button the actor can interact with."
        });

    /**
     * A scientific procedure, especially in a laboratory, to determine something.
     * @var {xAPI_Activity} experiment
     */
    experiment = new xAPI_Activity(
        "tagformance",
        "experiment",
        {
            "en-US": "experiment"
        },
        {
            "en-US": "A scientific procedure, especially in a laboratory, to determine something."
        });

    /**
     * A keyboard used to input text or interact with the lab and enter values as per the actor's wish.
     * @var {xAPI_Activity} keyboard
     */
    keyboard = new xAPI_Activity(
        "tagformance",
        "keyboard",
        {
            "en-US": "keyboard"
        },
        {
            "en-US": "A keyboard used to input text or interact with the lab and enter values as per the actor's wish."
        });

    constructor() {
        super("tagformance");
    }
}