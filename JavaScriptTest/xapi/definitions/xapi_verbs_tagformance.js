import { xAPI_Verb } from '../core/xapi_verb.js';
import { xAPI_Verbs } from '../core/xapi_verbs.js';

/**
 * Provides the xAPI_Verbs of the context tagformance as public properties.
 */
export class xAPI_Verbs_Tagformance extends xAPI_Verbs {
    /**
     * An actor logged in to the portal.
     * @var {xAPI_Verb} loggedIn
     */
    loggedIn = new xAPI_Verb(
        "tagformance",
        "loggedIn",
        {
            "en-US": "Log In"
        },
        {
            "en-US": "An actor logged in to the portal."
        });

    /**
     * An actor clicked on something on the screen.
     * @var {xAPI_Verb} clicked
     */
    clicked = new xAPI_Verb(
        "tagformance",
        "clicked",
        {
            "en-US": "clicked"
        },
        {
            "en-US": "An actor clicked on something on the screen."
        });

    /**
     * An actor changed a value of an ui element.
     * @var {xAPI_Verb} changed
     */
    changed = new xAPI_Verb(
        "tagformance",
        "changed",
        {
            "en-US": "selected"
        },
        {
            "en-US": "An actor changed a value of an ui element."
        });

    /**
     * An actor has started the measurements.
     * @var {xAPI_Verb} started
     */
    started = new xAPI_Verb(
        "tagformance",
        "started",
        {
            "en-US": "started"
        },
        {
            "en-US": "An actor has started the measurements."
        });

    /**
     * An actor has saved a digital artefact from the software.
     * @var {xAPI_Verb} saved
     */
    saved = new xAPI_Verb(
        "tagformance",
        "saved",
        {
            "en-US": "saved"
        },
        {
            "en-US": "An actor has saved a digital artefact from the software."
        });

    /**
     * An actor has exported a digital artefact from the software.
     * @var {xAPI_Verb} exported
     */
    exported = new xAPI_Verb(
        "tagformance",
        "exported",
        {
            "en-US": "exported"
        },
        {
            "en-US": "An actor has exported a digital artefact from the software."
        });

    /**
     * An actor has written down the his/her thought.
     * @var {xAPI_Verb} commented
     */
    commented = new xAPI_Verb(
        "tagformance",
        "commented",
        {
            "en-US": "commented"
        },
        {
            "en-US": "An actor has written down the his/her thought."
        });

    /**
     * An actor zoomed into the visual element like the graph generated.
     * @var {xAPI_Verb} zoomedIn
     */
    zoomedIn = new xAPI_Verb(
        "tagformance",
        "zoomedIn",
        {
            "en-US": "zoomed in"
        },
        {
            "en-US": "An actor zoomed into the visual element like the graph generated."
        });

    /**
     * An actor has generated a digital artefact from the software.
     * @var {xAPI_Verb} generated
     */
    generated = new xAPI_Verb(
        "tagformance",
        "generated",
        {
            "en-US": "generated"
        },
        {
            "en-US": "An actor has generated a digital artefact from the software."
        });

    /**
     * An actor logged out of the portal.
     * @var {xAPI_Verb} loggedOut
     */
    loggedOut = new xAPI_Verb(
        "tagformance",
        "loggedOut",
        {
            "en-US": "Log Out"
        },
        {
            "en-US": "An actor logged out of the portal."
        });

    /**
     * An actor has deleted a digital artefact from the software.
     * @var {xAPI_Verb} deleted
     */
    deleted = new xAPI_Verb(
        "tagformance",
        "deleted",
        {
            "en-US": "deleted"
        },
        {
            "en-US": "An actor has deleted a digital artefact from the software."
        });

    /**
     * An actor zoomed out of a visual element like the graph generated.
     * @var {xAPI_Verb} zoomedOut
     */
    zoomedOut = new xAPI_Verb(
        "tagformance",
        "zoomedOut",
        {
            "en-US": "zoomed out"
        },
        {
            "en-US": "An actor zoomed out of a visual element like the graph generated."
        });

    constructor() {
        super("tagformance");
    }
}