import { xAPI_Extensions_Result_Generic } from './xapi_extensions_result_generic.js';
import { xAPI_Extensions_Activity_Generic } from './xapi_extensions_activity_generic.js';

/**
 * Provides the extensions of the context generic as public properties.
 */
export class xAPI_Context_Generic_Extensions {

    constructor() {
    }

    get result() {
        return new xAPI_Extensions_Result_Generic();
    }

    get activity() {
        return new xAPI_Extensions_Activity_Generic();
    }
}