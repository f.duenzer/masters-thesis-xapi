import { xAPI_Extensions_Result } from './xapi_extensions_result.js';
import { xAPI_Extension } from '../core/xapi_extension.js';

/**
 * Provides all extensions of the context generic of type result as public properties.
 */
export class xAPI_Extensions_Result_Generic extends xAPI_Extensions_Result {

    constructor() {
        super("generic");
    }

    value(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "value",
                {
                    "en-US": "value",
                    "de-DE": "Wert"
                },
                {
                    "en-US": "It is a value of an element.",
                    "de-DE": "Das ist eine Wert eines Elements."
                }),
            value);
        return this;
    }

    timeSpan(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "timeSpan",
                {
                    "en-US": "Time Span",
                    "de-DE": "Zeitspanne"
                },
                {
                    "en-US": "A time span depending on context.",
                    "de-DE": "Eine bestimmte Zeitspanne abhängig vom Kontext."
                }),
            value);
        return this;
    }

    index(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "index",
                {
                    "en-US": "index",
                    "de-DE": "Index"
                },
                {
                    "en-US": "It is a number to indicies something. Can only be an integer.",
                    "de-DE": "Das ist eine Zahl für das Indexieren von etwas. Kann nur ein Integer sein."
                }),
            value);
        return this;
    }

    startValue(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "startValue",
                {
                    "en-US": "Start Value",
                    "de-DE": "Startwert"
                },
                {
                    "en-US": "Representation of a object directly before the statement occurred. 'Start Value' should be used with the 'End Value' extension to provide information about the actual change of the object. Can be used with 'edited' or 'changed' verbs.",
                    "de-DE": "Repräsentation von einem Objekt bevor das Statement auftrat. 'Startwert' sollte mit der Extension 'Endwert' benutzt werden um Information über die Änderung eines Objektes darzulegen. Kann zum Beispiel mit den Verben 'editierte' und 'veränderte' benutzt werden."
                }),
            value);
        return this;
    }

    endValue(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "endValue",
                {
                    "en-US": "End Value",
                    "de-DE": "Endwert"
                },
                {
                    "en-US": "Representation of a object directly after the statement occurred. 'End Value' should be used with the 'Start Value' extension to provide information about the actual change of the object. Can be used with 'edited' or 'changed' verbs.",
                    "de-DE": "Repräsentation von einem Objekt direkt nachdem das Statement auftrat. 'Endwert' sollte mit der Extension 'Startwert' benutzt werden um Information über die Änderung eines Objektes darzulegen. Kann zum Beispiel mit den Verben 'editierte' und 'veränderte' benutzt werden."
                }),
            value);
        return this;
    }
}