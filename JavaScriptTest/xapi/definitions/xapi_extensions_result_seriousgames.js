import { xAPI_Extensions_Result } from './xapi_extensions_result.js';
import { xAPI_Extension } from '../core/xapi_extension.js';

/**
 * Provides all extensions of the context seriousgames of type result as public properties.
 */
export class xAPI_Extensions_Result_Seriousgames extends xAPI_Extensions_Result {

    constructor() {
        super("seriousgames");
    }

    health(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "health",
                {
                    "en-US": "health",
                    "de-DE": "Leben"
                },
                {
                    "en-US": "The current health of a player character. Has to be an integer.",
                    "de-DE": "Das momentane Leben eines Spielers. Muss ein Integer sein."
                }),
            value);
        return this;
    }

    numberCommissionErrors(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "numberCommissionErrors",
                {
                    "en-US": "number commission-errors",
                    "de-DE": "Anzahl Commissionsfehler"
                },
                {
                    "en-US": "The number of instances in which the player marked an incorrect solution as correct.",
                    "de-DE": "Die Anzahl der Male, in denen der Spielende es eine falsche Lösung als richtig markiert hat."
                }),
            value);
        return this;
    }

    level(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "level",
                {
                    "en-US": "level",
                    "de-DE": "Level"
                },
                {
                    "en-US": "The result of a level change. Has to be a string or integer.",
                    "de-DE": "Das neue Level welches das Ergebnis des Ereignisses ist."
                }),
            value);
        return this;
    }

    numberErrors(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "numberErrors",
                {
                    "en-US": "number errors",
                    "de-DE": "Anzahl Fehler"
                },
                {
                    "en-US": "The number of Errors that have been made in a gamemode or level.",
                    "de-DE": "Die Anzahl der Fehler, die in einem Spielmodus oder Level gemacht wurden."
                }),
            value);
        return this;
    }

    score(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "score",
                {
                    "en-US": "score",
                    "de-DE": "Ergebnis"
                },
                {
                    "en-US": "Some relevant score which is tracked by the game. Can be an integer or double.",
                    "de-DE": "Ein relevantes Ergebnis welches während des Spiels augezeichnet wird. Kann ein Integer oder Double sein."
                }),
            value);
        return this;
    }

    numberCompleted(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "numberCompleted",
                {
                    "en-US": "number completed",
                    "de-DE": "Anzahl beendet"
                },
                {
                    "en-US": "The number attempts that have been completed, either by winning or loosing.",
                    "de-DE": "Die Anzahl der Versuche, die beendet wurden, sowohl durch Gewinnen als auch Verlieren."
                }),
            value);
        return this;
    }

    sumPlaytime(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "sumPlaytime",
                {
                    "en-US": "sum playtime",
                    "de-DE": "Summe Spielzeit"
                },
                {
                    "en-US": "The sum of the playtime that has been spent in a gamemode or level.",
                    "de-DE": "Die Summe der Spielzeit, die in einem Spielmodus oder Level verbracht wurde."
                }),
            value);
        return this;
    }

    gamemode(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "gamemode",
                {
                    "en-US": "gamemode",
                    "de-DE": "Spielmodus"
                },
                {
                    "en-US": "The result of a change of the gamemode. Can be a string or integer.",
                    "de-DE": "Der neue Spielmodus der aus dem Ereignis hervorgeht."
                }),
            value);
        return this;
    }

    position(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "position",
                {
                    "en-US": "position",
                    "de-DE": "Position"
                },
                {
                    "en-US": "The position of an object in the game. Can be in X, Y and Z coordinates.",
                    "de-DE": "Die Position eines Objektes im Spiel. Können X-, Y- und Z-Koordinaten sein."
                }),
            value);
        return this;
    }

    numberSucceeded(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "numberSucceeded",
                {
                    "en-US": "number succeeded",
                    "de-DE": "Anzahl geschafft"
                },
                {
                    "en-US": "The number of instances in which the user was able to complete a level successfully.",
                    "de-DE": "Die Anzahl der Male, in denen der Nutzende es geschafft hat, ein Level erfolgreich abzuschließen."
                }),
            value);
        return this;
    }

    progress(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "progress",
                {
                    "en-US": "progress",
                    "de-DE": "Fortschritt"
                },
                {
                    "en-US": "The progess a player or group achieved in the game or level. 1.0 Progress would mean the game or level was completed in every aspect. Has to be a double between 0 and 1.",
                    "de-DE": "Der Fortschritt welchen ein Spieler oder eine Gruppe erreichte in einem Level oder Spiel. 1.0 Fortschritt hieße das Spiel oder level wurde vollständig abgeschlossen. Muss ein double zwischen 0 und 1 sein."
                }),
            value);
        return this;
    }

    numberOmissionErrors(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "numberOmissionErrors",
                {
                    "en-US": "number omission-errors",
                    "de-DE": "Anzahl Omissionsfehler"
                },
                {
                    "en-US": "The number of instances in which the player failed to mark a correct solution as correct.",
                    "de-DE": "Die Anzahl der Male, in denen der Spielende es versäumt hat, eine korrekte Lösung als richtig zu markieren."
                }),
            value);
        return this;
    }

    numberStarted(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "numberStarted",
                {
                    "en-US": "number started",
                    "de-DE": "Anzahl Gestarted"
                },
                {
                    "en-US": "The number of levels that have been started in a gamemode.",
                    "de-DE": "Die Anzahl der Level, die in einem Spielmodus gestartet wurden."
                }),
            value);
        return this;
    }
}