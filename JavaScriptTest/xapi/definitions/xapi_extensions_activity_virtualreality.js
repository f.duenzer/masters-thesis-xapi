import { xAPI_Extensions_Activity } from './xapi_extensions_activity.js';
import { xAPI_Extension } from '../core/xapi_extension.js';

/**
 * Provides all extensions of the context virtualReality of type activity as public properties.
 */
export class xAPI_Extensions_Activity_VirtualReality extends xAPI_Extensions_Activity {

    constructor() {
        super("virtualReality");
    }

    vrObjectName(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "vrObjectName",
                {
                    "en-US": "VR object name",
                    "de-DE": "VR Objekt Name"
                },
                {
                    "en-US": "Name of a VR object. Must be a string.",
                    "de-DE": "Namen vom VR Objekt. Muss ein String sein."
                }),
            value);
        return this;
    }

    triangle(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "triangle",
                {
                    "en-US": "Triangle",
                    "de-DE": "Dreieck"
                },
                {
                    "en-US": "Sequence of vertices with it's index, color and position.",
                    "de-DE": "Sequenz von Knotenpunkten mit dessen Index, Farbe und Position."
                }),
            value);
        return this;
    }

    actionName(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "actionName",
                {
                    "en-US": "action name",
                    "de-DE": "Aktionsname"
                },
                {
                    "en-US": "It is the name of an action.",
                    "de-DE": "Es ist der Name einer Aktion."
                }),
            value);
        return this;
    }
}