import { xAPI_Extensions_Context } from './xapi_extensions_context.js';
import { xAPI_Extension } from '../core/xapi_extension.js';

/**
 * Provides all extensions of the context tagformance of type context as public properties.
 */
export class xAPI_Extensions_Context_Tagformance extends xAPI_Extensions_Context {

    constructor() {
        super("tagformance");
    }

    measurementUnit(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "measurementUnit",
                {
                    "en-US": "measurement unit dropdown"
                },
                {
                    "en-US": "An actor selects a unit in the Y-axis of the graph that is measured."
                }),
            value);
        return this;
    }

    theoreticalReadingRange(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "theoreticalReadingRange",
                {
                    "en-US": "theoretical reading range"
                },
                {
                    "en-US": "The Y-axis unit in the graph for measuring the reading range of transponder."
                }),
            value);
        return this;
    }

    orientationMeasure(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "orientationMeasure",
                {
                    "en-US": "orientation measure"
                },
                {
                    "en-US": "This is an experiment mode as per lab instruction."
                }),
            value);
        return this;
    }

    frequencyStep(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "frequencyStep",
                {
                    "en-US": "frequency step"
                },
                {
                    "en-US": "An actor selects a frequency step for the measurement of the graph."
                }),
            value);
        return this;
    }

    powerStep(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "powerStep",
                {
                    "en-US": "power step"
                },
                {
                    "en-US": "The power step selected for the measurement."
                }),
            value);
        return this;
    }

    energyMeasure(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "energyMeasure",
                {
                    "en-US": "energy measure"
                },
                {
                    "en-US": "Energy Measure is an experiment mode as per the lab instruction."
                }),
            value);
        return this;
    }

    transmittedPower(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "transmittedPower",
                {
                    "en-US": "transmitted power"
                },
                {
                    "en-US": "The Y-axis unit in dBm for measurement."
                }),
            value);
        return this;
    }

    stopFrequency(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "stopFrequency",
                {
                    "en-US": "stop frequency"
                },
                {
                    "en-US": "The selected frequency to stop the measurement."
                }),
            value);
        return this;
    }

    protocol(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "protocol",
                {
                    "en-US": "protocol"
                },
                {
                    "en-US": "The protocol used is ISO 18000-6c Query."
                }),
            value);
        return this;
    }

    selectTag(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "selectTag",
                {
                    "en-US": "select tag dropdown"
                },
                {
                    "en-US": "The RFID tag selected for measurement."
                }),
            value);
        return this;
    }

    startFrequency(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "startFrequency",
                {
                    "en-US": "start frequency"
                },
                {
                    "en-US": "The selected frequency to start the measurement."
                }),
            value);
        return this;
    }
}