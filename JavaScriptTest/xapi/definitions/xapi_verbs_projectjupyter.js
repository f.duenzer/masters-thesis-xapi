import { xAPI_Verb } from '../core/xapi_verb.js';
import { xAPI_Verbs } from '../core/xapi_verbs.js';

/**
 * Provides the xAPI_Verbs of the context projectJupyter as public properties.
 */
export class xAPI_Verbs_ProjectJupyter extends xAPI_Verbs {
    /**
     * Indicates, that an actor run some programming context. Typically a Jupyter Notebook Code Cell.
     * @var {xAPI_Verb} run
     */
    run = new xAPI_Verb(
        "projectJupyter",
        "run",
        {
            "en-US": "run",
            "de-DE": "führte aus"
        },
        {
            "en-US": "Indicates, that an actor run some programming context. Typically a Jupyter Notebook Code Cell.",
            "de-DE": "Signalisiert, dass ein Akteur ein Programmierkonstrukt ausführte. Typischerweise eine Jupyter Notebook Codezelle."
        });

    constructor() {
        super("projectJupyter");
    }
}