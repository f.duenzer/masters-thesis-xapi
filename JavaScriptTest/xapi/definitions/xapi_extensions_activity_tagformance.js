import { xAPI_Extensions_Activity } from './xapi_extensions_activity.js';
import { xAPI_Extension } from '../core/xapi_extension.js';

/**
 * Provides all extensions of the context tagformance of type activity as public properties.
 */
export class xAPI_Extensions_Activity_Tagformance extends xAPI_Extensions_Activity {

    constructor() {
        super("tagformance");
    }

    uiElementValue(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "uiElementValue",
                {
                    "en-US": "ui element value"
                },
                {
                    "en-US": "A value of an ui element (checkbox, dropdown, etc.)."
                }),
            value);
        return this;
    }

    graphName(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "graphName",
                {
                    "en-US": "graphName"
                },
                {
                    "en-US": "An actor has authored the name of the graph or entered the username."
                }),
            value);
        return this;
    }

    mouseButton(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "mouseButton",
                {
                    "en-US": "mouse button"
                },
                {
                    "en-US": "Name of the mouse button the actor has used (e.g. 'right', 'left')."
                }),
            value);
        return this;
    }

    experimentMode(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "experimentMode",
                {
                    "en-US": "experiment mode"
                },
                {
                    "en-US": "An experiment mode can be for example 'Energy Measurement' or 'Orientation Measurement'"
                }),
            value);
        return this;
    }
}