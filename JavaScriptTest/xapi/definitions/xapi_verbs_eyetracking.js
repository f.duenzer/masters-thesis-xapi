import { xAPI_Verb } from '../core/xapi_verb.js';
import { xAPI_Verbs } from '../core/xapi_verbs.js';

/**
 * Provides the xAPI_Verbs of the context eyeTracking as public properties.
 */
export class xAPI_Verbs_EyeTracking extends xAPI_Verbs {
    /**
     * An actor fixated an object with her eyes. A fixation was detected, i.e., the eyes stayed in a small area between 100 and 500 milliseconds without sakkades.
     * @var {xAPI_Verb} fixated
     */
    fixated = new xAPI_Verb(
        "eyeTracking",
        "fixated",
        {
            "en-US": "fixated",
            "de-DE": "fixierte"
        },
        {
            "en-US": "An actor fixated an object with her eyes. A fixation was detected, i.e., the eyes stayed in a small area between 100 and 500 milliseconds without sakkades.",
            "de-DE": "Eine Akteurin fixierte ein Objekt mit ihren Augen. Eine Fixation wurde erkannt, d.h. die Augen haben ohne größere Sprünge zwischen 100 und 500 Millisekunden in einem kleinen Bereich verweilt."
        });

    /**
     * An actor focused an object with her eyes. The term focused describes a fixation, which is longer than a normal fixation with more than 500 milliseconds. Compare fixated
     * @var {xAPI_Verb} focused
     */
    focused = new xAPI_Verb(
        "eyeTracking",
        "focused",
        {
            "en-US": "focused",
            "de-DE": "fokussierte"
        },
        {
            "en-US": "An actor focused an object with her eyes. The term focused describes a fixation, which is longer than a normal fixation with more than 500 milliseconds. Compare fixated",
            "de-DE": "Eine Aktuerin fokussierte ein Objekt mit ihren Augen. Die Fixation hat länger als 500 Millisekunden gedauert (Unterscheidung zur Fixation)."
        });

    constructor() {
        super("eyeTracking");
    }
}