import { xAPI_Context } from '../core/xapi_context.js';
import { xAPI_Verbs_VirtualReality } from './xapi_verbs_virtualreality.js';
import { xAPI_Activities_VirtualReality } from './xapi_activities_virtualreality.js';
import { xAPI_Context_VirtualReality_Extensions } from './xapi_context_virtualreality_extensions.js';

/**
 * Provides the definitions of the context virtualReality as public properties.
 */
export class xAPI_Context_VirtualReality extends xAPI_Context {
    verbs = new xAPI_Verbs_VirtualReality();

    activities = new xAPI_Activities_VirtualReality();

    extensions = new xAPI_Context_VirtualReality_Extensions();

    constructor() {
        super("virtualReality");
    }
}