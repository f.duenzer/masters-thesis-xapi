import { xAPI_Extensions_Activity } from './xapi_extensions_activity.js';
import { xAPI_Extension } from '../core/xapi_extension.js';

/**
 * Provides all extensions of the context gestures of type activity as public properties.
 */
export class xAPI_Extensions_Activity_Gestures extends xAPI_Extensions_Activity {

    constructor() {
        super("gestures");
    }

    hand(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "hand",
                {
                    "en-US": "hand",
                    "de-DE": "Hand"
                },
                {
                    "en-US": "Hand which is currently active.",
                    "de-DE": "Die Hand, die gerade verwendet wird."
                }),
            value);
        return this;
    }
}