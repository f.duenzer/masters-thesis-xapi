import { xAPI_Context } from '../core/xapi_context.js';
import { xAPI_Verbs_Lms } from './xapi_verbs_lms.js';
import { xAPI_Activities_Lms } from './xapi_activities_lms.js';
import { xAPI_Context_Lms_Extensions } from './xapi_context_lms_extensions.js';

/**
 * Provides the definitions of the context lms as public properties.
 */
export class xAPI_Context_Lms extends xAPI_Context {
    verbs = new xAPI_Verbs_Lms();

    activities = new xAPI_Activities_Lms();

    extensions = new xAPI_Context_Lms_Extensions();

    constructor() {
        super("lms");
    }
}