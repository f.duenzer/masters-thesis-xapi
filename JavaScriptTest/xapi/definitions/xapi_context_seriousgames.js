import { xAPI_Context } from '../core/xapi_context.js';
import { xAPI_Verbs_Seriousgames } from './xapi_verbs_seriousgames.js';
import { xAPI_Activities_Seriousgames } from './xapi_activities_seriousgames.js';
import { xAPI_Context_Seriousgames_Extensions } from './xapi_context_seriousgames_extensions.js';

/**
 * Provides the definitions of the context seriousgames as public properties.
 */
export class xAPI_Context_Seriousgames extends xAPI_Context {
    verbs = new xAPI_Verbs_Seriousgames();

    activities = new xAPI_Activities_Seriousgames();

    extensions = new xAPI_Context_Seriousgames_Extensions();

    constructor() {
        super("seriousgames");
    }
}