import { xAPI_Extensions_Result } from './xapi_extensions_result.js';
import { xAPI_Extension } from '../core/xapi_extension.js';

/**
 * Provides all extensions of the context uhfReader of type result as public properties.
 */
export class xAPI_Extensions_Result_UhfReader extends xAPI_Extensions_Result {

    constructor() {
        super("uhfReader");
    }

    transponderState(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "transponderState",
                {
                    "en-US": "transponderState"
                },
                {
                    "en-US": "State of the transpnder to the physical reader."
                }),
            value);
        return this;
    }
}