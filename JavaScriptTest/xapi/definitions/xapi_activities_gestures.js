import { xAPI_Activity } from '../core/xapi_activity.js';
import { xAPI_Activities } from '../core/xapi_activities.js';

/**
 * Provides the xAPI_Activities of the context gestures as public properties.
 */
export class xAPI_Activities_Gestures extends xAPI_Activities {
    /**
     * Actors physical head.
     * @var {xAPI_Activity} head
     */
    head = new xAPI_Activity(
        "gestures",
        "head",
        {
            "en-US": "head",
            "de-DE": "Kopf"
        },
        {
            "en-US": "Actors physical head.",
            "de-DE": "Physikalische Kopf vom Akteur."
        });

    /**
     * Actors physical (left or right) hand.
     * @var {xAPI_Activity} hand
     */
    hand = new xAPI_Activity(
        "gestures",
        "hand",
        {
            "en-US": "hand",
            "de-DE": "Hand"
        },
        {
            "en-US": "Actors physical (left or right) hand.",
            "de-DE": "Physikalische (linke oder rechte) Hand vom Akteur."
        });

    constructor() {
        super("gestures");
    }
}