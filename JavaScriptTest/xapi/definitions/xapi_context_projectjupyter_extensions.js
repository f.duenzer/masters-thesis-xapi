import { xAPI_Extensions_Context_ProjectJupyter } from './xapi_extensions_context_projectjupyter.js';
import { xAPI_Extensions_Result_ProjectJupyter } from './xapi_extensions_result_projectjupyter.js';
import { xAPI_Extensions_Activity_ProjectJupyter } from './xapi_extensions_activity_projectjupyter.js';

/**
 * Provides the extensions of the context projectJupyter as public properties.
 */
export class xAPI_Context_ProjectJupyter_Extensions {

    constructor() {
    }

    get context() {
        return new xAPI_Extensions_Context_ProjectJupyter();
    }

    get result() {
        return new xAPI_Extensions_Result_ProjectJupyter();
    }

    get activity() {
        return new xAPI_Extensions_Activity_ProjectJupyter();
    }
}