import { xAPI_Context } from '../core/xapi_context.js';
import { xAPI_Verbs_Multitouch } from './xapi_verbs_multitouch.js';
import { xAPI_Activities_Multitouch } from './xapi_activities_multitouch.js';
import { xAPI_Context_Multitouch_Extensions } from './xapi_context_multitouch_extensions.js';

/**
 * Provides the definitions of the context multitouch as public properties.
 */
export class xAPI_Context_Multitouch extends xAPI_Context {
    verbs = new xAPI_Verbs_Multitouch();

    activities = new xAPI_Activities_Multitouch();

    extensions = new xAPI_Context_Multitouch_Extensions();

    constructor() {
        super("multitouch");
    }
}