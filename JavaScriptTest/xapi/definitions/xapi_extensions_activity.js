import { xAPI_Extensions } from '../core/xapi_extensions.js';

export class xAPI_Extensions_Activity extends xAPI_Extensions {

    constructor(context = null) {
        super("activity", context);
    }
}