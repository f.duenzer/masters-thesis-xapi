import { xAPI_Verb } from '../core/xapi_verb.js';
import { xAPI_Verbs } from '../core/xapi_verbs.js';

/**
 * Provides the xAPI_Verbs of the context gestures as public properties.
 */
export class xAPI_Verbs_Gestures extends xAPI_Verbs {
    /**
     * An actor has shaked something, for example a part of her body.
     * @var {xAPI_Verb} shaked
     */
    shaked = new xAPI_Verb(
        "gestures",
        "shaked",
        {
            "en-US": "shaked",
            "de-DE": "schüttelte"
        },
        {
            "en-US": "An actor has shaked something, for example a part of her body.",
            "de-DE": "Eine Akteurin schüttelte etwas, zum Beispiel einen Körperteil wie ihren Kopf."
        });

    /**
     * An actor has gestured with part of her body (e.g. hands, feets, ...).
     * @var {xAPI_Verb} gestured
     */
    gestured = new xAPI_Verb(
        "gestures",
        "gestured",
        {
            "en-US": "gestured",
            "de-DE": "gestikulierte"
        },
        {
            "en-US": "An actor has gestured with part of her body (e.g. hands, feets, ...).",
            "de-DE": "Eine Akteurin gestikulierte mit einem ihrer Körperteile (z.B. Hände, Füße, ...)."
        });

    /**
     * An actor has nodded with her head. Could be collected by computation of headset movement or a webcam or by hand annotation.
     * @var {xAPI_Verb} nodded
     */
    nodded = new xAPI_Verb(
        "gestures",
        "nodded",
        {
            "en-US": "nodding",
            "de-DE": "nickte"
        },
        {
            "en-US": "An actor has nodded with her head. Could be collected by computation of headset movement or a webcam or by hand annotation.",
            "de-DE": "Eine Akteurin nickte mit ihrem Kopf. Könnte zum Beispiel erkannt worden sein, indem Headset Daten ausgewertet wurden, eine Erkennung in einem Video geschehen ist oder durch eine händische Annotation."
        });

    /**
     * An actor performed something, e.g. a gesture
     * @var {xAPI_Verb} performed
     */
    performed = new xAPI_Verb(
        "gestures",
        "performed",
        {
            "en-US": "performed",
            "de-DE": "verrichtete"
        },
        {
            "en-US": "An actor performed something, e.g. a gesture",
            "de-DE": "Ein Akteur hat etwas getan, z.B. eine Geste verrichtet."
        });

    /**
     * An actor has gazed a part of her body in a direction.
     * @var {xAPI_Verb} gazed
     */
    gazed = new xAPI_Verb(
        "gestures",
        "gazed",
        {
            "en-US": "gazed",
            "de-DE": "richtete aus"
        },
        {
            "en-US": "An actor has gazed a part of her body in a direction.",
            "de-DE": "Eine Akteurin richtete ihr Körperteil in eine Richtung aus."
        });

    constructor() {
        super("gestures");
    }
}