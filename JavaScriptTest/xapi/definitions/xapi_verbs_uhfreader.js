import { xAPI_Verb } from '../core/xapi_verb.js';
import { xAPI_Verbs } from '../core/xapi_verbs.js';

/**
 * Provides the xAPI_Verbs of the context uhfReader as public properties.
 */
export class xAPI_Verbs_UhfReader extends xAPI_Verbs {
    /**
     * An actor clicked on something on the UHF Reader software.
     * @var {xAPI_Verb} clicked
     */
    clicked = new xAPI_Verb(
        "uhfReader",
        "clicked",
        {
            "en-US": "clicked"
        },
        {
            "en-US": "An actor clicked on something on the UHF Reader software."
        });

    /**
     * An actor started an activity or process.
     * @var {xAPI_Verb} started
     */
    started = new xAPI_Verb(
        "uhfReader",
        "started",
        {
            "en-US": "started"
        },
        {
            "en-US": "An actor started an activity or process."
        });

    /**
     * A user evaluates a target (e.g. transponder).
     * @var {xAPI_Verb} evaluate
     */
    evaluate = new xAPI_Verb(
        "uhfReader",
        "evaluate",
        {
            "en-US": "evaluate"
        },
        {
            "en-US": "A user evaluates a target (e.g. transponder)."
        });

    /**
     * An actor selects an activity mode or experiment mode from menu.
     * @var {xAPI_Verb} selected
     */
    selected = new xAPI_Verb(
        "uhfReader",
        "selected",
        {
            "en-US": "selected"
        },
        {
            "en-US": "An actor selects an activity mode or experiment mode from menu."
        });

    /**
     * An actor copied the TID from the screen. Then the actor can paste it in google to research on the transponder.
     * @var {xAPI_Verb} tidCopied
     */
    tidCopied = new xAPI_Verb(
        "uhfReader",
        "tidCopied",
        {
            "en-US": "tid copied"
        },
        {
            "en-US": "An actor copied the TID from the screen. Then the actor can paste it in google to research on the transponder."
        });

    /**
     * An actor connected an device (e.g. transponder) to the physical reader.
     * @var {xAPI_Verb} connected
     */
    connected = new xAPI_Verb(
        "uhfReader",
        "connected",
        {
            "en-US": "connected"
        },
        {
            "en-US": "An actor connected an device (e.g. transponder) to the physical reader."
        });

    constructor() {
        super("uhfReader");
    }
}