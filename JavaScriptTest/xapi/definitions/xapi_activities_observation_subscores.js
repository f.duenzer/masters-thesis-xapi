import { xAPI_Activity } from '../core/xapi_activity.js';
import { xAPI_Activities } from '../core/xapi_activities.js';

/**
 * Provides the xAPI_Activities of the context observation as public properties.
 */
export class xAPI_Activities_Observation_Subscores extends xAPI_Activities {
    /**
     * Subscore (5/5 bzw. ++) wenn ein Akteuer das (eigene) Vorgehen plant und Schwerpunkte setzt
     * @var {xAPI_Activity} plant_das_eigene_Vorgehen_und_setzt_Schwerpunkte
     */
    plant_das_eigene_Vorgehen_und_setzt_Schwerpunkte = new xAPI_Activity(
        "observation",
        "plant_das_eigene_Vorgehen_und_setzt_Schwerpunkte",
        {
            "de-DE": "plant das (eigene) Vorgehen und setzt Schwerpunkte"
        },
        {
            "de-DE": "Subscore (5/5 bzw. ++) wenn ein Akteuer das (eigene) Vorgehen plant und Schwerpunkte setzt"
        });

    /**
     * Subscore (5/5 bzw. ++) wenn ein Akteuer sich hilfsbereit zeigt und andere aktiv unterstützt
     * @var {xAPI_Activity} zeigt_sich_hilfsbereit_und_unterstuetzt_andere_aktiv
     */
    zeigt_sich_hilfsbereit_und_unterstuetzt_andere_aktiv = new xAPI_Activity(
        "observation",
        "zeigt_sich_hilfsbereit_und_unterstuetzt_andere_aktiv",
        {
            "de-DE": "zeigt sich hilfsbereit und unterstützt andere aktiv"
        },
        {
            "de-DE": "Subscore (5/5 bzw. ++) wenn ein Akteuer sich hilfsbereit zeigt und andere aktiv unterstützt"
        });

    /**
     * Subscore (5/5 bzw. ++) wenn ein Akteuer andere ausreden lässt
     * @var {xAPI_Activity} laesst_andere_ausreden
     */
    laesst_andere_ausreden = new xAPI_Activity(
        "observation",
        "laesst_andere_ausreden",
        {
            "de-DE": "lässt andere ausreden"
        },
        {
            "de-DE": "Subscore (5/5 bzw. ++) wenn ein Akteuer andere ausreden lässt"
        });

    /**
     * Subscore (5/5 bzw. ++) wenn ein Akteuer eigene Meinungen vertritt und sich dabei kompromissbereit zeigt
     * @var {xAPI_Activity} vertritt_eigene_Meinungen_und_zeigt_sich_dabei_kompromissbereit
     */
    vertritt_eigene_Meinungen_und_zeigt_sich_dabei_kompromissbereit = new xAPI_Activity(
        "observation",
        "vertritt_eigene_Meinungen_und_zeigt_sich_dabei_kompromissbereit",
        {
            "de-DE": "vertritt eigene Meinungen und zeigt sich dabei kompromissbereit"
        },
        {
            "de-DE": "Subscore (5/5 bzw. ++) wenn ein Akteuer eigene Meinungen vertritt und sich dabei kompromissbereit zeigt"
        });

    /**
     * Subscore (5/5 bzw. ++) wenn ein AkteuerRücksicht auf eigene Bedürfnisse  nimmt/die eigene Meinung vertritt
     * @var {xAPI_Activity} nimmt_Ruecksicht_auf_eigene_Beduerfnissevertritt_die_eigene_Meinung
     */
    nimmt_Ruecksicht_auf_eigene_Beduerfnissevertritt_die_eigene_Meinung = new xAPI_Activity(
        "observation",
        "nimmt_Ruecksicht_auf_eigene_Beduerfnissevertritt_die_eigene_Meinung",
        {
            "de-DE": "nimmt Rücksicht auf eigene Bedürfnisse/vertritt die eigene Meinung"
        },
        {
            "de-DE": "Subscore (5/5 bzw. ++) wenn ein AkteuerRücksicht auf eigene Bedürfnisse  nimmt/die eigene Meinung vertritt"
        });

    /**
     * Subscore (5/5 bzw. ++) wenn ein Akteuer auch komplexe Sachverhalte ausdrücken und/oder verstehen kann
     * @var {xAPI_Activity} kann_auch_komplexe_Sachverhalte_ausdruecken_undoder_verstehen
     */
    kann_auch_komplexe_Sachverhalte_ausdruecken_undoder_verstehen = new xAPI_Activity(
        "observation",
        "kann_auch_komplexe_Sachverhalte_ausdruecken_undoder_verstehen",
        {
            "de-DE": "kann auch komplexe Sachverhalte ausdrücken und/oder verstehen"
        },
        {
            "de-DE": "Subscore (5/5 bzw. ++) wenn ein Akteuer auch komplexe Sachverhalte ausdrücken und/oder verstehen kann"
        });

    /**
     * Subscore (5/5 bzw. ++) wenn ein Akteuer nonverbal interagiert (zugewandte Körpersprache)
     * @var {xAPI_Activity} interagiert_nonverbal_zugewandte_Koerpersprache
     */
    interagiert_nonverbal_zugewandte_Koerpersprache = new xAPI_Activity(
        "observation",
        "interagiert_nonverbal_zugewandte_Koerpersprache",
        {
            "de-DE": "interagiert nonverbal (zugewandte Körpersprache)"
        },
        {
            "de-DE": "Subscore (5/5 bzw. ++) wenn ein Akteuer nonverbal interagiert (zugewandte Körpersprache)"
        });

    /**
     * Subscore (5/5 bzw. ++) wenn ein Akteuer Möglichkeiten zur Strukturierung einer Aufgabe findet
     * @var {xAPI_Activity} findet_Moeglichkeiten_zur_Strukturierung_einer_Aufgabe
     */
    findet_Moeglichkeiten_zur_Strukturierung_einer_Aufgabe = new xAPI_Activity(
        "observation",
        "findet_Moeglichkeiten_zur_Strukturierung_einer_Aufgabe",
        {
            "de-DE": "findet Möglichkeiten zur Strukturierung einer Aufgabe"
        },
        {
            "de-DE": "Subscore (5/5 bzw. ++) wenn ein Akteuer Möglichkeiten zur Strukturierung einer Aufgabe findet"
        });

    /**
     * Subscore (5/5 bzw. ++) wenn ein Akteuer sich höflich, freundlich und wohlwollend verhält
     * @var {xAPI_Activity} verhaelt_sich_hoeflich_freundlich_und_wohlwollend
     */
    verhaelt_sich_hoeflich_freundlich_und_wohlwollend = new xAPI_Activity(
        "observation",
        "verhaelt_sich_hoeflich_freundlich_und_wohlwollend",
        {
            "de-DE": "verhält sich höflich, freundlich und wohlwollend"
        },
        {
            "de-DE": "Subscore (5/5 bzw. ++) wenn ein Akteuer sich höflich, freundlich und wohlwollend verhält"
        });

    /**
     * Subscore (5/5 bzw. ++) wenn ein Akteuer Intresse, Stolz oder Freude zeigt
     * @var {xAPI_Activity} zeigt_Interesse_Stolz_oder_Freude
     */
    zeigt_Interesse_Stolz_oder_Freude = new xAPI_Activity(
        "observation",
        "zeigt_Interesse_Stolz_oder_Freude",
        {
            "de-DE": "zeigt Interesse, Stolz oder Freude"
        },
        {
            "de-DE": "Subscore (5/5 bzw. ++) wenn ein Akteuer Intresse, Stolz oder Freude zeigt"
        });

    /**
     * Subscore (5/5 bzw. ++) wenn ein Akteuer sich sprachlich an Lösung der Aufgabe beteiligt/Fragen stellt
     * @var {xAPI_Activity} beteiligt_sich_sprachlich_an_Loesung_der_Aufgabestellt_Fragen
     */
    beteiligt_sich_sprachlich_an_Loesung_der_Aufgabestellt_Fragen = new xAPI_Activity(
        "observation",
        "beteiligt_sich_sprachlich_an_Loesung_der_Aufgabestellt_Fragen",
        {
            "de-DE": "beteiligt sich sprachlich an Lösung der Aufgabe/stellt Fragen"
        },
        {
            "de-DE": "Subscore (5/5 bzw. ++) wenn ein Akteuer sich sprachlich an Lösung der Aufgabe beteiligt/Fragen stellt"
        });

    /**
     * Subscore (5/5 bzw. ++) wenn ein Akteuer eigene Gedanken klar und verständlich artikuliert
     * @var {xAPI_Activity} artikuliert_eigene_Gedanken_klar_und_verstaendlich
     */
    artikuliert_eigene_Gedanken_klar_und_verstaendlich = new xAPI_Activity(
        "observation",
        "artikuliert_eigene_Gedanken_klar_und_verstaendlich",
        {
            "de-DE": "artikuliert eigene Gedanken klar und verständlich"
        },
        {
            "de-DE": "Subscore (5/5 bzw. ++) wenn ein Akteuer eigene Gedanken klar und verständlich artikuliert"
        });

    /**
     * Subscore (5/5 bzw. ++) wenn ein Akteuer Pläne/Skizzen lesen, verstehen und umsetzen kann
     * @var {xAPI_Activity} kann_PlaeneSkizzen_lesen_verstehen_und_umsetzen
     */
    kann_PlaeneSkizzen_lesen_verstehen_und_umsetzen = new xAPI_Activity(
        "observation",
        "kann_PlaeneSkizzen_lesen_verstehen_und_umsetzen",
        {
            "de-DE": "kann Pläne/Skizzen lesen, verstehen und umsetzen"
        },
        {
            "de-DE": "Subscore (5/5 bzw. ++) wenn ein Akteuer Pläne/Skizzen lesen, verstehen und umsetzen kann"
        });

    /**
     * Subscore (5/5 bzw. ++) wenn ein Akteuer auch schwierige feinmotorische Handlungen ausführen kann
     * @var {xAPI_Activity} kann_auch_schwierige_feinmotorische_Handlungen_ausfuehren
     */
    kann_auch_schwierige_feinmotorische_Handlungen_ausfuehren = new xAPI_Activity(
        "observation",
        "kann_auch_schwierige_feinmotorische_Handlungen_ausfuehren",
        {
            "de-DE": "kann auch schwierige feinmotorische Handlungen ausführen"
        },
        {
            "de-DE": "Subscore (5/5 bzw. ++) wenn ein Akteuer auch schwierige feinmotorische Handlungen ausführen kann"
        });

    /**
     * Subscore (5/5 bzw. ++) wenn ein Akteuer verschiedne Lösungsansätze verfolgt
     * @var {xAPI_Activity} verfolgt_verschiedene_Loesungsansaetze
     */
    verfolgt_verschiedene_Loesungsansaetze = new xAPI_Activity(
        "observation",
        "verfolgt_verschiedene_Loesungsansaetze",
        {
            "de-DE": "verfolgt verschiedene Lösungsansätze"
        },
        {
            "de-DE": "Subscore (5/5 bzw. ++) wenn ein Akteuer verschiedne Lösungsansätze verfolgt"
        });

    /**
     * Subscore (5/5 bzw. ++) wenn ein Akteuer respektvoll und tolerant mit Meinungsverschiedenheiten umgeht
     * @var {xAPI_Activity} geht_respektvoll_und_tolerant_mit_Meinungsverschiedenheiten_um
     */
    geht_respektvoll_und_tolerant_mit_Meinungsverschiedenheiten_um = new xAPI_Activity(
        "observation",
        "geht_respektvoll_und_tolerant_mit_Meinungsverschiedenheiten_um",
        {
            "de-DE": "geht respektvoll und tolerant mit Meinungsverschiedenheiten um"
        },
        {
            "de-DE": "Subscore (5/5 bzw. ++) wenn ein Akteuer respektvoll und tolerant mit Meinungsverschiedenheiten umgeht"
        });

    /**
     * Subscore (5/5 bzw. ++) wenn ein Akteuer auf andere achtet/Rücksicht nimmt
     * @var {xAPI_Activity} achtet_auf_anderenimmt_Ruecksicht
     */
    achtet_auf_anderenimmt_Ruecksicht = new xAPI_Activity(
        "observation",
        "achtet_auf_anderenimmt_Ruecksicht",
        {
            "de-DE": "achtet auf anderenimmt Rücksicht"
        },
        {
            "de-DE": "Subscore (5/5 bzw. ++) wenn ein Akteuer auf andere achtet/Rücksicht nimmt"
        });

    /**
     * Subscore (5/5 bzw. ++) wenn ein Akteuer Maßstäbe im richtigen Verhältnis umsetzen kann (z.B. in Skizzen)
     * @var {xAPI_Activity} kann_Massstaebe_im_richtigen_Verhaeltnis_umsetzen_zB_in_Skizzen
     */
    kann_Massstaebe_im_richtigen_Verhaeltnis_umsetzen_zB_in_Skizzen = new xAPI_Activity(
        "observation",
        "kann_Massstaebe_im_richtigen_Verhaeltnis_umsetzen_zB_in_Skizzen",
        {
            "de-DE": "kann Maßstäbe im richtigen Verhältnis umsetzen (z.B. in Skizzen)"
        },
        {
            "de-DE": "Subscore (5/5 bzw. ++) wenn ein Akteuer Maßstäbe im richtigen Verhältnis umsetzen kann (z.B. in Skizzen)"
        });

    /**
     * Subscore (5/5 bzw. ++) wenn ein Akteuer aktiv Interesse an anderen Menschen und ihren Ansichten/Ideen zeigt
     * @var {xAPI_Activity} zeigt_aktiv_Interesse_an_anderen_Menschen_und_ihren_AnsichtenIdeen
     */
    zeigt_aktiv_Interesse_an_anderen_Menschen_und_ihren_AnsichtenIdeen = new xAPI_Activity(
        "observation",
        "zeigt_aktiv_Interesse_an_anderen_Menschen_und_ihren_AnsichtenIdeen",
        {
            "de-DE": "zeigt aktiv Interesse an anderen Menschen und ihren Ansichten/Ideen"
        },
        {
            "de-DE": "Subscore (5/5 bzw. ++) wenn ein Akteuer aktiv Interesse an anderen Menschen und ihren Ansichten/Ideen zeigt"
        });

    /**
     * Subscore (5/5 bzw. ++) wenn ein Akteuer immer wieder von sich aus aktiv wird
     * @var {xAPI_Activity} wird_immer_wieder_von_sich_aus_aktiv
     */
    wird_immer_wieder_von_sich_aus_aktiv = new xAPI_Activity(
        "observation",
        "wird_immer_wieder_von_sich_aus_aktiv",
        {
            "de-DE": "wird immer wieder von sich aus aktiv"
        },
        {
            "de-DE": "Subscore (5/5 bzw. ++) wenn ein Akteuer immer wieder von sich aus aktiv wird"
        });

    /**
     * Subscore (5/5 bzw. ++) wenn ein Akteuer geschriebene/bebilderte Informationen (z.B. Texte, Anweisungen) versteht
     * @var {xAPI_Activity} versteht_geschriebenebebilderte_Informationen_zB_Texte_Anweisungen
     */
    versteht_geschriebenebebilderte_Informationen_zB_Texte_Anweisungen = new xAPI_Activity(
        "observation",
        "versteht_geschriebenebebilderte_Informationen_zB_Texte_Anweisungen",
        {
            "de-DE": "versteht geschriebene/bebilderte Informationen (z.B. Texte, Anweisungen)"
        },
        {
            "de-DE": "Subscore (5/5 bzw. ++) wenn ein Akteuer geschriebene/bebilderte Informationen (z.B. Texte, Anweisungen) versteht"
        });

    /**
     * Subscore (5/5 bzw. ++) wenn ein Akteuer Ideen visualisiert oder umschreibt
     * @var {xAPI_Activity} visualisiert_oder_umschreibt_Ideen
     */
    visualisiert_oder_umschreibt_Ideen = new xAPI_Activity(
        "observation",
        "visualisiert_oder_umschreibt_Ideen",
        {
            "de-DE": "visualisiert oder umschreibt Ideen"
        },
        {
            "de-DE": "Subscore (5/5 bzw. ++) wenn ein Akteuer Ideen visualisiert oder umschreibt"
        });

    /**
     * Subscore (5/5 bzw. ++) wenn ein Akteuer Entfernungen/Höhen/Tiefen einschätzen kann (im Raum/von Gegenständen)
     * @var {xAPI_Activity} kann_EntfernungenHoehenTiefen_einschaetzen_im_Raumvon_Gegenstaenden
     */
    kann_EntfernungenHoehenTiefen_einschaetzen_im_Raumvon_Gegenstaenden = new xAPI_Activity(
        "observation",
        "kann_EntfernungenHoehenTiefen_einschaetzen_im_Raumvon_Gegenstaenden",
        {
            "de-DE": "kann Entfernungen/Höhen/Tiefen einschätzen (im Raum/von Gegenständen)"
        },
        {
            "de-DE": "Subscore (5/5 bzw. ++) wenn ein Akteuer Entfernungen/Höhen/Tiefen einschätzen kann (im Raum/von Gegenständen)"
        });

    /**
     * Subscore (5/5 bzw. ++) wenn ein Akteuer eigene Bedürfnisse hinten anstellt, wenn es die Situation erfordert
     * @var {xAPI_Activity} stellt_eigene_Beduerfnisse_hinten_an_wenn_es_die_Situation_erfordert
     */
    stellt_eigene_Beduerfnisse_hinten_an_wenn_es_die_Situation_erfordert = new xAPI_Activity(
        "observation",
        "stellt_eigene_Beduerfnisse_hinten_an_wenn_es_die_Situation_erfordert",
        {
            "de-DE": "stellt eigene Bedürfnisse hinten an, wenn es die Situation erfordert"
        },
        {
            "de-DE": "Subscore (5/5 bzw. ++) wenn ein Akteuer eigene Bedürfnisse hinten anstellt, wenn es die Situation erfordert"
        });

    /**
     * Subscore (5/5 bzw. ++) wenn ein Akteuer geeignete Mittel einsetzt/geeignete Handlungen ausführt, um das Ziel zu erreichen
     * @var {xAPI_Activity} setzt_geeignete_Mittel_einfuehrt_geeignete_Handlungen_aus_um_das_Ziel_zu_erreichen
     */
    setzt_geeignete_Mittel_einfuehrt_geeignete_Handlungen_aus_um_das_Ziel_zu_erreichen = new xAPI_Activity(
        "observation",
        "setzt_geeignete_Mittel_einfuehrt_geeignete_Handlungen_aus_um_das_Ziel_zu_erreichen",
        {
            "de-DE": "setzt geeignete Mittel ein/führt geeignete Handlungen aus, um das Ziel zu erreichen"
        },
        {
            "de-DE": "Subscore (5/5 bzw. ++) wenn ein Akteuer geeignete Mittel einsetzt/geeignete Handlungen ausführt, um das Ziel zu erreichen"
        });

    /**
     * Subscore (5/5 bzw. ++) wenn ein Akteuer den Zielzustand stets im Blick behält
     * @var {xAPI_Activity} behaelt_den_Zielzustand_stets_im_Blick
     */
    behaelt_den_Zielzustand_stets_im_Blick = new xAPI_Activity(
        "observation",
        "behaelt_den_Zielzustand_stets_im_Blick",
        {
            "de-DE": "behält den Zielzustand stets im Blick"
        },
        {
            "de-DE": "Subscore (5/5 bzw. ++) wenn ein Akteuer den Zielzustand stets im Blick behält"
        });

    /**
     * Subscore (5/5 bzw. ++) wenn ein Akteuer erarbeitet ein klares Bild der Ausgangssituation/Problemstellung
     * @var {xAPI_Activity} erarbeitet_ein_klares_Bild_der_AusgangssituationProblemstellung
     */
    erarbeitet_ein_klares_Bild_der_AusgangssituationProblemstellung = new xAPI_Activity(
        "observation",
        "erarbeitet_ein_klares_Bild_der_AusgangssituationProblemstellung",
        {
            "de-DE": "erarbeitet ein klares Bild der Ausgangssituation/Problemstellung"
        },
        {
            "de-DE": "Subscore (5/5 bzw. ++) wenn ein Akteuer erarbeitet ein klares Bild der Ausgangssituation/Problemstellung"
        });

    /**
     * Subscore (5/5 bzw. ++) wenn ein Akteuer eigene Ideen entwickelt
     * @var {xAPI_Activity} entwickelt_eigene_Ideen
     */
    entwickelt_eigene_Ideen = new xAPI_Activity(
        "observation",
        "entwickelt_eigene_Ideen",
        {
            "de-DE": "entwickelt eigene Ideen"
        },
        {
            "de-DE": "Subscore (5/5 bzw. ++) wenn ein Akteuer eigene Ideen entwickelt"
        });

    /**
     * Subscore (5/5 bzw. ++) wenn ein Akteuer Bewegungen präzisegenau ausführt
     * @var {xAPI_Activity} fuehrt_Bewegungen_praezisegenau_aus
     */
    fuehrt_Bewegungen_praezisegenau_aus = new xAPI_Activity(
        "observation",
        "fuehrt_Bewegungen_praezisegenau_aus",
        {
            "de-DE": "führt Bewegungen präzise/genau aus"
        },
        {
            "de-DE": "Subscore (5/5 bzw. ++) wenn ein Akteuer Bewegungen präzisegenau ausführt"
        });

    /**
     * Subscore (5/5 bzw. ++) wenn ein Akteuer Bewegungen schnell/routiniert ausübt
     * @var {xAPI_Activity} uebt_Bewegungen_schnellroutiniert_aus
     */
    uebt_Bewegungen_schnellroutiniert_aus = new xAPI_Activity(
        "observation",
        "uebt_Bewegungen_schnellroutiniert_aus",
        {
            "de-DE": "übt Bewegungen schnell/routiniert aus"
        },
        {
            "de-DE": "Subscore (5/5 bzw. ++) wenn ein Akteuer Bewegungen schnell/routiniert ausübt"
        });

    /**
     * Subscore (5/5 bzw. ++) wenn ein Akteuer Aufgaben zügig angeht
     * @var {xAPI_Activity} geht_Aufgaben_zuegig_an
     */
    geht_Aufgaben_zuegig_an = new xAPI_Activity(
        "observation",
        "geht_Aufgaben_zuegig_an",
        {
            "de-DE": "geht Aufgaben zügig an"
        },
        {
            "de-DE": "Subscore (5/5 bzw. ++) wenn ein Akteuer Aufgaben zügig angeht"
        });

    /**
     * Subscore (5/5 bzw. ++) wenn ein Akteuer stehts den Überblick behält
     * @var {xAPI_Activity} behaelt_stehts_den_Ueberblick
     */
    behaelt_stehts_den_Ueberblick = new xAPI_Activity(
        "observation",
        "behaelt_stehts_den_Ueberblick",
        {
            "de-DE": "behält stehts den Überblick"
        },
        {
            "de-DE": "Subscore (5/5 bzw. ++) wenn ein Akteuer stehts den Überblick behält"
        });

    /**
     * Subscore (5/5 bzw. ++) wenn ein Akteuer in schwierigen/unangenehmen Situationen gelassen und ruhig bleibt
     * @var {xAPI_Activity} bleibt_in_schwierigenunangenehmen_Situationen_gelassen_und_ruhig
     */
    bleibt_in_schwierigenunangenehmen_Situationen_gelassen_und_ruhig = new xAPI_Activity(
        "observation",
        "bleibt_in_schwierigenunangenehmen_Situationen_gelassen_und_ruhig",
        {
            "de-DE": "bleibt in schwierigen/unangenehmen Situationen gelassen und ruhig"
        },
        {
            "de-DE": "Subscore (5/5 bzw. ++) wenn ein Akteuer in schwierigen/unangenehmen Situationen gelassen und ruhig bleibt"
        });

    constructor() {
        super("observation");
    }
}