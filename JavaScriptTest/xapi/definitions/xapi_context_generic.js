import { xAPI_Context } from '../core/xapi_context.js';
import { xAPI_Verbs_Generic } from './xapi_verbs_generic.js';
import { xAPI_Activities_Generic } from './xapi_activities_generic.js';
import { xAPI_Context_Generic_Extensions } from './xapi_context_generic_extensions.js';

/**
 * Provides the definitions of the context generic as public properties.
 */
export class xAPI_Context_Generic extends xAPI_Context {
    verbs = new xAPI_Verbs_Generic();

    activities = new xAPI_Activities_Generic();

    extensions = new xAPI_Context_Generic_Extensions();

    constructor() {
        super("generic");
    }
}