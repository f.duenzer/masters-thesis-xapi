import { xAPI_Verb } from '../core/xapi_verb.js';
import { xAPI_Verbs } from '../core/xapi_verbs.js';

/**
 * Provides the xAPI_Verbs of the context generic as public properties.
 */
export class xAPI_Verbs_Generic extends xAPI_Verbs {
    /**
     * An actor answered some question.
     * @var {xAPI_Verb} answered
     */
    answered = new xAPI_Verb(
        "generic",
        "answered",
        {
            "en-US": "answered",
            "de-DE": "beantwortete"
        },
        {
            "en-US": "An actor answered some question.",
            "de-DE": "Ein Akteur beantwortete eine Frage."
        });

    /**
     * An actor has clicked a button.
     * @var {xAPI_Verb} clicked
     */
    clicked = new xAPI_Verb(
        "generic",
        "clicked",
        {
            "en-US": "clicked",
            "de-DE": "klickte"
        },
        {
            "en-US": "An actor has clicked a button.",
            "de-DE": "Der Akteur klickte einen Knopf."
        });

    /**
     * An actor changed something. This could be the level or some settings.
     * @var {xAPI_Verb} changed
     */
    changed = new xAPI_Verb(
        "generic",
        "changed",
        {
            "en-US": "changed",
            "de-DE": "veränderte"
        },
        {
            "en-US": "An actor changed something. This could be the level or some settings.",
            "de-DE": "Ein Akteur veränderte etwas. Dies könnte das Level sein oder die Einstellungen."
        });

    /**
     * A actor crossed out some text or object.
     * @var {xAPI_Verb} crossedOut
     */
    crossedOut = new xAPI_Verb(
        "generic",
        "crossedOut",
        {
            "en-US": "crossedOut",
            "de-DE": "streichte durch"
        },
        {
            "en-US": "A actor crossed out some text or object.",
            "de-DE": "Ein Acteur streichte einen Text oder ein Objekt durch."
        });

    /**
     * An actor deselected a option.
     * @var {xAPI_Verb} deselected
     */
    deselected = new xAPI_Verb(
        "generic",
        "deselected",
        {
            "en-US": "deselected",
            "de-DE": "nahm Auswahl zurück"
        },
        {
            "en-US": "An actor deselected a option.",
            "de-DE": "Ein Akteur nahm die Auswahl einer Option zurück."
        });

    /**
     * An actor collected some item.
     * @var {xAPI_Verb} collected
     */
    collected = new xAPI_Verb(
        "generic",
        "collected",
        {
            "en-US": "collected",
            "de-DE": "sammelte"
        },
        {
            "en-US": "An actor collected some item.",
            "de-DE": "Ein Akteur sammelte einen Gegenstand ein."
        });

    /**
     * An actor drew something.
     * @var {xAPI_Verb} drew
     */
    drew = new xAPI_Verb(
        "generic",
        "drew",
        {
            "en-US": "drew",
            "de-DE": "malte"
        },
        {
            "en-US": "An actor drew something.",
            "de-DE": "Ein Akteur malte etwas."
        });

    /**
     * An actor received for example an item or help.
     * @var {xAPI_Verb} received
     */
    received = new xAPI_Verb(
        "generic",
        "received",
        {
            "en-US": "received",
            "de-DE": "erhielt"
        },
        {
            "en-US": "An actor received for example an item or help.",
            "de-DE": "Ein Akteur erhielt zum Beispiel einen Gegenstand oder Hilfe."
        });

    /**
     * Refers to the copy-and-paste action performed by an actor. E.g. An actor pasted a text into a textbox
     * @var {xAPI_Verb} pasted
     */
    pasted = new xAPI_Verb(
        "generic",
        "pasted",
        {
            "en-US": "pasted",
            "de-DE": "eingefügt"
        },
        {
            "en-US": "Refers to the copy-and-paste action performed by an actor. E.g. An actor pasted a text into a textbox",
            "de-DE": "Dieses Verb bezieht sich auf die kopier-und-einfüge Operation eines Akteurs. Z.B. ein Akteur fügte den (vorher kopierten) Text in ein Textfeld ein."
        });

    /**
     * An actor played a game or something specific in a game.
     * @var {xAPI_Verb} played
     */
    played = new xAPI_Verb(
        "generic",
        "played",
        {
            "en-US": "played",
            "de-DE": "spielte"
        },
        {
            "en-US": "An actor played a game or something specific in a game.",
            "de-DE": "Ein Akteur spielte ein Spiel oder etwas spezifisches in einem Spiel."
        });

    /**
     * A actor dropped some draggable into a specific area.
     * @var {xAPI_Verb} dropped
     */
    dropped = new xAPI_Verb(
        "generic",
        "dropped",
        {
            "en-US": "dropped",
            "de-DE": "ließ fallen"
        },
        {
            "en-US": "A actor dropped some draggable into a specific area.",
            "de-DE": "Ein Acteur ließ einen ziehbaren Gegenstand in ein spezielles Feld fallen."
        });

    /**
     * Actor accessed/viewed a page, file, video, ...
     * @var {xAPI_Verb} accessed
     */
    accessed = new xAPI_Verb(
        "generic",
        "accessed",
        {
            "en-US": "accessed",
            "de-DE": "zugegriffen"
        },
        {
            "en-US": "Actor accessed/viewed a page, file, video, ...",
            "de-DE": "Akteur hat (auf) eine Seite, eine Datei, ein Video, ... zugegriffen/angesehen"
        });

    /**
     * A user or group used some local device to uploaded data onto a different distant disk.
     * @var {xAPI_Verb} uploaded
     */
    uploaded = new xAPI_Verb(
        "generic",
        "uploaded",
        {
            "en-US": "uploaded",
            "de-DE": "lud hoch"
        },
        {
            "en-US": "A user or group used some local device to uploaded data onto a different distant disk.",
            "de-DE": "Ein Benutzer oder eine Gruppe nutze ein lokales Gerät um daten auf ein entferntes Speichermedium zu laden."
        });

    /**
     * An actor shared an object with another actor or a group of actors.
     * @var {xAPI_Verb} shared
     */
    shared = new xAPI_Verb(
        "generic",
        "shared",
        {
            "en-US": "shared",
            "de-DE": "teilte"
        },
        {
            "en-US": "An actor shared an object with another actor or a group of actors.",
            "de-DE": "Ein Akteur teitle eine Gegenstand mit einem anderen Acteur oder einer Gruppe."
        });

    /**
     * An actor saved the progress.
     * @var {xAPI_Verb} saved
     */
    saved = new xAPI_Verb(
        "generic",
        "saved",
        {
            "en-US": "saved",
            "de-DE": "speicherte"
        },
        {
            "en-US": "An actor saved the progress.",
            "de-DE": "Ein Akteur speicherte den Fortschritt."
        });

    /**
     * An actor made a decision.
     * @var {xAPI_Verb} made
     */
    made = new xAPI_Verb(
        "generic",
        "made",
        {
            "en-US": "made",
            "de-DE": "fällte"
        },
        {
            "en-US": "An actor made a decision.",
            "de-DE": "Ein Akteur fällte eine Entscheidung."
        });

    /**
     * An actor found some item.
     * @var {xAPI_Verb} found
     */
    found = new xAPI_Verb(
        "generic",
        "found",
        {
            "en-US": "found",
            "de-DE": "fand"
        },
        {
            "en-US": "An actor found some item.",
            "de-DE": "Ein Akteur fand einen Gegenstand."
        });

    /**
     * A actor looked at a specific object. Only use if you can determine that the actor really looked at something, thriough eye-tracking for example.
     * @var {xAPI_Verb} lookedAt
     */
    lookedAt = new xAPI_Verb(
        "generic",
        "lookedAt",
        {
            "en-US": "looked at",
            "de-DE": "schaute auf"
        },
        {
            "en-US": "A actor looked at a specific object. Only use if you can determine that the actor really looked at something, thriough eye-tracking for example.",
            "de-DE": "Ein Acteur schaute auf eine spezifische Sache. Sollte nur genutzt werden wenn sichergestellt werden kann, dass der Spieler sich die Sache auch wirklich angeschaut hat. Mit Eyetracking kann das zum Beispiel gemacht werden."
        });

    /**
     * An actor asked a question. Could also asked for help or advice.
     * @var {xAPI_Verb} asked
     */
    asked = new xAPI_Verb(
        "generic",
        "asked",
        {
            "en-US": "asked",
            "de-DE": "fragte"
        },
        {
            "en-US": "An actor asked a question. Could also asked for help or advice.",
            "de-DE": "Ein Akteur stellte eine Frage. Es kann auch nach Hilfe oder Ratschlag gefragt werden."
        });

    /**
     * An actor solved some task.
     * @var {xAPI_Verb} solved
     */
    solved = new xAPI_Verb(
        "generic",
        "solved",
        {
            "en-US": "solved",
            "de-DE": "löste"
        },
        {
            "en-US": "An actor solved some task.",
            "de-DE": "Ein Akteur löste eine Aufgabe."
        });

    /**
     * An actor progressed in some game related way(achieved something or completed something).
     * @var {xAPI_Verb} progressed
     */
    progressed = new xAPI_Verb(
        "generic",
        "progressed",
        {
            "en-US": "progressed",
            "de-DE": "erzielte Fortschritt"
        },
        {
            "en-US": "An actor progressed in some game related way(achieved something or completed something).",
            "de-DE": "Ein Akteur erzielte in einer Form einen Fortschritt im Spiel(erreichte oder vollendete etwas)."
        });

    /**
     * An actor brought some objects into order.
     * @var {xAPI_Verb} ordered
     */
    ordered = new xAPI_Verb(
        "generic",
        "ordered",
        {
            "en-US": "ordered",
            "de-DE": "ordnete"
        },
        {
            "en-US": "An actor brought some objects into order.",
            "de-DE": "Ein Akteur ordnete ein Anzahl an Gegenständen."
        });

    /**
     * An actor failed some task.
     * @var {xAPI_Verb} failed
     */
    failed = new xAPI_Verb(
        "generic",
        "failed",
        {
            "en-US": "failed",
            "de-DE": "scheiterte"
        },
        {
            "en-US": "An actor failed some task.",
            "de-DE": "Ein Akteur scheiterte an einer Aufgabe."
        });

    /**
     * An actor used some item.
     * @var {xAPI_Verb} used
     */
    used = new xAPI_Verb(
        "generic",
        "used",
        {
            "en-US": "used",
            "de-DE": "benutzte"
        },
        {
            "en-US": "An actor used some item.",
            "de-DE": "Ein Akteur benutze einen Gegenstand."
        });

    /**
     * An actor unlocked some option or item.
     * @var {xAPI_Verb} unlocked
     */
    unlocked = new xAPI_Verb(
        "generic",
        "unlocked",
        {
            "en-US": "unlocked",
            "de-DE": "schaltete frei"
        },
        {
            "en-US": "An actor unlocked some option or item.",
            "de-DE": "Ein Akteur schaltete eine Option oder einen Gegenstand frei."
        });

    /**
     * An actor edited an object, for example their account profile. 'edited' has a strong connection to text-based input, e.g. a user edited the title. If the user uses preset values, for example realized by a dropdown menu, choose 'changed'. Also in most non-text-based input e.g. changing a color, 'changed' is the matching verb
     * @var {xAPI_Verb} edited
     */
    edited = new xAPI_Verb(
        "generic",
        "edited",
        {
            "en-US": "edited",
            "de-DE": "editierte"
        },
        {
            "en-US": "An actor edited an object, for example their account profile. 'edited' has a strong connection to text-based input, e.g. a user edited the title. If the user uses preset values, for example realized by a dropdown menu, choose 'changed'. Also in most non-text-based input e.g. changing a color, 'changed' is the matching verb",
            "de-DE": "Ein Acteur editierte ein Object, zum Beispiel ihren Account.'editierte' hat eine startke Verbindung zu text-basiertem Input. Z.B. Ein Benutzer editierte den Titel. Wenn dem Benutzer eine Auswahl benutzt, wie bei einem Dropdown Menü, sollte 'geändert' benutzt werden. Darüberhinaus ist in den meisten nicht-text-basierten Input, z.B. beim Ändern einer Farbe, 'änderte' das passende Verb"
        });

    /**
     * An actor sorted some objects.
     * @var {xAPI_Verb} sorted
     */
    sorted = new xAPI_Verb(
        "generic",
        "sorted",
        {
            "en-US": "sorted",
            "de-DE": "sortierte"
        },
        {
            "en-US": "An actor sorted some objects.",
            "de-DE": "Ein Akteur sortierte Objekte."
        });

    /**
     * An actor collapsed something, a menu for example.
     * @var {xAPI_Verb} collapsed
     */
    collapsed = new xAPI_Verb(
        "generic",
        "collapsed",
        {
            "en-US": "collapsed",
            "de-DE": "klappte ein"
        },
        {
            "en-US": "An actor collapsed something, a menu for example.",
            "de-DE": "Ein Akteur klappte etwas ein, ein Menü zum Beispiel ein."
        });

    /**
     * A actor or gorup pressed a specific button. Only use if the press of specific buttons should be tracked. Use the specific button as activity then. Otherwise use press as verb and button as activity.
     * @var {xAPI_Verb} pressedButton
     */
    pressedButton = new xAPI_Verb(
        "generic",
        "pressedButton",
        {
            "en-US": "pressedButton",
            "de-DE": "drückte Knopf"
        },
        {
            "en-US": "A actor or gorup pressed a specific button. Only use if the press of specific buttons should be tracked. Use the specific button as activity then. Otherwise use press as verb and button as activity.",
            "de-DE": "Ein Akteur drückte einen bestimmten Knopf. Sollte nur genutzt werden, wenn das Drücken eines bestimmten Knopfes aufgezeichnet werden soll. Nutze dann den speziellen Knopf als Aktivität der Aussage. Andernfalls nutze drückte als Verb und Knopf als Aktivität."
        });

    /**
     * An actor moved itself or some object in the game world.
     * @var {xAPI_Verb} moved
     */
    moved = new xAPI_Verb(
        "generic",
        "moved",
        {
            "en-US": "moved",
            "de-DE": "bewegte"
        },
        {
            "en-US": "An actor moved itself or some object in the game world.",
            "de-DE": "Ein Akteur bewegte sich oder einen Gegenstand in der Spielwelt."
        });

    /**
     * An actor has pressed a button.
     * @var {xAPI_Verb} pressed
     */
    pressed = new xAPI_Verb(
        "generic",
        "pressed",
        {
            "en-US": "pressed",
            "de-DE": "drückte"
        },
        {
            "en-US": "An actor has pressed a button.",
            "de-DE": "Der Akteur drückte eine Taste."
        });

    /**
     * An actor gave some input.
     * @var {xAPI_Verb} entered
     */
    entered = new xAPI_Verb(
        "generic",
        "entered",
        {
            "en-US": "entered",
            "de-DE": "gab ein"
        },
        {
            "en-US": "An actor gave some input.",
            "de-DE": "Ein Akteur gab eine Eingabe."
        });

    /**
     * An actor achieved some goal or score.
     * @var {xAPI_Verb} achieved
     */
    achieved = new xAPI_Verb(
        "generic",
        "achieved",
        {
            "en-US": "achieved",
            "de-DE": "erreichte"
        },
        {
            "en-US": "An actor achieved some goal or score.",
            "de-DE": "Ein Akteur erreichte ein Ziel oder ein Ergebnis."
        });

    /**
     * An actor deleted something.
     * @var {xAPI_Verb} deleted
     */
    deleted = new xAPI_Verb(
        "generic",
        "deleted",
        {
            "en-US": "deleted",
            "de-DE": "löschte"
        },
        {
            "en-US": "An actor deleted something.",
            "de-DE": "Ein Akteur löschte etwas."
        });

    /**
     * Indicates that that the actor has accepted the object. For instance, a person accepting an award, or accepting an assignment.
     * @var {xAPI_Verb} accepted
     */
    accepted = new xAPI_Verb(
        "generic",
        "accepted",
        {
            "en-US": "accepted",
            "de-DE": "akzeptierte"
        },
        {
            "en-US": "Indicates that that the actor has accepted the object. For instance, a person accepting an award, or accepting an assignment.",
            "de-DE": "Signalisiert, dass ein Akteur ein Objekt akzeptiert hat. Zum Beispiel: Eine Person akzeptiert eine Auszeichnung, oder eine Aufgabe."
        });

    /**
     * A actor put some text into a text field.
     * @var {xAPI_Verb} wrote
     */
    wrote = new xAPI_Verb(
        "generic",
        "wrote",
        {
            "en-US": "wrote",
            "de-DE": "schrieb"
        },
        {
            "en-US": "A actor put some text into a text field.",
            "de-DE": "Ein Akteur gab einen Text in ein Textfeld ein."
        });

    /**
     * A actor read some text. Only use if it can be assured that the text is read.
     * @var {xAPI_Verb} read
     */
    read = new xAPI_Verb(
        "generic",
        "read",
        {
            "en-US": "read",
            "de-DE": "las"
        },
        {
            "en-US": "A actor read some text. Only use if it can be assured that the text is read.",
            "de-DE": "Ein Acteur las einen Text. Sollte nur genutzt werden wenn sichergestellt werden kann, dass der Text auch wirklich gelesen wurde."
        });

    /**
     * An actor searched for an item or searched a location.
     * @var {xAPI_Verb} searched
     */
    searched = new xAPI_Verb(
        "generic",
        "searched",
        {
            "en-US": "searched",
            "de-DE": "suchte"
        },
        {
            "en-US": "An actor searched for an item or searched a location.",
            "de-DE": "Ein Acteur oder ein Gruppe suchte nach einem Gegenstand oder zum Beispiel auch einen Ort ab."
        });

    /**
     * An actor won a game.
     * @var {xAPI_Verb} won
     */
    won = new xAPI_Verb(
        "generic",
        "won",
        {
            "en-US": "won",
            "de-DE": "gewann"
        },
        {
            "en-US": "An actor won a game.",
            "de-DE": "Ein Akteur gewann ein Spiel."
        });

    /**
     * An actor found some item and collected it.
     * @var {xAPI_Verb} looted
     */
    looted = new xAPI_Verb(
        "generic",
        "looted",
        {
            "en-US": "looted",
            "de-DE": "plünderte"
        },
        {
            "en-US": "An actor found some item and collected it.",
            "de-DE": "Ein Akteur fand einen Gegenstand und sammelte ihn ein."
        });

    /**
     * A user used a local device to download data from a different distant disk.
     * @var {xAPI_Verb} downloaded
     */
    downloaded = new xAPI_Verb(
        "generic",
        "downloaded",
        {
            "en-US": "downloaded",
            "de-DE": "downloadete"
        },
        {
            "en-US": "A user used a local device to download data from a different distant disk.",
            "de-DE": "Ein Benutzer nutze ein lokales Gerät um Daten von einem entfernten Speichermedium zu laden."
        });

    /**
     * An actor brought two or more objects together. This can create a new object out of the combined ones. If just a logical connection is meant, use connected.
     * @var {xAPI_Verb} combined
     */
    combined = new xAPI_Verb(
        "generic",
        "combined",
        {
            "en-US": "combined",
            "de-DE": "kombinierte"
        },
        {
            "en-US": "An actor brought two or more objects together. This can create a new object out of the combined ones. If just a logical connection is meant, use connected.",
            "de-DE": "Ein Akteur brachte zwei oder mehr Gegenstände zusammen. Aus den kombinierten Gegenständen können neue Gegenstände entstehen. Wenn eine logische Kombination der Gegenstände gemeint ist, nutzte verband."
        });

    /**
     * An actor created something.
     * @var {xAPI_Verb} created
     */
    created = new xAPI_Verb(
        "generic",
        "created",
        {
            "en-US": "created",
            "de-DE": "erstellte"
        },
        {
            "en-US": "An actor created something.",
            "de-DE": "Ein Akteur erstellte etwas."
        });

    /**
     * An actor has released a button.
     * @var {xAPI_Verb} released
     */
    released = new xAPI_Verb(
        "generic",
        "released",
        {
            "en-US": "released",
            "de-DE": "löste"
        },
        {
            "en-US": "An actor has released a button.",
            "de-DE": "Der Akteur löste eine Taste."
        });

    /**
     * An actor unfolded something, a menu for example.
     * @var {xAPI_Verb} unfolded
     */
    unfolded = new xAPI_Verb(
        "generic",
        "unfolded",
        {
            "en-US": "unfolded",
            "de-DE": "klappte aus"
        },
        {
            "en-US": "An actor unfolded something, a menu for example.",
            "de-DE": "Ein Akteur klappte etwas aus, zum Beispiel ein Menü."
        });

    /**
     * An actor synchronized data with an LMS for example, which means, that two objects/processes/devices synchronized between A and B or with something.
     * @var {xAPI_Verb} synchronized
     */
    synchronized = new xAPI_Verb(
        "generic",
        "synchronized",
        {
            "en-US": "synchronized",
            "de-DE": "synchronisierte"
        },
        {
            "en-US": "An actor synchronized data with an LMS for example, which means, that two objects/processes/devices synchronized between A and B or with something.",
            "de-DE": "Ein Akteur synchronisierte zum Beispiel Daten mit einem LMS, d.h. zwei Objekte/Vorgänge/Geräte wurden auf einen Stand gebracht."
        });

    /**
     * An actor left behind some object.
     * @var {xAPI_Verb} left
     */
    left = new xAPI_Verb(
        "generic",
        "left",
        {
            "en-US": "left",
            "de-DE": "lies zurück"
        },
        {
            "en-US": "An actor left behind some object.",
            "de-DE": "Ein Akteur lies einen Gegenstand zurück."
        });

    /**
     * An actor selected some option. This could be an answer or a level for example.
     * @var {xAPI_Verb} selected
     */
    selected = new xAPI_Verb(
        "generic",
        "selected",
        {
            "en-US": "selected",
            "de-DE": "wählte aus"
        },
        {
            "en-US": "An actor selected some option. This could be an answer or a level for example.",
            "de-DE": "Ein Akteur wählte eine Option. Dies könnte eine Antwort auf eine Frage oder auch ein Level sein."
        });

    /**
     * An actor closed something. Could for example be the settings.
     * @var {xAPI_Verb} closed
     */
    closed = new xAPI_Verb(
        "generic",
        "closed",
        {
            "en-US": "closed",
            "de-DE": "schloss"
        },
        {
            "en-US": "An actor closed something. Could for example be the settings.",
            "de-DE": "Ein Akteur schloss etwas. Könnten zum Beispiel die Einstellungen sein."
        });

    /**
     * An actor toggled some switch.
     * @var {xAPI_Verb} toggled
     */
    toggled = new xAPI_Verb(
        "generic",
        "toggled",
        {
            "en-US": "toggled",
            "de-DE": "schaltete um"
        },
        {
            "en-US": "An actor toggled some switch.",
            "de-DE": "Ein Akteur schaltete einen Schalter um."
        });

    /**
     * An actor opened something. Could also be the settings for example.
     * @var {xAPI_Verb} opened
     */
    opened = new xAPI_Verb(
        "generic",
        "opened",
        {
            "en-US": "opened",
            "de-DE": "öffnete"
        },
        {
            "en-US": "An actor opened something. Could also be the settings for example.",
            "de-DE": "Ein Akteur öffnete etwas. Könnten zum Beispiel auch die Einstellungen sein."
        });

    /**
     * An actor did some action to lock up some object. Could also be locked out of a progression path for example.
     * @var {xAPI_Verb} locked
     */
    locked = new xAPI_Verb(
        "generic",
        "locked",
        {
            "en-US": "locked",
            "de-DE": "verschloss"
        },
        {
            "en-US": "An actor did some action to lock up some object. Could also be locked out of a progression path for example.",
            "de-DE": "Ein Akteur verschloss einen Gegenstand. Kann zum Beispiel aber auch ausschließen von einem Weg zu voran kommen sein."
        });

    /**
     * A group of actors voted on something.
     * @var {xAPI_Verb} voted
     */
    voted = new xAPI_Verb(
        "generic",
        "voted",
        {
            "en-US": "voted",
            "de-DE": "stimmte ab"
        },
        {
            "en-US": "A group of actors voted on something.",
            "de-DE": "Eine Gruppe von Acteuren stimmte für etwas ab."
        });

    /**
     * An actor calibrated something. This could be an eye tracking system for example.
     * @var {xAPI_Verb} calibrated
     */
    calibrated = new xAPI_Verb(
        "generic",
        "calibrated",
        {
            "en-US": "calibrated",
            "de-DE": "kalibrierte"
        },
        {
            "en-US": "An actor calibrated something. This could be an eye tracking system for example.",
            "de-DE": "Ein Akteur kalibrierte etwas. Dies kann zum Beispiel ein Eyetracking System sein."
        });

    /**
     * An actor triggered a event.
     * @var {xAPI_Verb} triggered
     */
    triggered = new xAPI_Verb(
        "generic",
        "triggered",
        {
            "en-US": "triggered",
            "de-DE": "löste aus"
        },
        {
            "en-US": "An actor triggered a event.",
            "de-DE": "Ein Akteur löste ein Ereignis aus."
        });

    /**
     * An actor lost the game.
     * @var {xAPI_Verb} lost
     */
    lost = new xAPI_Verb(
        "generic",
        "lost",
        {
            "en-US": "lost",
            "de-DE": "verlor"
        },
        {
            "en-US": "An actor lost the game.",
            "de-DE": "Ein Akteur verlor das Spiel."
        });

    /**
     * An actor completed some task, goal or level.
     * @var {xAPI_Verb} completed
     */
    completed = new xAPI_Verb(
        "generic",
        "completed",
        {
            "en-US": "completed",
            "de-DE": "schloss ab"
        },
        {
            "en-US": "An actor completed some task, goal or level.",
            "de-DE": "Ein Akteur schloss eine Aufgabe, ein Ziel oder ein Level ab."
        });

    /**
     * An actor connected two or more items with each other. This is meant in the sense of drawn a line between the objects, while combined can make something new out of the objects.
     * @var {xAPI_Verb} connected
     */
    connected = new xAPI_Verb(
        "generic",
        "connected",
        {
            "en-US": "connected",
            "de-DE": "verband"
        },
        {
            "en-US": "An actor connected two or more items with each other. This is meant in the sense of drawn a line between the objects, while combined can make something new out of the objects.",
            "de-DE": "Ein Akteur verband zwei oder mehr Gegenstände mit einander. Dies ist eher im Sinne von eine Linie zwischen den Objekten malen, während mit kombinierte auch zum Beispiel neue Gegenstände aus den verbundenen entstehen können."
        });

    /**
     * An actor watched a video. Only use if you can assure the actors really watched the video, use eye-tracking for example.
     * @var {xAPI_Verb} watched
     */
    watched = new xAPI_Verb(
        "generic",
        "watched",
        {
            "en-US": "watched",
            "de-DE": "schaute"
        },
        {
            "en-US": "An actor watched a video. Only use if you can assure the actors really watched the video, use eye-tracking for example.",
            "de-DE": "Ein Akteur schaute ein Video. Sollte nur genutzt werden wenn sichergestellt werden kann, dass das Video wirklich geschaut wurde. Eyetracking könnte dafür zum Beispiel benutzt werden."
        });

    constructor() {
        super("generic");
    }
}