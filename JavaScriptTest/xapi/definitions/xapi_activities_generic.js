import { xAPI_Activity } from '../core/xapi_activity.js';
import { xAPI_Activities } from '../core/xapi_activities.js';

/**
 * Provides the xAPI_Activities of the context generic as public properties.
 */
export class xAPI_Activities_Generic extends xAPI_Activities {
    /**
     * Data which is gathered by a sensor.
     * @var {xAPI_Activity} senorData
     */
    senorData = new xAPI_Activity(
        "generic",
        "senorData",
        {
            "en-US": "sensor data",
            "de-DE": "Sensorendaten"
        },
        {
            "en-US": "Data which is gathered by a sensor.",
            "de-DE": "Daten welche von Sensoren erfasst werden."
        });

    /**
     * A number shown on the screen, with which for example can be interacted with in some way.
     * @var {xAPI_Activity} number
     */
    number = new xAPI_Activity(
        "generic",
        "number",
        {
            "en-US": "number",
            "de-DE": "Nummer"
        },
        {
            "en-US": "A number shown on the screen, with which for example can be interacted with in some way.",
            "de-DE": "Eine Nummer die auf dem Bildschirm angezeigt wird und mit der zum Beispiel interagiert werden kann."
        });

    /**
     * A button to start a video.
     * @var {xAPI_Activity} startVideoButton
     */
    startVideoButton = new xAPI_Activity(
        "generic",
        "startVideoButton",
        {
            "en-US": "start video button",
            "de-DE": "Videostarten Knopf"
        },
        {
            "en-US": "A button to start a video.",
            "de-DE": "Ein Knopf um ein Video zu starten."
        });

    /**
     * Data which is gathered by a sensor.
     * @var {xAPI_Activity} sensorData
     */
    sensorData = new xAPI_Activity(
        "generic",
        "sensorData",
        {
            "en-US": "sensor data",
            "de-DE": "Sensorendaten"
        },
        {
            "en-US": "Data which is gathered by a sensor.",
            "de-DE": "Daten, welche von einem Sensor erfasst werden."
        });

    /**
     * A card showning something inside the game.
     * @var {xAPI_Activity} card
     */
    card = new xAPI_Activity(
        "generic",
        "card",
        {
            "en-US": "card",
            "de-DE": "Karte"
        },
        {
            "en-US": "A card showning something inside the game.",
            "de-DE": "Eine Karte die etwas im Spiel anzeigt."
        });

    /**
     * A word shown on the screen, with which for example can be interacted with in some way.
     * @var {xAPI_Activity} word
     */
    word = new xAPI_Activity(
        "generic",
        "word",
        {
            "en-US": "word",
            "de-DE": "Wort"
        },
        {
            "en-US": "A word shown on the screen, with which for example can be interacted with in some way.",
            "de-DE": "Ein Wort welches auf dem Bildschirm angezeigt wird und mit dem man zum Beispiel interagieren kann."
        });

    /**
     * The overall thing to be achieved in the game.
     * @var {xAPI_Activity} goal
     */
    goal = new xAPI_Activity(
        "generic",
        "goal",
        {
            "en-US": "goal",
            "de-DE": "Ziel"
        },
        {
            "en-US": "The overall thing to be achieved in the game.",
            "de-DE": "Das Übergreifende Ziel des Spiels."
        });

    /**
     * A tip to overcome some ingame task.
     * @var {xAPI_Activity} tip
     */
    tip = new xAPI_Activity(
        "generic",
        "tip",
        {
            "en-US": "tip",
            "de-DE": "Tipp"
        },
        {
            "en-US": "A tip to overcome some ingame task.",
            "de-DE": "Ein Tipp um eine Aufgabe im Spiel zu lösen."
        });

    /**
     * A question can be asked or answered.
     * @var {xAPI_Activity} question
     */
    question = new xAPI_Activity(
        "generic",
        "question",
        {
            "en-US": "question",
            "de-DE": "Frage"
        },
        {
            "en-US": "A question can be asked or answered.",
            "de-DE": "Eine Frage kann gestellt oder beantwortet werden."
        });

    /**
     * The settings of a game.
     * @var {xAPI_Activity} settings
     */
    settings = new xAPI_Activity(
        "generic",
        "settings",
        {
            "en-US": "settings",
            "de-DE": "Einstellungen"
        },
        {
            "en-US": "The settings of a game.",
            "de-DE": "Die EInstellungen eines Spiels."
        });

    /**
     * The solution to a task in the game.
     * @var {xAPI_Activity} solution
     */
    solution = new xAPI_Activity(
        "generic",
        "solution",
        {
            "en-US": "solution",
            "de-DE": "Lösung"
        },
        {
            "en-US": "The solution to a task in the game.",
            "de-DE": "Die Lösung zu einer Aufgabe im Spiel."
        });

    /**
     * A device to interact with the game.
     * @var {xAPI_Activity} tangible
     */
    tangible = new xAPI_Activity(
        "generic",
        "tangible",
        {
            "en-US": "tangible",
            "de-DE": "Tangible"
        },
        {
            "en-US": "A device to interact with the game.",
            "de-DE": "Ein Gerät mit dem man mit dem Spiel interagieren kann."
        });

    /**
     * An actor which plays the game and interacts with it.
     * @var {xAPI_Activity} player
     */
    player = new xAPI_Activity(
        "generic",
        "player",
        {
            "en-US": "player",
            "de-DE": "Spieler"
        },
        {
            "en-US": "An actor which plays the game and interacts with it.",
            "de-DE": "Ein Akteur der das Spiel spielt und mit ihm interagiert."
        });

    /**
     * A collection of in - most cases - connected data on a disk which is stored together under a particular name.
     * @var {xAPI_Activity} file
     */
    file = new xAPI_Activity(
        "generic",
        "file",
        {
            "en-US": "File",
            "de-DE": "Datei"
        },
        {
            "en-US": "A collection of in - most cases - connected data on a disk which is stored together under a particular name.",
            "de-DE": "Eine Sammlung von meist zusammenhängenden Daten auf einem Speichermedium die zusammen unter einem bestimmten Namen gespeichert werden."
        });

    /**
     * The status of if a task can be completed in the current gamestate.
     * @var {xAPI_Activity} solvability
     */
    solvability = new xAPI_Activity(
        "generic",
        "solvability",
        {
            "en-US": "solvability",
            "de-DE": "Lösbarkeit"
        },
        {
            "en-US": "The status of if a task can be completed in the current gamestate.",
            "de-DE": "Der Zustand ob die aktuelle Aufgabe noch gelöst werden kann mit dem aktuelle Spielzustand."
        });

    /**
     * A button to stop a video.
     * @var {xAPI_Activity} stopVideoButton
     */
    stopVideoButton = new xAPI_Activity(
        "generic",
        "stopVideoButton",
        {
            "en-US": "stop video button",
            "de-DE": "Videostoppen Knopf"
        },
        {
            "en-US": "A button to stop a video.",
            "de-DE": "Ein Knopf um ein Video zu stoppen."
        });

    /**
     * A mouse to use UI or interact with the game in another way.
     * @var {xAPI_Activity} mouse
     */
    mouse = new xAPI_Activity(
        "generic",
        "mouse",
        {
            "en-US": "mouse",
            "de-DE": "Maus"
        },
        {
            "en-US": "A mouse to use UI or interact with the game in another way.",
            "de-DE": "Eine Maus um die Benutzeroberfläche zu benutzen oder in einer anderen Form mit dem Spiel zu interagieren."
        });

    /**
     * A text which can be read or entered.
     * @var {xAPI_Activity} text
     */
    text = new xAPI_Activity(
        "generic",
        "text",
        {
            "en-US": "text",
            "de-DE": "Text"
        },
        {
            "en-US": "A text which can be read or entered.",
            "de-DE": "Ein Text der gelesen oder eingegeben werden kann."
        });

    /**
     * A system to track the eyes of a player and use the gathered data in some way.
     * @var {xAPI_Activity} eyeTrackingSystem
     */
    eyeTrackingSystem = new xAPI_Activity(
        "generic",
        "eyeTrackingSystem",
        {
            "en-US": "eye tracking system",
            "de-DE": "Eye-Tracking System"
        },
        {
            "en-US": "A system to track the eyes of a player and use the gathered data in some way.",
            "de-DE": "Ein system um die Augenbewegungen des Spielers aufzuzeichnen und diese Daten in einer Form zu nutzen."
        });

    /**
     * Something a player can ask for to find the solution to a task.
     * @var {xAPI_Activity} help
     */
    help = new xAPI_Activity(
        "generic",
        "help",
        {
            "en-US": "help",
            "de-DE": "Hilfe"
        },
        {
            "en-US": "Something a player can ask for to find the solution to a task.",
            "de-DE": "Etwas wonach der Spieler fragen kann um eine Lösung für eine Aufgabe zu finden."
        });

    /**
     * A document ingame. Could be relevant for the game or not.
     * @var {xAPI_Activity} document
     */
    document = new xAPI_Activity(
        "generic",
        "document",
        {
            "en-US": "document",
            "de-DE": "Dokument"
        },
        {
            "en-US": "A document ingame. Could be relevant for the game or not.",
            "de-DE": "Ein Dokument im Spiel."
        });

    /**
     * The gamemode in which a game is played (Competitive or cooperative for example).
     * @var {xAPI_Activity} gamemode
     */
    gamemode = new xAPI_Activity(
        "generic",
        "gamemode",
        {
            "en-US": "gamemode",
            "de-DE": "Spielmodus"
        },
        {
            "en-US": "The gamemode in which a game is played (Competitive or cooperative for example).",
            "de-DE": "Der Spielmodus in dem ein Spiel gespielt wird (Zusammen oder Gegeneinander zum Beispiel)."
        });

    /**
     * A video which can be watched inside the game.
     * @var {xAPI_Activity} video
     */
    video = new xAPI_Activity(
        "generic",
        "video",
        {
            "en-US": "video",
            "de-DE": "Video"
        },
        {
            "en-US": "A video which can be watched inside the game.",
            "de-DE": "Ein Video welches im Spiel angeschaut werden kann."
        });

    /**
     * A group of players playing a game together.
     * @var {xAPI_Activity} group
     */
    group = new xAPI_Activity(
        "generic",
        "group",
        {
            "en-US": "group",
            "de-DE": "Gruppe"
        },
        {
            "en-US": "A group of players playing a game together.",
            "de-DE": "Eine Gruppe von Spielern welche zusammen ein Spiel spielen."
        });

    /**
     * A button to go forward on a page or a screen.
     * @var {xAPI_Activity} forwardButton
     */
    forwardButton = new xAPI_Activity(
        "generic",
        "forwardButton",
        {
            "en-US": "forward button",
            "de-DE": "Weiter Knopf"
        },
        {
            "en-US": "A button to go forward on a page or a screen.",
            "de-DE": "Ein Knopf um auf einer Seite oder dem Bildschirm weiter zu gehen."
        });

    /**
     * Some part of the goal or something to be completed to progress in the game or a level.
     * @var {xAPI_Activity} task
     */
    task = new xAPI_Activity(
        "generic",
        "task",
        {
            "en-US": "task",
            "de-DE": "Aufgabe"
        },
        {
            "en-US": "Some part of the goal or something to be completed to progress in the game or a level.",
            "de-DE": "Eine Aufgabe die zum Erreichen des Spielziels erfüllt werden muss oder zum Abschließen eines Levels."
        });

    /**
     * A start menu or ingame menu for settings for example.
     * @var {xAPI_Activity} menu
     */
    menu = new xAPI_Activity(
        "generic",
        "menu",
        {
            "en-US": "menu",
            "de-DE": "Menü"
        },
        {
            "en-US": "A start menu or ingame menu for settings for example.",
            "de-DE": "Ein Startmenü oder ein Menü im Spiel für Einstellungen zum Beispiel."
        });

    /**
     * A representation of the player ingame. Could also be an object the player can interact with.
     * @var {xAPI_Activity} playstone
     */
    playstone = new xAPI_Activity(
        "generic",
        "playstone",
        {
            "en-US": "playstone",
            "de-DE": "Spielstein"
        },
        {
            "en-US": "A representation of the player ingame. Could also be an object the player can interact with.",
            "de-DE": "Eine Representation des Spielers im Spiel. Könnte auch ein Objekt sein mit welchem der Spieler im Spiel interagieren kann."
        });

    /**
     * A decision to be made which is relevant for the game outcome.
     * @var {xAPI_Activity} decision
     */
    decision = new xAPI_Activity(
        "generic",
        "decision",
        {
            "en-US": "decision",
            "de-DE": "Entscheidung"
        },
        {
            "en-US": "A decision to be made which is relevant for the game outcome.",
            "de-DE": "Eine Entscheidung die getroffen werden muss und welche den Spielausgang beieinflusst."
        });

    /**
     * A reference to data that the user can follow. Links can either point to a whole document (e.g. website) or to a specific element within a document.
     * @var {xAPI_Activity} link
     */
    link = new xAPI_Activity(
        "generic",
        "link",
        {
            "en-US": "Hyperlink",
            "de-DE": "Hyperlink"
        },
        {
            "en-US": "A reference to data that the user can follow. Links can either point to a whole document (e.g. website) or to a specific element within a document.",
            "de-DE": "Ein Querverweis auf ein elektronischces Dokument, welches dem Benutzer ermöglicht zu dem Dokument (z.B. einer Website) oder zu einem bestimmten Abschnitt in dem Dokument zu springen."
        });

    /**
     * A keyboard to input text or interact with the game in another way.
     * @var {xAPI_Activity} keyboard
     */
    keyboard = new xAPI_Activity(
        "generic",
        "keyboard",
        {
            "en-US": "keyboard",
            "de-DE": "Tastatur"
        },
        {
            "en-US": "A keyboard to input text or interact with the game in another way.",
            "de-DE": "Eine Tastatur um Text einzugeben oder in einer anderen Form mit dem Spiel zu interagieren."
        });

    /**
     * A button to go backward on a page or a screen.
     * @var {xAPI_Activity} backwardButton
     */
    backwardButton = new xAPI_Activity(
        "generic",
        "backwardButton",
        {
            "en-US": "backward button",
            "de-DE": "Zurück Knopf"
        },
        {
            "en-US": "A button to go backward on a page or a screen.",
            "de-DE": "Ein Knopf um auf einer Seite oder dem Bildschirm zurück zu gehen."
        });

    /**
     * An image of something ingame. Can be an interactible.
     * @var {xAPI_Activity} image
     */
    image = new xAPI_Activity(
        "generic",
        "image",
        {
            "en-US": "image",
            "de-DE": "Bild"
        },
        {
            "en-US": "An image of something ingame. Can be an interactible.",
            "de-DE": "Ein Bild von etwas im Spiel. Mit einem Bild könnte interagiert werden."
        });

    /**
     * The response to a question a user can give or get.
     * @var {xAPI_Activity} answer
     */
    answer = new xAPI_Activity(
        "generic",
        "answer",
        {
            "en-US": "answer",
            "de-DE": "Antwort"
        },
        {
            "en-US": "The response to a question a user can give or get.",
            "de-DE": "Eine Antwort kann auf eine Frage gegeben oder bekommen werden."
        });

    /**
     * A system interacting with the game which is not running on the same machine as the game.
     * @var {xAPI_Activity} remoteSystem
     */
    remoteSystem = new xAPI_Activity(
        "generic",
        "remoteSystem",
        {
            "en-US": "remote system",
            "de-DE": "entferntes System"
        },
        {
            "en-US": "A system interacting with the game which is not running on the same machine as the game.",
            "de-DE": "Ein System welches mit dem Spiel interagiert aber nicht auf der selben Maschine wie das Spiel läuft."
        });

    /**
     * An option is something a player can decide for or against in the game.
     * @var {xAPI_Activity} option
     */
    option = new xAPI_Activity(
        "generic",
        "option",
        {
            "en-US": "option",
            "de-DE": "Option"
        },
        {
            "en-US": "An option is something a player can decide for or against in the game.",
            "de-DE": "Ein Spieler kann sich für oder gegen eine Option entscheiden."
        });

    constructor() {
        super("generic");
    }
}