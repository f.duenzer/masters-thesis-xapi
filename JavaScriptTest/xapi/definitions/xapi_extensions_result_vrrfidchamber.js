import { xAPI_Extensions_Result } from './xapi_extensions_result.js';
import { xAPI_Extension } from '../core/xapi_extension.js';

/**
 * Provides all extensions of the context vrRfidChamber of type result as public properties.
 */
export class xAPI_Extensions_Result_VrRfidChamber extends xAPI_Extensions_Result {

    constructor() {
        super("vrRfidChamber");
    }

    success(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "success",
                {
                    "en-US": "success"
                },
                {
                    "en-US": "Experiment is successful."
                }),
            value);
        return this;
    }

    startTime(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "startTime",
                {
                    "en-US": "start time"
                },
                {
                    "en-US": "The time when the experiment began. This is captured from the system time of the local machine."
                }),
            value);
        return this;
    }

    endTime(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "endTime",
                {
                    "en-US": "end time"
                },
                {
                    "en-US": "The time when the experiment was completed that is when the software was closed. This is captured from the system time of the local machine."
                }),
            value);
        return this;
    }

    duration(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "duration",
                {
                    "en-US": "duration"
                },
                {
                    "en-US": "Value representing a length of time required to complete the experiment."
                }),
            value);
        return this;
    }

    failure(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "failure",
                {
                    "en-US": "failure"
                },
                {
                    "en-US": "The experiment is a failure."
                }),
            value);
        return this;
    }
}