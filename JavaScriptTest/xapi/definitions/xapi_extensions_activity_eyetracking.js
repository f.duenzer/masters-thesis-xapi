import { xAPI_Extensions_Activity } from './xapi_extensions_activity.js';
import { xAPI_Extension } from '../core/xapi_extension.js';

/**
 * Provides all extensions of the context eyeTracking of type activity as public properties.
 */
export class xAPI_Extensions_Activity_EyeTracking extends xAPI_Extensions_Activity {

    constructor() {
        super("eyeTracking");
    }

    eye(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "eye",
                {
                    "en-US": "eye",
                    "de-DE": "Auge"
                },
                {
                    "en-US": "Organ of the visual system. Actors physical (left or right) eye.",
                    "de-DE": "Sinnesorgan zur Wahrnehmung von Lichtreizen, in diesem Fall physikalisches Auge des Akteurs (links oder rechts)."
                }),
            value);
        return this;
    }
}