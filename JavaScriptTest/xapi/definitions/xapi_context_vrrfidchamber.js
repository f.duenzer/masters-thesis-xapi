import { xAPI_Context } from '../core/xapi_context.js';
import { xAPI_Verbs_VrRfidChamber } from './xapi_verbs_vrrfidchamber.js';
import { xAPI_Activities_VrRfidChamber } from './xapi_activities_vrrfidchamber.js';
import { xAPI_Context_VrRfidChamber_Extensions } from './xapi_context_vrrfidchamber_extensions.js';

/**
 * Provides the definitions of the context vrRfidChamber as public properties.
 */
export class xAPI_Context_VrRfidChamber extends xAPI_Context {
    verbs = new xAPI_Verbs_VrRfidChamber();

    activities = new xAPI_Activities_VrRfidChamber();

    extensions = new xAPI_Context_VrRfidChamber_Extensions();

    constructor() {
        super("vrRfidChamber");
    }
}