import { xAPI_Extensions_Result } from './xapi_extensions_result.js';
import { xAPI_Extension } from '../core/xapi_extension.js';

/**
 * Provides all extensions of the context eyeTracking of type result as public properties.
 */
export class xAPI_Extensions_Result_EyeTracking extends xAPI_Extensions_Result {

    constructor() {
        super("eyeTracking");
    }

    numberOfBlinking(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "numberOfBlinking",
                {
                    "en-US": "number of blinking",
                    "de-DE": "Zwinker Anzahl"
                },
                {
                    "en-US": "The number of actors eye blinking within a focus sequence. Has to be an integer.",
                    "de-DE": "Die Anzahl an Zwinker, die die Akteurin während einer Fokus Sequence getätigt hat. Muss ein Integer sein."
                }),
            value);
        return this;
    }

    duration(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "duration",
                {
                    "en-US": "fixation duration",
                    "de-DE": "Fixationsdauer"
                },
                {
                    "en-US": "The fixation duration is the measure how long the actor spend looking at a particular location.",
                    "de-DE": "Die Fixationsdauer beschreibt die Zeit, die ein Akteur oder eine Akteurin den Blick auf einem bestimmten Ort hat verweilen lassen."
                }),
            value);
        return this;
    }
}