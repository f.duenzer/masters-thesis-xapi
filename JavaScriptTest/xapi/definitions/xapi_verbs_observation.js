import { xAPI_Verb } from '../core/xapi_verb.js';
import { xAPI_Verbs } from '../core/xapi_verbs.js';

/**
 * Provides the xAPI_Verbs of the context observation as public properties.
 */
export class xAPI_Verbs_Observation extends xAPI_Verbs {
    /**
     * Ein Akteur unterbricht jemand oder etwas anderen verbal.
     * @var {xAPI_Verb} unterbricht
     */
    unterbricht = new xAPI_Verb(
        "observation",
        "unterbricht",
        {
            "de-DE": "unterbricht"
        },
        {
            "de-DE": "Ein Akteur unterbricht jemand oder etwas anderen verbal."
        });

    /**
     * Ein Akteur erzählt etwas verbal
     * @var {xAPI_Verb} erzaehlt
     */
    erzaehlt = new xAPI_Verb(
        "observation",
        "erzaehlt",
        {
            "de-DE": "erzählt"
        },
        {
            "de-DE": "Ein Akteur erzählt etwas verbal"
        });

    /**
     * Ein Akteur motiviert jemanden
     * @var {xAPI_Verb} motiviert
     */
    motiviert = new xAPI_Verb(
        "observation",
        "motiviert",
        {
            "de-DE": "motiviert"
        },
        {
            "de-DE": "Ein Akteur motiviert jemanden"
        });

    /**
     * Ein Akteur sieht etwas
     * @var {xAPI_Verb} sieht
     */
    sieht = new xAPI_Verb(
        "observation",
        "sieht",
        {
            "de-DE": "sieht"
        },
        {
            "de-DE": "Ein Akteur sieht etwas"
        });

    /**
     * Ein Akteur beschäftigt sich mit etwas
     * @var {xAPI_Verb} beschaeftigt
     */
    beschaeftigt = new xAPI_Verb(
        "observation",
        "beschaeftigt",
        {
            "de-DE": "beschäftigt"
        },
        {
            "de-DE": "Ein Akteur beschäftigt sich mit etwas"
        });

    /**
     * Ein Akteur erklärt jemand anderen etwas verbal.
     * @var {xAPI_Verb} erklaert
     */
    erklaert = new xAPI_Verb(
        "observation",
        "erklaert",
        {
            "de-DE": "erklärt"
        },
        {
            "de-DE": "Ein Akteur erklärt jemand anderen etwas verbal."
        });

    /**
     * Ein Akteur gibt etwas
     * @var {xAPI_Verb} gibt
     */
    gibt = new xAPI_Verb(
        "observation",
        "gibt",
        {
            "de-DE": "gibt"
        },
        {
            "de-DE": "Ein Akteur gibt etwas"
        });

    /**
     * Ein Akteur fragt etwas
     * @var {xAPI_Verb} fragt
     */
    fragt = new xAPI_Verb(
        "observation",
        "fragt",
        {
            "de-DE": "fragt"
        },
        {
            "de-DE": "Ein Akteur fragt etwas"
        });

    /**
     * Ein Akteur nimmt etwas
     * @var {xAPI_Verb} nimmt
     */
    nimmt = new xAPI_Verb(
        "observation",
        "nimmt",
        {
            "de-DE": "nimmt"
        },
        {
            "de-DE": "Ein Akteur nimmt etwas"
        });

    constructor() {
        super("observation");
    }
}