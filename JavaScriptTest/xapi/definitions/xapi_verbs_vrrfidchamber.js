import { xAPI_Verb } from '../core/xapi_verb.js';
import { xAPI_Verbs } from '../core/xapi_verbs.js';

/**
 * Provides the xAPI_Verbs of the context vrRfidChamber as public properties.
 */
export class xAPI_Verbs_VrRfidChamber extends xAPI_Verbs {
    /**
     * An actor clicked something on the screen.
     * @var {xAPI_Verb} clicked
     */
    clicked = new xAPI_Verb(
        "vrRfidChamber",
        "clicked",
        {
            "en-US": "clicked"
        },
        {
            "en-US": "An actor clicked something on the screen."
        });

    /**
     * An actor dragged and pushed an object on the screen.
     * @var {xAPI_Verb} dragPush
     */
    dragPush = new xAPI_Verb(
        "vrRfidChamber",
        "dragPush",
        {
            "en-US": "dragPush"
        },
        {
            "en-US": "An actor dragged and pushed an object on the screen."
        });

    /**
     * An actor has started the object.
     * @var {xAPI_Verb} started
     */
    started = new xAPI_Verb(
        "vrRfidChamber",
        "started",
        {
            "en-US": "started"
        },
        {
            "en-US": "An actor has started the object."
        });

    /**
     * An actor unforcused the object
     * @var {xAPI_Verb} unfocussed
     */
    unfocussed = new xAPI_Verb(
        "vrRfidChamber",
        "unfocussed",
        {
            "en-US": "unfocused"
        },
        {
            "en-US": "An actor unforcused the object"
        });

    /**
     * An actor has terminated the operation.
     * @var {xAPI_Verb} terminated
     */
    terminated = new xAPI_Verb(
        "vrRfidChamber",
        "terminated",
        {
            "en-US": "terminated"
        },
        {
            "en-US": "An actor has terminated the operation."
        });

    /**
     * An actor has interacted with the object.
     * @var {xAPI_Verb} interacted
     */
    interacted = new xAPI_Verb(
        "vrRfidChamber",
        "interacted",
        {
            "en-US": "interacted"
        },
        {
            "en-US": "An actor has interacted with the object."
        });

    /**
     * An actor panned the screen.
     * @var {xAPI_Verb} panned
     */
    panned = new xAPI_Verb(
        "vrRfidChamber",
        "panned",
        {
            "en-US": "panned"
        },
        {
            "en-US": "An actor panned the screen."
        });

    /**
     * An actor zoomed into some part of the screen.
     * @var {xAPI_Verb} zoomedIn
     */
    zoomedIn = new xAPI_Verb(
        "vrRfidChamber",
        "zoomedIn",
        {
            "en-US": "zoomed in"
        },
        {
            "en-US": "An actor zoomed into some part of the screen."
        });

    /**
     * An actor has focused on the RFID tag.
     * @var {xAPI_Verb} focussed
     */
    focussed = new xAPI_Verb(
        "vrRfidChamber",
        "focussed",
        {
            "en-US": "focused"
        },
        {
            "en-US": "An actor has focused on the RFID tag."
        });

    /**
     * An actor has pressed the keyboard.
     * @var {xAPI_Verb} pressed
     */
    pressed = new xAPI_Verb(
        "vrRfidChamber",
        "pressed",
        {
            "en-US": "pressed"
        },
        {
            "en-US": "An actor has pressed the keyboard."
        });

    /**
     * An actor has dragged an object across the screen.
     * @var {xAPI_Verb} dragged
     */
    dragged = new xAPI_Verb(
        "vrRfidChamber",
        "dragged",
        {
            "en-US": "dragged"
        },
        {
            "en-US": "An actor has dragged an object across the screen."
        });

    /**
     * An actor rotated a lab object.
     * @var {xAPI_Verb} rotated
     */
    rotated = new xAPI_Verb(
        "vrRfidChamber",
        "rotated",
        {
            "en-US": "rotated"
        },
        {
            "en-US": "An actor rotated a lab object."
        });

    /**
     * An actor dragged and pulled an object.
     * @var {xAPI_Verb} dragPull
     */
    dragPull = new xAPI_Verb(
        "vrRfidChamber",
        "dragPull",
        {
            "en-US": "dragPull"
        },
        {
            "en-US": "An actor dragged and pulled an object."
        });

    /**
     * An actor has visited the virtual lab.
     * @var {xAPI_Verb} visited
     */
    visited = new xAPI_Verb(
        "vrRfidChamber",
        "visited",
        {
            "en-US": "visited"
        },
        {
            "en-US": "An actor has visited the virtual lab."
        });

    /**
     * An actor has removed an object from the target.
     * @var {xAPI_Verb} removed
     */
    removed = new xAPI_Verb(
        "vrRfidChamber",
        "removed",
        {
            "en-US": "removed"
        },
        {
            "en-US": "An actor has removed an object from the target."
        });

    /**
     * An actor has released the object.
     * @var {xAPI_Verb} released
     */
    released = new xAPI_Verb(
        "vrRfidChamber",
        "released",
        {
            "en-US": "released"
        },
        {
            "en-US": "An actor has released the object."
        });

    /**
     * An actor zoomed out of some part of the screen.
     * @var {xAPI_Verb} zoomedOut
     */
    zoomedOut = new xAPI_Verb(
        "vrRfidChamber",
        "zoomedOut",
        {
            "en-US": "zoomed out"
        },
        {
            "en-US": "An actor zoomed out of some part of the screen."
        });

    /**
     * An actor has viewed the object details
     * @var {xAPI_Verb} viewed
     */
    viewed = new xAPI_Verb(
        "vrRfidChamber",
        "viewed",
        {
            "en-US": "viewed"
        },
        {
            "en-US": "An actor has viewed the object details"
        });

    /**
     * An actor closed an the door of the RFID chamber.
     * @var {xAPI_Verb} closed
     */
    closed = new xAPI_Verb(
        "vrRfidChamber",
        "closed",
        {
            "en-US": "closed"
        },
        {
            "en-US": "An actor closed an the door of the RFID chamber."
        });

    /**
     * An actor has opened the object.
     * @var {xAPI_Verb} opened
     */
    opened = new xAPI_Verb(
        "vrRfidChamber",
        "opened",
        {
            "en-US": "opened"
        },
        {
            "en-US": "An actor has opened the object."
        });

    /**
     * The system scan is completed to check the RFID output.
     * @var {xAPI_Verb} completed
     */
    completed = new xAPI_Verb(
        "vrRfidChamber",
        "completed",
        {
            "en-US": "completed"
        },
        {
            "en-US": "The system scan is completed to check the RFID output."
        });

    /**
     * An actor has watched the formation of the graph.
     * @var {xAPI_Verb} watched
     */
    watched = new xAPI_Verb(
        "vrRfidChamber",
        "watched",
        {
            "en-US": "Watched"
        },
        {
            "en-US": "An actor has watched the formation of the graph."
        });

    constructor() {
        super("vrRfidChamber");
    }
}