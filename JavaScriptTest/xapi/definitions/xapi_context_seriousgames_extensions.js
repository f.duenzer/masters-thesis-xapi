import { xAPI_Extensions_Context_Seriousgames } from './xapi_extensions_context_seriousgames.js';
import { xAPI_Extensions_Result_Seriousgames } from './xapi_extensions_result_seriousgames.js';
import { xAPI_Extensions_Activity_Seriousgames } from './xapi_extensions_activity_seriousgames.js';

/**
 * Provides the extensions of the context seriousgames as public properties.
 */
export class xAPI_Context_Seriousgames_Extensions {

    constructor() {
    }

    get context() {
        return new xAPI_Extensions_Context_Seriousgames();
    }

    get result() {
        return new xAPI_Extensions_Result_Seriousgames();
    }

    get activity() {
        return new xAPI_Extensions_Activity_Seriousgames();
    }
}