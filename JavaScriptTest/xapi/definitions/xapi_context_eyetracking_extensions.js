import { xAPI_Extensions_Result_EyeTracking } from './xapi_extensions_result_eyetracking.js';
import { xAPI_Extensions_Activity_EyeTracking } from './xapi_extensions_activity_eyetracking.js';

/**
 * Provides the extensions of the context eyeTracking as public properties.
 */
export class xAPI_Context_EyeTracking_Extensions {

    constructor() {
    }

    get result() {
        return new xAPI_Extensions_Result_EyeTracking();
    }

    get activity() {
        return new xAPI_Extensions_Activity_EyeTracking();
    }
}