import { xAPI_Context } from '../core/xapi_context.js';
import { xAPI_Verbs_UhfReader } from './xapi_verbs_uhfreader.js';
import { xAPI_Activities_UhfReader } from './xapi_activities_uhfreader.js';
import { xAPI_Context_UhfReader_Extensions } from './xapi_context_uhfreader_extensions.js';

/**
 * Provides the definitions of the context uhfReader as public properties.
 */
export class xAPI_Context_UhfReader extends xAPI_Context {
    verbs = new xAPI_Verbs_UhfReader();

    activities = new xAPI_Activities_UhfReader();

    extensions = new xAPI_Context_UhfReader_Extensions();

    constructor() {
        super("uhfReader");
    }
}