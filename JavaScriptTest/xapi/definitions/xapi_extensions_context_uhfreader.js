import { xAPI_Extensions_Context } from './xapi_extensions_context.js';
import { xAPI_Extension } from '../core/xapi_extension.js';

/**
 * Provides all extensions of the context uhfReader of type context as public properties.
 */
export class xAPI_Extensions_Context_UhfReader extends xAPI_Extensions_Context {

    constructor() {
        super("uhfReader");
    }

    readTid(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "readTid",
                {
                    "en-US": "read Tid"
                },
                {
                    "en-US": "The selected element to read for the transponder."
                }),
            value);
        return this;
    }

    readingHead(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "readingHead",
                {
                    "en-US": "reading head"
                },
                {
                    "en-US": "The head that is selected to read the TID."
                }),
            value);
        return this;
    }

    evaluationType(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "evaluationType",
                {
                    "en-US": "evaluation type"
                },
                {
                    "en-US": "An evaluation can have a type. For example a transponder can be evaluated externally using google by copying the id and pasting it externally."
                }),
            value);
        return this;
    }

    actionName(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "actionName",
                {
                    "en-US": "action name"
                },
                {
                    "en-US": "Name of an UI element action, which the user interacted with. For example the read value checkbox has the name 'readValue'."
                }),
            value);
        return this;
    }
}