import { xAPI_Extensions_Result } from './xapi_extensions_result.js';
import { xAPI_Extension } from '../core/xapi_extension.js';

/**
 * Provides all extensions of the context virtualReality of type result as public properties.
 */
export class xAPI_Extensions_Result_VirtualReality extends xAPI_Extensions_Result {

    constructor() {
        super("virtualReality");
    }

    scale(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "scale",
                {
                    "en-US": "scale",
                    "de-DE": "Skalierung"
                },
                {
                    "en-US": "Scale in scene.",
                    "de-DE": "Skalierung in der Szene."
                }),
            value);
        return this;
    }

    rotation(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "rotation",
                {
                    "en-US": "rotation",
                    "de-DE": "Rotation"
                },
                {
                    "en-US": "Rotation in scene.",
                    "de-DE": "Rotation in der Szene."
                }),
            value);
        return this;
    }

    position(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "position",
                {
                    "en-US": "position",
                    "de-DE": "Position"
                },
                {
                    "en-US": "Position in scene.",
                    "de-DE": "Position in der Szene."
                }),
            value);
        return this;
    }
}