import { xAPI_Verb } from '../core/xapi_verb.js';
import { xAPI_Verbs } from '../core/xapi_verbs.js';

/**
 * Provides the xAPI_Verbs of the context virtualReality as public properties.
 */
export class xAPI_Verbs_VirtualReality extends xAPI_Verbs {
    /**
     * An actor has placed a virtual object in the VR environment.
     * @var {xAPI_Verb} placed
     */
    placed = new xAPI_Verb(
        "virtualReality",
        "placed",
        {
            "en-US": "placed",
            "de-DE": "platzierte"
        },
        {
            "en-US": "An actor has placed a virtual object in the VR environment.",
            "de-DE": "Eine Akteurin hat ein virtuelles Objekt in der VR Umgebung platziert."
        });

    /**
     * An actor has pointed on an VR object.
     * @var {xAPI_Verb} pointed
     */
    pointed = new xAPI_Verb(
        "virtualReality",
        "pointed",
        {
            "en-US": "pointed",
            "de-DE": "zeigte"
        },
        {
            "en-US": "An actor has pointed on an VR object.",
            "de-DE": "Eine Akteurin zeigte auf ein VR Objekt."
        });

    /**
     * An actor interacted with the VR object in the VR environment.
     * @var {xAPI_Verb} interacted
     */
    interacted = new xAPI_Verb(
        "virtualReality",
        "interacted",
        {
            "en-US": "interacted",
            "de-DE": "interagierte"
        },
        {
            "en-US": "An actor interacted with the VR object in the VR environment.",
            "de-DE": "Eine Akteurin interagierte einem VR Objekt in der VR Umgebung."
        });

    /**
     * An actor has touched a virtual object in VR environment.
     * @var {xAPI_Verb} touched
     */
    touched = new xAPI_Verb(
        "virtualReality",
        "touched",
        {
            "en-US": "touched",
            "de-DE": "berührte"
        },
        {
            "en-US": "An actor has touched a virtual object in VR environment.",
            "de-DE": "Eine Akteurin hat ein virtuelles Objekt berührt."
        });

    /**
     * An actor has changed a virtual object's position in VR environment.
     * @var {xAPI_Verb} moved
     */
    moved = new xAPI_Verb(
        "virtualReality",
        "moved",
        {
            "en-US": "moved",
            "de-DE": "bewegte"
        },
        {
            "en-US": "An actor has changed a virtual object's position in VR environment.",
            "de-DE": "Eine Akteurin hat die Position eines virtuellen Objekts verändert."
        });

    /**
     * The actor has pressed a controller button.
     * @var {xAPI_Verb} pressed
     */
    pressed = new xAPI_Verb(
        "virtualReality",
        "pressed",
        {
            "en-US": "pressed",
            "de-DE": "drückte"
        },
        {
            "en-US": "The actor has pressed a controller button.",
            "de-DE": "Eine Akteurin drückte einen Controller Taste."
        });

    /**
     * An actor has changed a virtual object's rotation in VR environment.
     * @var {xAPI_Verb} rotated
     */
    rotated = new xAPI_Verb(
        "virtualReality",
        "rotated",
        {
            "en-US": "rotated",
            "de-DE": "rotierte"
        },
        {
            "en-US": "An actor has changed a virtual object's rotation in VR environment.",
            "de-DE": "Ein Akteur hat die Rotation eines virtuellen Objekts verändert."
        });

    /**
     * An actor has changed her own position in VR environment by teleport.
     * @var {xAPI_Verb} teleported
     */
    teleported = new xAPI_Verb(
        "virtualReality",
        "teleported",
        {
            "en-US": "teleported",
            "de-DE": "teleportierte"
        },
        {
            "en-US": "An actor has changed her own position in VR environment by teleport.",
            "de-DE": "Eine Akteurin hat die eigene Position in der VR Umgebung durch Teleportation verändert."
        });

    /**
     * An actor has removed a virtual object from the VR environment.
     * @var {xAPI_Verb} removed
     */
    removed = new xAPI_Verb(
        "virtualReality",
        "removed",
        {
            "en-US": "removed",
            "de-DE": "entfernte"
        },
        {
            "en-US": "An actor has removed a virtual object from the VR environment.",
            "de-DE": "Ein Akteur hat ein virtuelles Objekt aus der VR Umgebung entfernt."
        });

    /**
     * An actor has created a virtual object in the VR environment.
     * @var {xAPI_Verb} created
     */
    created = new xAPI_Verb(
        "virtualReality",
        "created",
        {
            "en-US": "created",
            "de-DE": "erstellte"
        },
        {
            "en-US": "An actor has created a virtual object in the VR environment.",
            "de-DE": "Eine Akteurin hat ein virtuelles Objekt in der VR Umgebung erstellt."
        });

    /**
     * The actor has released a controller button.
     * @var {xAPI_Verb} released
     */
    released = new xAPI_Verb(
        "virtualReality",
        "released",
        {
            "en-US": "released",
            "de-DE": "löste"
        },
        {
            "en-US": "The actor has released a controller button.",
            "de-DE": "Eine Akteurin löste einen Controller Taste."
        });

    /**
     * An actor has changed a virtual object's scale in VR environment.
     * @var {xAPI_Verb} scaled
     */
    scaled = new xAPI_Verb(
        "virtualReality",
        "scaled",
        {
            "en-US": "scaled",
            "de-DE": "skalierte"
        },
        {
            "en-US": "An actor has changed a virtual object's scale in VR environment.",
            "de-DE": "Eine Akteurin hat die Skalierung eines virtuellen Objekts verändert."
        });

    constructor() {
        super("virtualReality");
    }
}