import { xAPI_Extensions_Context_UhfReader } from './xapi_extensions_context_uhfreader.js';
import { xAPI_Extensions_Result_UhfReader } from './xapi_extensions_result_uhfreader.js';

/**
 * Provides the extensions of the context uhfReader as public properties.
 */
export class xAPI_Context_UhfReader_Extensions {

    constructor() {
    }

    get context() {
        return new xAPI_Extensions_Context_UhfReader();
    }

    get result() {
        return new xAPI_Extensions_Result_UhfReader();
    }
}