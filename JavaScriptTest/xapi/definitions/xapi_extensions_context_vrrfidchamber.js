import { xAPI_Extensions_Context } from './xapi_extensions_context.js';
import { xAPI_Extension } from '../core/xapi_extension.js';

/**
 * Provides all extensions of the context vrRfidChamber of type context as public properties.
 */
export class xAPI_Extensions_Context_VrRfidChamber extends xAPI_Extensions_Context {

    constructor() {
        super("vrRfidChamber");
    }

    session(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "session",
                {
                    "en-US": "session"
                },
                {
                    "en-US": "A session is one execution of the lab from entering the lab to exiting it."
                }),
            value);
        return this;
    }

    orientationmeasure(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "orientationmeasure",
                {
                    "en-US": "orientationmeasure"
                },
                {
                    "en-US": "In this case the best possible orientation of the RFID tag is measured."
                }),
            value);
        return this;
    }

    energymeasure(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "energymeasure",
                {
                    "en-US": "energymeasure"
                },
                {
                    "en-US": "In this case the energy of RFID tag is measured."
                }),
            value);
        return this;
    }

    readingrangemeasure(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "readingrangemeasure",
                {
                    "en-US": "readingrangemeasure"
                },
                {
                    "en-US": "In this case the reading range of transponder is measured."
                }),
            value);
        return this;
    }

    experimentmode(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "experimentmode",
                {
                    "en-US": "experimentmode"
                },
                {
                    "en-US": "The experimentmode in which the experiment is carried. Can be energy measurement or reading range measurement"
                }),
            value);
        return this;
    }
}