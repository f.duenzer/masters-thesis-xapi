import { xAPI_Extensions_Activity } from './xapi_extensions_activity.js';
import { xAPI_Extension } from '../core/xapi_extension.js';

/**
 * Provides all extensions of the context systemControl of type activity as public properties.
 */
export class xAPI_Extensions_Activity_SystemControl extends xAPI_Extensions_Activity {

    constructor() {
        super("systemControl");
    }

    name(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "name",
                {
                    "en-US": "System control activity name",
                    "de-DE": "System Control Activity Name"
                },
                {
                    "en-US": "Name of a system control activity. Must be a string.",
                    "de-DE": "Namen von einer Aktivität von des Systemkontrollers. Muss ein String sein."
                }),
            value);
        return this;
    }
}