import { xAPI_Extensions_Result_VirtualReality } from './xapi_extensions_result_virtualreality.js';
import { xAPI_Extensions_Activity_VirtualReality } from './xapi_extensions_activity_virtualreality.js';

/**
 * Provides the extensions of the context virtualReality as public properties.
 */
export class xAPI_Context_VirtualReality_Extensions {

    constructor() {
    }

    get result() {
        return new xAPI_Extensions_Result_VirtualReality();
    }

    get activity() {
        return new xAPI_Extensions_Activity_VirtualReality();
    }
}