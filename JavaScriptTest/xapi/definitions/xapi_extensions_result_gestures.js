import { xAPI_Extensions_Result } from './xapi_extensions_result.js';
import { xAPI_Extension } from '../core/xapi_extension.js';

/**
 * Provides all extensions of the context gestures of type result as public properties.
 */
export class xAPI_Extensions_Result_Gestures extends xAPI_Extensions_Result {

    constructor() {
        super("gestures");
    }

    primaryHand(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "primaryHand",
                {
                    "en-US": "primary hand",
                    "de-DE": "Primärhand"
                },
                {
                    "en-US": "Setting, which hand is currently the primary hand.",
                    "de-DE": "Einstellung, welche Hand die Primärhand ist."
                }),
            value);
        return this;
    }

    numberOfGestures(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "numberOfGestures",
                {
                    "en-US": "number of gestures",
                    "de-DE": "Gestenanzahl"
                },
                {
                    "en-US": "The number of actors gestures. Has to be an integer.",
                    "de-DE": "Die Anzahl an Gesten, die die Akteurin tätigt. Muss ein Integer sein."
                }),
            value);
        return this;
    }
}