import { xAPI_Activity } from '../core/xapi_activity.js';
import { xAPI_Activities } from '../core/xapi_activities.js';

/**
 * Provides the xAPI_Activities of the context lms as public properties.
 */
export class xAPI_Activities_Lms extends xAPI_Activities {
    /**
     * A course resource as a single file (e.g. PDF).
     * @var {xAPI_Activity} resource
     */
    resource = new xAPI_Activity(
        "lms",
        "resource",
        {
            "en-US": "Resource",
            "de-DE": "Ressource"
        },
        {
            "en-US": "A course resource as a single file (e.g. PDF).",
            "de-DE": "Eine Kursressource als einzelne Datei (z. B. PDF)."
        });

    /**
     * A chapter of a book.
     * @var {xAPI_Activity} book_chapter
     */
    book_chapter = new xAPI_Activity(
        "lms",
        "book_chapter",
        {
            "en-US": "Book Chapter",
            "de-DE": "Buchkapitel"
        },
        {
            "en-US": "A chapter of a book.",
            "de-DE": "Ein Kapitel in einem Buch."
        });

    /**
     * A course within an LMS. Contains learning materials and activities
     * @var {xAPI_Activity} course
     */
    course = new xAPI_Activity(
        "lms",
        "course",
        {
            "en-US": "Course",
            "de-DE": "Kurs"
        },
        {
            "en-US": "A course within an LMS. Contains learning materials and activities",
            "de-DE": "Ein Kurs in einem LMS. Enthält Lernmaterialien und -aktivitäten"
        });

    /**
     * An assignment object. Provides the students with a task description and further needed materials and allows to upload a submission.
     * @var {xAPI_Activity} assign
     */
    assign = new xAPI_Activity(
        "lms",
        "assign",
        {
            "en-US": "Assignment",
            "de-DE": "Aufgabe"
        },
        {
            "en-US": "An assignment object. Provides the students with a task description and further needed materials and allows to upload a submission.",
            "de-DE": "Eine Aufgabe. Gibt eine Aufgabenstellung vor und weiteres benötigtes Material und ermöglicht das Hochladen einer Abgabe."
        });

    /**
     * A page within a course displaying custom content like text, images, videos and more.
     * @var {xAPI_Activity} page
     */
    page = new xAPI_Activity(
        "lms",
        "page",
        {
            "en-US": "Page",
            "de-DE": "Seite"
        },
        {
            "en-US": "A page within a course displaying custom content like text, images, videos and more.",
            "de-DE": "Eine Seite im Kurs, die verschiedene Inhalte wie Text, Bilder, Videos und andere darstellt."
        });

    /**
     * A book, containing multiple texts and images, structured as chapters.
     * @var {xAPI_Activity} book
     */
    book = new xAPI_Activity(
        "lms",
        "book",
        {
            "en-US": "Book",
            "de-DE": "Buch"
        },
        {
            "en-US": "A book, containing multiple texts and images, structured as chapters.",
            "de-DE": "Ein Buch welches Texte und Bilder in einzelnen Kapiteln darstellt."
        });

    /**
     * A forum for discussion.
     * @var {xAPI_Activity} forum
     */
    forum = new xAPI_Activity(
        "lms",
        "forum",
        {
            "en-US": "Forum",
            "de-DE": "Forum"
        },
        {
            "en-US": "A forum for discussion.",
            "de-DE": "Ein Forum für Diskussionen."
        });

    /**
     * A Learning Management System (LMS)
     * @var {xAPI_Activity} lms
     */
    lms = new xAPI_Activity(
        "lms",
        "lms",
        {
            "en-US": "LMS",
            "de-DE": "LMS"
        },
        {
            "en-US": "A Learning Management System (LMS)",
            "de-DE": "Ein Lernmanagementsystem (LMS)"
        });

    constructor() {
        super("lms");
    }
}