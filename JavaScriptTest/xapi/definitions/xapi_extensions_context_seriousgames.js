import { xAPI_Extensions_Context } from './xapi_extensions_context.js';
import { xAPI_Extension } from '../core/xapi_extension.js';

/**
 * Provides all extensions of the context seriousgames of type context as public properties.
 */
export class xAPI_Extensions_Context_Seriousgames extends xAPI_Extensions_Context {

    constructor() {
        super("seriousgames");
    }

    game(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "game",
                {
                    "en-US": "game",
                    "de-DE": "Spiel"
                },
                {
                    "en-US": "The identifier for the game in which the event happened. Can be a string or integer.",
                    "de-DE": "Der Identifikator dafür welches Spiel gespielt wird. Kann ein String oder Integer sein."
                }),
            value);
        return this;
    }

    level(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "level",
                {
                    "en-US": "level",
                    "de-DE": "Level"
                },
                {
                    "en-US": "The level in which or for which something happened. Can be an integer or a string.",
                    "de-DE": "Das Level in welchem ein Ereignis stattfand. Kann ein String oder Integer sein."
                }),
            value);
        return this;
    }

    numberOfPlayers(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "numberOfPlayers",
                {
                    "en-US": "number of players",
                    "de-DE": "Spieleranzahl"
                },
                {
                    "en-US": "The number of players participating at the game. Has to be an integer.",
                    "de-DE": "Die Anzahl an Spielern welche am Spiel teilnehmen. Muss ein Integer sein."
                }),
            value);
        return this;
    }

    gamemode(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "gamemode",
                {
                    "en-US": "gamemode",
                    "de-DE": "Spielmodus"
                },
                {
                    "en-US": "The gamemode in which the game is played. Can be a string or integer.",
                    "de-DE": "Der Spielmodus in welchem das Spiel gespielt wird. Kann ein String oder ein Integer sein."
                }),
            value);
        return this;
    }
}