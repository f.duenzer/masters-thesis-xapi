import { xAPI_Extensions_Result } from './xapi_extensions_result.js';
import { xAPI_Extension } from '../core/xapi_extension.js';

/**
 * Provides all extensions of the context tagformance of type result as public properties.
 */
export class xAPI_Extensions_Result_Tagformance extends xAPI_Extensions_Result {

    constructor() {
        super("tagformance");
    }

    empty(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "empty",
                {
                    "en-US": "empty"
                },
                {
                    "en-US": "Boolean value which is true when something is empty."
                }),
            value);
        return this;
    }

    duration(value) {
        this.add(new xAPI_Extension(
                this.context,
                this.extensionType,
                "duration",
                {
                    "en-US": "duration"
                },
                {
                    "en-US": "The time taken to complete the experiment. This is captured from the system time of the local machine."
                }),
            value);
        return this;
    }
}