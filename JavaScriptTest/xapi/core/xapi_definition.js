export class xAPI_Definition {
    #context;
    #key;
    #names;
    #descriptions;

    constructor(context, key, names, descriptions) {
        if (new.target === xAPI_Definition) {
            throw new TypeError('Cannot construct xAPI_Definition instances directly.');
        }
        this.#context = context;
        this.#key = key;
        this.#names = names;
        this.#descriptions = descriptions;
    }

    get context() {
        return this.#context;
    }

    get key() {
        return this.#key;
    }

    get names() {
        return this.#names;
    }

    get descriptions() {
        return this.#descriptions;
    }

    getName(language) {
        if (!(language in this.#names)) {
            throw new Error(`There is no name for the language ${language}.`);
        }
        return this.#names[language];
    }

    getDescription(language) {
        if (!(language in this.#descriptions)) {
            throw new Error(`There is no description for the language ${language}.`);
        }
        return this.#descriptions[language];
    }

    getNameDescription(language) {
        let namesContainLang = language in this.#names;
        let descsContainLang = language in this.#descriptions;

        if (!namesContainLang) {
            if (!descsContainLang) {
                throw new Error(`There is no name and no description for the language ${language}.`);
            }
            throw new Error(`There is no name for the language ${language}.`);
        } else if (!descsContainLang) {
            throw new Error(`There is no description for the language ${language}.`);
        }
        return { key: this.#names[language], value: this.#descriptions[language] };
    }

    getLanguages() {
        return [...Object.keys(this.#names), ...Object.keys(this.#descriptions).filter(l => !this.#names.includes(l))];
    }

    getPath() {
        return this.#key;
    }

    createValidId(uri) {
        let path = this.getPath();
        if (uri[uri.length - 1] == '/') {
            uri = uri.substring(0, uri.length - 1);
        }
        if (path[0] == '/') {
            path = path.substring(1);
        }
        return `${uri}/${path}`;
    }

    static #dictToString(dict) {
        return dict.join(',');
    }

    toString() {
        return `[${typeof (this)}: name=[${dictToString(this.#names)}], description=[${dictToString(this.#descriptions)}]]`;
    }
}