import { createRequire } from 'module';
import { xAPI_Extensions_Activity } from '../definitions/xapi_extensions_activity.js';
import { xAPI_Extensions_Context } from '../definitions/xapi_extensions_context.js';
import { xAPI_Extensions_Result } from '../definitions/xapi_extensions_result.js';
const require = createRequire(import.meta.url);
const TinCan = require('tincanjs');

export class xAPI_Statements {
    static #createAgent(actor) {
        return new TinCan.Agent(
            {
                name: actor.name,
                mbox: "mailto:" + actor.email
            }
        );
    }

    static #createVerb(uri, verb) {
        return new TinCan.Verb(
            {
                id: verb.createValidId(uri),
                display: verb.names
            }
        );
    }

    static #createActivity(uri, activity, extensions = null) {
        var a = new TinCan.Activity(
            {
                id: activity.createValidId(uri),
                definition: this.toTinCanActivityDefinition(activity)
            }
        );
        a.definition.extensions = (extensions == null) ? null : this.toTinCanExtensions(extensions, uri);
        return a;
    }

    static splitExtensions(extensions) {
        if (extensions == null) {
            return { activityExtensions: null, contextExtensions: null, resultExtensions: null };
        }

        let activityExtensions = new xAPI_Extensions_Activity();
        let contextExtensions = new xAPI_Extensions_Context();
        let resultExtensions = new xAPI_Extensions_Result();

        for (let i = 0; i < extensions.extensions.length; ++i) {
            let ext = extensions.extensions[i];
            if (activityExtensions.extensionType == ext.key.extensionType)
                activityExtensions.addPair(ext);
            else if (contextExtensions.extensionType == ext.key.extensionType)
                contextExtensions.addPair(ext);
            else if (resultExtensions.extensionType == ext.key.extensionType)
                resultExtensions.addPair(ext);
        }

        return { activityExtensions: activityExtensions, contextExtensions: contextExtensions, resultExtensions: resultExtensions };
    }

    static createStatement(
        uri,
        actor,
        verb,
        activity,
        activityExtensions = null,
        contextExtensions = null,
        resultExtensions = null,
        score = null,
        completion = true,
        success = true,
        instructor = null) {
        return new TinCan.Statement(
            {
                actor: this.#createAgent(actor),
                verb: this.#createVerb(uri, verb),
                target: this.#createActivity(uri, activity, activityExtensions),
                context: this.#createContext(uri, contextExtensions, instructor),
                result: this.createResult(uri, score, completion, success, resultExtensions),
                timestamp: Date.now()
            }
        );
    }

    static #createContext(uri, extensions = null, instructor = null) {
        return new TinCan.Context(
            {
                instructor: (instructor == null) ? null : this.#createAgent(instructor),
                extensions: (extensions == null) ? null : this.toTinCanExtensions(extensions, uri)
            }
        );
    }

    static createResult(uri, score = null, completion = null, success = null, extensions = null) {
        return new TinCan.Result(
            {
                score: score,
                completion: completion,
                success: success,
                extensions: (extensions == null) ? null : this.toTinCanExtensions(extensions, uri)
            }
        );
    }

    static createStatementExt(uri, actor, verb, activity, extensions = null, score = null, completion = true, success = true) {
        let allExtensions = this.splitExtensions(extensions);
        return this.createStatement(uri, actor, verb, activity, allExtensions.activityExtensions, allExtensions.contextExtensions,
            allExtensions.resultExtensions, score, completion, success);
    }

    static createStatementBody(uri, actor, body) {
        let result = this.createResult(
            uri,
            new TinCan.Score(
                {
                    max: body.maxScore,
                    min: body.minScore,
                    raw: body.rawScore,
                    scaled: body.scaledScore
                }
            ),
            body.completion,
            body.success,
            body.resultExtensions
        );
        return new TinCan.Statement(
            {
                actor: this.#createAgent(actor),
                verb: this.#createVerb(uri, body.verb),
                target: this.#createActivity(uri, body.activity, body.activityExtensions),
                result: result,
                context: this.#createContext(uri, body.contextExtensions),
                timestamp: body.timestamp
            }
        );
    }

    static toTinCanExtensions(extensions, uri) {
        let object = {};
        for (let i = 0; i < extensions.extensions.length; ++i) {
            var ext = extensions.extensions[i];
            var extension = ext.key;
            var id = extension.createValidId(uri);
            object[id] = ext.value.toString();
        }
        return object;
    }

    static toTinCanActivityDefinition(definition) {
        return new TinCan.ActivityDefinition(
            {
                name: definition.names,
                description: definition.descriptions
            }
        );
    }
}