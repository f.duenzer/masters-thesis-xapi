export class xAPI_Verbs {
    #contextName

    constructor(contextName) {
        if (new.target === xAPI_Verbs) {
            throw new TypeError('Cannot construct xAPI_Verbs instances directly.');
        }
        this.#contextName = contextName;
    }

    get contextName() {
        return this.#contextName;
    }
}