'use strict';
import { xAPI_Actor } from './xapi/core/xapi_actor.js';
import { xAPI_Statements } from './xapi/core/xapi_statements.js';
import { xAPI_Definitions } from './xapi/definitions/xapi_definitions.js';
import { xAPI_Activities_Lms } from './xapi/definitions/xapi_activities_lms.js';
import { xAPI_Activities_Tagformance } from './xapi/definitions/xapi_activities_tagformance.js';
import { xAPI_Activities_Observation } from './xapi/definitions/xapi_activities_observation.js';
import { xAPI_Verbs_ProjectJupyter } from './xapi/definitions/xapi_verbs_projectjupyter.js';
import { xAPI_Verbs_UhfReader } from './xapi/definitions/xapi_verbs_uhfreader.js';
import { xAPI_Verbs_Gestures } from './xapi/definitions/xapi_verbs_gestures.js';
import { xAPI_Context_EyeTracking_Extensions } from './xapi/definitions/xapi_context_eyetracking_extensions.js';
import { xAPI_Context_Gestures_Extensions } from './xapi/definitions/xapi_context_gestures_extensions.js';
import { createRequire } from 'module';
import { xAPI_Activities } from './xapi/core/xapi_activities.js';
import { xAPI_Definition } from './xapi/core/xapi_definition.js';
const require = createRequire(import.meta.url);
var assert = require('assert');
var TinCan = require('tincanjs');

var test1 = "A chapter of a book." == new xAPI_Activities_Lms().book_chapter.descriptions["en-US"]
    && "tagformance" == new xAPI_Activities_Tagformance().experiment.context
    && "plant das (eigene) Vorgehen und setzt Schwerpunkte" == new xAPI_Activities_Observation().subscores.plant_das_eigene_Vorgehen_und_setzt_Schwerpunkte.names["de-DE"];

var test2 = "Indicates, that an actor run some programming context. Typically a Jupyter Notebook Code Cell." == new xAPI_Verbs_ProjectJupyter().run.descriptions["en-US"]
    && "uhfReader" == new xAPI_Verbs_UhfReader().contextName
    && "shaked" == new xAPI_Verbs_Gestures().shaked.names['en-US'];

var exts = new xAPI_Context_EyeTracking_Extensions().result.duration(5).numberOfBlinking(2);
var test3 = exts.extensions.length == 2
    && exts.extensions[0].value == 5
    && "Hand" == new xAPI_Context_Gestures_Extensions().activity.hand('right').extensions[0].key.names["de-DE"];

var test4 = xAPI_Definitions.virtualReality.activities.touchpad != null
    && "German" == xAPI_Definitions.systemControl.extensions.result.language("German").extensions[0].value
    && "An actor dragged and pulled an object." == xAPI_Definitions.vrRfidChamber.verbs.dragPull.descriptions["en-US"];

var statement = xAPI_Statements.createStatementExt(
    "http://example.com",
    new xAPI_Actor("Name", "Email@example.com"),
    xAPI_Definitions.generic.verbs.clicked,
    xAPI_Definitions.generic.activities.mouse,
    xAPI_Definitions.generic.extensions.activity.mouseButton("left").mousePosition("100,100"),
    new TinCan.Score(), true, true
);
exts = statement.target.definition.extensions;
var props = Object.getOwnPropertyNames(exts);
var test5 = "mailto:Email@example.com" == statement.actor.mbox
    && "http://example.com/generic/verbs/clicked" == statement.verb.id
    && "A mouse to use UI or interact with the game in another way." == statement.target.definition.description["en-US"]
    && statement.result.success
    && "{\"http://example.com/generic/extensions/activity/mouseButton\": \"left\","
    + " \"http://example.com/generic/extensions/activity/mousePosition\": \"100,100\"}"
    == "{\"" + props[0] + "\": \"" + exts[props[0]] + "\", \"" + props[1] + "\": \"" + exts[props[1]] + "\"}";

console.log("Test Activities: " + (test1 ? "OK" : "Failed"));
console.log("Test Verbs: " + (test2 ? "OK" : "Failed"));
console.log("Test Extensions: " + (test3 ? "OK" : "Failed"));
console.log("Test Definitions: " + (test4 ? "OK" : "Failed"));
console.log("Test Statement: " + (test5 ? "OK" : "Failed"));

process.stdin.read();