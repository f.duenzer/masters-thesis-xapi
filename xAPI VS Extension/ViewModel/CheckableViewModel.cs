﻿using Microsoft.Toolkit.Mvvm.ComponentModel;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using xAPI_Definitions_Parser.IO;

namespace xAPI_VS_Extension.ViewModel {
    public class CheckableViewModel : ObservableObject {
        private bool? isChecked;
        private bool isExpanded;
        private string name;
        private ObservableCollection<CheckableViewModel> children;
        private CheckableViewModel parent;

        private bool updating;

        public bool? IsChecked {
            get => isChecked;
            set {
                // update lock so that children being changed don't change this as their parent again and vice-versa
                if (!updating) {
                    updating = true;
                    if (SetProperty(ref isChecked, value)) {
                        // change children, if item was checked or unchecked
                        if (isChecked != null) {
                            foreach (var child in children) {
                                child.IsChecked = isChecked;
                            }
                        }
                        if (parent != null) {
                            // change parents based on all of this item's siblings
                            // true/false if all siblings are the same, otherwise null
                            bool? parentChecked = isChecked;
                            if (parentChecked != null) {
                                foreach (var sibling in parent.children) {
                                    if (sibling.isChecked != parentChecked) {
                                        parentChecked = null;
                                        break;
                                    }
                                }
                            }
                            parent.IsChecked = parentChecked;
                        }
                    }
                    updating = false;
                }
            }
        }

        public bool IsExpanded {
            get => isExpanded;
            set => SetProperty(ref isExpanded, value);
        }

        public string Name {
            get => name;
            set => SetProperty(ref name, value);
        }

        public ObservableCollection<CheckableViewModel> Children {
            get => children;
            set => SetProperty(ref children, value);
        }

        public CheckableViewModel Parent {
            get => parent;
            set => SetProperty(ref parent, value);
        }

        public CheckableViewModel(string name, bool isExpanded = false) {
            children = new ObservableCollection<CheckableViewModel>();
            isChecked = true;
            this.isExpanded = isExpanded;
            this.name = name;
        }

        public CheckableViewModel(Context context) : this(context.Name) {
            if (context.Activities.Count > 0) {
                AddChild(new CheckableViewModel(MainViewModel.ActivitiesItemString, context.Activities));
            }

            if (context.Extensions.Count > 0) {
                var extensionsVM = new CheckableViewModel(MainViewModel.ExtensionsItemString);
                foreach (var kvp in context.Extensions) {
                    if (kvp.Value.Count > 0) {
                        extensionsVM.AddChild(new CheckableViewModel(kvp.Key, kvp.Value));
                    }
                }
                AddChild(extensionsVM);
            }

            if (context.Verbs.Count > 0) {
                AddChild(new CheckableViewModel(MainViewModel.VerbsItemString, context.Verbs));
            }
        }

        public CheckableViewModel(string name, List<FileMeta> files) : this(name) {
            foreach (var file in files) {
                if (file.IsDirectory) {
                    AddChild(new CheckableViewModel(file.Name, file.DirContent));
                } else {
                    AddChild(new CheckableViewModel(file.Name));
                }
            }
        }

        public void AddChild(CheckableViewModel child) {
            Children.Add(child);
            child.Parent = this;
        }
    }
}