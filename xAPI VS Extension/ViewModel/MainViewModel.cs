﻿using EnvDTE;
using Microsoft.Toolkit.Mvvm.ComponentModel;
using Microsoft.Toolkit.Mvvm.Input;
using Microsoft.VisualStudio.Shell;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using System.Xml;
using xAPI_Definitions_Parser;
using xAPI_Definitions_Parser.Code.CSharp;
using xAPI_Definitions_Parser.Definitions;
using xAPI_Definitions_Parser.IO;
using xAPI_WebAPI_Methods;
using Task = System.Threading.Tasks.Task;

namespace xAPI_VS_Extension.ViewModel {
    public enum OutputType {
        Code,
        DLL
    }

    public class MainViewModel : ObservableObject {
        internal const string ActivitiesItemString = "activities";
        internal const string ExtensionsItemString = "extensions";
        internal const string VerbsItemString = "verbs";

        private ObservableCollection<TreeViewModel> trees = new ObservableCollection<TreeViewModel>();

        private SourceType sourceType;
        private string path;
        private string projectId;
        private string privateKey;
        private string branch;

        private CodeLanguage language;
        private Dictionary<CodeLanguage, ObservableCollection<OutputType>> outputTypes;
        private OutputType outputType;
        private string project;
        private string destination;

        private bool isWorking;
        private string status;

        public ObservableCollection<TreeViewModel> Trees {
            get => trees;
            set => SetProperty(ref trees, value);
        }

        public SourceType SourceType {
            get => sourceType;
            set => SetProperty(ref sourceType, value);
        }

        public IDictionary<SourceType, string> SourceTypes {
            get => EnumMethods.GetEnumDict<SourceType>();
        }

        public string Path {
            get => path;
            set => SetProperty(ref path, value);
        }

        public string ProjectId {
            get => projectId;
            set => SetProperty(ref projectId, value);
        }

        public string PrivateKey{
            get => privateKey;
            set => SetProperty(ref privateKey, value);
        }

        public string Branch {
            get => branch;
            set => SetProperty(ref branch, value);
        }

        public CodeLanguage Language {
            get => language;
            set {
                if (SetProperty(ref language, value)) {
                    Project = string.Empty;
                    Destination = string.Empty;
                }
                outputType = OutputType.Code;
                OnPropertyChanged("OutputTypes");
                OnPropertyChanged("OutputType");
            }
        }

        public IDictionary<CodeLanguage, string> Languages {
            get => EnumMethods.GetEnumDict<CodeLanguage>();
        }

        public OutputType OutputType {
            get => outputType;
            set => SetProperty(ref outputType, value);
        }

        public ObservableCollection<OutputType> OutputTypes {
            get => outputTypes[Language];
        }

        public string Project {
            get => project;
            set {
                SetProperty(ref project, value);
                UpdateDest();
            }
        }

        public string Destination {
            get => destination;
            set => SetProperty(ref destination, value);
        }

        public bool IsWorking {
            get => isWorking;
            set => SetProperty(ref isWorking, value);
        }

        public string Status {
            get => status;
            set => SetProperty(ref status, value);
        }

        public ICommand AddSrcBtnCmd { get; set; }
        public ICommand AddToProjBtnCmd { get; set; }

        public MainViewModel() {
            AddSrcBtnCmd = new RelayCommand(AddSource);
            AddToProjBtnCmd = new RelayCommand(AddToProject);
            InitOutputTypes();
            IsWorking = true;
            Task.Run(InitTree);
            //InitTree();
            /*var asd = HttpMethods.GetGitlabContexts("definitions", "16349193", "glpat-x-z-38tnYLDYeNnTaP5Z");
            var code = HttpMethods.GetGitlabCode(CodeLanguage.CSharp, "xAPI", null, null, "definitions", "16349193", "glpat-x-z-38tnYLDYeNnTaP5Z");
            var comp = HttpMethods.GetGitlabCompile(CodeLanguage.CSharp, "xAPI", null, null, null, "xAPI.dll", null, null, "definitions", "16349193", "glpat-x-z-38tnYLDYeNnTaP5Z");*/

            /*var asd = HttpMethods.GetGitlabContexts("definitions", "16349193", "glpat-x-z-38tnYLDYeNnTaP5Z");
            var das = HttpMethods.GetCode(asd, CodeLanguage.CSharp, "xAPI");*/

            /*var contexts = HttpMethods.GetGitlabContexts("definitions", "16349193", "glpat-x-z-38tnYLDYeNnTaP5Z");
            var code = HttpMethods.GetCode(contexts, CodeLanguage.CSharp, "xAPI");
            var compile = HttpMethods.GetCompile(contexts, CodeLanguage.CSharp, "xAPI", null, null, null, "xAPI.dll", null, null);
            int i = 0;*/
            /*var asd = HttpMethods.GetLocalContexts(@"D:\test\xapi-master\definitions");
            var asd2 = HttpMethods.GetLocalCode(CodeLanguage.CSharp, "xAPI", null, null, @"D:\test\xapi-master\definitions");
            var asd3 = HttpMethods.GetCode(asd, CodeLanguage.CSharp, "xAPI");
            int i = 0;*/
        }

        private void InitOutputTypes() {
            outputTypes = new Dictionary<CodeLanguage, ObservableCollection<OutputType>>();
            outputTypes.Add(CodeLanguage.CSharp, new ObservableCollection<OutputType>() { OutputType.Code, OutputType.DLL });
            outputTypes.Add(CodeLanguage.JavaScript, new ObservableCollection<OutputType>() { OutputType.Code });
            outputTypes.Add(CodeLanguage.Python, new ObservableCollection<OutputType>() { OutputType.Code });
        }

        public void InitTree() {
            var learntech = "learntech-rwth/xAPI";
            var treeViewModel = new TreeViewModel(learntech);
            try {
                var contexts = HttpMethods.GetAPIList();
                var sourceVM = new SourceViewModel(null, learntech, path, projectId, privateKey, branch, contexts);

                Application.Current.Dispatcher.Invoke(new Action(() => {
                    treeViewModel.Sources.Add(sourceVM);
                    trees.Add(treeViewModel);
                }));
            } catch (Exception ex) {
                try {
                    var manager = Generator.CreateManager(SourceType.GitLab, "16349193", null, null);
                    var contexts = manager.ListContextsFull("definitions");

                    var sourceVM = new SourceViewModel(SourceType.GitLab, "learntech-rwth/xAPI", "definitions", "16349193", null, null, contexts);

                    Application.Current.Dispatcher.Invoke(new Action(() => {
                        treeViewModel.Sources.Add(sourceVM);
                        trees.Add(treeViewModel);
                    }));
                } catch (Exception ex2) { }
            }
            Application.Current.Dispatcher.Invoke(new Action(() => IsWorking = false));
        }

        public void RemoveTree(TreeViewModel tree) {
            trees.Remove(tree);
        }

        private void AddSource() {
            if (!IsWorking) {
                if (IsValidSource()) {
                    IsWorking = true;
                    var task = Task.Run(AddSourceRun);
                } else {
                    MessageBox.Show("A valid source path must be set.");
                }
            }
        }

        private void AddSourceRun() {
            if (IsValidSource()) {
                var manager = Generator.CreateManager(sourceType, projectId, privateKey, branch);
                var contexts = manager.ListContextsFull(path);

                var name = string.Empty;
                if (sourceType == SourceType.Local) {
                    name = path;
                } else if (sourceType == SourceType.GitLab) {
                    name = $"{projectId}:{(string.IsNullOrEmpty(branch) ? "" : $"{branch}:")}{path}";
                }

                var source = new SourceViewModel(sourceType, name, path, projectId, privateKey, branch, contexts);
                if (source.Children.Count > 0) {
                    var treeViewModel = new TreeViewModel(name);
                    treeViewModel.Sources.Add(source);
                    Application.Current.Dispatcher.Invoke(new Action(() => Trees.Add(treeViewModel)));
                }
            }

            Application.Current.Dispatcher.Invoke(new Action(() => IsWorking = false));
        }

        private bool IsValidSource() {
            if (sourceType == SourceType.Local) {
                return Directory.Exists(path);
            } else if (sourceType == SourceType.GitLab) {
                var tree = HttpHelper.GetGitLabTree(projectId, string.IsNullOrEmpty(branch) ? "master" : branch, path, privateKey);
                return tree != null;
            }
            return false;
        }

        private void AddToProject() {
            if (!IsWorking) {
                if (IsDuplicateContext()) {
                    MessageBox.Show("There can not be two contexts with the same name.");
                } else if (IsValidProject()) {
                    IsWorking = true;
                    var task = Task.Run(AddToProjectRun);
                } else {
                    MessageBox.Show("A project must be selected.");
                }
            }
        }

        private bool IsDuplicateContext() {
            var contextNames = new Collection<string>();

            foreach (var tree in trees) {
                foreach (var context in tree.Sources[0].Children) {
                    if (context.IsChecked != false) {
                        if (!contextNames.Contains(context.Name)) {
                            contextNames.Add(context.Name);
                        } else {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        private void AddToProjectRun() {
            Dispatcher.CurrentDispatcher.VerifyAccess();

            if (IsValidProject()) {
                var proj = GetProject(project);
                if (proj != null) {
                    var ns = "xAPI";//proj.Name.Replace(' ', '_');
                    var projPath = System.IO.Path.GetDirectoryName(proj.FullName);
                    /*var relativeFolder = destination.Remove(0, projPath.Length);
                    if (relativeFolder.StartsWith("\\")) {
                        relativeFolder = relativeFolder.Substring(1);
                    }

                    if (!String.IsNullOrEmpty(relativeFolder)) {
                        foreach (var dir in relativeFolder.Split('\\')) {
                            ns += "." + dir.Replace(' ', '_');
                        }
                    }*/

                    List<xAPIContext> contexts = new List<xAPIContext>();

                    try {
                        if (trees.Count > 0
                            && trees[0].Sources.Count > 0
                            && trees[0].Sources[0].Type == null) {
                            Status = $"Receiving {trees[0].Sources[0].Name} files";
                            var contextsBase = HttpMethods.GetAPIDefinitions(trees[0].Sources[0].ToContexts());
                            var contextsBaseDeserialized = xAPI_Definitions_Parser.Deserialization.Deserializer.DeserializeContexts(contextsBase);
                            contexts.AddRange(contextsBaseDeserialized);
                        }
                    } catch (Exception ex) {
                    }

                    var tasksRead = new List<Task<List<xAPIContext>>>();

                    foreach (var tree in trees) {
                        foreach (var source in tree.Sources) {
                            if (source.Type != null) {
                                tasksRead.Add(AddCustomSourceAsync(source));
                            }
                        }
                    }

                    Task.WhenAll(tasksRead).Wait();

                    xAPI_Definitions_Parser.Code.CodeGenerator generator = Generator.CreateGenerator(language, ns);
                    var writer = new FileManager();

                    foreach (var task in tasksRead) {
                        contexts.AddRange(task.Result);
                    }

                    var coreFiles = generator.GenerateCoreFiles();
                    var files = generator.GenerateContexts(contexts);

                    var dest = Language == CodeLanguage.CSharp ? destination : destination.ToLower();

                    if (outputType == OutputType.Code) {
                        var tasksWrite = new List<Task>();
                        var corePath = System.IO.Path.Combine(dest, generator.CoreDirName);
                        var defPath = System.IO.Path.Combine(dest, generator.DefinitionsDirName);

                        tasksWrite.Add(writer.WriteFilesAsync(corePath, coreFiles));
                        tasksWrite.Add(writer.WriteFilesAsync(defPath, files));

                        Task.WhenAll(tasksWrite).Wait();

                        proj.ProjectItems.AddFromDirectory(dest);
                        /*proj.ProjectItems.AddFromDirectory(corePath);
                        proj.ProjectItems.AddFromDirectory(defPath);*/
                        if (!proj.Saved) {
                            proj.Save();
                        }
                    } else {
                        var args = Generator.CreateCompilationArguments(language, System.IO.Path.Combine(dest, "xAPI.dll"), "5.0");
                        var compiled = generator.Compile(coreFiles, files, args);
                        if (compiled.Success && compiled is CSharpCompilationResult res) {
                            if (!Directory.Exists(dest)) {
                                Directory.CreateDirectory(dest);
                            }
                            File.WriteAllBytes(System.IO.Path.Combine(dest, "xAPI.dll"), res.Compiled);
                            File.WriteAllText(System.IO.Path.Combine(dest, res.StatementsFile.Name), res.StatementsFile.FileContent);
                            AddProjectReference(proj, System.IO.Path.Combine(dest, "xAPI.dll"));
                            proj.ProjectItems.AddFromFile(System.IO.Path.Combine(dest, res.StatementsFile.Name));
                            if (!proj.Saved) {
                                proj.Save();
                            }
                        }
                    }

                    Status = null;
                }
            }

            Application.Current.Dispatcher.Invoke(new Action(() => {
                IsWorking = false;
                MessageBox.Show("Code generation complete.");
            }));
        }

        private async Task<List<xAPIContext>> AddCustomSourceAsync(SourceViewModel source) {
            var reader = Generator.CreateManager(source.Type, source.ProjectId, source.PrivateKey, source.Branch);
            reader.OnLog += (str => { Status = str; });
            var contextsSource = await reader.ReadContextsAsync(source.ToContexts());
            return xAPI_Definitions_Parser.Deserialization.Deserializer.DeserializeContexts(contextsSource);
        }

        private void AddProjectReference(Project proj, string refPath) {
            XmlDocument doc = new XmlDocument();
            doc.Load(proj.FullName);

            var path = doc.CreateNode(XmlNodeType.Element, "HintPath", doc.DocumentElement.NamespaceURI);
            path.InnerText = refPath;
            var reference = doc.CreateNode(XmlNodeType.Element, "Reference", doc.DocumentElement.NamespaceURI);
            reference.AppendChild(path);
            var include = doc.CreateAttribute("Include");
            include.Value = System.IO.Path.GetFileName(refPath);
            reference.Attributes.Append(include);
            var items = doc.CreateNode(XmlNodeType.Element, "ItemGroup", doc.DocumentElement.NamespaceURI);
            items.AppendChild(reference);

            doc.FirstChild.AppendChild(items);

            doc.Save(proj.FullName);
        }

        private void UpdateDest() {
            Dispatcher.CurrentDispatcher.VerifyAccess();
            var proj = GetProject(project);
            if (proj != null) {
                Destination = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(proj.FullName), "xAPI");
            }
        }

        private Project GetProject(string name) {
            Dispatcher.CurrentDispatcher.VerifyAccess();
            try {
                var dte = (DTE)Package.GetGlobalService(typeof(DTE));
                foreach (var proj in dte.Solution.Projects.Cast<Project>()) {
                    if (proj.Name == name) {
                        return proj;
                    }
                }
            } catch (Exception ex) { }
            return null;
        }

        private bool IsValidProject() {
            Dispatcher.CurrentDispatcher.VerifyAccess();
            var proj = GetProject(project);
            return proj != null
                   && destination.Contains(System.IO.Path.GetDirectoryName(proj.FullName));
        }
    }
}