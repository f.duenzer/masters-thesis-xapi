﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using xAPI_Definitions_Parser;
using xAPI_Definitions_Parser.Definitions;
using xAPI_Definitions_Parser.IO;

namespace xAPI_WebAPI_Methods {
    /// <summary>
    /// Class with HTTP Methods for the ASP.NET Web API
    /// </summary>
    public static class HttpMethods {
        private static readonly string xAPIWebAPIBaseUri = GetConnectionString("xAPIWebAPIConnectionString");

        private static HttpClient client = new HttpClient();

        private static string GetConnectionString(string name) {
            var config = ConfigurationManager.OpenExeConfiguration(typeof(HttpMethods).Assembly.Location);
            var conSetStr = (ConnectionStringsSection)config.GetSection("connectionStrings");
            return conSetStr.ConnectionStrings[name]?.ConnectionString;
        }

        /// <summary>
        /// Retrieves the definitions in the learntech-rwth/xAPI GitLab project
        /// </summary>
        /// <returns>Unread contexts</returns>
        public static List<Context> GetAPIList() {
            var uri = HttpHelper.BuildUriPaths(xAPIWebAPIBaseUri, "api", "Definitions", "ListDefs");
            var response = client.GetAsync(uri);
            var content = response.Result.Content.ReadAsStringAsync().Result;
            return JsonConvert.DeserializeObject<List<Context>>(content);
        }

        /// <summary>
        /// Asynchronously retrieves the definitions in the learntech-rwth/xAPI GitLab project
        /// </summary>
        /// <returns>Unread contexts</returns>
        public static async Task<List<Context>> GetAPIListAsync() {
            var uri = HttpHelper.BuildUriPaths(xAPIWebAPIBaseUri, "api", "Definitions", "ListDefs");
            var response = client.GetAsync(uri);
            var msg = await response;
            var content = await msg.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<List<Context>>(content);
        }

        /// <summary>
        /// Reads the specified definitions in the learntech-rwth/xAPI GitLab project
        /// </summary>
        /// <param name="contexts">Definitions to read</param>
        /// <returns>Read contexts</returns>
        public static List<Context> GetAPIDefinitions(List<Context> contexts) {
            return GetPost<List<Context>>("ReadDefs", contexts);
        }

        /// <summary>
        /// Asynchronously reads the specified definitions in the learntech-rwth/xAPI GitLab project
        /// </summary>
        /// <param name="contexts">Definitions to read</param>
        /// <returns>Read contexts</returns>
        public static async Task<List<Context>> GetAPIDefinitionsAsync(List<Context> contexts) {
            var uri = HttpHelper.BuildUriPaths(xAPIWebAPIBaseUri, "api", "Definitions", "ReadDefs");

            var content = new StringContent(JsonConvert.SerializeObject(contexts));
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            var response = client.PostAsync(uri, content);
            var msg = await response;
            var contentStr = await msg.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<List<Context>>(contentStr);
        }

        public static List<xAPIContext> GetLocalContexts(string rootpath, string[] exclude = null) {
            var files = new List<FileMeta>();
            if (Directory.Exists(rootpath)) {
                foreach (var path in Directory.GetFiles(rootpath, "*", SearchOption.AllDirectories)) {
                    var name = Path.GetFileNameWithoutExtension(path);
                    if (exclude == null || !exclude.Contains(name)) {
                        files.Add(new FileMeta(name, path, File.ReadAllText(path)));
                    }
                }
            }

            var args = new Dictionary<string, object>();
            args.Add("rootpath", rootpath);
            args.Add("files", files);

            return GetPost<List<xAPIContext>>("FilesContexts", args);
        }

        public static async Task<List<xAPIContext>> GetLocalContextsAsync(string rootpath, string[] exclude = null) {
            var files = new List<FileMeta>();
            if (Directory.Exists(rootpath)) {
                foreach (var path in Directory.GetFiles(rootpath, "*", SearchOption.AllDirectories)) {
                    var name = Path.GetFileNameWithoutExtension(path);
                    if (exclude == null || !exclude.Contains(name)) {
                        files.Add(new FileMeta(name, path, File.ReadAllText(path)));
                    }
                }
            }

            var args = new Dictionary<string, object>();
            args.Add("rootpath", rootpath);
            args.Add("files", files);

            return await GetPostAsync<List<xAPIContext>>("FilesContexts", args);
        }

        public static List<xAPIContext> GetLocalContexts(string rootpath, List<FileMeta> files) {
            var args = new Dictionary<string, object>();
            args.Add("rootpath", rootpath);
            args.Add("files", files);

            return GetPost<List<xAPIContext>>("FilesContexts", args);
        }

        public static async Task<List<xAPIContext>> GetLocalContextsAsync(string rootpath, List<FileMeta> files) {
            var args = new Dictionary<string, object>();
            args.Add("rootpath", rootpath);
            args.Add("files", files);

            return await GetPostAsync<List<xAPIContext>>("FilesContexts", args);
        }

        public static List<FileMeta> GetLocalCode(CodeLanguage lang, string ns, string nsCore, string nsDef,
            string rootpath, string[] exclude = null) {
            var files = new List<FileMeta>();
            if (Directory.Exists(rootpath)) {
                foreach (var path in Directory.GetFiles(rootpath, "*", SearchOption.AllDirectories)) {
                    var name = Path.GetFileNameWithoutExtension(path);
                    if (exclude == null || !exclude.Contains(name)) {
                        files.Add(new FileMeta(name, path, File.ReadAllText(path)));
                    }
                }
            }

            var args = new Dictionary<string, object>();
            args.Add("lang", lang);
            args.Add("ns", ns);
            args.Add("nsCore", nsCore);
            args.Add("nsDef", nsDef);
            args.Add("rootpath", rootpath);
            args.Add("files", files);

            return GetPost<List<FileMeta>>("FilesCode", args);
        }

        public static async Task<List<FileMeta>> GetLocalCodeAsync(CodeLanguage lang, string ns, string nsCore, string nsDef,
            string rootpath, string[] exclude = null) {
            var files = new List<FileMeta>();
            if (Directory.Exists(rootpath)) {
                foreach (var path in Directory.GetFiles(rootpath, "*", SearchOption.AllDirectories)) {
                    var name = Path.GetFileNameWithoutExtension(path);
                    if (exclude == null || !exclude.Contains(name)) {
                        files.Add(new FileMeta(name, path, File.ReadAllText(path)));
                    }
                }
            }

            var args = new Dictionary<string, object>();
            args.Add("lang", lang);
            args.Add("ns", ns);
            args.Add("nsCore", nsCore);
            args.Add("nsDef", nsDef);
            args.Add("rootpath", rootpath);
            args.Add("files", files);

            return await GetPostAsync<List<FileMeta>>("FilesCode", args);
        }

        public static List<FileMeta> GetLocalCode(CodeLanguage lang, string ns, string nsCore, string nsDef,
            string rootpath, List<FileMeta> files) {
            var args = new Dictionary<string, object>();
            args.Add("lang", lang);
            args.Add("ns", ns);
            args.Add("nsCore", nsCore);
            args.Add("nsDef", nsDef);
            args.Add("rootpath", rootpath);
            args.Add("files", files);

            return GetPost<List<FileMeta>>("FilesCode", args);
        }

        public static async Task<List<FileMeta>> GetLocalCodeAsync(CodeLanguage lang, string ns, string nsCore, string nsDef,
            string rootpath, List<FileMeta> files) {
            var args = new Dictionary<string, object>();
            args.Add("lang", lang);
            args.Add("ns", ns);
            args.Add("nsCore", nsCore);
            args.Add("nsDef", nsDef);
            args.Add("rootpath", rootpath);
            args.Add("files", files);

            return await GetPostAsync<List<FileMeta>>("FilesCode", args);
        }

        public static List<xAPIContext> GetGitlabContexts(string rootpath, string projectId,
            string privateKey = default, string branch = "master") {
            var args = new Dictionary<string, object>();
            args.Add("rootpath", rootpath);
            args.Add("projectId", projectId);
            args.Add("privateKey", privateKey);
            args.Add("branch", branch);

            return GetPost<List<xAPIContext>>("GitlabContexts", args);
        }

        public static async Task<List<xAPIContext>> GetGitlabContextsAsync(string rootpath, string projectId,
            string privateKey = default, string branch = "master") {
            var args = new Dictionary<string, object>();
            args.Add("rootpath", rootpath);
            args.Add("projectId", projectId);
            args.Add("privateKey", privateKey);
            args.Add("branch", branch);

            return await GetPostAsync<List<xAPIContext>>("GitlabContexts", args);
        }

        public static List<FileMeta> GetGitlabCode(CodeLanguage lang, string ns, string nsCore, string nsDef,
            string rootpath, string projectId, string privateKey = default, string branch = "master") {
            var args = new Dictionary<string, object>();
            args.Add("lang", lang);
            args.Add("ns", ns);
            args.Add("nsCore", nsCore);
            args.Add("nsDef", nsDef);
            args.Add("rootpath", rootpath);
            args.Add("projectId", projectId);
            args.Add("privateKey", privateKey);
            args.Add("branch", branch);

            return GetPost<List<FileMeta>>("GitlabCode", args);
        }

        public static async Task<List<FileMeta>> GetGitlabCodeAsync(CodeLanguage lang, string ns, string nsCore, string nsDef,
            string rootpath, string projectId, string privateKey = default, string branch = "master") {
            var args = new Dictionary<string, object>();
            args.Add("lang", lang);
            args.Add("ns", ns);
            args.Add("nsCore", nsCore);
            args.Add("nsDef", nsDef);
            args.Add("rootpath", rootpath);
            args.Add("projectId", projectId);
            args.Add("privateKey", privateKey);
            args.Add("branch", branch);

            return await GetPostAsync<List<FileMeta>>("GitlabCode", args);
        }

        /*public static byte[] GetGitlabCompile(CodeLanguage lang, string ns, string nsCore, string nsDef,
            string dest, string name, string dotnet, string refPath, string rootpath,
            string projectId, string privateKey = default, string branch = "master") {
            var args = new Dictionary<string, object>();
            args.Add("lang", lang);
            args.Add("ns", ns);
            args.Add("nsCore", nsCore);
            args.Add("nsDef", nsDef);
            args.Add("dest", dest);
            args.Add("name", name);
            args.Add("dotnet", dotnet);
            args.Add("refPath", refPath);
            args.Add("rootpath", rootpath);
            args.Add("projectId", projectId);
            args.Add("privateKey", privateKey);
            args.Add("branch", branch);

            return GetPost<byte[]>("GitlabCompile", args);
        }*/

        /*public static List<xAPIContext> GetFilesToxAPIContexts(string rootpath, List<FileMeta> files) {
            var args = new Dictionary<string, object>();
            args.Add("rootpath", rootpath);
            args.Add("files", files);

            return GetPost<List<xAPIContext>>("FilesToxAPIContexts", args);
        }*/

        /*public static List<xAPIContext> GetContextsToxAPIContexts(List<Context> contexts) {
            return GetPost<List<xAPIContext>>("ContextsToxAPIContexts", contexts);
        }*/

        public static List<FileMeta> GetCode(List<xAPIContext> contexts, CodeLanguage lang, string ns,
            string nsCore = default, string nsDef = default) {
            var args = new Dictionary<string, object>();
            args.Add("contexts", contexts);
            args.Add("lang", lang);
            args.Add("ns", ns);
            args.Add("nsCore", nsCore);
            args.Add("nsDef", nsDef);

            return GetPost<List<FileMeta>>("ContextsToCode", args);
        }

        public static async Task<List<FileMeta>> GetCodeAsync(List<xAPIContext> contexts, CodeLanguage lang, string ns,
            string nsCore = default, string nsDef = default) {
            var args = new Dictionary<string, object>();
            args.Add("contexts", contexts);
            args.Add("lang", lang);
            args.Add("ns", ns);
            args.Add("nsCore", nsCore);
            args.Add("nsDef", nsDef);

            return await GetPostAsync<List<FileMeta>>("ContextsToCode", args);
        }

        /*public static byte[] GetCompile(List<xAPIContext> contexts, CodeLanguage lang, string ns,
            string nsCore, string nsDef, string dest, string name, string dotnet,
            string refPath) {
            var args = new Dictionary<string, object>();
            args.Add("contexts", contexts);
            args.Add("lang", lang);
            args.Add("ns", ns);
            args.Add("nsCore", nsCore);
            args.Add("nsDef", nsDef);
            args.Add("dest", dest);
            args.Add("name", name);
            args.Add("dotnet", dotnet);
            args.Add("refPath", refPath);

            return GetPost<byte[]>("ContextsToCompile", args);
        }*/

        private static T GetPost<T>(string action, object args) {
            var uri = HttpHelper.BuildUriPaths(xAPIWebAPIBaseUri, "api", "Definitions", action);

            var content = new StringContent(JsonConvert.SerializeObject(args));
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            var response = client.PostAsync(uri, content);
            var contentStr = response.Result.Content.ReadAsStringAsync().Result;
            return JsonConvert.DeserializeObject<T>(contentStr);
        }

        private static async Task<T> GetPostAsync<T>(string action, object args) {
            var uri = HttpHelper.BuildUriPaths(xAPIWebAPIBaseUri, "api", "Definitions", action);

            var content = new StringContent(JsonConvert.SerializeObject(args));
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            var response = await client.PostAsync(uri, content);
            var contentStr = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<T>(contentStr);
        }
    }
}