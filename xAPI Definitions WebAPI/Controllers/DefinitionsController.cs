﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using xAPI_Definitions_Parser;
using xAPI_Definitions_Parser.Definitions;
using xAPI_Definitions_Parser.Deserialization;
using xAPI_Definitions_Parser.IO;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace xAPI_Definitions_WebAPI.Controllers {
    [Route("api/[controller]")]
    [ApiController]
    public class DefinitionsController : ControllerBase {
        [HttpGet("ListDefs")]
        public List<Context> GetListDefinitions() {
            var reader = new FileManager();
            //TODO: als env variable in docker
#if DEBUG
            return reader.ListContextsFull(@"D:\test\git\definitions");
#else
            return reader.ListContextsFull(Path.Combine(Environment.GetEnvironmentVariable("LEARNTECH_LOCAL"), "definitions"));
#endif
        }

        [HttpPost("ReadDefs")]
        public List<Context> GetReadDefinitions(List<Context> contexts) {
            var reader = new FileManager();
            foreach (var context in contexts) {
                ReadDefinitions(reader, context.Activities);
                foreach (var extList in context.Extensions.Values) {
                    ReadDefinitions(reader, extList);
                }
                ReadDefinitions(reader, context.Verbs);
            }
            return contexts;
        }

        private void ReadDefinitions(FileManager reader, List<FileMeta> files) {
            foreach (var file in files) {
                file.Path = file.Path.Replace('\\', Path.DirectorySeparatorChar).Replace('/', Path.DirectorySeparatorChar);
                if (file.IsDirectory) {
                    ReadDefinitions(reader, file.DirContent);
                } else {
                    //TODO: als env variable in docker
#if DEBUG
                    file.FileContent = reader.ReadFile(Path.Combine(@"D:\test\git\definitions", file.Path));
#else
                    file.FileContent = reader.ReadFile(Path.Combine(Environment.GetEnvironmentVariable("LEARNTECH_LOCAL"), "definitions", file.Path));
#endif
                }
            }
        }
        
        [HttpPost("FilesContexts")]
        public List<xAPIContext> GetFilesContexts(Dictionary<string, object> data) {
            string rootpath = (string)data["rootpath"];
            List<FileMeta> files = JsonConvert.DeserializeObject<List<FileMeta>>(data["files"].ToString());

            //TODO: als env variable in docker
#if DEBUG
            var tempDir = Path.Combine(@"D:\test\temp", (data.GetHashCode() + DateTime.Now.GetHashCode()).ToString());
#else
            var tempDir = Path.Combine(Environment.GetEnvironmentVariable("WEBAPI_TEMP"), (data.GetHashCode() + DateTime.Now.GetHashCode()).ToString());
#endif
            Directory.CreateDirectory(tempDir);

            foreach (var file in files) {
                if (file.Path.StartsWith(rootpath)) {
                    var path = tempDir + file.Path.Substring(rootpath.Length).Replace("\\", "/");
                    if (!Directory.Exists(Path.GetDirectoryName(path))) {
                        Directory.CreateDirectory(Path.GetDirectoryName(path));
                    }

                    using (var writer = System.IO.File.CreateText(path)) {
                        writer.Write(file.FileContent);
                    }
                }
            }

            var reader = new FileManager();
            var contexts = reader.ReadContexts(tempDir);
            var deserializedContexts = Deserializer.DeserializeContexts(contexts);

            Directory.Delete(tempDir, true);
            return deserializedContexts;
        }

        [HttpPost("FilesCode")]
        public List<FileMeta> GetFilesCode(Dictionary<string, object> data) {
            CodeLanguage lang = (CodeLanguage)Enum.ToObject(typeof(CodeLanguage), data["lang"]);
            string ns = (string)data["ns"];
            string nsCore = (string)data["nsCore"];
            string nsDef = (string)data["nsDef"];

            var contexts = GetFilesContexts(data);
            var generator = Generator.CreateGenerator(lang, ns, nsCore, nsDef);
            var files = generator.GenerateContexts(contexts);
            files.AddRange(generator.GenerateCoreFiles());
            return files;
        }

        [HttpPost("GitlabContexts")]
        public List<xAPIContext> GetGitlabContexts(Dictionary<string, object> data) {
            string rootpath = (string)data["rootpath"];
            string projectId = (string)data["projectId"];
            string privateKey = (string)data["privateKey"];
            string branch = (string)data["branch"];

            var reader = new GitLabManager(projectId, privateKey, branch);
            var contexts = reader.ReadContextsAsync(rootpath).Result;
            return Deserializer.DeserializeContexts(contexts);
        }

        [HttpPost("GitlabCode")]
        public List<FileMeta> GetGitlabCode(Dictionary<string, object> data) {
            CodeLanguage lang = (CodeLanguage)Enum.ToObject(typeof(CodeLanguage), data["lang"]);
            string ns = (string)data["ns"];
            string nsCore = (string)data["nsCore"];
            string nsDef = (string)data["nsDef"];

            var contexts = GetGitlabContexts(data);
            var generator = Generator.CreateGenerator(lang, ns, nsCore, nsDef);
            var files = generator.GenerateContexts(contexts);
            files.AddRange(generator.GenerateCoreFiles());
            return files;
        }

        // get gitlab compile
        /*[HttpPost("GitlabCompile")]
        public byte[] GetGitlabCompile(Dictionary<string, object> data) {
            CodeLanguage lang = (CodeLanguage)Enum.ToObject(typeof(CodeLanguage), data["lang"]);
            string ns = (string)data["ns"];
            string nsCore = (string)data["nsCore"];
            string nsDef = (string)data["nsDef"];
            string dest = (string)data["dest"];
            string name = (string)data["name"];
            string dotnet = (string)data["dotnet"];
            string refPath = (string)data["refPath"];

            var files = GetGitlabCode(data);
            var generator = Generator.CreateGenerator(lang, ns, nsCore, nsDef);
            var args = Generator.CreateCompilationArguments(lang, dest, name, dotnet, refPath);
            //return generator.Compile(files, args);
            return null;
        }*/

        

        // make contexts from sent files
        /*[HttpPost("FilesToxAPIContexts")]
        public List<xAPIContext> GetFilesToxAPIContexts(Dictionary<string, object> data) {
            string rootpath = (string)data["rootpath"];
            List<FileMeta> files = JsonConvert.DeserializeObject<List<FileMeta>>(data["files"].ToString());

            if (rootpath.EndsWith("\\")) {
                rootpath = rootpath.Substring(0, rootpath.Length - 1);
            }
            var contexts = new List<Context>();
            foreach (var file in files) {
                if (file.Path.StartsWith(rootpath)) {
                    var relativePath = file.Path.Substring(rootpath.Length) + 1;
                    var parts = file.Path.Split('\\');
                    if (parts.Length > 2) {
                        if (!contexts.Any(c => c.Name == parts[0])) {
                            contexts.Add(new Context(parts[0]));
                        }
                        var context = contexts.First(c => c.Name == parts[0]);

                        if (parts[1] == "activities") {
                            context.AddActivity(file);
                        } else if (parts[1] == "extensions") {
                            context.AddExtension(parts[2], file);
                        } else if (parts[1] == "verbs") {
                            context.AddVerb(file);
                        }
                    }
                }
            }
            return Deserializer.DeserializeContexts(contexts);
        }*/

        // make contexts from sent files
        /*[HttpPost("ContextsToxAPIContexts")]
        public List<xAPIContext> GetContextsToxAPIContexts(List<Context> contexts) {
            return Deserializer.DeserializeContexts(contexts);
        }*/

        [HttpPost("ContextsToCode")]
        public List<FileMeta> GetCode(Dictionary<string, object> data) {
            List<xAPIContext> contexts = JsonConvert.DeserializeObject<List<xAPIContext>>(data["contexts"].ToString());
            CodeLanguage lang = (CodeLanguage)Enum.ToObject(typeof(CodeLanguage), data["lang"]);
            string ns = (string)data["ns"];
            string nsCore = (string)data["nsCore"];
            string nsDef = (string)data["nsDef"];

            var generator = Generator.CreateGenerator(lang, ns, nsCore, nsDef);
            var files = generator.GenerateContexts(contexts);
            files.AddRange(generator.GenerateCoreFiles());
            return files;
        }

        // aus contexten compile erstellen
        /*[HttpPost("ContextsToCompile")]
        public byte[] GetCompile(Dictionary<string, object> data) {
            CodeLanguage lang = (CodeLanguage)Enum.ToObject(typeof(CodeLanguage), data["lang"]);
            string ns = (string)data["ns"];
            string nsCore = (string)data["nsCore"];
            string nsDef = (string)data["nsDef"];
            string dest = (string)data["dest"];
            string name = (string)data["name"];
            string dotnet = (string)data["dotnet"];
            string refPath = (string)data["refPath"];

            var files = GetCode(data);
            var generator = Generator.CreateGenerator(lang, ns, nsCore, nsDef);
            var args = Generator.CreateCompilationArguments(lang, dest, name, dotnet, refPath);
            //return generator.Compile(files, args);
            return null;
        }*/
    }
}