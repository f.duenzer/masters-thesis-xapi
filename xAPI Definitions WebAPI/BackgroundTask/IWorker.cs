﻿using System.Threading;
using System.Threading.Tasks;

namespace xAPI_Definitions_WebAPI.BackgroundTask {
    public interface IWorker {
        Task DoWork(CancellationToken cancellationToken);
    }
}