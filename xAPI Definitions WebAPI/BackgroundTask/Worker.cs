﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace xAPI_Definitions_WebAPI.BackgroundTask {
    public class Worker : IWorker {
        public async Task DoWork(CancellationToken cancellationToken) {
            while (!cancellationToken.IsCancellationRequested) {
                await Task.Delay(1000 * 1800);
#if DEBUG
                Program.PullDefinitionsMaster(@"D:\test\git");
#else
                Program.PullDefinitionsMaster(Environment.GetEnvironmentVariable("LEARNTECH_LOCAL"));
#endif
            }
        }
    }
}