﻿using Microsoft.Extensions.Hosting;
using System.Threading;
using System.Threading.Tasks;

namespace xAPI_Definitions_WebAPI.BackgroundTask {
    public class BackgroundWorker : BackgroundService {
        private readonly IWorker worker;

        public BackgroundWorker(IWorker worker) {
            this.worker = worker;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken) {
            await worker.DoWork(stoppingToken);
        }
    }
}