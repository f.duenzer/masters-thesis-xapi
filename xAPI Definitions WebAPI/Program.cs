using LibGit2Sharp;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using System;
using System.IO;

namespace xAPI_Definitions_WebAPI {
    public class Program {
        public static void Main(string[] args) {
            PullDefinitionsMaster();
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder => {
                    webBuilder.UseStartup<Startup>();
                });

        private static void PullDefinitionsMaster() {
            //TODO: als env variable in docker
#if DEBUG
            var localRepo = @"D:\test\git";
#else
            var localRepo = Environment.GetEnvironmentVariable("LEARNTECH_LOCAL");
#endif
            if (!Directory.Exists(localRepo)) {
                Repository.Clone("https://gitlab.com/learntech-rwth/xapi", localRepo);
            } else {
                PullDefinitionsMaster(localRepo);
            }
            // dateien via git command im prozess holen
            // als json file abspeichern und creation und lastaccesstime auf commit und first commit setzen, beim �ffnen und einlesen commit zeiten vergleichen
        }

        internal static void PullDefinitionsMaster(string localRepo) {
            using (var repo = new Repository(localRepo)) {
                Commands.Pull(repo, new Signature("xAPI Definitions WebAPI", "xAPI Definitions WebAPI", DateTimeOffset.Now), new PullOptions());
            }
        }
    }
}