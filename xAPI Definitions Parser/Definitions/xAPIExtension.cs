﻿using System.Collections.Generic;

namespace xAPI_Definitions_Parser.Definitions {
    /// <summary>
    /// Class that represents an xAPI extension
    /// </summary>
    public class xAPIExtension : xAPIDefinition {
        /// <summary>
        /// Creates an empty xAPIExtension
        /// </summary>
        public xAPIExtension() : base() { }

        /// <summary>
        /// Creates an xAPIExtension
        /// </summary>
        /// <param name="context">Context of the extension</param>
        /// <param name="name">(File) name of the extension</param>
        /// <param name="names">Names of the extension by language</param>
        /// <param name="descriptions">Descriptions of the extension by language.</param>
        public xAPIExtension(string context, string name, Dictionary<string, string> names, Dictionary<string, string> descriptions)
            : base(context, name, names, descriptions) { }
    }
}