﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace xAPI_Definitions_Parser {
    /// <summary>
    /// Source types
    /// </summary>
    public enum SourceType {
        Local,
        GitLab
    }

    /// <summary>
    /// Code languages
    /// </summary>
    public enum CodeLanguage {
        CSharp,
        JavaScript,
        Python
    }

    /// <summary>
    /// Access modifiers
    /// </summary>
    public enum Modifier {
        Public,
        Private,
        Protected,
        Static,
        Virtual
    }

    /// <summary>
    /// Class with enum methods
    /// </summary>
    public static class EnumMethods {
        private static IDictionary<CodeLanguage, string> extensions = new Dictionary<CodeLanguage, string>() {
            [CodeLanguage.CSharp] = ".cs",
            [CodeLanguage.JavaScript] = ".js",
            [CodeLanguage.Python] = ".py"
        };

        /// <summary>
        /// Gets the extension of a code language
        /// </summary>
        /// <param name="lang">Language</param>
        /// <returns>Corresponding extension</returns>
        public static string GetExtension(this CodeLanguage lang) {
            return extensions[lang];
        }

        /// <summary>
        /// Gets the string of a source key
        /// </summary>
        /// <param name="type">Source type</param>
        /// <returns>String of a source type</returns>
        public static string GetSourceKeyString(this SourceType type) {
            return type.GetName().ToLower();
        }

        /// <summary>
        /// Gets a source type from a string
        /// </summary>
        /// <param name="str">Source type string</param>
        /// <returns>Corresponding source type</returns>
        public static SourceType? GetSourceKeyFromString(this string str) {
            foreach (SourceType val in Enum.GetValues(typeof(SourceType))) {
                if (val.GetName() == str || val.GetSourceKeyString() == str) {
                    return val;
                }
            }
            return null;
        }

        /// <summary>
        /// Gets the name of an enum
        /// </summary>
        /// <param name="enu">Enum value</param>
        /// <returns>Name of the enum value</returns>
        public static string GetName(this Enum enu) {
            return Enum.GetName(enu.GetType(), enu);
        }

        /// <summary>
        /// Gets the names of all values of an enum type
        /// </summary>
        /// <param name="enumType">Enum type</param>
        /// <returns>Comma separated names</returns>
        public static string GetNames(Type enumType) {
            var names = Enum.GetNames(enumType);
            var namesStr = string.Empty;
            for (int i = 0; i < names.Length; ++i) {
                namesStr += names[i];
                if (i < names.Length - 1) {
                    namesStr += ", ";
                }
            }
            return namesStr;
        }

        /// <summary>
        /// Gets a dictionary of enum values and their names
        /// </summary>
        /// <typeparam name="T">Enum type</typeparam>
        /// <returns>Dictionary of enum values and their names</returns>
        public static IDictionary<T, string> GetEnumDict<T>() where T : Enum {
            var dict = new Dictionary<T, string>();
            foreach (var val in Enum.GetValues(typeof(T)).Cast<T>()) {
                dict.Add(val, GetName(val));
            }
            return dict;
        }

        /// <summary>
        /// Gets the CodeLanguage from the value's string
        /// </summary>
        /// <param name="lang">String</param>
        /// <returns>Corresponding CodeLanguage</returns>
        public static CodeLanguage? GetLanguage(string lang) {
            try {
                return (CodeLanguage)Enum.Parse(CodeLanguage.CSharp.GetType(), lang);
            } catch (Exception) {
                return null;
            }
        }

        /// <summary>
        /// Does the language use namespace or not
        /// </summary>
        /// <param name="lang">Language to check</param>
        /// <returns>True, if C#, false if others</returns>
        public static bool UsesNamespace(this CodeLanguage lang) {
            return lang == CodeLanguage.CSharp;
        }
    }
}