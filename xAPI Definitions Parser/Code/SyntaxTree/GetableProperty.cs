﻿using System.Collections.Generic;

namespace xAPI_Definitions_Parser.Code.SyntaxTree {
    /// <summary>
    /// A property in the syntax tree with a getter
    /// </summary>
    public class GetableProperty {
        private string name;
        private string type;
        private List<Modifier> modifiers = new List<Modifier>();
        private Getter getter;

        /// <summary>
        /// Name of the property
        /// </summary>
        public string Name => name;
        /// <summary>
        /// Type of the property
        /// </summary>
        public string Type => type;
        /// <summary>
        /// Access modifiers of the property
        /// </summary>
        public IReadOnlyList<Modifier> Modifiers => modifiers.AsReadOnly();
        /// <summary>
        /// Getter of the property
        /// </summary>
        public ref readonly Getter Getter => ref getter;

        public GetableProperty() { }

        public GetableProperty(string name, string type, Getter getter = default) {
            this.name = name;
            this.type = type;
            this.getter = getter;
        }

        public GetableProperty WithName(string name) {
            this.name = name;
            return this;
        }

        public GetableProperty WithType(string type) {
            this.type = type;
            return this;
        }

        public GetableProperty AddModifier(Modifier modifier) {
            modifiers.Add(modifier);
            return this;
        }

        public GetableProperty AddModifiers(params Modifier[] modifiers) {
            this.modifiers.AddRange(modifiers);
            return this;
        }

        public GetableProperty AddModifiers(IEnumerable<Modifier> modifiers) {
            this.modifiers.AddRange(modifiers);
            return this;
        }

        public GetableProperty WithGetter(Getter getter) {
            this.getter = getter;
            return this;
        }
    }
}