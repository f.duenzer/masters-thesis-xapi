﻿namespace xAPI_Definitions_Parser.Code {
    /// <summary>
    /// Contains arguments for the CodeGenerator's compile method
    /// </summary>
    public abstract class CompilationArguments {
        /// <summary>
        /// CodeLanguage of the arguments
        /// </summary>
        public CodeLanguage Language { get; }
        /// <summary>
        /// Compilation destination location
        /// </summary>
        public string Destination { get; }

        public CompilationArguments(CodeLanguage lang, string dest) {
            Language = lang;
            Destination = dest;
        }
    }
}