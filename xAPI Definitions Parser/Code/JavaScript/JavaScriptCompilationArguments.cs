﻿namespace xAPI_Definitions_Parser.Code.JavaScript {
    /// <summary>
    /// Contains arguments for the JavaScriptCodeGenerator's compile method
    /// </summary>
    public class JavaScriptCompilationArguments : CompilationArguments {
        public JavaScriptCompilationArguments(string dest)
            : base(CodeLanguage.JavaScript, dest) {
        }
    }
}