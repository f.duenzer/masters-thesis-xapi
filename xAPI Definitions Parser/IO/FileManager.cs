﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace xAPI_Definitions_Parser.IO {
    /// <summary>
    /// Manages the read and write operations of local files
    /// </summary>
    public class FileManager : IOManager {
        /// <summary>
        /// Key of the manager's source type
        /// </summary>
        public static SourceType SourceKey => SourceType.Local;

        /// <summary>
        /// Gets the directories in a path
        /// </summary>
        /// <param name="rootpath">Path to get directories from</param>
        /// <param name="exclude">Files to exclude</param>
        /// <returns>List of directories</returns>
        private List<FileMeta> GetDirectories(string rootpath, string[] exclude = null) {
            var dirs = new List<FileMeta>();
            if (Directory.Exists(rootpath)) {
                foreach (var path in Directory.GetDirectories(rootpath)) {
                    var name = Path.GetFileNameWithoutExtension(path);
                    if (exclude == null || !exclude.Contains(name)) {
                        dirs.Add(new FileMeta(name, path));
                    }
                }
            }
            return dirs;
        }
        
        /// <summary>
        /// Gets the files in a path
        /// </summary>
        /// <param name="rootpath">Path to get files from</param>
        /// <param name="exclude">Files to exclude</param>
        /// <returns>List of files</returns>
        private List<FileMeta> GetFiles(string rootpath, string[] exclude = null) {
            var files = new List<FileMeta>();
            if (Directory.Exists(rootpath)) {
                foreach (var path in Directory.GetDirectories(rootpath)) {
                    var name = Path.GetFileNameWithoutExtension(path);
                    if (exclude == null || !exclude.Contains(name)) {
                        files.Add(new FileMeta(name, path, GetFiles(path, exclude)));
                    }
                }
                foreach (var path in Directory.GetFiles(rootpath)) {
                    if (Path.GetExtension(path) == ".json") {
                        var name = Path.GetFileNameWithoutExtension(path);
                        if (exclude == null || !exclude.Contains(name)) {
                            files.Add(new FileMeta(name, path));
                        }
                    }
                }
            }
            return files;
        }

        public override List<FileMeta> ListActivities(FileMeta context) => GetFiles(Path.Combine(context.Path, "activities"));

        public override List<FileMeta> ListContexts(string rootpath, string[] exclude = null) => GetDirectories(rootpath, exclude);

        public override List<FileMeta> ListExtensions(FileMeta contextExtType) => GetFiles(contextExtType.Path);

        public override List<FileMeta> ListExtensionTypes(FileMeta context) => GetDirectories(Path.Combine(context.Path, "extensions"));

        public override List<FileMeta> ListVerbs(FileMeta context) => GetFiles(Path.Combine(context.Path, "verbs"));

        public override List<FileMeta> ReadFiles(List<FileMeta> files) {
            foreach (var file in files) {
                file.Name = file.Name.ClearName();
                if (file.IsDirectory) {
                    file.DirContent = ReadFiles(file.DirContent);
                } else {
                    file.FileContent = ReadFile(file.Path);
                }
            }
            return files;
        }

        public override async Task<List<FileMeta>> ReadFilesAsync(List<FileMeta> files) {
            foreach (var file in files) {
                file.Name = file.Name.ClearName();
                if (file.IsDirectory) {
                    file.DirContent = await ReadFilesAsync(file.DirContent);
                } else {
                    file.FileContent = await ReadFileAsync(file.Path);
                }
            }
            return files;
        }

        public override string ReadFile(string path) {
            Log($"FileManager reading file: {path}");
            return File.ReadAllText(path);
        }

        public override async Task<string> ReadFileAsync(string path) {
            Log($"FileManager reading file: {path}");
            var task = Task.Run(() => File.ReadAllText(path));
            return await task;
        }

        public override void WriteFiles(string dest, List<FileMeta> files) {
            if (!Directory.Exists(dest)) {
                Directory.CreateDirectory(dest);
            }

            foreach (var file in files) {
                var filePath = CombinePaths(dest, file.Name);
                Log($"FileManager writing file: {filePath}");
                if (File.Exists(filePath)) {
                    File.Delete(filePath);
                }

                using (var writer = File.CreateText(filePath)) {
                    writer.Write(file.FileContent);
                }
            }
        }

        public override async Task WriteFilesAsync(string dest, List<FileMeta> files) {
            if (!Directory.Exists(dest)) {
                Directory.CreateDirectory(dest);
            }

            var tasks = new List<Task>();

            foreach (var file in files) {
                var filePath = CombinePaths(dest, file.Name);
                Log($"FileManager writing file: {filePath}");
                if (File.Exists(filePath)) {
                    File.Delete(filePath);
                }

                tasks.Add(Task.Run(() => {
                    using (var writer = File.CreateText(filePath)) {
                        writer.Write(file.FileContent);
                    }
                }));

                await Task.WhenAll(tasks);
            }
        }

        protected override string CombinePaths(params string[] paths) {
            return Path.Combine(paths);
        }
    }
}