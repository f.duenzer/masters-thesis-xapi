﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace xAPI_Definitions_Parser.IO {
    /// <summary>
    /// Manages the read and write operations of GitLab files
    /// </summary>
    public class GitLabManager : IOManager {
        private readonly string privateKey;

        /// <summary>
        /// Branch to work on
        /// </summary>
        public string Branch { get; set; }
        /// <summary>
        /// Project to work on
        /// </summary>
        public string ProjectId { get; set; }

        /// <summary>
        /// Key of the manager's source type
        /// </summary>
        public static SourceType SourceKey => SourceType.GitLab;

        /// <summary>
        /// Creates a GitLabManager
        /// </summary>
        /// <param name="projectId">ID of the project to work on</param>
        /// <param name="privateKey">Private key to work with</param>
        /// <param name="branch">Branch of the project to work on</param>
        public GitLabManager(string projectId, string privateKey = default, string branch = "master") {
            ProjectId = projectId;
            this.privateKey = privateKey;
            Branch = branch;
        }

        /// <summary>
        /// Gets the directories in a path
        /// </summary>
        /// <param name="path">Path to get directories from</param>
        /// <param name="exclude">Files to exclude</param>
        /// <returns>List of directories</returns>
        private List<FileMeta> GetDirectories(string rootpath, string[] exclude = null) {
            var files = FetchTree(rootpath);
            var filteredFiles = new List<FileMeta>();

            foreach (var file in files) {
                if (Path.GetExtension(file.Path) == string.Empty) {
                    var name = Path.GetFileNameWithoutExtension(file.Path);
                    if (exclude == null || !exclude.Contains(name)) {
                        filteredFiles.Add(new FileMeta(name, file.Path));
                    }
                }
            }

            return filteredFiles;
        }

        /// <summary>
        /// Gets the definition files in a path
        /// </summary>
        /// <param name="rootpath">Path to get files from</param>
        /// <param name="exclude">Files to exclude</param>
        /// <returns>List of files</returns>
        private List<FileMeta> GetDefinitionFiles(string rootpath, string[] exclude = null) {
            var files = FetchTree(rootpath, true);
            files.Sort(CompareFileMeta);
            
            var filteredFiles = new List<FileMeta>();

            foreach (var file in files) {
                var ext = Path.GetExtension(file.Path);
                var name = Path.GetFileNameWithoutExtension(file.Path);
                if (exclude == null || !exclude.Contains(name)) {
                    if (ext == string.Empty) {
                        filteredFiles.Add(new FileMeta(name, file.Path, new List<FileMeta>()));
                    } else if (ext == ".json") {
                        var relativePath = file.Path.Substring(rootpath.Length);
                        if (relativePath.Length > 0 && relativePath.StartsWith("/")) {
                            relativePath = relativePath.Substring(1);
                        }
                        var parts = relativePath.Split('/');

                        if (parts.Length == 1) {
                            filteredFiles.Add(new FileMeta(name, file.Path));
                        } else {
                            FileMeta ownerFile = filteredFiles.First(f => f.Path == CombinePaths(rootpath, parts[0]));
                            for (int i = 1; i < parts.Length - 1 && ownerFile != null; ++i) {
                                ownerFile = ownerFile.DirContent.First(f => f.Name == parts[i]);
                            }

                            if (ownerFile != null) {
                                ownerFile.DirContent.Add(new FileMeta(name, file.Path));
                            }
                        }
                    }
                }
            }

            return filteredFiles;
        }

        /// <summary>
        /// Compares two FileMetas for sorting like a file explorer
        /// </summary>
        /// <param name="fm1">First FileMeta</param>
        /// <param name="fm2">Second FileMeta</param>
        /// <returns>1 if fm1 comes after fm2, 0 if they are the same, -1 if fm1 comes before fm2</returns>
        private int CompareFileMeta(FileMeta fm1, FileMeta fm2) {
            var parts1 = fm1.Path.Split('/');
            var parts2 = fm2.Path.Split('/');
            string ext1, ext2;
            int i = 0;

            while (i < parts1.Length && i < parts2.Length) {
                int compare = parts1[i].CompareTo(parts2[i]);
                if (compare != 0) {
                    ext1 = Path.GetExtension(parts1[i]);
                    ext2 = Path.GetExtension(parts2[i]);
                    if (ext1 == string.Empty && ext2 == ".json") {
                        return -1;
                    } else if (ext1 == ".json" && ext2 == string.Empty) {
                        return 1;
                    } else {
                        return compare;
                    }
                }
                ++i;
            }

            if (parts1.Length < parts2.Length) {
                return -1;
            } else {
                return 1;
            }
        }

        /// <summary>
        /// Fetches a directory tree from GitLab
        /// </summary>
        /// <param name="path">Path to fetch a tree from</param>
        /// <param name="rec">Should the tree be fetched recursively or only for the direct path</param>
        /// <returns>List of files</returns>
        private List<FileMeta> FetchTree(string path, bool rec = false) {
            var trees = HttpHelper.GetGitLabTree(ProjectId, Branch, path, privateKey, rec);

            var files = new List<FileMeta>();
            if (trees != null) {
                foreach (var tree in trees) {
                    files.AddRange(JsonConvert.DeserializeObject<FileMeta[]>(tree));
                }
            }
            return files;
        }

        /// <summary>
        /// Fetches a file from GitLab
        /// </summary>
        /// <param name="path">Path of the file to fetch</param>
        /// <returns>Content of the fetched file</returns>
        private string FetchFile(string path) {
            return HttpHelper.GetGitLabFile(ProjectId, Branch, path, privateKey);
        }

        /// <summary>
        /// Asynchronously fetches a file from GitLab
        /// </summary>
        /// <param name="path">Path of the file to fetch</param>
        /// <returns>Content of the fetched file</returns>
        private async Task<string> FetchFileAsync(string path) {
            return await HttpHelper.GetGitLabFileAsync(ProjectId, Branch, path, privateKey);
        }

        public override List<FileMeta> ListActivities(FileMeta context) {
            return GetDefinitionFiles(context.Path + "/activities");
        }

        public override List<FileMeta> ListContexts(string rootpath, string[] exclude = null) {
            return GetDirectories(rootpath, exclude);
        }

        public override List<Context> ListContextsFull(string rootpath, string[] exclude = null) {
            var files = FetchTree(rootpath, true);
            files.Sort(CompareFileMeta);

            var contexts = new List<Context>();

            foreach (var file in files) {
                var ext = Path.GetExtension(file.Path);
                var name = Path.GetFileNameWithoutExtension(file.Path);
                if (exclude == null || !exclude.Contains(name)) {
                    var relativePath = file.Path.Substring(rootpath.Length);
                    if (relativePath.Length > 0 && relativePath.StartsWith("/")) {
                        relativePath = relativePath.Substring(1);
                    }
                    var parts = relativePath.Split('/');

                    if (parts.Length == 1) {
                        contexts.Add(new Context(parts[0]));
                    } else if (parts.Length >= 3) {
                        string owner = CombinePaths(rootpath, parts[0], parts[1]);
                        Context context = contexts.First(c => c.Name == parts[0]);
                        List<FileMeta> container = null;

                        if (parts[1] == "activities") {
                            container = context.Activities;
                        } else if (parts[1] == "extensions") {
                            if (!context.Extensions.ContainsKey(parts[2])) {
                                context.Extensions.Add(parts[2], new List<FileMeta>());
                            }
                            container = context.Extensions[parts[2]];

                        } else if (parts[1] == "verbs") {
                            container = context.Verbs;
                        }

                        if (   container != null
                            && (  parts[1] != "extensions"
                               || parts.Length > 3)) {
                            int start = parts[1] == "extensions" ? 3 : 2;
                            for (int i = start; i < parts.Length - 1 && container != null; ++i) {
                                container = container.Find(f => f.Name == parts[i])?.DirContent;
                            }

                            if (container != null) {
                                if (ext == string.Empty) {
                                    container.Add(new FileMeta(name, file.Path, new List<FileMeta>()));
                                } else {
                                    container.Add(new FileMeta(name, file.Path));
                                }
                            }
                        }
                    }
                }
            }

            return contexts;
        }

        public override List<FileMeta> ListExtensions(FileMeta contextExtType) {
            return GetDefinitionFiles(contextExtType.Path);
        }

        public override List<FileMeta> ListExtensionTypes(FileMeta context) {
            return GetDirectories(context.Path + "/extensions");
        }

        public override List<FileMeta> ListVerbs(FileMeta context) {
            return GetDefinitionFiles(context.Path + "/verbs");
        }

        public override List<FileMeta> ReadFiles(List<FileMeta> files) {
            foreach (var file in files) {
                file.Name = Path.GetFileNameWithoutExtension(file.Name).ClearName();
                if (file.IsDirectory) {
                    file.DirContent = ReadFiles(file.DirContent);
                } else {
                    file.FileContent = ReadFile(file.Path);
                }
            }
            return files;
        }

        public override async Task<List<FileMeta>> ReadFilesAsync(List<FileMeta> files) {
            var fileTaskDict = new Dictionary<FileMeta, Task>();
            foreach (var file in files) {
                file.Name = Path.GetFileNameWithoutExtension(file.Name).ClearName();
                if (file.IsDirectory) {
                    fileTaskDict.Add(file, ReadFilesAsync(file.DirContent));
                } else {
                    fileTaskDict.Add(file, ReadFileAsync(file.Path));
                }
            }
            foreach (var kvp in fileTaskDict) {
                if (kvp.Key.IsDirectory) {
                    kvp.Key.DirContent = await (Task<List<FileMeta>>)kvp.Value;
                } else {
                    kvp.Key.FileContent = await (Task<string>)kvp.Value;
                }
            }
            return files;
        }

        public override string ReadFile(string path) {
            Log($"GitLabManager reading file: {path}");
            return FetchFile(path);
        }

        public override async Task<string> ReadFileAsync(string path) {
            Log($"GitLabManager reading file: {path}");
            return await FetchFileAsync(path);
        }

        public override void WriteCodeFiles(string dest, List<FileMeta> coreFiles, string coreDirName,
            List<FileMeta> files, string definitionsDirName) {
            HttpHelper.InitGitLabCommitActions();

            base.WriteCodeFiles(dest, coreFiles, coreDirName, files, definitionsDirName);

            HttpHelper.PostGitLabCommit(ProjectId, Branch, privateKey, "Generated xAPI Definitions Files");
            HttpHelper.ResetGitLabCommitActions();
        }

        protected override string CombinePaths(params string[] paths) {
            var str = string.Empty;
            foreach (var path in paths) {
                str += str == string.Empty ? path : "/" + path;
            }
            return str;
        }
        
        public override void WriteFiles(string dest, List<FileMeta> files) {
            var directCall = !HttpHelper.IsActionsInitialized;
            if (directCall) {
                HttpHelper.InitGitLabCommitActions();
            }

            var gitFiles = FetchTree(dest);

            foreach (var file in files) {
                var filePath = $"{dest}/{file.Name}";
                Log($"GitLabManager writing file: {filePath}");
                var exists = gitFiles.Any(f => f.Path == filePath);
                if (exists) {
                    HttpHelper.AddGitLabCommitUpdate(filePath, file.FileContent);
                } else {
                    HttpHelper.AddGitLabCommitCreate(filePath, file.FileContent);
                }
            }

            if (directCall) {
                HttpHelper.PostGitLabCommit(ProjectId, Branch, privateKey, "Generated xAPI Definitions Files"); ;
                HttpHelper.ResetGitLabCommitActions();
            }
        }

        public override async Task WriteFilesAsync(string dest, List<FileMeta> files) {
            var directCall = !HttpHelper.IsActionsInitialized;
            if (directCall) {
                HttpHelper.InitGitLabCommitActions();
            }

            var gitFiles = FetchTree(dest);

            foreach (var file in files) {
                var filePath = $"{dest}/{file.Name}";
                Log($"GitLabManager writing file: {filePath}");
                var exists = gitFiles.Any(f => f.Path == filePath);
                if (exists) {
                    HttpHelper.AddGitLabCommitUpdate(filePath, file.FileContent);
                } else {
                    HttpHelper.AddGitLabCommitCreate(filePath, file.FileContent);
                }
            }

            if (directCall) {
                await HttpHelper.PostGitLabCommitAsync(ProjectId, Branch, privateKey, "Generated xAPI Definitions Files"); ;
                HttpHelper.ResetGitLabCommitActions();
            }
        }
    }
}