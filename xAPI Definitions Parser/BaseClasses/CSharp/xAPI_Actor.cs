﻿public struct xAPI_Actor {
    public string Name { get; }
    public string Email { get; }

    public xAPI_Actor(string name, string email) {
        Name = name;
        Email = email;
    }
}