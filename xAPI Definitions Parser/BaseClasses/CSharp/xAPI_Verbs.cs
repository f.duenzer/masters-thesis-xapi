﻿public abstract class xAPI_Verbs {
    public string ContextName { get; }

    public xAPI_Verbs(string contextName) {
        ContextName = contextName;
    }
}