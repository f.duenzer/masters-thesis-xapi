﻿import { xAPI_Definition } from './xapi_definition.js';

export class xAPI_Extension extends xAPI_Definition {
    #extensionType;

    constructor(context, extensionType, key, names, descriptions) {
        super(context, key, names, descriptions);
        this.#extensionType = extensionType;
    }

    get extensionType() {
        return this.#extensionType;
    }

    getPath() {
        return `/${this.context}/extensions/${this.extensionType}/${this.key}`;
    }
}