﻿export class xAPI_Activities {
    #contextName

    constructor(contextName) {
        if (new.target === xAPI_Activities) {
            throw new TypeError('Cannot construct xAPI_Activities instances directly.');
        }
        this.#contextName = contextName;
    }

    get contextName() {
        return this.#contextName;
    }
}