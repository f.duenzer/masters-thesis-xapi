﻿from .xapi_definition import xAPI_Definition

class xAPI_Extension(xAPI_Definition):
    @property
    def extensionType(self):
        return self.__extensionType

    def __init__(self, context: str, extensionType: str, key: str, names: dict, descriptions: dict):
        super().__init__(context, key, names, descriptions)
        self.__extensionType = extensionType

    def getPath(self):
        return f'/{self.context}/extensions/{self.extensionType}/{self.key}'