﻿from .xapi_definition import xAPI_Definition

class xAPI_Activity(xAPI_Definition):
    def __init__(self, context: str, key: str, names: dict, descriptions: dict):
        super().__init__(context, key, names, descriptions)

    def getPath(self):
        return f'/{self.context}/activities/{self.key}'