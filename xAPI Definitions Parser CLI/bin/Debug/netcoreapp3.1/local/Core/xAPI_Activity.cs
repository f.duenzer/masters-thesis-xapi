using System.Collections.Generic;

public class xAPI_Activity : xAPI_Definition {
    public xAPI_Activity(string context, string key, IDictionary<string, string> names, IDictionary<string, string> descriptions)
        : base(context, key, names, descriptions) { }

    public override string GetPath() => $"/{Context}/activities/{Key}";
}