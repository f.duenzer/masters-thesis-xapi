using System.Collections.Generic;

public class xAPI_Extension : xAPI_Definition {
    public readonly string extensionType;

    public xAPI_Extension(string context, string extensionType, string key, IDictionary<string, string> names,
        IDictionary<string, string> descriptions)
        : base(context, key, names, descriptions) {
        this.extensionType = extensionType;
    }

    public override string GetPath() => $"/{Context}/extensions/{extensionType}/{Key}";
}