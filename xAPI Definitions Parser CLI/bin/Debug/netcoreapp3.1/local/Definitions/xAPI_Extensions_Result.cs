namespace xAPI.Definitions {
    using xAPI.Core;

    public class xAPI_Extensions_Result : xAPI_Extensions {

        public xAPI_Extensions_Result(string context) 
            : base("Result", context) {
        }

        public xAPI_Extensions_Result() 
            : base("Result", null) {
        }
    }
}