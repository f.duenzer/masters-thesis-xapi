namespace xAPI.Definitions {
    using System.Collections.Generic;
    using xAPI.Core;

    public class xAPI_Verbs_VirtualReality : xAPI_Verbs {
        /// <summary>
        /// An actor has created a virtual object in the VR environment.
        /// </summary>
        public xAPI_Verb created = new xAPI_Verb(
            context: "VirtualReality",
            key: "created",
            names: new Dictionary<string, string> {
                ["en-US"] = "created",
                ["de-DE"] = "erstellte" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "An actor has created a virtual object in the VR environment.",
                ["de-DE"] = "Eine Akteurin hat ein virtuelles Objekt in der VR Umgebung erstellt." });

        /// <summary>
        /// An actor interacted with the VR object in the VR environment.
        /// </summary>
        public xAPI_Verb interacted = new xAPI_Verb(
            context: "VirtualReality",
            key: "interacted",
            names: new Dictionary<string, string> {
                ["en-US"] = "interacted",
                ["de-DE"] = "interagierte" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "An actor interacted with the VR object in the VR environment.",
                ["de-DE"] = "Eine Akteurin interagierte einem VR Objekt in der VR Umgebung." });

        /// <summary>
        /// An actor has changed a virtual object's position in VR environment.
        /// </summary>
        public xAPI_Verb moved = new xAPI_Verb(
            context: "VirtualReality",
            key: "moved",
            names: new Dictionary<string, string> {
                ["en-US"] = "moved",
                ["de-DE"] = "bewegte" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "An actor has changed a virtual object's position in VR environment.",
                ["de-DE"] = "Eine Akteurin hat die Position eines virtuellen Objekts verändert." });

        /// <summary>
        /// An actor has placed a virtual object in the VR environment.
        /// </summary>
        public xAPI_Verb placed = new xAPI_Verb(
            context: "VirtualReality",
            key: "placed",
            names: new Dictionary<string, string> {
                ["en-US"] = "placed",
                ["de-DE"] = "platzierte" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "An actor has placed a virtual object in the VR environment.",
                ["de-DE"] = "Eine Akteurin hat ein virtuelles Objekt in der VR Umgebung platziert." });

        /// <summary>
        /// An actor has pointed on an VR object.
        /// </summary>
        public xAPI_Verb pointed = new xAPI_Verb(
            context: "VirtualReality",
            key: "pointed",
            names: new Dictionary<string, string> {
                ["en-US"] = "pointed",
                ["de-DE"] = "zeigte" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "An actor has pointed on an VR object.",
                ["de-DE"] = "Eine Akteurin zeigte auf ein VR Objekt." });

        /// <summary>
        /// The actor has pressed a controller button.
        /// </summary>
        public xAPI_Verb pressed = new xAPI_Verb(
            context: "VirtualReality",
            key: "pressed",
            names: new Dictionary<string, string> {
                ["en-US"] = "pressed",
                ["de-DE"] = "drückte" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "The actor has pressed a controller button.",
                ["de-DE"] = "Eine Akteurin drückte einen Controller Taste." });

        /// <summary>
        /// The actor has released a controller button.
        /// </summary>
        public xAPI_Verb released = new xAPI_Verb(
            context: "VirtualReality",
            key: "released",
            names: new Dictionary<string, string> {
                ["en-US"] = "released",
                ["de-DE"] = "löste" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "The actor has released a controller button.",
                ["de-DE"] = "Eine Akteurin löste einen Controller Taste." });

        /// <summary>
        /// An actor has removed a virtual object from the VR environment.
        /// </summary>
        public xAPI_Verb removed = new xAPI_Verb(
            context: "VirtualReality",
            key: "removed",
            names: new Dictionary<string, string> {
                ["en-US"] = "removed",
                ["de-DE"] = "entfernte" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "An actor has removed a virtual object from the VR environment.",
                ["de-DE"] = "Ein Akteur hat ein virtuelles Objekt aus der VR Umgebung entfernt." });

        /// <summary>
        /// An actor has changed a virtual object's rotation in VR environment.
        /// </summary>
        public xAPI_Verb rotated = new xAPI_Verb(
            context: "VirtualReality",
            key: "rotated",
            names: new Dictionary<string, string> {
                ["en-US"] = "rotated",
                ["de-DE"] = "rotierte" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "An actor has changed a virtual object's rotation in VR environment.",
                ["de-DE"] = "Ein Akteur hat die Rotation eines virtuellen Objekts verändert." });

        /// <summary>
        /// An actor has changed a virtual object's scale in VR environment.
        /// </summary>
        public xAPI_Verb scaled = new xAPI_Verb(
            context: "VirtualReality",
            key: "scaled",
            names: new Dictionary<string, string> {
                ["en-US"] = "scaled",
                ["de-DE"] = "skalierte" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "An actor has changed a virtual object's scale in VR environment.",
                ["de-DE"] = "Ein Akteur hat die Skalierung eines virtuellen Objekts verändert." });

        /// <summary>
        /// An actor has changed her own position in VR environment by teleport.
        /// </summary>
        public xAPI_Verb teleported = new xAPI_Verb(
            context: "VirtualReality",
            key: "teleported",
            names: new Dictionary<string, string> {
                ["en-US"] = "teleported",
                ["de-DE"] = "teleportierte" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "An actor has changed her own position in VR environment by teleport.",
                ["de-DE"] = "Eine Akteurin hat die eigene Position in der VR Umgebung durch Teleportation verändert." });

        /// <summary>
        /// An actor has changed a virtual object's scale in VR environment.
        /// </summary>
        public xAPI_Verb touched = new xAPI_Verb(
            context: "VirtualReality",
            key: "touched",
            names: new Dictionary<string, string> {
                ["en-US"] = "touched",
                ["de-DE"] = "benutzte" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "An actor has changed a virtual object's scale in VR environment.",
                ["de-DE"] = "Eine Akteurin hat die Skalierung eines virtuellen Objekts verändert." });

        public xAPI_Verbs_VirtualReality() 
            : base("VirtualReality") {
        }
    }
}