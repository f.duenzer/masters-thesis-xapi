namespace xAPI.Definitions {
    using System.Collections.Generic;
    using xAPI.Core;

    public class xAPI_Extensions_Result_EyeTracking : xAPI_Extensions_Result {

        public xAPI_Extensions_Result_EyeTracking() 
            : base("EyeTracking") {
        }

        public virtual xAPI_Extensions_Result_EyeTracking numberOfBlinking(object value) {
            Add(new xAPI_Extension(
                    context: Context,
                    extensionType: ExtensionType,
                    key: "numberOfBlinking",
                    names: new Dictionary<string, string> {
                        ["en-US"] = "number of blinking",
                        ["de-DE"] = "Zwinker Anzahl" },
                    descriptions: new Dictionary<string, string> {
                        ["en-US"] = "The number of actors eye blinking. Has to be an integer.",
                        ["de-DE"] = "Die Anzahl an Zwinker, die die Akteurin tätigt. Muss ein Integer sein." }),
                 value);
            return this;
        }
    }
}