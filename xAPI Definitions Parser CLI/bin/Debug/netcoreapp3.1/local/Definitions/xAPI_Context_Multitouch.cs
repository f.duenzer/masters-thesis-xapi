namespace xAPI.Definitions {
    using xAPI.Core;

    public class xAPI_Context_Multitouch : xAPI_Context {
        public xAPI_Verbs_Multitouch verbs = new xAPI_Verbs_Multitouch();

        public xAPI_Activities_Multitouch activities = new xAPI_Activities_Multitouch();

        public xAPI_Context_Multitouch_Extensions extensions = new xAPI_Context_Multitouch_Extensions();

        public xAPI_Context_Multitouch() 
            : base("Multitouch") {
        }
    }
}