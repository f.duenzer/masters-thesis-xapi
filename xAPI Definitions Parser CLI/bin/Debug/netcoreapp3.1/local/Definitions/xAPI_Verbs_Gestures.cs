namespace xAPI.Definitions {
    using System.Collections.Generic;
    using xAPI.Core;

    public class xAPI_Verbs_Gestures : xAPI_Verbs {
        /// <summary>
        /// An actor has gazed a part of her body in a direction.
        /// </summary>
        public xAPI_Verb gazed = new xAPI_Verb(
            context: "Gestures",
            key: "gazed",
            names: new Dictionary<string, string> {
                ["en-US"] = "gazed",
                ["de-DE"] = "richtete aus" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "An actor has gazed a part of her body in a direction.",
                ["de-DE"] = "Eine Akteurin richtete ihr Körperteil in eine Richtung aus." });

        /// <summary>
        /// An actor has nodded part of her head.
        /// </summary>
        public xAPI_Verb nodded = new xAPI_Verb(
            context: "Gestures",
            key: "nodded",
            names: new Dictionary<string, string> {
                ["en-US"] = "nodding",
                ["de-DE"] = "nickte" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "An actor has nodded part of her head.",
                ["de-DE"] = "Eine Akteurin nickte mit ihrem Kopf." });

        /// <summary>
        /// An actor has shaked part of her body.
        /// </summary>
        public xAPI_Verb shaked = new xAPI_Verb(
            context: "Gestures",
            key: "shaked",
            names: new Dictionary<string, string> {
                ["en-US"] = "shaked",
                ["de-DE"] = "schüttelte" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "An actor has shaked part of her body.",
                ["de-DE"] = "Eine Akteurin schüttelte ihren Körperteil." });

        /// <summary>
        /// An actor has used part of her body.
        /// </summary>
        public xAPI_Verb used = new xAPI_Verb(
            context: "Gestures",
            key: "used",
            names: new Dictionary<string, string> {
                ["en-US"] = "used",
                ["de-DE"] = "verwendete" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "An actor has used part of her body.",
                ["de-DE"] = "Eine Akteurin verwendete ihren Körperteil." });

        public xAPI_Verbs_Gestures() 
            : base("Gestures") {
        }
    }
}