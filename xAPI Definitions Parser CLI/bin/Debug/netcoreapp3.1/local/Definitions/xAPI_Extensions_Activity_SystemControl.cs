namespace xAPI.Definitions {
    using System.Collections.Generic;
    using xAPI.Core;

    public class xAPI_Extensions_Activity_SystemControl : xAPI_Extensions_Activity {

        public xAPI_Extensions_Activity_SystemControl() 
            : base("SystemControl") {
        }

        public virtual xAPI_Extensions_Activity_SystemControl name(object value) {
            Add(new xAPI_Extension(
                    context: Context,
                    extensionType: ExtensionType,
                    key: "name",
                    names: new Dictionary<string, string> {
                        ["en-US"] = "System control activity name",
                        ["de-DE"] = "System Control Activity Name" },
                    descriptions: new Dictionary<string, string> {
                        ["en-US"] = "Name of a system control activity. Must be a string.",
                        ["de-DE"] = "Namen von einer Aktivitäts von System Control. Muss ein String sein." }),
                 value);
            return this;
        }
    }
}