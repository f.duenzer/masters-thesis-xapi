namespace xAPI.Definitions {
    using System.Collections.Generic;
    using xAPI.Core;

    public class xAPI_Activities_Gestures : xAPI_Activities {
        /// <summary>
        /// Actors physical (left or right) hand.
        /// </summary>
        public xAPI_Activity hand = new xAPI_Activity(
            context: "Gestures",
            key: "hand",
            names: new Dictionary<string, string> {
                ["en-US"] = "hand",
                ["de-DE"] = "Hand" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "Actors physical (left or right) hand.",
                ["de-DE"] = "Physikalische (linke oder rechte) Hand vom Akteur." });

        /// <summary>
        /// Actors physical head.
        /// </summary>
        public xAPI_Activity head = new xAPI_Activity(
            context: "Gestures",
            key: "head",
            names: new Dictionary<string, string> {
                ["en-US"] = "head",
                ["de-DE"] = "Kopf" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "Actors physical head.",
                ["de-DE"] = "Physikalische Kopf vom Akteur." });

        public xAPI_Activities_Gestures() 
            : base("Gestures") {
        }
    }
}