namespace xAPI.Definitions {
    using xAPI.Core;

    public class xAPI_Context_EyeTracking_Extensions {

        public xAPI_Context_EyeTracking_Extensions() {
        }

        public virtual xAPI_Extensions_Activity_EyeTracking activity {
            get {
                return new xAPI_Extensions_Activity_EyeTracking();
            }
        }

        public virtual xAPI_Extensions_Result_EyeTracking result {
            get {
                return new xAPI_Extensions_Result_EyeTracking();
            }
        }
    }
}