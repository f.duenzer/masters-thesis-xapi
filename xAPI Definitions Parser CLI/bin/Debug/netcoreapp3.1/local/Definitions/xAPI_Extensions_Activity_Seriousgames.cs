namespace xAPI.Definitions {
    using System.Collections.Generic;
    using xAPI.Core;

    public class xAPI_Extensions_Activity_Seriousgames : xAPI_Extensions_Activity {

        public xAPI_Extensions_Activity_Seriousgames() 
            : base("Seriousgames") {
        }

        public virtual xAPI_Extensions_Activity_Seriousgames buttontype(object value) {
            Add(new xAPI_Extension(
                    context: Context,
                    extensionType: ExtensionType,
                    key: "buttontype",
                    names: new Dictionary<string, string> {
                        ["en-US"] = "button type",
                        ["de-DE"] = "Knopf Typ" },
                    descriptions: new Dictionary<string, string> {
                        ["en-US"] = "The type of button which was pressed. Has to be String.",
                        ["de-DE"] = "Der Typ des Knopfes der gedrückt wurde. Muss ein String sein." }),
                 value);
            return this;
        }
    }
}