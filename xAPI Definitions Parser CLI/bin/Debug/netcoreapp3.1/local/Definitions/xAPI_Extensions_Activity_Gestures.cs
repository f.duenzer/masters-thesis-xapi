namespace xAPI.Definitions {
    using System.Collections.Generic;
    using xAPI.Core;

    public class xAPI_Extensions_Activity_Gestures : xAPI_Extensions_Activity {

        public xAPI_Extensions_Activity_Gestures() 
            : base("Gestures") {
        }

        public virtual xAPI_Extensions_Activity_Gestures hand(object value) {
            Add(new xAPI_Extension(
                    context: Context,
                    extensionType: ExtensionType,
                    key: "hand",
                    names: new Dictionary<string, string> {
                        ["en-US"] = "hand",
                        ["de-DE"] = "Hand" },
                    descriptions: new Dictionary<string, string> {
                        ["en-US"] = "Hand which is currently active.",
                        ["de-DE"] = "Die Hand, die gerade verwendet wird." }),
                 value);
            return this;
        }
    }
}