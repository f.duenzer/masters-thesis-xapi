namespace xAPI.Definitions {
    using System.Collections.Generic;
    using xAPI.Core;

    public class xAPI_Extensions_Context_Seriousgames : xAPI_Extensions_Context {

        public xAPI_Extensions_Context_Seriousgames() 
            : base("Seriousgames") {
        }

        public virtual xAPI_Extensions_Context_Seriousgames game(object value) {
            Add(new xAPI_Extension(
                    context: Context,
                    extensionType: ExtensionType,
                    key: "game",
                    names: new Dictionary<string, string> {
                        ["en-US"] = "game",
                        ["de-DE"] = "Spiel" },
                    descriptions: new Dictionary<string, string> {
                        ["en-US"] = "The identifier for the game in which the event happened. Can be a string or integer.",
                        ["de-DE"] = "Der Identifikator dafür welches Spiel gespielt wird. Kann ein String oder Integer sein." }),
                 value);
            return this;
        }

        public virtual xAPI_Extensions_Context_Seriousgames gamemode(object value) {
            Add(new xAPI_Extension(
                    context: Context,
                    extensionType: ExtensionType,
                    key: "gamemode",
                    names: new Dictionary<string, string> {
                        ["en-US"] = "gamemode",
                        ["de-DE"] = "Spielmodus" },
                    descriptions: new Dictionary<string, string> {
                        ["en-US"] = "The gamemode in which the game is played. Can be a string or integer.",
                        ["de-DE"] = "Der Spielmodus in welchem das Spiel gespielt wird. Kann ein String oder ein Integer sein." }),
                 value);
            return this;
        }

        public virtual xAPI_Extensions_Context_Seriousgames level(object value) {
            Add(new xAPI_Extension(
                    context: Context,
                    extensionType: ExtensionType,
                    key: "level",
                    names: new Dictionary<string, string> {
                        ["en-US"] = "level",
                        ["de-DE"] = "Level" },
                    descriptions: new Dictionary<string, string> {
                        ["en-US"] = "The level in which or for which something happened. Can be an integer or a string.",
                        ["de-DE"] = "Das Level in welchem ein Ereignis stattfand. Kann ein String oder Integer sein." }),
                 value);
            return this;
        }

        public virtual xAPI_Extensions_Context_Seriousgames numberOfPlayers(object value) {
            Add(new xAPI_Extension(
                    context: Context,
                    extensionType: ExtensionType,
                    key: "numberOfPlayers",
                    names: new Dictionary<string, string> {
                        ["en-US"] = "number of players",
                        ["de-DE"] = "Spieleranzahl" },
                    descriptions: new Dictionary<string, string> {
                        ["en-US"] = "The number of players participating at the game. Has to be an integer.",
                        ["de-DE"] = "Die Anzahl an Spielern welche am Spiel teilnehmen. Muss ein Integer sein." }),
                 value);
            return this;
        }
    }
}