namespace xAPI.Definitions {
    using System.Collections.Generic;
    using xAPI.Core;

    public class xAPI_Extensions_Result_Generic : xAPI_Extensions_Result {

        public xAPI_Extensions_Result_Generic() 
            : base("Generic") {
        }

        public virtual xAPI_Extensions_Result_Generic index(object value) {
            Add(new xAPI_Extension(
                    context: Context,
                    extensionType: ExtensionType,
                    key: "index",
                    names: new Dictionary<string, string> {
                        ["en-US"] = "index",
                        ["de-DE"] = "Index" },
                    descriptions: new Dictionary<string, string> {
                        ["en-US"] = "It is a number to indicies something. Can only be an integer.",
                        ["de-DE"] = "Das ist eine Zahl für das Indexieren von etwas. Kann nur ein Integer sein." }),
                 value);
            return this;
        }

        public virtual xAPI_Extensions_Result_Generic timeSpan(object value) {
            Add(new xAPI_Extension(
                    context: Context,
                    extensionType: ExtensionType,
                    key: "timeSpan",
                    names: new Dictionary<string, string> {
                        ["en-US"] = "Time Span",
                        ["de-DE"] = "Zeitspanne" },
                    descriptions: new Dictionary<string, string> {
                        ["en-US"] = "A time span depending on context.",
                        ["de-DE"] = "Eine bestimmte Zeitspanne abhängig vom Kontext." }),
                 value);
            return this;
        }
    }
}