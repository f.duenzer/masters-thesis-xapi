namespace xAPI {
    using xAPI.Definitions;

    public static class xAPI_Definitions {
        public static xAPI_Context_EyeTracking eyeTracking = new xAPI_Context_EyeTracking();

        public static xAPI_Context_Generic generic = new xAPI_Context_Generic();

        public static xAPI_Context_Gestures gestures = new xAPI_Context_Gestures();

        public static xAPI_Context_Lms lms = new xAPI_Context_Lms();

        public static xAPI_Context_Multitouch multitouch = new xAPI_Context_Multitouch();

        public static xAPI_Context_Seriousgames seriousgames = new xAPI_Context_Seriousgames();

        public static xAPI_Context_SystemControl systemControl = new xAPI_Context_SystemControl();

        public static xAPI_Context_VirtualReality virtualReality = new xAPI_Context_VirtualReality();
    }
}