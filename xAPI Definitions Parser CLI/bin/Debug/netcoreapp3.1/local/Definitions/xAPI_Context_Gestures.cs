namespace xAPI.Definitions {
    using xAPI.Core;

    public class xAPI_Context_Gestures : xAPI_Context {
        public xAPI_Verbs_Gestures verbs = new xAPI_Verbs_Gestures();

        public xAPI_Activities_Gestures activities = new xAPI_Activities_Gestures();

        public xAPI_Context_Gestures_Extensions extensions = new xAPI_Context_Gestures_Extensions();

        public xAPI_Context_Gestures() 
            : base("Gestures") {
        }
    }
}