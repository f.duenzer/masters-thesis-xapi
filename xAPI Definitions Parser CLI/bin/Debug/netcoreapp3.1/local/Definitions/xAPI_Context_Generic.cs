namespace xAPI.Definitions {
    using xAPI.Core;

    public class xAPI_Context_Generic : xAPI_Context {
        public xAPI_Verbs_Generic verbs = new xAPI_Verbs_Generic();

        public xAPI_Activities_Generic activities = new xAPI_Activities_Generic();

        public xAPI_Context_Generic_Extensions extensions = new xAPI_Context_Generic_Extensions();

        public xAPI_Context_Generic() 
            : base("Generic") {
        }
    }
}