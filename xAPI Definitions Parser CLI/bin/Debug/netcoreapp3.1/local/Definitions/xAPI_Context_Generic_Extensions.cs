namespace xAPI.Definitions {
    using xAPI.Core;

    public class xAPI_Context_Generic_Extensions {

        public xAPI_Context_Generic_Extensions() {
        }

        public virtual xAPI_Extensions_Activity_Generic activity {
            get {
                return new xAPI_Extensions_Activity_Generic();
            }
        }

        public virtual xAPI_Extensions_Result_Generic result {
            get {
                return new xAPI_Extensions_Result_Generic();
            }
        }
    }
}