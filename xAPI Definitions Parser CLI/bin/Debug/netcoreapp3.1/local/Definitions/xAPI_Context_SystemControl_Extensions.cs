namespace xAPI.Definitions {
    using xAPI.Core;

    public class xAPI_Context_SystemControl_Extensions {

        public xAPI_Context_SystemControl_Extensions() {
        }

        public virtual xAPI_Extensions_Activity_SystemControl activity {
            get {
                return new xAPI_Extensions_Activity_SystemControl();
            }
        }
    }
}