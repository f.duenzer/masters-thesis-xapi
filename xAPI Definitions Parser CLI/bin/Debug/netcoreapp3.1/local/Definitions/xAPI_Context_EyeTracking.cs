namespace xAPI.Definitions {
    using xAPI.Core;

    public class xAPI_Context_EyeTracking : xAPI_Context {
        public xAPI_Verbs_EyeTracking verbs = new xAPI_Verbs_EyeTracking();

        public xAPI_Activities_EyeTracking activities = new xAPI_Activities_EyeTracking();

        public xAPI_Context_EyeTracking_Extensions extensions = new xAPI_Context_EyeTracking_Extensions();

        public xAPI_Context_EyeTracking() 
            : base("EyeTracking") {
        }
    }
}