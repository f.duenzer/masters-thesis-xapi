namespace xAPI.Definitions {
    using System.Collections.Generic;
    using xAPI.Core;

    public class xAPI_Extensions_Activity_VirtualReality : xAPI_Extensions_Activity {

        public xAPI_Extensions_Activity_VirtualReality() 
            : base("VirtualReality") {
        }

        public virtual xAPI_Extensions_Activity_VirtualReality actionName(object value) {
            Add(new xAPI_Extension(
                    context: Context,
                    extensionType: ExtensionType,
                    key: "actionName",
                    names: new Dictionary<string, string> {
                        ["en-US"] = "action name",
                        ["de-DE"] = "Aktionsname" },
                    descriptions: new Dictionary<string, string> {
                        ["en-US"] = "It is the name of an action.",
                        ["de-DE"] = "Es ist der Name einer Aktion." }),
                 value);
            return this;
        }

        public virtual xAPI_Extensions_Activity_VirtualReality triangle(object value) {
            Add(new xAPI_Extension(
                    context: Context,
                    extensionType: ExtensionType,
                    key: "triangle",
                    names: new Dictionary<string, string> {
                        ["en-US"] = "Triangle",
                        ["de-DE"] = "Dreieck" },
                    descriptions: new Dictionary<string, string> {
                        ["en-US"] = "Sequence of vertices with it's index, color and position.",
                        ["de-DE"] = "Sequenz von Knotenpunkten mit dessen Index, Farbe und Position." }),
                 value);
            return this;
        }

        public virtual xAPI_Extensions_Activity_VirtualReality vrObjectName(object value) {
            Add(new xAPI_Extension(
                    context: Context,
                    extensionType: ExtensionType,
                    key: "vrObjectName",
                    names: new Dictionary<string, string> {
                        ["en-US"] = "VR object name",
                        ["de-DE"] = "VR Objekt Name" },
                    descriptions: new Dictionary<string, string> {
                        ["en-US"] = "Name of a VR object. Must be a string.",
                        ["de-DE"] = "Namen vom VR Objekt. Muss ein String sein." }),
                 value);
            return this;
        }
    }
}