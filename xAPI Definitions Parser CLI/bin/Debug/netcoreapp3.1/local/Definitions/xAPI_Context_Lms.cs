namespace xAPI.Definitions {
    using xAPI.Core;

    public class xAPI_Context_Lms : xAPI_Context {
        public xAPI_Verbs_Lms verbs = new xAPI_Verbs_Lms();

        public xAPI_Activities_Lms activities = new xAPI_Activities_Lms();

        public xAPI_Context_Lms_Extensions extensions = new xAPI_Context_Lms_Extensions();

        public xAPI_Context_Lms() 
            : base("Lms") {
        }
    }
}