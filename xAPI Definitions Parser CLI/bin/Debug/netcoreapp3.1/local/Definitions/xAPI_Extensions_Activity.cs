namespace xAPI.Definitions {
    using xAPI.Core;

    public class xAPI_Extensions_Activity : xAPI_Extensions {

        public xAPI_Extensions_Activity(string context) 
            : base("Activity", context) {
        }

        public xAPI_Extensions_Activity() 
            : base("Activity", null) {
        }
    }
}