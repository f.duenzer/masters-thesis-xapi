namespace xAPI.Definitions {
    using System.Collections.Generic;
    using xAPI.Core;

    public class xAPI_Extensions_Result_Seriousgames : xAPI_Extensions_Result {

        public xAPI_Extensions_Result_Seriousgames() 
            : base("Seriousgames") {
        }

        public virtual xAPI_Extensions_Result_Seriousgames gamemode(object value) {
            Add(new xAPI_Extension(
                    context: Context,
                    extensionType: ExtensionType,
                    key: "gamemode",
                    names: new Dictionary<string, string> {
                        ["en-US"] = "gamemode",
                        ["de-DE"] = "Spielmodus" },
                    descriptions: new Dictionary<string, string> {
                        ["en-US"] = "The result of a change of the gamemode. Can be a string or integer.",
                        ["de-DE"] = "Der neue Spielmodus der aus dem Ereignis hervorgeht." }),
                 value);
            return this;
        }

        public virtual xAPI_Extensions_Result_Seriousgames health(object value) {
            Add(new xAPI_Extension(
                    context: Context,
                    extensionType: ExtensionType,
                    key: "health",
                    names: new Dictionary<string, string> {
                        ["en-US"] = "health",
                        ["de-DE"] = "Leben" },
                    descriptions: new Dictionary<string, string> {
                        ["en-US"] = "The current health of a player character. Has to be an integer.",
                        ["de-DE"] = "Das momentane Leben eines Spielers. Muss ein Integer sein." }),
                 value);
            return this;
        }

        public virtual xAPI_Extensions_Result_Seriousgames level(object value) {
            Add(new xAPI_Extension(
                    context: Context,
                    extensionType: ExtensionType,
                    key: "level",
                    names: new Dictionary<string, string> {
                        ["en-US"] = "level",
                        ["de-DE"] = "Level" },
                    descriptions: new Dictionary<string, string> {
                        ["en-US"] = "The result of a level change. Has to be a string or integer.",
                        ["de-DE"] = "Das neue Level welches das Ergebnis des Ereignisses ist." }),
                 value);
            return this;
        }

        public virtual xAPI_Extensions_Result_Seriousgames position(object value) {
            Add(new xAPI_Extension(
                    context: Context,
                    extensionType: ExtensionType,
                    key: "position",
                    names: new Dictionary<string, string> {
                        ["en-US"] = "position",
                        ["de-DE"] = "Position" },
                    descriptions: new Dictionary<string, string> {
                        ["en-US"] = "The position of an object in the game. Can be in X, Y and Z coordinates.",
                        ["de-DE"] = "Die Position eines Objektes im Spiel. Können X-, Y- und Z-Koordinaten sein." }),
                 value);
            return this;
        }

        public virtual xAPI_Extensions_Result_Seriousgames progress(object value) {
            Add(new xAPI_Extension(
                    context: Context,
                    extensionType: ExtensionType,
                    key: "progress",
                    names: new Dictionary<string, string> {
                        ["en-US"] = "progress",
                        ["de-DE"] = "Fortschritt" },
                    descriptions: new Dictionary<string, string> {
                        ["en-US"] = "The progess a player or group achieved in the game or level. 1.0 Progress would mean the game or level was completed in every aspect. Has to be a double between 0 and 1.",
                        ["de-DE"] = "Der Fortschritt welchen ein Spieler oder eine Gruppe erreichte in einem Level oder Spiel. 1.0 Fortschritt hieße das Spiel oder level wurde vollständig abgeschlossen. Muss ein double zwischen 0 und 1 sein." }),
                 value);
            return this;
        }

        public virtual xAPI_Extensions_Result_Seriousgames score(object value) {
            Add(new xAPI_Extension(
                    context: Context,
                    extensionType: ExtensionType,
                    key: "score",
                    names: new Dictionary<string, string> {
                        ["en-US"] = "score",
                        ["de-DE"] = "Ergebnis" },
                    descriptions: new Dictionary<string, string> {
                        ["en-US"] = "Some relevant score which is tracked by the game. Can be an integer or double.",
                        ["de-DE"] = "Ein relevantes Ergebnis welches während des Spiels augezeichnet wird. Kann ein Integer oder Double sein." }),
                 value);
            return this;
        }
    }
}