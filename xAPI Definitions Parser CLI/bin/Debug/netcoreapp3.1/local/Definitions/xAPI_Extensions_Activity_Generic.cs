namespace xAPI.Definitions {
    using System.Collections.Generic;
    using xAPI.Core;

    public class xAPI_Extensions_Activity_Generic : xAPI_Extensions_Activity {

        public xAPI_Extensions_Activity_Generic() 
            : base("Generic") {
        }

        public virtual xAPI_Extensions_Activity_Generic color(object value) {
            Add(new xAPI_Extension(
                    context: Context,
                    extensionType: ExtensionType,
                    key: "color",
                    names: new Dictionary<string, string> {
                        ["en-US"] = "color",
                        ["de-DE"] = "Color" },
                    descriptions: new Dictionary<string, string> {
                        ["en-US"] = "The color of an object.",
                        ["de-DE"] = "Das Level in welchem ein Ereignis stattfand. Kann ein String oder Integer sein." }),
                 value);
            return this;
        }
    }
}