namespace xAPI.Definitions {
    using System.Collections.Generic;
    using xAPI.Core;

    public class xAPI_Extensions_Activity_EyeTracking : xAPI_Extensions_Activity {

        public xAPI_Extensions_Activity_EyeTracking() 
            : base("EyeTracking") {
        }

        public virtual xAPI_Extensions_Activity_EyeTracking eye(object value) {
            Add(new xAPI_Extension(
                    context: Context,
                    extensionType: ExtensionType,
                    key: "eye",
                    names: new Dictionary<string, string> {
                        ["en-US"] = "eye",
                        ["de-DE"] = "Auge" },
                    descriptions: new Dictionary<string, string> {
                        ["en-US"] = "Actors physical (left or right) eye.",
                        ["de-DE"] = "Physikalische (linke oder rechte) Auge vom Akteur." }),
                 value);
            return this;
        }
    }
}