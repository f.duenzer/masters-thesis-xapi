namespace xAPI.Definitions {
    using System.Collections.Generic;
    using xAPI.Core;

    public class xAPI_Activities_Generic : xAPI_Activities {
        /// <summary>
        /// The response to a question a user can give or get.
        /// </summary>
        public xAPI_Activity answer = new xAPI_Activity(
            context: "Generic",
            key: "answer",
            names: new Dictionary<string, string> {
                ["en-US"] = "answer",
                ["de-DE"] = "Antwort" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "The response to a question a user can give or get.",
                ["de-DE"] = "Eine Antwort kann auf eine Frage gegeben oder bekommen werden." });

        /// <summary>
        /// A button to go backward on a page or a screen.
        /// </summary>
        public xAPI_Activity backwardButton = new xAPI_Activity(
            context: "Generic",
            key: "backwardButton",
            names: new Dictionary<string, string> {
                ["en-US"] = "backward button",
                ["de-DE"] = "Zurück Knopf" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A button to go backward on a page or a screen.",
                ["de-DE"] = "Ein Knopf um auf einer Seite oder dem Bildschirm zurück zu gehen." });

        /// <summary>
        /// A card showning something inside the game.
        /// </summary>
        public xAPI_Activity card = new xAPI_Activity(
            context: "Generic",
            key: "card",
            names: new Dictionary<string, string> {
                ["en-US"] = "card",
                ["de-DE"] = "Karte" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A card showning something inside the game.",
                ["de-DE"] = "Eine Karte die etwas im Spiel anzeigt." });

        /// <summary>
        /// A decision to be made which is relevant for the game outcome.
        /// </summary>
        public xAPI_Activity decision = new xAPI_Activity(
            context: "Generic",
            key: "decision",
            names: new Dictionary<string, string> {
                ["en-US"] = "decision",
                ["de-DE"] = "Entscheidung" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A decision to be made which is relevant for the game outcome.",
                ["de-DE"] = "Eine Entscheidung die getroffen werden muss und welche den Spielausgang beieinflusst." });

        /// <summary>
        /// A document ingame. Could be relevant for the game or not.
        /// </summary>
        public xAPI_Activity document = new xAPI_Activity(
            context: "Generic",
            key: "document",
            names: new Dictionary<string, string> {
                ["en-US"] = "document",
                ["de-DE"] = "Dokument" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A document ingame. Could be relevant for the game or not.",
                ["de-DE"] = "Ein Dokument im Spiel." });

        /// <summary>
        /// A system to track the eyes of a player and use the gathered data in some way.
        /// </summary>
        public xAPI_Activity eyeTrackingSystem = new xAPI_Activity(
            context: "Generic",
            key: "eyeTrackingSystem",
            names: new Dictionary<string, string> {
                ["en-US"] = "eye tracking system",
                ["de-DE"] = "Eye-Tracking System" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A system to track the eyes of a player and use the gathered data in some way.",
                ["de-DE"] = "Ein system um die Augenbewegungen des Spielers aufzuzeichnen und diese Daten in einer Form zu nutzen." });

        /// <summary>
        /// A button to go forward on a page or a screen.
        /// </summary>
        public xAPI_Activity forwardButton = new xAPI_Activity(
            context: "Generic",
            key: "forwardButton",
            names: new Dictionary<string, string> {
                ["en-US"] = "forward button",
                ["de-DE"] = "Weiter Knopf" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A button to go forward on a page or a screen.",
                ["de-DE"] = "Ein Knopf um auf einer Seite oder dem Bildschirm weiter zu gehen." });

        /// <summary>
        /// The gamemode in which a game is played (Competitive or cooperative for example).
        /// </summary>
        public xAPI_Activity gamemode = new xAPI_Activity(
            context: "Generic",
            key: "gamemode",
            names: new Dictionary<string, string> {
                ["en-US"] = "gamemode",
                ["de-DE"] = "Spielmodus" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "The gamemode in which a game is played (Competitive or cooperative for example).",
                ["de-DE"] = "Der Spielmodus in dem ein Spiel gespielt wird (Zusammen oder Gegeneinander zum Beispiel)." });

        /// <summary>
        /// The overall thing to be achieved in the game.
        /// </summary>
        public xAPI_Activity goal = new xAPI_Activity(
            context: "Generic",
            key: "goal",
            names: new Dictionary<string, string> {
                ["en-US"] = "goal",
                ["de-DE"] = "Ziel" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "The overall thing to be achieved in the game.",
                ["de-DE"] = "Das Übergreifende Ziel des Spiels." });

        /// <summary>
        /// A group of players playing a game together.
        /// </summary>
        public xAPI_Activity group = new xAPI_Activity(
            context: "Generic",
            key: "group",
            names: new Dictionary<string, string> {
                ["en-US"] = "group",
                ["de-DE"] = "Gruppe" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A group of players playing a game together.",
                ["de-DE"] = "Eine Gruppe von Spielern welche zusammen ein Spiel spielen." });

        /// <summary>
        /// Something a player can ask for to find the solution to a task.
        /// </summary>
        public xAPI_Activity help = new xAPI_Activity(
            context: "Generic",
            key: "help",
            names: new Dictionary<string, string> {
                ["en-US"] = "help",
                ["de-DE"] = "Hilfe" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "Something a player can ask for to find the solution to a task.",
                ["de-DE"] = "Etwas wonach der Spieler fragen kann um eine Lösung für eine Aufgabe zu finden." });

        /// <summary>
        /// An image of something ingame. Can be an interactible.
        /// </summary>
        public xAPI_Activity image = new xAPI_Activity(
            context: "Generic",
            key: "image",
            names: new Dictionary<string, string> {
                ["en-US"] = "image",
                ["de-DE"] = "Bild" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "An image of something ingame. Can be an interactible.",
                ["de-DE"] = "Ein Bild von etwas im Spiel. Mit einem Bild könnte interagiert werden." });

        /// <summary>
        /// A keyboard to input text or interact with the game in another way.
        /// </summary>
        public xAPI_Activity keyboard = new xAPI_Activity(
            context: "Generic",
            key: "keyboard",
            names: new Dictionary<string, string> {
                ["en-US"] = "keyboard",
                ["de-DE"] = "Tastatur" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A keyboard to input text or interact with the game in another way.",
                ["de-DE"] = "Eine Tastatur um Text einzugeben oder in einer anderen Form mit dem Spiel zu interagieren." });

        /// <summary>
        /// A start menu or ingame menu for settings for example.
        /// </summary>
        public xAPI_Activity menu = new xAPI_Activity(
            context: "Generic",
            key: "menu",
            names: new Dictionary<string, string> {
                ["en-US"] = "menu",
                ["de-DE"] = "Menü" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A start menu or ingame menu for settings for example.",
                ["de-DE"] = "Ein Startmenü oder ein Menü im Spiel für Einstellungen zum Beispiel." });

        /// <summary>
        /// A number shown on the screen, with which for example can be interacted with in some way.
        /// </summary>
        public xAPI_Activity number = new xAPI_Activity(
            context: "Generic",
            key: "number",
            names: new Dictionary<string, string> {
                ["en-US"] = "number",
                ["de-DE"] = "Nummer" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A number shown on the screen, with which for example can be interacted with in some way.",
                ["de-DE"] = "Eine Nummer die auf dem Bildschirm angezeigt wird und mit der zum Beispiel interagiert werden kann." });

        /// <summary>
        /// An option is something a player can decide for or against in the game.
        /// </summary>
        public xAPI_Activity option = new xAPI_Activity(
            context: "Generic",
            key: "option",
            names: new Dictionary<string, string> {
                ["en-US"] = "option",
                ["de-DE"] = "Option" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "An option is something a player can decide for or against in the game.",
                ["de-DE"] = "Ein Spieler kann sich für oder gegen eine Option entscheiden." });

        /// <summary>
        /// An actor which plays the game and interacts with it.
        /// </summary>
        public xAPI_Activity player = new xAPI_Activity(
            context: "Generic",
            key: "player",
            names: new Dictionary<string, string> {
                ["en-US"] = "player",
                ["de-DE"] = "Spieler" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "An actor which plays the game and interacts with it.",
                ["de-DE"] = "Ein Akteur der das Spiel spielt und mit ihm interagiert." });

        /// <summary>
        /// A representation of the player ingame. Could also be an object the player can interact with.
        /// </summary>
        public xAPI_Activity playstone = new xAPI_Activity(
            context: "Generic",
            key: "playstone",
            names: new Dictionary<string, string> {
                ["en-US"] = "playstone",
                ["de-DE"] = "Spielstein" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A representation of the player ingame. Could also be an object the player can interact with.",
                ["de-DE"] = "Eine Representation des Spielers im Spiel. Könnte auch ein Objekt sein mit welchem der Spieler im Spiel interagieren kann." });

        /// <summary>
        /// A question can be asked or answered.
        /// </summary>
        public xAPI_Activity question = new xAPI_Activity(
            context: "Generic",
            key: "question",
            names: new Dictionary<string, string> {
                ["en-US"] = "question",
                ["de-DE"] = "Frage" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A question can be asked or answered.",
                ["de-DE"] = "Eine Frage kann gestellt oder beantwortet werden." });

        /// <summary>
        /// A system interacting with the game which is not running on the same machine as the game.
        /// </summary>
        public xAPI_Activity remoteSystem = new xAPI_Activity(
            context: "Generic",
            key: "remoteSystem",
            names: new Dictionary<string, string> {
                ["en-US"] = "remote system",
                ["de-DE"] = "entferntes System" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A system interacting with the game which is not running on the same machine as the game.",
                ["de-DE"] = "Ein System welches mit dem Spiel interagiert aber nicht auf der selben Maschine wie das Spiel läuft." });

        /// <summary>
        /// Data which is gathered by a sensor.
        /// </summary>
        public xAPI_Activity sensorData = new xAPI_Activity(
            context: "Generic",
            key: "sensorData",
            names: new Dictionary<string, string> {
                ["en-US"] = "sensor data",
                ["de-DE"] = "Sensorendaten" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "Data which is gathered by a sensor.",
                ["de-DE"] = "Daten welche von Sensoren erfasst werden." });

        /// <summary>
        /// The settings of a game.
        /// </summary>
        public xAPI_Activity settings = new xAPI_Activity(
            context: "Generic",
            key: "settings",
            names: new Dictionary<string, string> {
                ["en-US"] = "settings",
                ["de-DE"] = "Einstellungen" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "The settings of a game.",
                ["de-DE"] = "Die EInstellungen eines Spiels." });

        /// <summary>
        /// The solution to a task in the game.
        /// </summary>
        public xAPI_Activity solution = new xAPI_Activity(
            context: "Generic",
            key: "solution",
            names: new Dictionary<string, string> {
                ["en-US"] = "solution",
                ["de-DE"] = "Lösung" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "The solution to a task in the game.",
                ["de-DE"] = "Die Lösung zu einer Aufgabe im Spiel." });

        /// <summary>
        /// The status of if a task can be completed in the current gamestate.
        /// </summary>
        public xAPI_Activity solvability = new xAPI_Activity(
            context: "Generic",
            key: "solvability",
            names: new Dictionary<string, string> {
                ["en-US"] = "solvability",
                ["de-DE"] = "Lösbarkeit" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "The status of if a task can be completed in the current gamestate.",
                ["de-DE"] = "Der Zustand ob die aktuelle Aufgabe noch gelöst werden kann mit dem aktuelle Spielzustand." });

        /// <summary>
        /// A button to start a video.
        /// </summary>
        public xAPI_Activity startVideoButton = new xAPI_Activity(
            context: "Generic",
            key: "startVideoButton",
            names: new Dictionary<string, string> {
                ["en-US"] = "start video button",
                ["de-DE"] = "Videostarten Knopf" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A button to start a video.",
                ["de-DE"] = "Ein Knopf um ein Video zu starten." });

        /// <summary>
        /// A button to stop a video.
        /// </summary>
        public xAPI_Activity stopVideoButton = new xAPI_Activity(
            context: "Generic",
            key: "stopVideoButton",
            names: new Dictionary<string, string> {
                ["en-US"] = "stop video button",
                ["de-DE"] = "Videostoppen Knopf" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A button to stop a video.",
                ["de-DE"] = "Ein Knopf um ein Video zu stoppen." });

        /// <summary>
        /// A device to interact with the game.
        /// </summary>
        public xAPI_Activity tangible = new xAPI_Activity(
            context: "Generic",
            key: "tangible",
            names: new Dictionary<string, string> {
                ["en-US"] = "tangible",
                ["de-DE"] = "Tangible" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A device to interact with the game.",
                ["de-DE"] = "Ein Gerät mit dem man mit dem Spiel interagieren kann." });

        /// <summary>
        /// Some part of the goal or something to be completed to progress in the game or a level.
        /// </summary>
        public xAPI_Activity task = new xAPI_Activity(
            context: "Generic",
            key: "task",
            names: new Dictionary<string, string> {
                ["en-US"] = "task",
                ["de-DE"] = "Aufgabe" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "Some part of the goal or something to be completed to progress in the game or a level.",
                ["de-DE"] = "Eine Aufgabe die zum Erreichen des Spielziels erfüllt werden muss oder zum Abschließen eines Levels." });

        /// <summary>
        /// A text which can be read or entered.
        /// </summary>
        public xAPI_Activity text = new xAPI_Activity(
            context: "Generic",
            key: "text",
            names: new Dictionary<string, string> {
                ["en-US"] = "text",
                ["de-DE"] = "Text" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A text which can be read or entered.",
                ["de-DE"] = "Ein Text der gelesen oder eingegeben werden kann." });

        /// <summary>
        /// A tip to overcome some ingame task.
        /// </summary>
        public xAPI_Activity tip = new xAPI_Activity(
            context: "Generic",
            key: "tip",
            names: new Dictionary<string, string> {
                ["en-US"] = "tip",
                ["de-DE"] = "Tipp" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A tip to overcome some ingame task.",
                ["de-DE"] = "Ein Tipp um eine Aufgabe im Spiel zu lösen." });

        /// <summary>
        /// A video which can be watched inside the game.
        /// </summary>
        public xAPI_Activity video = new xAPI_Activity(
            context: "Generic",
            key: "video",
            names: new Dictionary<string, string> {
                ["en-US"] = "video",
                ["de-DE"] = "Video" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A video which can be watched inside the game.",
                ["de-DE"] = "Ein Video welches im Spiel angeschaut werden kann." });

        /// <summary>
        /// A word shown on the screen, with which for example can be interacted with in some way.
        /// </summary>
        public xAPI_Activity word = new xAPI_Activity(
            context: "Generic",
            key: "word",
            names: new Dictionary<string, string> {
                ["en-US"] = "word",
                ["de-DE"] = "Wort" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A word shown on the screen, with which for example can be interacted with in some way.",
                ["de-DE"] = "Ein Wort welches auf dem Bildschirm angezeigt wird und mit dem man zum Beispiel interagieren kann." });

        public xAPI_Activities_Generic() 
            : base("Generic") {
        }
    }
}