namespace xAPI.Definitions {
    using xAPI.Core;

    public class xAPI_Context_VirtualReality : xAPI_Context {
        public xAPI_Verbs_VirtualReality verbs = new xAPI_Verbs_VirtualReality();

        public xAPI_Activities_VirtualReality activities = new xAPI_Activities_VirtualReality();

        public xAPI_Context_VirtualReality_Extensions extensions = new xAPI_Context_VirtualReality_Extensions();

        public xAPI_Context_VirtualReality() 
            : base("VirtualReality") {
        }
    }
}