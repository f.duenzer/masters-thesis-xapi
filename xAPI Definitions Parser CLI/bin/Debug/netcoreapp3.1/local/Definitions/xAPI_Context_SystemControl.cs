namespace xAPI.Definitions {
    using xAPI.Core;

    public class xAPI_Context_SystemControl : xAPI_Context {
        public xAPI_Verbs_SystemControl verbs = new xAPI_Verbs_SystemControl();

        public xAPI_Activities_SystemControl activities = new xAPI_Activities_SystemControl();

        public xAPI_Context_SystemControl_Extensions extensions = new xAPI_Context_SystemControl_Extensions();

        public xAPI_Context_SystemControl() 
            : base("SystemControl") {
        }
    }
}