namespace xAPI.Definitions {
    using System.Collections.Generic;
    using xAPI.Core;

    public class xAPI_Activities_EyeTracking : xAPI_Activities {
        /// <summary>
        /// Actors physical (left or right) eye.
        /// </summary>
        public xAPI_Activity eye = new xAPI_Activity(
            context: "EyeTracking",
            key: "eye",
            names: new Dictionary<string, string> {
                ["en-US"] = "eye",
                ["de-DE"] = "Auge" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "Actors physical (left or right) eye.",
                ["de-DE"] = "Physikalische (linke oder rechte) Auge vom Akteur." });

        public xAPI_Activities_EyeTracking() 
            : base("EyeTracking") {
        }
    }
}