namespace xAPI.Definitions {
    using xAPI.Core;

    public class xAPI_Extensions_Context : xAPI_Extensions {

        public xAPI_Extensions_Context(string context) 
            : base("Context", context) {
        }

        public xAPI_Extensions_Context() 
            : base("Context", null) {
        }
    }
}