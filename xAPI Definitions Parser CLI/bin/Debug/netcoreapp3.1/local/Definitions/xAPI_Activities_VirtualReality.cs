namespace xAPI.Definitions {
    using System.Collections.Generic;
    using xAPI.Core;

    public class xAPI_Activities_VirtualReality : xAPI_Activities {
        /// <summary>
        /// An action the actor can trigger.
        /// </summary>
        public xAPI_Activity action = new xAPI_Activity(
            context: "VirtualReality",
            key: "action",
            names: new Dictionary<string, string> {
                ["en-US"] = "action",
                ["de-DE"] = "Aktion" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "An action the actor can trigger.",
                ["de-DE"] = "Eine Aktion, die von der Akteurin ausgelöst wird." });

        /// <summary>
        /// An avatar in a VR environment.
        /// </summary>
        public xAPI_Activity avatar = new xAPI_Activity(
            context: "VirtualReality",
            key: "avatar",
            names: new Dictionary<string, string> {
                ["en-US"] = "avatar",
                ["de-DE"] = "Avatar" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "An avatar in a VR environment.",
                ["de-DE"] = "Ein Avatar in einer VR Umgebung." });

        /// <summary>
        /// A VR controller button the actor can use.
        /// </summary>
        public xAPI_Activity controllerButton = new xAPI_Activity(
            context: "VirtualReality",
            key: "controllerButton",
            names: new Dictionary<string, string> {
                ["en-US"] = "controller button",
                ["de-DE"] = "Controller Taste" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A VR controller button the actor can use.",
                ["de-DE"] = "Eine Controller Taste, die die Akteurin benutzen kann." });

        /// <summary>
        /// A VR object which can be pointed with a controller laser.
        /// </summary>
        public xAPI_Activity pointable = new xAPI_Activity(
            context: "VirtualReality",
            key: "pointable",
            names: new Dictionary<string, string> {
                ["en-US"] = "pointable object",
                ["de-DE"] = "ein markierbares Objekt" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A VR object which can be pointed with a controller laser.",
                ["de-DE"] = "Ein VR Objekt, welches mit einem Laser vom Controller markiert werden kann." });

        /// <summary>
        /// A fixed teleport point in VR environment.
        /// </summary>
        public xAPI_Activity teleportPoint = new xAPI_Activity(
            context: "VirtualReality",
            key: "teleportPoint",
            names: new Dictionary<string, string> {
                ["en-US"] = "Teleport point",
                ["de-DE"] = "Teleport Punkt" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A fixed teleport point in VR environment.",
                ["de-DE"] = "Ein fester Teleport Punkt in der VR Umgebung." });

        /// <summary>
        /// A VR controller touchpad the actor can use.
        /// </summary>
        public xAPI_Activity touchpad = new xAPI_Activity(
            context: "VirtualReality",
            key: "touchpad",
            names: new Dictionary<string, string> {
                ["en-US"] = "controller touchpad",
                ["de-DE"] = "Controller Touchpad" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A VR controller touchpad the actor can use.",
                ["de-DE"] = "Ein VR Controller Touchpad, das die Akteurin benutzen kann." });

        /// <summary>
        /// An UI element in VR the user can interact with (e.g. Panel, Button, Slider).
        /// </summary>
        public xAPI_Activity uiElement = new xAPI_Activity(
            context: "VirtualReality",
            key: "uiElement",
            names: new Dictionary<string, string> {
                ["en-US"] = "VR user interface element",
                ["de-DE"] = "VR Benutzerflächenelement" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "An UI element in VR the user can interact with (e.g. Panel, Button, Slider).",
                ["de-DE"] = "Ein Benutzerflächenelement mit dem der Benutzer interagieren kann (z.B. Panel, Button, Slider)." });

        /// <summary>
        /// A virtual object in a VR environment.
        /// </summary>
        public xAPI_Activity vrObject = new xAPI_Activity(
            context: "VirtualReality",
            key: "vrObject",
            names: new Dictionary<string, string> {
                ["en-US"] = "VR object",
                ["de-DE"] = "VR Objekt" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A virtual object in a VR environment.",
                ["de-DE"] = "Ein virtuelles Objekt in einer VR Umgebung." });

        public xAPI_Activities_VirtualReality() 
            : base("VirtualReality") {
        }
    }
}