namespace xAPI.Definitions {
    using xAPI.Core;

    public class xAPI_Context_VirtualReality_Extensions {

        public xAPI_Context_VirtualReality_Extensions() {
        }

        public virtual xAPI_Extensions_Activity_VirtualReality activity {
            get {
                return new xAPI_Extensions_Activity_VirtualReality();
            }
        }
    }
}