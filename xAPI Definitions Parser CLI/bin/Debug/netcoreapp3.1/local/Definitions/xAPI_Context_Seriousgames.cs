namespace xAPI.Definitions {
    using xAPI.Core;

    public class xAPI_Context_Seriousgames : xAPI_Context {
        public xAPI_Verbs_Seriousgames verbs = new xAPI_Verbs_Seriousgames();

        public xAPI_Activities_Seriousgames activities = new xAPI_Activities_Seriousgames();

        public xAPI_Context_Seriousgames_Extensions extensions = new xAPI_Context_Seriousgames_Extensions();

        public xAPI_Context_Seriousgames() 
            : base("Seriousgames") {
        }
    }
}