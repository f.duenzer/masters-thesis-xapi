namespace xAPI.Definitions {
    using xAPI.Core;

    public class xAPI_Context_Seriousgames_Extensions {

        public xAPI_Context_Seriousgames_Extensions() {
        }

        public virtual xAPI_Extensions_Activity_Seriousgames activity {
            get {
                return new xAPI_Extensions_Activity_Seriousgames();
            }
        }

        public virtual xAPI_Extensions_Context_Seriousgames context {
            get {
                return new xAPI_Extensions_Context_Seriousgames();
            }
        }

        public virtual xAPI_Extensions_Result_Seriousgames result {
            get {
                return new xAPI_Extensions_Result_Seriousgames();
            }
        }
    }
}