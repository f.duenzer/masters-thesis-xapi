namespace xAPI.Definitions {
    using System.Collections.Generic;
    using xAPI.Core;

    public class xAPI_Verbs_EyeTracking : xAPI_Verbs {
        /// <summary>
        /// An actor fixated an object with her eyes.
        /// </summary>
        public xAPI_Verb fixated = new xAPI_Verb(
            context: "EyeTracking",
            key: "fixated",
            names: new Dictionary<string, string> {
                ["en-US"] = "fixated",
                ["de-DE"] = "fixierte" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "An actor fixated an object with her eyes.",
                ["de-DE"] = "Eine Aktuerin fixierte ein Objekt mit ihren Augen." });

        /// <summary>
        /// An actor focused an object with her eyes.
        /// </summary>
        public xAPI_Verb focused = new xAPI_Verb(
            context: "EyeTracking",
            key: "focused",
            names: new Dictionary<string, string> {
                ["en-US"] = "focused",
                ["de-DE"] = "fokussierte" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "An actor focused an object with her eyes.",
                ["de-DE"] = "Eine Aktuerin fokussierte ein Objekt mit ihren Augen." });

        public xAPI_Verbs_EyeTracking() 
            : base("EyeTracking") {
        }
    }
}