namespace xAPI.Definitions {
    using System.Collections.Generic;
    using xAPI.Core;

    public class xAPI_Verbs_Generic : xAPI_Verbs {
        /// <summary>
        /// Indicates that that the actor has accepted the object. For instance, a person accepting an award, or accepting an assignment.
        /// </summary>
        public xAPI_Verb accepted = new xAPI_Verb(
            context: "Generic",
            key: "accepted",
            names: new Dictionary<string, string> {
                ["en-US"] = "accepted",
                ["de-DE"] = "akzeptierte" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "Indicates that that the actor has accepted the object. For instance, a person accepting an award, or accepting an assignment.",
                ["de-DE"] = "Signalisiert, dass ein Akteur ein Objekt akzeptiert hat. Zum Beispiel: Eine Person akzeptiert eine Auszeichnung, oder eine Aufgabe." });

        /// <summary>
        /// A player or group achieved some game related goal or score.
        /// </summary>
        public xAPI_Verb achieved = new xAPI_Verb(
            context: "Generic",
            key: "achieved",
            names: new Dictionary<string, string> {
                ["en-US"] = "achieved",
                ["de-DE"] = "erreichte" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A player or group achieved some game related goal or score.",
                ["de-DE"] = "Ein Spieler oder eine Gruppe erreichte ein spielrelevantes Ziel oder ein Ergebnis ." });

        /// <summary>
        /// A player or group answered some game related question.
        /// </summary>
        public xAPI_Verb answered = new xAPI_Verb(
            context: "Generic",
            key: "answered",
            names: new Dictionary<string, string> {
                ["en-US"] = "answered",
                ["de-DE"] = "beantwortete" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A player or group answered some game related question.",
                ["de-DE"] = "Ein Spieler oder eine Gruppe beantwortete eine für das Spiel relevante Frage." });

        /// <summary>
        /// A player or group asked a question. Could also asked for help or advice.
        /// </summary>
        public xAPI_Verb asked = new xAPI_Verb(
            context: "Generic",
            key: "asked",
            names: new Dictionary<string, string> {
                ["en-US"] = "asked",
                ["de-DE"] = "fragte" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A player or group asked a question. Could also asked for help or advice.",
                ["de-DE"] = "Ein Spieler oder eine Gruppe stellte eine Frage. Es kann auch nach Hilfe oder Ratschlag fragen." });

        /// <summary>
        /// An actor calibrated something. This could be an eye tracking system for example.
        /// </summary>
        public xAPI_Verb calibrated = new xAPI_Verb(
            context: "Generic",
            key: "calibrated",
            names: new Dictionary<string, string> {
                ["en-US"] = "calibrated",
                ["de-DE"] = "kalibrierte" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "An actor calibrated something. This could be an eye tracking system for example.",
                ["de-DE"] = "Ein Akteur kalibrierte etwas. Dies kann zum Beispiel ein Eyetracking System sein." });

        /// <summary>
        /// An actor changed something ingame. This could be the level, some settings or something ingame related.
        /// </summary>
        public xAPI_Verb changed = new xAPI_Verb(
            context: "Generic",
            key: "changed",
            names: new Dictionary<string, string> {
                ["en-US"] = "changed",
                ["de-DE"] = "veränderte" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "An actor changed something ingame. This could be the level, some settings or something ingame related.",
                ["de-DE"] = "Ein Akteur veränderte etwas im Spiel. Dies könnte das Level sein, die Einstellungen oder etwas im Spiel selbst." });

        /// <summary>
        /// A player or group closed something ingame. Could for example be the settings.
        /// </summary>
        public xAPI_Verb closed = new xAPI_Verb(
            context: "Generic",
            key: "closed",
            names: new Dictionary<string, string> {
                ["en-US"] = "closed",
                ["de-DE"] = "schloss" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A player or group closed something ingame. Could for example be the settings.",
                ["de-DE"] = "Ein Spieler oder eine Gruppe schloss etwas im Spiel. Könnten zum Beispiel die Einstellungen sein." });

        /// <summary>
        /// A player or group collapsed a menu for example.
        /// </summary>
        public xAPI_Verb collapsed = new xAPI_Verb(
            context: "Generic",
            key: "collapsed",
            names: new Dictionary<string, string> {
                ["en-US"] = "collapsed",
                ["de-DE"] = "klappte ein" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A player or group collapsed a menu for example.",
                ["de-DE"] = "Ein Spieler oder eine Gruppe klappte ein Menü zum Beispiel ein." });

        /// <summary>
        /// A player or group collected some game related item.
        /// </summary>
        public xAPI_Verb collected = new xAPI_Verb(
            context: "Generic",
            key: "collected",
            names: new Dictionary<string, string> {
                ["en-US"] = "collected",
                ["de-DE"] = "sammelte" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A player or group collected some game related item.",
                ["de-DE"] = "Ein Spieler oder eine Gruppe sammelte einen für das Spiel relevanten Gegenstand ein." });

        /// <summary>
        /// A player or group brought two or more game related objects together. This can create a new object out of the combined ones. If just a logical connection is meant, use connected.
        /// </summary>
        public xAPI_Verb combined = new xAPI_Verb(
            context: "Generic",
            key: "combined",
            names: new Dictionary<string, string> {
                ["en-US"] = "combined",
                ["de-DE"] = "kombinierte" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A player or group brought two or more game related objects together. This can create a new object out of the combined ones. If just a logical connection is meant, use connected.",
                ["de-DE"] = "Ein Spieler oder eine Gruppe brachte zwei oder mehr spielrelevante Gegenstände zusammen. Aus den kombinierten Gegenständen können neue Gegenstände entstehen. Wenn eine logische Kombination der Gegenstände gemeint ist, nutzte verband." });

        /// <summary>
        /// A player or group completed some game related task, goal or level.
        /// </summary>
        public xAPI_Verb completed = new xAPI_Verb(
            context: "Generic",
            key: "completed",
            names: new Dictionary<string, string> {
                ["en-US"] = "completed",
                ["de-DE"] = "schloss ab" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A player or group completed some game related task, goal or level.",
                ["de-DE"] = "Ein Spieler oder eine Gruppe schloss eine spielrelevante Aufgabe, ein Ziel oder ein Level ab." });

        /// <summary>
        /// A player or group connected two or more game related items with each other. This is meant in the sense of drawn a line between the objects, while combined can make something new out of the objects.
        /// </summary>
        public xAPI_Verb connected = new xAPI_Verb(
            context: "Generic",
            key: "connected",
            names: new Dictionary<string, string> {
                ["en-US"] = "connected",
                ["de-DE"] = "verband" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A player or group connected two or more game related items with each other. This is meant in the sense of drawn a line between the objects, while combined can make something new out of the objects.",
                ["de-DE"] = "Ein Spieler oder eine Gruppe verband zwei oder mehr für das Spiel relevante Gegenstände mit einander. Dies ist eher im Sinne von eine Linie zwischen den Objekten malen, während mit kombinierte auch zum Beispiel neue Gegenstände aus den verbundenen entstehen können." });

        /// <summary>
        /// A player or group created something relevant for the game.
        /// </summary>
        public xAPI_Verb created = new xAPI_Verb(
            context: "Generic",
            key: "created",
            names: new Dictionary<string, string> {
                ["en-US"] = "created",
                ["de-DE"] = "erstellte" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A player or group created something relevant for the game.",
                ["de-DE"] = "Ein Spieler oder eine Gruppe erstellte etwas relevantes für das Spiel." });

        /// <summary>
        /// A player crossed out some text or object.
        /// </summary>
        public xAPI_Verb crossedOut = new xAPI_Verb(
            context: "Generic",
            key: "crossedOut",
            names: new Dictionary<string, string> {
                ["en-US"] = "crossedOut",
                ["de-DE"] = "streichte durch" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A player crossed out some text or object.",
                ["de-DE"] = "Ein Spieler streichte einen Text oder ein Objekt durch." });

        /// <summary>
        /// A player or group deleted something relevant for the game.
        /// </summary>
        public xAPI_Verb deleted = new xAPI_Verb(
            context: "Generic",
            key: "deleted",
            names: new Dictionary<string, string> {
                ["en-US"] = "deleted",
                ["de-DE"] = "löschte" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A player or group deleted something relevant for the game.",
                ["de-DE"] = "Ein Spieler oder eine Gruppe löschte etwas relevantes für das Spiel." });

        /// <summary>
        /// A player or group deselected a game related option.
        /// </summary>
        public xAPI_Verb deselected = new xAPI_Verb(
            context: "Generic",
            key: "deselected",
            names: new Dictionary<string, string> {
                ["en-US"] = "deselected",
                ["de-DE"] = "nahm Auswahl zurück" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A player or group deselected a game related option.",
                ["de-DE"] = "Ein Spieler oder eine Gruppe nahm die Auswahl einer spielrelevanten Option zurück." });

        /// <summary>
        /// A player or group drew something inside the game.
        /// </summary>
        public xAPI_Verb drew = new xAPI_Verb(
            context: "Generic",
            key: "drew",
            names: new Dictionary<string, string> {
                ["en-US"] = "drew",
                ["de-DE"] = "malte" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A player or group drew something inside the game.",
                ["de-DE"] = "Ein Spieler oder eine Gruppe malte etwas in einem Spiel." });

        /// <summary>
        /// A player dropped some draggable into a specific area.
        /// </summary>
        public xAPI_Verb dropped = new xAPI_Verb(
            context: "Generic",
            key: "dropped",
            names: new Dictionary<string, string> {
                ["en-US"] = "dropped",
                ["de-DE"] = "ließ fallen" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A player dropped some draggable into a specific area.",
                ["de-DE"] = "Ein Spieler ließ einen ziehbaren Gegenstand in ein spezielles Feld fallen." });

        /// <summary>
        /// A player or group gave some input to the game.
        /// </summary>
        public xAPI_Verb entered = new xAPI_Verb(
            context: "Generic",
            key: "entered",
            names: new Dictionary<string, string> {
                ["en-US"] = "entered",
                ["de-DE"] = "gab ein" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A player or group gave some input to the game.",
                ["de-DE"] = "Ein Spieler oder eine Gruppe gab dem Spiel eine Eingabe." });

        /// <summary>
        /// A player or group failed some task.
        /// </summary>
        public xAPI_Verb failed = new xAPI_Verb(
            context: "Generic",
            key: "failed",
            names: new Dictionary<string, string> {
                ["en-US"] = "failed",
                ["de-DE"] = "scheiterte" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A player or group failed some task.",
                ["de-DE"] = "Ein Spieler oder eine Gruppe scheiterte an einer Aufgabe." });

        /// <summary>
        /// A player or group found some game related item.
        /// </summary>
        public xAPI_Verb found = new xAPI_Verb(
            context: "Generic",
            key: "found",
            names: new Dictionary<string, string> {
                ["en-US"] = "found",
                ["de-DE"] = "fand" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A player or group found some game related item.",
                ["de-DE"] = "Ein Spieler oder eine Gruppe fand einen für das Spiel relevanten Gegenstand." });

        /// <summary>
        /// A player or group left behind some game related object.
        /// </summary>
        public xAPI_Verb left = new xAPI_Verb(
            context: "Generic",
            key: "left",
            names: new Dictionary<string, string> {
                ["en-US"] = "left",
                ["de-DE"] = "lies zurück" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A player or group left behind some game related object.",
                ["de-DE"] = "Ein Spieler oder eine Gruppe lies einen spielrelevanten Gegenstand zurück." });

        /// <summary>
        /// A player or group did some action to lock up some game related object. Could also be locked out of a progression path for example.
        /// </summary>
        public xAPI_Verb locked = new xAPI_Verb(
            context: "Generic",
            key: "locked",
            names: new Dictionary<string, string> {
                ["en-US"] = "locked",
                ["de-DE"] = "verschloss" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A player or group did some action to lock up some game related object. Could also be locked out of a progression path for example.",
                ["de-DE"] = "Ein Spieler oder eine Gruppe verschloss einen spielrelevantenten Gegenstand. Kann zum Beispiel aber auch ausschließen von einem Weg zu voran kommen sein." });

        /// <summary>
        /// A player looked at a specific object. Only use if you can determine that the player really looked at something, thriough eye-tracking for example.
        /// </summary>
        public xAPI_Verb lookedAt = new xAPI_Verb(
            context: "Generic",
            key: "lookedAt",
            names: new Dictionary<string, string> {
                ["en-US"] = "looked at",
                ["de-DE"] = "schaute auf" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A player looked at a specific object. Only use if you can determine that the player really looked at something, thriough eye-tracking for example.",
                ["de-DE"] = "Ein Spieler schaute auf eine spezifische Sache. Sollte nur genutzt werden wenn sichergestellt werden kann, dass der Spieler sich die Sache auch wirklich angeschaut hat. Mit Eyetracking kann das zum Beispiel gemacht werden." });

        /// <summary>
        /// A player or group found some item and collected it.
        /// </summary>
        public xAPI_Verb looted = new xAPI_Verb(
            context: "Generic",
            key: "looted",
            names: new Dictionary<string, string> {
                ["en-US"] = "looted",
                ["de-DE"] = "plünderte" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A player or group found some item and collected it.",
                ["de-DE"] = "Ein Spieler oder eine Gruppe fand einen Gegenstand und sammelte ihn ein." });

        /// <summary>
        /// A player or group lost the game.
        /// </summary>
        public xAPI_Verb lost = new xAPI_Verb(
            context: "Generic",
            key: "lost",
            names: new Dictionary<string, string> {
                ["en-US"] = "lost",
                ["de-DE"] = "verlor" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A player or group lost the game.",
                ["de-DE"] = "Ein Spieler oder eine Gruppe verlor das Spiel." });

        /// <summary>
        /// A player or group made a decision relevant for the game state.
        /// </summary>
        public xAPI_Verb made = new xAPI_Verb(
            context: "Generic",
            key: "made",
            names: new Dictionary<string, string> {
                ["en-US"] = "made",
                ["de-DE"] = "fällte" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A player or group made a decision relevant for the game state.",
                ["de-DE"] = "Ein Spieler oder eine Gruppe fällte eine spielrelevante Entscheidung." });

        /// <summary>
        /// A player or group moved itself or some game related object in the game world.
        /// </summary>
        public xAPI_Verb moved = new xAPI_Verb(
            context: "Generic",
            key: "moved",
            names: new Dictionary<string, string> {
                ["en-US"] = "moved",
                ["de-DE"] = "bewegte" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A player or group moved itself or some game related object in the game world.",
                ["de-DE"] = "Ein Spieler oder eine Gruppe bewegte sich oder einen spielrelevanten Gegenstand in der Spielwelt." });

        /// <summary>
        /// A player or group opened something ingame. Could also be the settings for example.
        /// </summary>
        public xAPI_Verb opened = new xAPI_Verb(
            context: "Generic",
            key: "opened",
            names: new Dictionary<string, string> {
                ["en-US"] = "opened",
                ["de-DE"] = "öffnete" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A player or group opened something ingame. Could also be the settings for example.",
                ["de-DE"] = "Ein Spieler oder eine Gruppe öffnete etwas im Spiel. Könnten zum Beispiel auch die Einstellungen sein." });

        /// <summary>
        /// A player or group brought some game objects into order.
        /// </summary>
        public xAPI_Verb ordered = new xAPI_Verb(
            context: "Generic",
            key: "ordered",
            names: new Dictionary<string, string> {
                ["en-US"] = "ordered",
                ["de-DE"] = "ordnete" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A player or group brought some game objects into order.",
                ["de-DE"] = "Ein Spieler oder eine Gruppe ordnete ein Anzahl an spielrelevanten Gegenständen." });

        /// <summary>
        /// A player or group played a game or something specific in a game.
        /// </summary>
        public xAPI_Verb played = new xAPI_Verb(
            context: "Generic",
            key: "played",
            names: new Dictionary<string, string> {
                ["en-US"] = "played",
                ["de-DE"] = "spielte" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A player or group played a game or something specific in a game.",
                ["de-DE"] = "Ein Spieler oder eine Gruppe spielte ein Spiel oder etwas spezifisches in einem Spiel." });

        /// <summary>
        /// A player or gorup pressed a specific button. Only use if the press of specific buttons should be tracked. Use the specific button as activity then. Otherwise use press as verb and button as activity.
        /// </summary>
        public xAPI_Verb pressedButton = new xAPI_Verb(
            context: "Generic",
            key: "pressedButton",
            names: new Dictionary<string, string> {
                ["en-US"] = "pressedButton",
                ["de-DE"] = "drückte Knopf" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A player or gorup pressed a specific button. Only use if the press of specific buttons should be tracked. Use the specific button as activity then. Otherwise use press as verb and button as activity.",
                ["de-DE"] = "Ein Spieler oder eine Gruppe drückte einen bestimmten Knopf. Sollte nur genutzt werden, wenn das Drücken eines bestimmten Knopfes aufgezeichnet werden soll. Nutze dann den speziellen Knopf als Aktivität der Aussage. Andernfalls nutze drückte als Verb und Knopf als Aktivität." });

        /// <summary>
        /// A player or group progressed in some game related way(achieved something or completed something).
        /// </summary>
        public xAPI_Verb progressed = new xAPI_Verb(
            context: "Generic",
            key: "progressed",
            names: new Dictionary<string, string> {
                ["en-US"] = "progressed",
                ["de-DE"] = "erzielte Fortschritt" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A player or group progressed in some game related way(achieved something or completed something).",
                ["de-DE"] = "Ein Spieler oder eine Gruppe erzielte in einer Form einen Fortschritt im Spiel(erreichte oder vollendete etwas)." });

        /// <summary>
        /// A player read some text. Only use if it can be assured that the text is read.
        /// </summary>
        public xAPI_Verb read = new xAPI_Verb(
            context: "Generic",
            key: "read",
            names: new Dictionary<string, string> {
                ["en-US"] = "read",
                ["de-DE"] = "las" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A player read some text. Only use if it can be assured that the text is read.",
                ["de-DE"] = "Ein Spieler las einen Text. Sollte nur genutzt werden wenn sichergestellt werden kann, dass der Text auch wirklich gelesen wurde." });

        /// <summary>
        /// A player or group received for example an item or help.
        /// </summary>
        public xAPI_Verb received = new xAPI_Verb(
            context: "Generic",
            key: "received",
            names: new Dictionary<string, string> {
                ["en-US"] = "received",
                ["de-DE"] = "erhielt" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A player or group received for example an item or help.",
                ["de-DE"] = "Ein Spieler oder eine Gruppe erhielt zum Beispiel einen Gegenstand oder Hilfe." });

        /// <summary>
        /// A player or group saved their progress in a game session.
        /// </summary>
        public xAPI_Verb saved = new xAPI_Verb(
            context: "Generic",
            key: "saved",
            names: new Dictionary<string, string> {
                ["en-US"] = "saved",
                ["de-DE"] = "speicherte" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A player or group saved their progress in a game session.",
                ["de-DE"] = "Ein Spieler oder eine Gruppe speicherte ihren Spielfortschritt in einer Spielinstanz ab." });

        /// <summary>
        /// A player or group searched for an game related item or searched a game location.
        /// </summary>
        public xAPI_Verb searched = new xAPI_Verb(
            context: "Generic",
            key: "searched",
            names: new Dictionary<string, string> {
                ["en-US"] = "searched",
                ["de-DE"] = "suchte" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A player or group searched for an game related item or searched a game location.",
                ["de-DE"] = "Ein Spieler oder ein Gruppe suchte nach einem spielrelevanten Gegenstand oder zum Beispiel auch einen Spielort ab." });

        /// <summary>
        /// A player or group selected some game related option. This could be an answer or a level for example.
        /// </summary>
        public xAPI_Verb selected = new xAPI_Verb(
            context: "Generic",
            key: "selected",
            names: new Dictionary<string, string> {
                ["en-US"] = "selected",
                ["de-DE"] = "wählte aus" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A player or group selected some game related option. This could be an answer or a level for example.",
                ["de-DE"] = "Ein Spieler oder eine Gruppe wählte eine für das Spiel relevante option. Dies könnte eine Antwort auf eine Frage oder auch ein Level sein." });

        /// <summary>
        /// A player or group shared an ingame object with another player or a group of players.
        /// </summary>
        public xAPI_Verb shared = new xAPI_Verb(
            context: "Generic",
            key: "shared",
            names: new Dictionary<string, string> {
                ["en-US"] = "shared",
                ["de-DE"] = "teilte" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A player or group shared an ingame object with another player or a group of players.",
                ["de-DE"] = "Ein Spieler oder eine Gruppe teitle eine Spielgegenstand mit einem anderen Spieler oder einer Gruppe." });

        /// <summary>
        /// A player or group solved some task.
        /// </summary>
        public xAPI_Verb solved = new xAPI_Verb(
            context: "Generic",
            key: "solved",
            names: new Dictionary<string, string> {
                ["en-US"] = "solved",
                ["de-DE"] = "löste" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A player or group solved some task.",
                ["de-DE"] = "Ein Spieler oder eine Gruppe löste eine Aufgabe." });

        /// <summary>
        /// A player or group sorted some game related objects.
        /// </summary>
        public xAPI_Verb sorted = new xAPI_Verb(
            context: "Generic",
            key: "sorted",
            names: new Dictionary<string, string> {
                ["en-US"] = "sorted",
                ["de-DE"] = "sortierte" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A player or group sorted some game related objects.",
                ["de-DE"] = "Ein Spieler oder eine Gruppe sortierte spielrelevante Objekte." });

        /// <summary>
        /// An actor synchronized game data with an LMS for example.
        /// </summary>
        public xAPI_Verb synchronized = new xAPI_Verb(
            context: "Generic",
            key: "synchronized",
            names: new Dictionary<string, string> {
                ["en-US"] = "synchronized",
                ["de-DE"] = "synchronisierte" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "An actor synchronized game data with an LMS for example.",
                ["de-DE"] = "Ein akteur synchronisierte Spieldaten mit einem LMS zum Beispiel." });

        /// <summary>
        /// A player or group toggled some switch.
        /// </summary>
        public xAPI_Verb toggled = new xAPI_Verb(
            context: "Generic",
            key: "toggled",
            names: new Dictionary<string, string> {
                ["en-US"] = "toggled",
                ["de-DE"] = "schaltete um" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A player or group toggled some switch.",
                ["de-DE"] = "Ein Spieler oder eine Gruppe schaltete eine Schalter um." });

        /// <summary>
        /// A player or group triggered a game related event.
        /// </summary>
        public xAPI_Verb triggered = new xAPI_Verb(
            context: "Generic",
            key: "triggered",
            names: new Dictionary<string, string> {
                ["en-US"] = "triggered",
                ["de-DE"] = "löste aus" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A player or group triggered a game related event.",
                ["de-DE"] = "Ein Spieler oder eine Gruppe löste ein spielrelevantes Ereignis aus." });

        /// <summary>
        /// A player or group unfolded a menu for example.
        /// </summary>
        public xAPI_Verb unfolded = new xAPI_Verb(
            context: "Generic",
            key: "unfolded",
            names: new Dictionary<string, string> {
                ["en-US"] = "unfolded",
                ["de-DE"] = "klappte aus" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A player or group unfolded a menu for example.",
                ["de-DE"] = "Ein Spieler oder eine Gruppe klappte ein Menü zum Beispiel aus." });

        /// <summary>
        /// A player or group unlocked some game related option or item.
        /// </summary>
        public xAPI_Verb unlocked = new xAPI_Verb(
            context: "Generic",
            key: "unlocked",
            names: new Dictionary<string, string> {
                ["en-US"] = "unlocked",
                ["de-DE"] = "schaltete frei" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A player or group unlocked some game related option or item.",
                ["de-DE"] = "Ein Spieler oder eine Gruppe schaltete eine spielrelevante Option oder einen Gegenstand frei." });

        /// <summary>
        /// A player or group used some game related item.
        /// </summary>
        public xAPI_Verb used = new xAPI_Verb(
            context: "Generic",
            key: "used",
            names: new Dictionary<string, string> {
                ["en-US"] = "used",
                ["de-DE"] = "benutzte" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A player or group used some game related item.",
                ["de-DE"] = "Ein Spieler oder eine Gruppe benutze einen spielrelevanten Gegenstand." });

        /// <summary>
        /// A group of players voted on something.
        /// </summary>
        public xAPI_Verb voted = new xAPI_Verb(
            context: "Generic",
            key: "voted",
            names: new Dictionary<string, string> {
                ["en-US"] = "voted",
                ["de-DE"] = "stimmte ab" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A group of players voted on something.",
                ["de-DE"] = "Eine Gruppe von Spielern stimmte für etwas ab." });

        /// <summary>
        /// A player or group watched a video. Only use if you can assure the players really watched the video, use eye-tracking for example.
        /// </summary>
        public xAPI_Verb watched = new xAPI_Verb(
            context: "Generic",
            key: "watched",
            names: new Dictionary<string, string> {
                ["en-US"] = "watched",
                ["de-DE"] = "schaute" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A player or group watched a video. Only use if you can assure the players really watched the video, use eye-tracking for example.",
                ["de-DE"] = "Ein Spieler oder eine Gruppe schaute ein Video. Sollte nur genutzt werden wenn sichergestellt werden kann, dass das Video wirklich geschaut wurde. Eyetracking könnte dafür zum Beispiel benutzt werden." });

        /// <summary>
        /// A player or group won a game.
        /// </summary>
        public xAPI_Verb won = new xAPI_Verb(
            context: "Generic",
            key: "won",
            names: new Dictionary<string, string> {
                ["en-US"] = "won",
                ["de-DE"] = "gewann" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A player or group won a game.",
                ["de-DE"] = "Ein Spieler oder eine Gruppe gewann ein Spiel." });

        /// <summary>
        /// A player put some text into a text field.
        /// </summary>
        public xAPI_Verb wrote = new xAPI_Verb(
            context: "Generic",
            key: "wrote",
            names: new Dictionary<string, string> {
                ["en-US"] = "wrote",
                ["de-DE"] = "schrieb" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A player put some text into a text field.",
                ["de-DE"] = "Ein Spieler oder eine Gruppe gab einen Text in ein Textfeld ein." });

        public xAPI_Verbs_Generic() 
            : base("Generic") {
        }
    }
}