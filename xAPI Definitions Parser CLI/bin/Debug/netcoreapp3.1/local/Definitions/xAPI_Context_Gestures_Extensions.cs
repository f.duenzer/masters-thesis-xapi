namespace xAPI.Definitions {
    using xAPI.Core;

    public class xAPI_Context_Gestures_Extensions {

        public xAPI_Context_Gestures_Extensions() {
        }

        public virtual xAPI_Extensions_Activity_Gestures activity {
            get {
                return new xAPI_Extensions_Activity_Gestures();
            }
        }

        public virtual xAPI_Extensions_Result_Gestures result {
            get {
                return new xAPI_Extensions_Result_Gestures();
            }
        }
    }
}