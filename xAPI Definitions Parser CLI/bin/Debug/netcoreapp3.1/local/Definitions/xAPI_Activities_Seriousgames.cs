namespace xAPI.Definitions {
    using System.Collections.Generic;
    using xAPI.Core;

    public class xAPI_Activities_Seriousgames : xAPI_Activities {
        /// <summary>
        /// Represents a game or competition of any kind.
        /// </summary>
        public xAPI_Activity game = new xAPI_Activity(
            context: "Seriousgames",
            key: "game",
            names: new Dictionary<string, string> {
                ["en-US"] = "game",
                ["de-DE"] = "Spiel" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "Represents a game or competition of any kind.",
                ["de-DE"] = "Repräsentiert jede Art von Spiel." });

        /// <summary>
        /// A level is some, in itself closed, part of the game which the game itself calls level.
        /// </summary>
        public xAPI_Activity level = new xAPI_Activity(
            context: "Seriousgames",
            key: "level",
            names: new Dictionary<string, string> {
                ["en-US"] = "level",
                ["de-DE"] = "Level" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A level is some, in itself closed, part of the game which the game itself calls level.",
                ["de-DE"] = "Ein Level ist ein in sich selbst geschlossener Teil eines Spiels, den das Spiel selber Level nennt." });

        /// <summary>
        /// A session is one execution of a game from booting the game to exiting it.
        /// </summary>
        public xAPI_Activity session = new xAPI_Activity(
            context: "Seriousgames",
            key: "session",
            names: new Dictionary<string, string> {
                ["en-US"] = "session",
                ["de-DE"] = "Spiel Instanz" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A session is one execution of a game from booting the game to exiting it.",
                ["de-DE"] = "Eine Spiel Instanz ist ein Durchlauf eines Spiels vom Initalisieren des Spiels bis zum Beenden." });

        /// <summary>
        /// A stage is a part of a level.
        /// </summary>
        public xAPI_Activity stage = new xAPI_Activity(
            context: "Seriousgames",
            key: "stage",
            names: new Dictionary<string, string> {
                ["en-US"] = "stage",
                ["de-DE"] = "Spielabschnitt" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "A stage is a part of a level.",
                ["de-DE"] = "Ein Spielabschnitt ist ein Teil eines Levels." });

        public xAPI_Activities_Seriousgames() 
            : base("Seriousgames") {
        }
    }
}