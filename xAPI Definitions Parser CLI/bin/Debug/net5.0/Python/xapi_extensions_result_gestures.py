from core.xapi_extensions_result import xAPI_Extensions_Result

class xAPI_Extensions_Result_Gestures(xAPI_Extensions_Result):

    def __init__(self):
        super().__init__("Gestures")

    def numberOfGestures(self, value):
        add(xAPI_Extension(
                context,
                extensionType,
                "numberOfGestures",
                {
                    "en-US": "number of gestures",
                    "de-DE": "Gestenanzahl"
                },
                {
                    "en-US": "The number of actors gestures. Has to be an integer.",
                    "de-DE": "Die Anzahl an Gesten, die die Akteurin tätigt. Muss ein Integer sein."
                }),
            value);
        return self;

    def primaryHand(self, value):
        add(xAPI_Extension(
                context,
                extensionType,
                "primaryHand",
                {
                    "en-US": "primary hand",
                    "de-DE": "Primärhand"
                },
                {
                    "en-US": "Setting, which hand is currently the primary hand.",
                    "de-DE": "Einstellung, welche Hand die Primärhand ist."
                }),
            value);
        return self;
