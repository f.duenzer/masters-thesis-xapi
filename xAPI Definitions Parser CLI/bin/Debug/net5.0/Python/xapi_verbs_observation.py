from core import xapi_verb
from core import xapi_verbs

class xAPI_Verbs_Observation(xAPI_Verbs):
    beschaeftigt = xAPI_Verb(
        "Observation",
        "beschaeftigt",
        {
            "de-DE": "beschäftigt"
        },
        {
            "de-DE": "Ein Akteur beschäftigt sich mit etwas"
        })
    """Ein Akteur beschäftigt sich mit etwas"""

    erklaert = xAPI_Verb(
        "Observation",
        "erklaert",
        {
            "de-DE": "erklärt"
        },
        {
            "de-DE": "Ein Akteur erklärt jemand anderen etwas verbal."
        })
    """Ein Akteur erklärt jemand anderen etwas verbal."""

    erzaehlt = xAPI_Verb(
        "Observation",
        "erzaehlt",
        {
            "de-DE": "erzählt"
        },
        {
            "de-DE": "Ein Akteur erzählt etwas verbal"
        })
    """Ein Akteur erzählt etwas verbal"""

    fragt = xAPI_Verb(
        "Observation",
        "fragt",
        {
            "de-DE": "fragt"
        },
        {
            "de-DE": "Ein Akteur fragt etwas"
        })
    """Ein Akteur fragt etwas"""

    gibt = xAPI_Verb(
        "Observation",
        "gibt",
        {
            "de-DE": "gibt"
        },
        {
            "de-DE": "Ein Akteur gibt etwas"
        })
    """Ein Akteur gibt etwas"""

    motiviert = xAPI_Verb(
        "Observation",
        "motiviert",
        {
            "de-DE": "motiviert"
        },
        {
            "de-DE": "Ein Akteur motiviert jemanden"
        })
    """Ein Akteur motiviert jemanden"""

    nimmt = xAPI_Verb(
        "Observation",
        "nimmt",
        {
            "de-DE": "nimmt"
        },
        {
            "de-DE": "Ein Akteur nimmt etwas"
        })
    """Ein Akteur nimmt etwas"""

    sieht = xAPI_Verb(
        "Observation",
        "sieht",
        {
            "de-DE": "sieht"
        },
        {
            "de-DE": "Ein Akteur sieht etwas"
        })
    """Ein Akteur sieht etwas"""

    unterbricht = xAPI_Verb(
        "Observation",
        "unterbricht",
        {
            "de-DE": "unterbricht"
        },
        {
            "de-DE": "Ein Akteur unterbricht jemand oder etwas anderen verbal."
        })
    """Ein Akteur unterbricht jemand oder etwas anderen verbal."""

    def __init__(self):
        super().__init__("Observation")
