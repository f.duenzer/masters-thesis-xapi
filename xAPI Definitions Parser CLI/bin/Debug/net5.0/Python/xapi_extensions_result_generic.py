from core.xapi_extensions_result import xAPI_Extensions_Result

class xAPI_Extensions_Result_Generic(xAPI_Extensions_Result):

    def __init__(self):
        super().__init__("Generic")

    def endValue(self, value):
        add(xAPI_Extension(
                context,
                extensionType,
                "endValue",
                {
                    "en-US": "End Value",
                    "de-DE": "Endwert"
                },
                {
                    "en-US": "Representation of a object directly after the statement occurred. 'End Value' should be used with the 'Start Value' extension to provide information about the actual change of the object. Can be used with 'edited' or 'changed' verbs.",
                    "de-DE": "Repräsentation von einem Objekt direkt nachdem das Statement auftrat. 'Endwert' sollte mit der Extension 'Startwert' benutzt werden um Information über die Änderung eines Objektes darzulegen. Kann zum Beispiel mit den Verben 'editierte' und 'veränderte' benutzt werden."
                }),
            value);
        return self;

    def index(self, value):
        add(xAPI_Extension(
                context,
                extensionType,
                "index",
                {
                    "en-US": "index",
                    "de-DE": "Index"
                },
                {
                    "en-US": "It is a number to indicies something. Can only be an integer.",
                    "de-DE": "Das ist eine Zahl für das Indexieren von etwas. Kann nur ein Integer sein."
                }),
            value);
        return self;

    def startValue(self, value):
        add(xAPI_Extension(
                context,
                extensionType,
                "startValue",
                {
                    "en-US": "Start Value",
                    "de-DE": "Startwert"
                },
                {
                    "en-US": "Representation of a object directly before the statement occurred. 'Start Value' should be used with the 'End Value' extension to provide information about the actual change of the object. Can be used with 'edited' or 'changed' verbs.",
                    "de-DE": "Repräsentation von einem Objekt bevor das Statement auftrat. 'Startwert' sollte mit der Extension 'Endwert' benutzt werden um Information über die Änderung eines Objektes darzulegen. Kann zum Beispiel mit den Verben 'editierte' und 'veränderte' benutzt werden."
                }),
            value);
        return self;

    def timeSpan(self, value):
        add(xAPI_Extension(
                context,
                extensionType,
                "timeSpan",
                {
                    "en-US": "Time Span",
                    "de-DE": "Zeitspanne"
                },
                {
                    "en-US": "A time span depending on context.",
                    "de-DE": "Eine bestimmte Zeitspanne abhängig vom Kontext."
                }),
            value);
        return self;

    def value(self, value):
        add(xAPI_Extension(
                context,
                extensionType,
                "value",
                {
                    "en-US": "value",
                    "de-DE": "Wert"
                },
                {
                    "en-US": "It is a value of an element.",
                    "de-DE": "Das ist eine Wert eines Elements."
                }),
            value);
        return self;
