from core.xapi_extensions_context import xAPI_Extensions_Context

class xAPI_Extensions_Context_VrRfidChamber(xAPI_Extensions_Context):

    def __init__(self):
        super().__init__("VrRfidChamber")

    def energymeasure(self, value):
        add(xAPI_Extension(
                context,
                extensionType,
                "energymeasure",
                {
                    "en-US": "energymeasure"
                },
                {
                    "en-US": "In this case the energy of RFID tag is measured."
                }),
            value);
        return self;

    def experimentmode(self, value):
        add(xAPI_Extension(
                context,
                extensionType,
                "experimentmode",
                {
                    "en-US": "experimentmode"
                },
                {
                    "en-US": "The experimentmode in which the experiment is carried. Can be energy measurement or reading range measurement"
                }),
            value);
        return self;

    def orientationmeasure(self, value):
        add(xAPI_Extension(
                context,
                extensionType,
                "orientationmeasure",
                {
                    "en-US": "orientationmeasure"
                },
                {
                    "en-US": "In this case the best possible orientation of the RFID tag is measured."
                }),
            value);
        return self;

    def readingrangemeasure(self, value):
        add(xAPI_Extension(
                context,
                extensionType,
                "readingrangemeasure",
                {
                    "en-US": "readingrangemeasure"
                },
                {
                    "en-US": "In this case the reading range of transponder is measured."
                }),
            value);
        return self;

    def session(self, value):
        add(xAPI_Extension(
                context,
                extensionType,
                "session",
                {
                    "en-US": "session"
                },
                {
                    "en-US": "A session is one execution of the lab from entering the lab to exiting it."
                }),
            value);
        return self;
