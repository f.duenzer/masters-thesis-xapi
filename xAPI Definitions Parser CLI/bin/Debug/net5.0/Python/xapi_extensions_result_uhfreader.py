from core.xapi_extensions_result import xAPI_Extensions_Result

class xAPI_Extensions_Result_UhfReader(xAPI_Extensions_Result):

    def __init__(self):
        super().__init__("UhfReader")

    def transponderState(self, value):
        add(xAPI_Extension(
                context,
                extensionType,
                "transponderState",
                {
                    "en-US": "transponderState"
                },
                {
                    "en-US": "State of the transpnder to the physical reader."
                }),
            value);
        return self;
