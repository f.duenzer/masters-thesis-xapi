from core.xapi_extensions_result import xAPI_Extensions_Result

class xAPI_Extensions_Result_VirtualReality(xAPI_Extensions_Result):

    def __init__(self):
        super().__init__("VirtualReality")

    def position(self, value):
        add(xAPI_Extension(
                context,
                extensionType,
                "position",
                {
                    "en-US": "position",
                    "de-DE": "Position"
                },
                {
                    "en-US": "Position in scene.",
                    "de-DE": "Position in der Szene."
                }),
            value);
        return self;

    def rotation(self, value):
        add(xAPI_Extension(
                context,
                extensionType,
                "rotation",
                {
                    "en-US": "rotation",
                    "de-DE": "Rotation"
                },
                {
                    "en-US": "Rotation in scene.",
                    "de-DE": "Rotation in der Szene."
                }),
            value);
        return self;

    def scale(self, value):
        add(xAPI_Extension(
                context,
                extensionType,
                "scale",
                {
                    "en-US": "scale",
                    "de-DE": "Skalierung"
                },
                {
                    "en-US": "Scale in scene.",
                    "de-DE": "Skalierung in der Szene."
                }),
            value);
        return self;
