from core import xapi_context
from core.xapi_verbs_tagformance import xAPI_Verbs_Tagformance
from core.xapi_activities_tagformance import xAPI_Activities_Tagformance
from core.xapi_context_tagformance_extensions import xAPI_Context_Tagformance_Extensions

class xAPI_Context_Tagformance(xAPI_Context):
    verbs = xAPI_Verbs_Tagformance()

    activities = xAPI_Activities_Tagformance()

    extensions = xAPI_Context_Tagformance_Extensions()

    def __init__(self):
        super().__init__("xAPI_Definitions_Parser.Definitions.xAPIContext")
