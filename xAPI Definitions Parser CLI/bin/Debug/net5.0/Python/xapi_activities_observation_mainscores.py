from core import xapi_activity
from core import xapi_activities

class xAPI_Activities_Observation_Mainscores(xAPI_Activities):
    achtsamkeit = xAPI_Activity(
        "Observation",
        "achtsamkeit",
        {
            "de-DE": "Achtsamkeit"
        },
        {
            "de-DE": "Mainscore (5/5) wenn ein Akteuer Achtsamkeit zeigt"
        })
    """Mainscore (5/5) wenn ein Akteuer Achtsamkeit zeigt"""

    geduld = xAPI_Activity(
        "Observation",
        "geduld",
        {
            "de-DE": "Geduld"
        },
        {
            "de-DE": "Mainscore (5/5) wenn ein Akteuer Geduld zeigt"
        })
    """Mainscore (5/5) wenn ein Akteuer Geduld zeigt"""

    kommunikationsfaehigkeiten = xAPI_Activity(
        "Observation",
        "kommunikationsfaehigkeiten",
        {
            "de-DE": "Kommunikationsfähigkeiten"
        },
        {
            "de-DE": "Mainscore (5/5) wenn ein Akteuer Kommunikationsfähigkeiten zeigt"
        })
    """Mainscore (5/5) wenn ein Akteuer Kommunikationsfähigkeiten zeigt"""

    kreativitaet = xAPI_Activity(
        "Observation",
        "kreativitaet",
        {
            "de-DE": "Kreativität"
        },
        {
            "de-DE": "Mainscore (5/5) wenn ein Akteuer Kreativität zeigt"
        })
    """Mainscore (5/5) wenn ein Akteuer Kreativität zeigt"""

    methodisches_Potenzial = xAPI_Activity(
        "Observation",
        "methodisches_Potenzial",
        {
            "de-DE": "Methodisches Potenzial"
        },
        {
            "de-DE": "Mainscore (5/5) wenn ein Akteuer Methodisches Potenzial zeigt"
        })
    """Mainscore (5/5) wenn ein Akteuer Methodisches Potenzial zeigt"""

    motivation_und_Leistungsbereitschaft = xAPI_Activity(
        "Observation",
        "motivation_und_Leistungsbereitschaft",
        {
            "de-DE": "Motivation und Leistungsbereitschaft"
        },
        {
            "de-DE": "Mainscore (5/5) wenn ein Akteuer Motivation und Leistungsbereitschaft zeigt"
        })
    """Mainscore (5/5) wenn ein Akteuer Motivation und Leistungsbereitschaft zeigt"""

    praktisches_Potenzial = xAPI_Activity(
        "Observation",
        "praktisches_Potenzial",
        {
            "de-DE": "Praktisches Potenzial"
        },
        {
            "de-DE": "Mainscore (5/5) wenn ein Akteuer Praktisches Potenzial zeigt"
        })
    """Mainscore (5/5) wenn ein Akteuer Praktisches Potenzial zeigt"""

    problemloesen = xAPI_Activity(
        "Observation",
        "problemloesen",
        {
            "de-DE": "Problemlösen"
        },
        {
            "de-DE": "Mainscore (5/5) wenn ein Akteuer Problemlösent zeigt"
        })
    """Mainscore (5/5) wenn ein Akteuer Problemlösent zeigt"""

    raeumliches_Vorstellungsvermoegen = xAPI_Activity(
        "Observation",
        "raeumliches_Vorstellungsvermoegen",
        {
            "de-DE": "Räumliches Vorstellungsvermögen"
        },
        {
            "de-DE": "Mainscore (5/5) wenn ein Akteuer Räumliches Vorstellungsvermögen zeigt"
        })
    """Mainscore (5/5) wenn ein Akteuer Räumliches Vorstellungsvermögen zeigt"""

    soziale_Potenziale = xAPI_Activity(
        "Observation",
        "soziale_Potenziale",
        {
            "de-DE": "Soziale Potenziale"
        },
        {
            "de-DE": "Mainscore (5/5) wenn ein Akteuer Soziale Potenziale zeigt"
        })
    """Mainscore (5/5) wenn ein Akteuer Soziale Potenziale zeigt"""

    sprachkompetenz = xAPI_Activity(
        "Observation",
        "sprachkompetenz",
        {
            "de-DE": "Sprachkompetenz"
        },
        {
            "de-DE": "Mainscore (5/5) wenn ein Akteuer Sprachkompetenz zeigt"
        })
    """Mainscore (5/5) wenn ein Akteuer Sprachkompetenz zeigt"""

    def __init__(self):
        super().__init__("Observation")
