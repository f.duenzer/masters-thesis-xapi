from core import xapi_verb
from core import xapi_verbs

class xAPI_Verbs_Lms(xAPI_Verbs):
    accessed = xAPI_Verb(
        "Lms",
        "accessed",
        {
            "en-US": "accessed",
            "de-DE": "zugegriffen"
        },
        {
            "en-US": "Actor accessed/viewed a page, file, video, ...",
            "de-DE": "Akteur hat (auf) eine Seite, eine Datei, ein Video, ... zugegriffen/angesehen"
        })
    """Actor accessed/viewed a page, file, video, ..."""

    added = xAPI_Verb(
        "Lms",
        "added",
        {
            "en-US": "added",
            "de-DE": "hinzugefügt"
        },
        {
            "en-US": "Actor added an object/person to a collection",
            "de-DE": "Akteur hat eine/n Person/Gegenstand zu einer Sammlung hinzugefügt"
        })
    """Actor added an object/person to a collection"""

    approved = xAPI_Verb(
        "Lms",
        "approved",
        {
            "en-US": "approved",
            "de-DE": "genehmigt"
        },
        {
            "en-US": "Actor approved an object (in Moodle \"learning plan\")",
            "de-DE": "Akteur genehmigte ein Objekt (in Moodle \"Lernplan\")"
        })
    """Actor approved an object (in Moodle "learning plan")"""

    assigned = xAPI_Verb(
        "Lms",
        "assigned",
        {
            "en-US": "assigned",
            "de-DE": "zugeteilt"
        },
        {
            "en-US": "Actor assigned an object/task/activity to someone",
            "de-DE": "Akteur wurde ein/e Objekt/Aufgabe/Aktivität zugeteilt"
        })
    """Actor assigned an object/task/activity to someone"""

    blocked = xAPI_Verb(
        "Lms",
        "blocked",
        {
            "en-US": "blocked",
            "de-DE": "gesperrt"
        },
        {
            "en-US": "Actor blocked a person",
            "de-DE": "Akteur hat eine Person gesperrt"
        })
    """Actor blocked a person"""

    cancelled = xAPI_Verb(
        "Lms",
        "cancelled",
        {
            "en-US": "cancelled",
            "de-DE": "abgesagt"
        },
        {
            "en-US": "Actor cancelled an activity, a meetin",
            "de-DE": "Akteur hat eine Aktivität, ein Meeting abgesagt"
        })
    """Actor cancelled an activity, a meetin"""

    closed = xAPI_Verb(
        "Lms",
        "closed",
        {
            "en-US": "closed",
            "de-DE": "geschlossen"
        },
        {
            "en-US": "Actor closed an object (forum post)",
            "de-DE": "Akteur hat ein Objekt (Forumeintrag) geschlossen"
        })
    """Actor closed an object (forum post)"""

    completed = xAPI_Verb(
        "Lms",
        "completed",
        {
            "en-US": "completed",
            "de-DE": "fertiggestellt"
        },
        {
            "en-US": "Actor completed an activity/task",
            "de-DE": "Akteur hat eine Aktivität/Aufgabe fertiggestellt"
        })
    """Actor completed an activity/task"""

    created = xAPI_Verb(
        "Lms",
        "created",
        {
            "en-US": "created",
            "de-DE": "erstellt"
        },
        {
            "en-US": "Actor created an object",
            "de-DE": "Akteur hat ein Objekt erstellt"
        })
    """Actor created an object"""

    deleted = xAPI_Verb(
        "Lms",
        "deleted",
        {
            "en-US": "deleted",
            "de-DE": "gelöscht"
        },
        {
            "en-US": "Actor deleted an object",
            "de-DE": "Akteur hat ein Objekt gelöscht"
        })
    """Actor deleted an object"""

    downloaded = xAPI_Verb(
        "Lms",
        "downloaded",
        {
            "en-US": "downloaded",
            "de-DE": "heruntergeladen"
        },
        {
            "en-US": "Actor downloaded an object (e. g. a PDF file)",
            "de-DE": "Akteur hat ein Objekt (z. B. eine PDF Datei) heruntergeladen"
        })
    """Actor downloaded an object (e. g. a PDF file)"""

    exported = xAPI_Verb(
        "Lms",
        "exported",
        {
            "en-US": "exported",
            "de-DE": "exportiert"
        },
        {
            "en-US": "Actor exported an object/data",
            "de-DE": "Akteur hat ein Objekt/Daten exportiert"
        })
    """Actor exported an object/data"""

    failed = xAPI_Verb(
        "Lms",
        "failed",
        {
            "en-US": "failed",
            "de-DE": "scheiterte"
        },
        {
            "en-US": "Actor failed an activity (quiz attempt, upload, log in)",
            "de-DE": "Akteur scheiterte an einer Aktivität (Quizversuch, Upload, Einloggen)"
        })
    """Actor failed an activity (quiz attempt, upload, log in)"""

    graded = xAPI_Verb(
        "Lms",
        "graded",
        {
            "en-US": "graded",
            "de-DE": "benotet"
        },
        {
            "en-US": "Actor rated an object/person",
            "de-DE": "Akteur hat ein/e Objekt/Person benotet"
        })
    """Actor rated an object/person"""

    imported = xAPI_Verb(
        "Lms",
        "imported",
        {
            "en-US": "imported",
            "de-DE": "importiert"
        },
        {
            "en-US": "Actor imported an object/data",
            "de-DE": "Akteur hat ein Objekt/Daten importiert"
        })
    """Actor imported an object/data"""

    linked = xAPI_Verb(
        "Lms",
        "linked",
        {
            "en-US": "linked",
            "de-DE": "verknüpft"
        },
        {
            "en-US": "Actor linked an object/person to an object",
            "de-DE": "Akteur hat ein/e Objekt/Person mit einem Objekt verknüpft"
        })
    """Actor linked an object/person to an object"""

    locked = xAPI_Verb(
        "Lms",
        "locked",
        {
            "en-US": "locked",
            "de-DE": "gesperrt"
        },
        {
            "en-US": "Actor locked an object",
            "de-DE": "Akteur hat ein Objekt gesperrt"
        })
    """Actor locked an object"""

    logged_in_as = xAPI_Verb(
        "Lms",
        "logged_in_as",
        {
            "en-US": "logged in as",
            "de-DE": "angemeldet als"
        },
        {
            "en-US": "Actor logged in as another person",
            "de-DE": "Akteur hat sich als eine andere Person angemeldet"
        })
    """Actor logged in as another person"""

    logged_in = xAPI_Verb(
        "Lms",
        "logged_in",
        {
            "en-US": "logged in",
            "de-DE": "angemeldet"
        },
        {
            "en-US": "Actor logged in (as him-/herself)",
            "de-DE": "Akteur hat sich angemeldet (als er/sie selbst)"
        })
    """Actor logged in (as him-/herself)"""

    logged_out = xAPI_Verb(
        "Lms",
        "logged_out",
        {
            "en-US": "logged out",
            "de-DE": "abgemeldet"
        },
        {
            "en-US": "Actor logged out",
            "de-DE": "Akteur hat sich abgemeldet"
        })
    """Actor logged out"""

    loggedIn = xAPI_Verb(
        "Lms",
        "loggedIn",
        {
            "en-US": "logged in",
            "de-DE": "meldet sich an"
        },
        {
            "en-US": "Actor logged in (as him-/herself)",
            "de-DE": "Akteur hat sich angemeldet (als er/sie selbst)"
        })
    """Actor logged in (as him-/herself)"""

    loggedInAs = xAPI_Verb(
        "Lms",
        "loggedInAs",
        {
            "en-US": "logged in as",
            "de-DE": "angemeldet als"
        },
        {
            "en-US": "Actor logged in as another person",
            "de-DE": "Akteur hat sich als eine andere Person angemeldet"
        })
    """Actor logged in as another person"""

    loggedOut = xAPI_Verb(
        "Lms",
        "loggedOut",
        {
            "en-US": "logged out",
            "de-DE": "abgemeldet"
        },
        {
            "en-US": "Actor logged out",
            "de-DE": "Akteur hat sich abgemeldet"
        })
    """Actor logged out"""

    moved = xAPI_Verb(
        "Lms",
        "moved",
        {
            "en-US": "moved",
            "de-DE": "bewegt"
        },
        {
            "en-US": "Actor moved an object/person from one group/area to another",
            "de-DE": "Akteuer hat ein/e Objekt/Person von einer Gruppe/Region in eine andere bewegt"
        })
    """Actor moved an object/person from one group/area to another"""

    removed = xAPI_Verb(
        "Lms",
        "removed",
        {
            "en-US": "removed",
            "de-DE": "entfernt"
        },
        {
            "en-US": "Actor removed an object/person from a collection",
            "de-DE": "Akteur hat ein/e Objekt/Person von einer Sammlung entfernt"
        })
    """Actor removed an object/person from a collection"""

    reopened = xAPI_Verb(
        "Lms",
        "reopened",
        {
            "en-US": "reopened",
            "de-DE": "wiedereröffnet"
        },
        {
            "en-US": "Actor reopened an activity (assignment submission, forum)",
            "de-DE": "Akteuer hat eine Aktivität (Aufgabenabgabe, Forum) wiedereröffnet"
        })
    """Actor reopened an activity (assignment submission, forum)"""

    replaced = xAPI_Verb(
        "Lms",
        "replaced",
        {
            "en-US": "replaced",
            "de-DE": "ersetzt"
        },
        {
            "en-US": "Actor replaced an object/person",
            "de-DE": "Akteur hat ein/e Objekt/Person ersetzt"
        })
    """Actor replaced an object/person"""

    requested = xAPI_Verb(
        "Lms",
        "requested",
        {
            "en-US": "requested",
            "de-DE": "angefordert"
        },
        {
            "en-US": "Actor requested an object/access/information",
            "de-DE": "Akteur hat ein/en Objekt/Zugang/Informationen angefordert"
        })
    """Actor requested an object/access/information"""

    resetted = xAPI_Verb(
        "Lms",
        "resetted",
        {
            "en-US": "resetted",
            "de-DE": "zurückgesetzt"
        },
        {
            "en-US": "Actor resetted an object",
            "de-DE": "Akteur hat ein Objekt zurückgesetzt"
        })
    """Actor resetted an object"""

    restored = xAPI_Verb(
        "Lms",
        "restored",
        {
            "en-US": "restored",
            "de-DE": "wiederhergestellt"
        },
        {
            "en-US": "Actor restored a previous version of an object",
            "de-DE": "Akteur hat eine vorherige Version eines Objektes wiederhergestellt"
        })
    """Actor restored a previous version of an object"""

    reviewed = xAPI_Verb(
        "Lms",
        "reviewed",
        {
            "en-US": "reviewed",
            "de-DE": "geprüft"
        },
        {
            "en-US": "Actor reviewed an object",
            "de-DE": "Akteur hat ein Objekt geprüft"
        })
    """Actor reviewed an object"""

    searched = xAPI_Verb(
        "Lms",
        "searched",
        {
            "en-US": "searched",
            "de-DE": "gesucht"
        },
        {
            "en-US": "Actor searched for something",
            "de-DE": "Akteur hat nach etwas gesucht"
        })
    """Actor searched for something"""

    sent = xAPI_Verb(
        "Lms",
        "sent",
        {
            "en-US": "sent",
            "de-DE": "gesendet"
        },
        {
            "en-US": "Actor sent a message to a person/group",
            "de-DE": "Akteur hat eine Nachricht zu einer Person/Gruppe gesendet"
        })
    """Actor sent a message to a person/group"""

    started = xAPI_Verb(
        "Lms",
        "started",
        {
            "en-US": "started",
            "de-DE": "gestartet"
        },
        {
            "en-US": "Actor started an activity",
            "de-DE": "Akteur hat eine Aktvität gestartet"
        })
    """Actor started an activity"""

    stopped = xAPI_Verb(
        "Lms",
        "stopped",
        {
            "en-US": "stopped",
            "de-DE": "gestoppt"
        },
        {
            "en-US": "Actor stopped an activity/process",
            "de-DE": "Akteur hat eine/n Aktivität/Prozess gestoppt"
        })
    """Actor stopped an activity/process"""

    succeeded = xAPI_Verb(
        "Lms",
        "succeeded",
        {
            "en-US": "succeeded",
            "de-DE": "gelang"
        },
        {
            "en-US": "Actor succeeded an activity",
            "de-DE": "Akteur ist eine Aktivität gelungen"
        })
    """Actor succeeded an activity"""

    triggered = xAPI_Verb(
        "Lms",
        "triggered",
        {
            "en-US": "triggered",
            "de-DE": "ausgelöst"
        },
        {
            "en-US": "Actor triggered a process",
            "de-DE": "Akteur hat einen Prozess ausgelöst"
        })
    """Actor triggered a process"""

    unapproved = xAPI_Verb(
        "Lms",
        "unapproved",
        {
            "en-US": "unapproved",
            "de-DE": "nicht genehmigt"
        },
        {
            "en-US": "Actor unapproved an object (in Moodle \"learning plan\")",
            "de-DE": "Akteur hat ein Objekt (in Moodle \"learning plan\") nicht genehmigt"
        })
    """Actor unapproved an object (in Moodle "learning plan")"""

    unblocked = xAPI_Verb(
        "Lms",
        "unblocked",
        {
            "en-US": "unblocked",
            "de-DE": "entblockt"
        },
        {
            "en-US": "Actor unblocked a blocked person",
            "de-DE": "Akteur hat eine blockierte Person entblockt"
        })
    """Actor unblocked a blocked person"""

    unlinked = xAPI_Verb(
        "Lms",
        "unlinked",
        {
            "en-US": "unlinked",
            "de-DE": "getrennt"
        },
        {
            "en-US": "Actor unlinked an object/person from an object",
            "de-DE": "Akteur hat ein/e Objekt/Person von einem Objekt getrennt"
        })
    """Actor unlinked an object/person from an object"""

    unlocked = xAPI_Verb(
        "Lms",
        "unlocked",
        {
            "en-US": "unlocked",
            "de-DE": "freigegeben"
        },
        {
            "en-US": "Actor unlocked an object",
            "de-DE": "Akteur hat ein Objekt freigegeben"
        })
    """Actor unlocked an object"""

    updated = xAPI_Verb(
        "Lms",
        "updated",
        {
            "en-US": "updated",
            "de-DE": "aktualisiert"
        },
        {
            "en-US": "Actor updated the content of an object",
            "de-DE": "Akteur aktualisierte den Inhalt eines Objekts."
        })
    """Actor updated the content of an object"""

    withdrew = xAPI_Verb(
        "Lms",
        "withdrew",
        {
            "en-US": "withdrew",
            "de-DE": "zog zurück"
        },
        {
            "en-US": "Actor removed the assignment of an object/task/activity to someone",
            "de-DE": "Akteur hat die Zuteilung eines/r Objektes/Aufgabe/Aktivität zurückgezogen"
        })
    """Actor removed the assignment of an object/task/activity to someone"""

    def __init__(self):
        super().__init__("Lms")
