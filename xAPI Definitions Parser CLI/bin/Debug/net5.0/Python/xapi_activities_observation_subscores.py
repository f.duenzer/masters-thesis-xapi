from core import xapi_activity
from core import xapi_activities

class xAPI_Activities_Observation_Subscores(xAPI_Activities):
    achtet_auf_anderenimmt_Ruecksicht = xAPI_Activity(
        "Observation",
        "achtet_auf_anderenimmt_Ruecksicht",
        {
            "de-DE": "achtet auf anderenimmt Rücksicht"
        },
        {
            "de-DE": "Subscore (5/5 bzw. ++) wenn ein Akteuer auf andere achtet/Rücksicht nimmt"
        })
    """Subscore (5/5 bzw. ++) wenn ein Akteuer auf andere achtet/Rücksicht nimmt"""

    artikuliert_eigene_Gedanken_klar_und_verstaendlich = xAPI_Activity(
        "Observation",
        "artikuliert_eigene_Gedanken_klar_und_verstaendlich",
        {
            "de-DE": "artikuliert eigene Gedanken klar und verständlich"
        },
        {
            "de-DE": "Subscore (5/5 bzw. ++) wenn ein Akteuer eigene Gedanken klar und verständlich artikuliert"
        })
    """Subscore (5/5 bzw. ++) wenn ein Akteuer eigene Gedanken klar und verständlich artikuliert"""

    behaelt_den_Zielzustand_stets_im_Blick = xAPI_Activity(
        "Observation",
        "behaelt_den_Zielzustand_stets_im_Blick",
        {
            "de-DE": "behält den Zielzustand stets im Blick"
        },
        {
            "de-DE": "Subscore (5/5 bzw. ++) wenn ein Akteuer den Zielzustand stets im Blick behält"
        })
    """Subscore (5/5 bzw. ++) wenn ein Akteuer den Zielzustand stets im Blick behält"""

    behaelt_stehts_den_Ueberblick = xAPI_Activity(
        "Observation",
        "behaelt_stehts_den_Ueberblick",
        {
            "de-DE": "behält stehts den Überblick"
        },
        {
            "de-DE": "Subscore (5/5 bzw. ++) wenn ein Akteuer stehts den Überblick behält"
        })
    """Subscore (5/5 bzw. ++) wenn ein Akteuer stehts den Überblick behält"""

    beteiligt_sich_sprachlich_an_Loesung_der_Aufgabestellt_Fragen = xAPI_Activity(
        "Observation",
        "beteiligt_sich_sprachlich_an_Loesung_der_Aufgabestellt_Fragen",
        {
            "de-DE": "beteiligt sich sprachlich an Lösung der Aufgabe/stellt Fragen"
        },
        {
            "de-DE": "Subscore (5/5 bzw. ++) wenn ein Akteuer sich sprachlich an Lösung der Aufgabe beteiligt/Fragen stellt"
        })
    """Subscore (5/5 bzw. ++) wenn ein Akteuer sich sprachlich an Lösung der Aufgabe beteiligt/Fragen stellt"""

    bleibt_in_schwierigenunangenehmen_Situationen_gelassen_und_ruhig = xAPI_Activity(
        "Observation",
        "bleibt_in_schwierigenunangenehmen_Situationen_gelassen_und_ruhig",
        {
            "de-DE": "bleibt in schwierigen/unangenehmen Situationen gelassen und ruhig"
        },
        {
            "de-DE": "Subscore (5/5 bzw. ++) wenn ein Akteuer in schwierigen/unangenehmen Situationen gelassen und ruhig bleibt"
        })
    """Subscore (5/5 bzw. ++) wenn ein Akteuer in schwierigen/unangenehmen Situationen gelassen und ruhig bleibt"""

    entwickelt_eigene_Ideen = xAPI_Activity(
        "Observation",
        "entwickelt_eigene_Ideen",
        {
            "de-DE": "entwickelt eigene Ideen"
        },
        {
            "de-DE": "Subscore (5/5 bzw. ++) wenn ein Akteuer eigene Ideen entwickelt"
        })
    """Subscore (5/5 bzw. ++) wenn ein Akteuer eigene Ideen entwickelt"""

    erarbeitet_ein_klares_Bild_der_AusgangssituationProblemstellung = xAPI_Activity(
        "Observation",
        "erarbeitet_ein_klares_Bild_der_AusgangssituationProblemstellung",
        {
            "de-DE": "erarbeitet ein klares Bild der Ausgangssituation/Problemstellung"
        },
        {
            "de-DE": "Subscore (5/5 bzw. ++) wenn ein Akteuer erarbeitet ein klares Bild der Ausgangssituation/Problemstellung"
        })
    """Subscore (5/5 bzw. ++) wenn ein Akteuer erarbeitet ein klares Bild der Ausgangssituation/Problemstellung"""

    findet_Moeglichkeiten_zur_Strukturierung_einer_Aufgabe = xAPI_Activity(
        "Observation",
        "findet_Moeglichkeiten_zur_Strukturierung_einer_Aufgabe",
        {
            "de-DE": "findet Möglichkeiten zur Strukturierung einer Aufgabe"
        },
        {
            "de-DE": "Subscore (5/5 bzw. ++) wenn ein Akteuer Möglichkeiten zur Strukturierung einer Aufgabe findet"
        })
    """Subscore (5/5 bzw. ++) wenn ein Akteuer Möglichkeiten zur Strukturierung einer Aufgabe findet"""

    fuehrt_Bewegungen_praezisegenau_aus = xAPI_Activity(
        "Observation",
        "fuehrt_Bewegungen_praezisegenau_aus",
        {
            "de-DE": "führt Bewegungen präzise/genau aus"
        },
        {
            "de-DE": "Subscore (5/5 bzw. ++) wenn ein Akteuer Bewegungen präzisegenau ausführt"
        })
    """Subscore (5/5 bzw. ++) wenn ein Akteuer Bewegungen präzisegenau ausführt"""

    geht_Aufgaben_zuegig_an = xAPI_Activity(
        "Observation",
        "geht_Aufgaben_zuegig_an",
        {
            "de-DE": "geht Aufgaben zügig an"
        },
        {
            "de-DE": "Subscore (5/5 bzw. ++) wenn ein Akteuer Aufgaben zügig angeht"
        })
    """Subscore (5/5 bzw. ++) wenn ein Akteuer Aufgaben zügig angeht"""

    geht_respektvoll_und_tolerant_mit_Meinungsverschiedenheiten_um = xAPI_Activity(
        "Observation",
        "geht_respektvoll_und_tolerant_mit_Meinungsverschiedenheiten_um",
        {
            "de-DE": "geht respektvoll und tolerant mit Meinungsverschiedenheiten um"
        },
        {
            "de-DE": "Subscore (5/5 bzw. ++) wenn ein Akteuer respektvoll und tolerant mit Meinungsverschiedenheiten umgeht"
        })
    """Subscore (5/5 bzw. ++) wenn ein Akteuer respektvoll und tolerant mit Meinungsverschiedenheiten umgeht"""

    interagiert_nonverbal_zugewandte_Koerpersprache = xAPI_Activity(
        "Observation",
        "interagiert_nonverbal_zugewandte_Koerpersprache",
        {
            "de-DE": "interagiert nonverbal (zugewandte Körpersprache)"
        },
        {
            "de-DE": "Subscore (5/5 bzw. ++) wenn ein Akteuer nonverbal interagiert (zugewandte Körpersprache)"
        })
    """Subscore (5/5 bzw. ++) wenn ein Akteuer nonverbal interagiert (zugewandte Körpersprache)"""

    kann_auch_komplexe_Sachverhalte_ausdruecken_undoder_verstehen = xAPI_Activity(
        "Observation",
        "kann_auch_komplexe_Sachverhalte_ausdruecken_undoder_verstehen",
        {
            "de-DE": "kann auch komplexe Sachverhalte ausdrücken und/oder verstehen"
        },
        {
            "de-DE": "Subscore (5/5 bzw. ++) wenn ein Akteuer auch komplexe Sachverhalte ausdrücken und/oder verstehen kann"
        })
    """Subscore (5/5 bzw. ++) wenn ein Akteuer auch komplexe Sachverhalte ausdrücken und/oder verstehen kann"""

    kann_auch_schwierige_feinmotorische_Handlungen_ausfuehren = xAPI_Activity(
        "Observation",
        "kann_auch_schwierige_feinmotorische_Handlungen_ausfuehren",
        {
            "de-DE": "kann auch schwierige feinmotorische Handlungen ausführen"
        },
        {
            "de-DE": "Subscore (5/5 bzw. ++) wenn ein Akteuer auch schwierige feinmotorische Handlungen ausführen kann"
        })
    """Subscore (5/5 bzw. ++) wenn ein Akteuer auch schwierige feinmotorische Handlungen ausführen kann"""

    kann_EntfernungenHoehenTiefen_einschaetzen_im_Raumvon_Gegenstaenden = xAPI_Activity(
        "Observation",
        "kann_EntfernungenHoehenTiefen_einschaetzen_im_Raumvon_Gegenstaenden",
        {
            "de-DE": "kann Entfernungen/Höhen/Tiefen einschätzen (im Raum/von Gegenständen)"
        },
        {
            "de-DE": "Subscore (5/5 bzw. ++) wenn ein Akteuer Entfernungen/Höhen/Tiefen einschätzen kann (im Raum/von Gegenständen)"
        })
    """Subscore (5/5 bzw. ++) wenn ein Akteuer Entfernungen/Höhen/Tiefen einschätzen kann (im Raum/von Gegenständen)"""

    kann_Massstaebe_im_richtigen_Verhaeltnis_umsetzen_zB_in_Skizzen = xAPI_Activity(
        "Observation",
        "kann_Massstaebe_im_richtigen_Verhaeltnis_umsetzen_zB_in_Skizzen",
        {
            "de-DE": "kann Maßstäbe im richtigen Verhältnis umsetzen (z.B. in Skizzen)"
        },
        {
            "de-DE": "Subscore (5/5 bzw. ++) wenn ein Akteuer Maßstäbe im richtigen Verhältnis umsetzen kann (z.B. in Skizzen)"
        })
    """Subscore (5/5 bzw. ++) wenn ein Akteuer Maßstäbe im richtigen Verhältnis umsetzen kann (z.B. in Skizzen)"""

    kann_PlaeneSkizzen_lesen_verstehen_und_umsetzen = xAPI_Activity(
        "Observation",
        "kann_PlaeneSkizzen_lesen_verstehen_und_umsetzen",
        {
            "de-DE": "kann Pläne/Skizzen lesen, verstehen und umsetzen"
        },
        {
            "de-DE": "Subscore (5/5 bzw. ++) wenn ein Akteuer Pläne/Skizzen lesen, verstehen und umsetzen kann"
        })
    """Subscore (5/5 bzw. ++) wenn ein Akteuer Pläne/Skizzen lesen, verstehen und umsetzen kann"""

    laesst_andere_ausreden = xAPI_Activity(
        "Observation",
        "laesst_andere_ausreden",
        {
            "de-DE": "lässt andere ausreden"
        },
        {
            "de-DE": "Subscore (5/5 bzw. ++) wenn ein Akteuer andere ausreden lässt"
        })
    """Subscore (5/5 bzw. ++) wenn ein Akteuer andere ausreden lässt"""

    nimmt_Ruecksicht_auf_eigene_Beduerfnissevertritt_die_eigene_Meinung = xAPI_Activity(
        "Observation",
        "nimmt_Ruecksicht_auf_eigene_Beduerfnissevertritt_die_eigene_Meinung",
        {
            "de-DE": "nimmt Rücksicht auf eigene Bedürfnisse/vertritt die eigene Meinung"
        },
        {
            "de-DE": "Subscore (5/5 bzw. ++) wenn ein AkteuerRücksicht auf eigene Bedürfnisse  nimmt/die eigene Meinung vertritt"
        })
    """Subscore (5/5 bzw. ++) wenn ein AkteuerRücksicht auf eigene Bedürfnisse  nimmt/die eigene Meinung vertritt"""

    plant_das_eigene_Vorgehen_und_setzt_Schwerpunkte = xAPI_Activity(
        "Observation",
        "plant_das_eigene_Vorgehen_und_setzt_Schwerpunkte",
        {
            "de-DE": "plant das (eigene) Vorgehen und setzt Schwerpunkte"
        },
        {
            "de-DE": "Subscore (5/5 bzw. ++) wenn ein Akteuer das (eigene) Vorgehen plant und Schwerpunkte setzt"
        })
    """Subscore (5/5 bzw. ++) wenn ein Akteuer das (eigene) Vorgehen plant und Schwerpunkte setzt"""

    setzt_geeignete_Mittel_einfuehrt_geeignete_Handlungen_aus_um_das_Ziel_zu_erreichen = xAPI_Activity(
        "Observation",
        "setzt_geeignete_Mittel_einfuehrt_geeignete_Handlungen_aus_um_das_Ziel_zu_erreichen",
        {
            "de-DE": "setzt geeignete Mittel ein/führt geeignete Handlungen aus, um das Ziel zu erreichen"
        },
        {
            "de-DE": "Subscore (5/5 bzw. ++) wenn ein Akteuer geeignete Mittel einsetzt/geeignete Handlungen ausführt, um das Ziel zu erreichen"
        })
    """Subscore (5/5 bzw. ++) wenn ein Akteuer geeignete Mittel einsetzt/geeignete Handlungen ausführt, um das Ziel zu erreichen"""

    stellt_eigene_Beduerfnisse_hinten_an_wenn_es_die_Situation_erfordert = xAPI_Activity(
        "Observation",
        "stellt_eigene_Beduerfnisse_hinten_an_wenn_es_die_Situation_erfordert",
        {
            "de-DE": "stellt eigene Bedürfnisse hinten an, wenn es die Situation erfordert"
        },
        {
            "de-DE": "Subscore (5/5 bzw. ++) wenn ein Akteuer eigene Bedürfnisse hinten anstellt, wenn es die Situation erfordert"
        })
    """Subscore (5/5 bzw. ++) wenn ein Akteuer eigene Bedürfnisse hinten anstellt, wenn es die Situation erfordert"""

    verfolgt_verschiedene_Loesungsansaetze = xAPI_Activity(
        "Observation",
        "verfolgt_verschiedene_Loesungsansaetze",
        {
            "de-DE": "verfolgt verschiedene Lösungsansätze"
        },
        {
            "de-DE": "Subscore (5/5 bzw. ++) wenn ein Akteuer verschiedne Lösungsansätze verfolgt"
        })
    """Subscore (5/5 bzw. ++) wenn ein Akteuer verschiedne Lösungsansätze verfolgt"""

    verhaelt_sich_hoeflich_freundlich_und_wohlwollend = xAPI_Activity(
        "Observation",
        "verhaelt_sich_hoeflich_freundlich_und_wohlwollend",
        {
            "de-DE": "verhält sich höflich, freundlich und wohlwollend"
        },
        {
            "de-DE": "Subscore (5/5 bzw. ++) wenn ein Akteuer sich höflich, freundlich und wohlwollend verhält"
        })
    """Subscore (5/5 bzw. ++) wenn ein Akteuer sich höflich, freundlich und wohlwollend verhält"""

    versteht_geschriebenebebilderte_Informationen_zB_Texte_Anweisungen = xAPI_Activity(
        "Observation",
        "versteht_geschriebenebebilderte_Informationen_zB_Texte_Anweisungen",
        {
            "de-DE": "versteht geschriebene/bebilderte Informationen (z.B. Texte, Anweisungen)"
        },
        {
            "de-DE": "Subscore (5/5 bzw. ++) wenn ein Akteuer geschriebene/bebilderte Informationen (z.B. Texte, Anweisungen) versteht"
        })
    """Subscore (5/5 bzw. ++) wenn ein Akteuer geschriebene/bebilderte Informationen (z.B. Texte, Anweisungen) versteht"""

    vertritt_eigene_Meinungen_und_zeigt_sich_dabei_kompromissbereit = xAPI_Activity(
        "Observation",
        "vertritt_eigene_Meinungen_und_zeigt_sich_dabei_kompromissbereit",
        {
            "de-DE": "vertritt eigene Meinungen und zeigt sich dabei kompromissbereit"
        },
        {
            "de-DE": "Subscore (5/5 bzw. ++) wenn ein Akteuer eigene Meinungen vertritt und sich dabei kompromissbereit zeigt"
        })
    """Subscore (5/5 bzw. ++) wenn ein Akteuer eigene Meinungen vertritt und sich dabei kompromissbereit zeigt"""

    visualisiert_oder_umschreibt_Ideen = xAPI_Activity(
        "Observation",
        "visualisiert_oder_umschreibt_Ideen",
        {
            "de-DE": "visualisiert oder umschreibt Ideen"
        },
        {
            "de-DE": "Subscore (5/5 bzw. ++) wenn ein Akteuer Ideen visualisiert oder umschreibt"
        })
    """Subscore (5/5 bzw. ++) wenn ein Akteuer Ideen visualisiert oder umschreibt"""

    wird_immer_wieder_von_sich_aus_aktiv = xAPI_Activity(
        "Observation",
        "wird_immer_wieder_von_sich_aus_aktiv",
        {
            "de-DE": "wird immer wieder von sich aus aktiv"
        },
        {
            "de-DE": "Subscore (5/5 bzw. ++) wenn ein Akteuer immer wieder von sich aus aktiv wird"
        })
    """Subscore (5/5 bzw. ++) wenn ein Akteuer immer wieder von sich aus aktiv wird"""

    zeigt_aktiv_Interesse_an_anderen_Menschen_und_ihren_AnsichtenIdeen = xAPI_Activity(
        "Observation",
        "zeigt_aktiv_Interesse_an_anderen_Menschen_und_ihren_AnsichtenIdeen",
        {
            "de-DE": "zeigt aktiv Interesse an anderen Menschen und ihren Ansichten/Ideen"
        },
        {
            "de-DE": "Subscore (5/5 bzw. ++) wenn ein Akteuer aktiv Interesse an anderen Menschen und ihren Ansichten/Ideen zeigt"
        })
    """Subscore (5/5 bzw. ++) wenn ein Akteuer aktiv Interesse an anderen Menschen und ihren Ansichten/Ideen zeigt"""

    zeigt_Interesse_Stolz_oder_Freude = xAPI_Activity(
        "Observation",
        "zeigt_Interesse_Stolz_oder_Freude",
        {
            "de-DE": "zeigt Interesse, Stolz oder Freude"
        },
        {
            "de-DE": "Subscore (5/5 bzw. ++) wenn ein Akteuer Intresse, Stolz oder Freude zeigt"
        })
    """Subscore (5/5 bzw. ++) wenn ein Akteuer Intresse, Stolz oder Freude zeigt"""

    zeigt_sich_hilfsbereit_und_unterstuetzt_andere_aktiv = xAPI_Activity(
        "Observation",
        "zeigt_sich_hilfsbereit_und_unterstuetzt_andere_aktiv",
        {
            "de-DE": "zeigt sich hilfsbereit und unterstützt andere aktiv"
        },
        {
            "de-DE": "Subscore (5/5 bzw. ++) wenn ein Akteuer sich hilfsbereit zeigt und andere aktiv unterstützt"
        })
    """Subscore (5/5 bzw. ++) wenn ein Akteuer sich hilfsbereit zeigt und andere aktiv unterstützt"""

    uebt_Bewegungen_schnellroutiniert_aus = xAPI_Activity(
        "Observation",
        "uebt_Bewegungen_schnellroutiniert_aus",
        {
            "de-DE": "übt Bewegungen schnell/routiniert aus"
        },
        {
            "de-DE": "Subscore (5/5 bzw. ++) wenn ein Akteuer Bewegungen schnell/routiniert ausübt"
        })
    """Subscore (5/5 bzw. ++) wenn ein Akteuer Bewegungen schnell/routiniert ausübt"""

    def __init__(self):
        super().__init__("Observation")
