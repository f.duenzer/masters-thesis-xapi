from core.xapi_extensions_context_vrrfidchamber import xAPI_Extensions_Context_VrRfidChamber
from core.xapi_extensions_result_vrrfidchamber import xAPI_Extensions_Result_VrRfidChamber

class xAPI_Context_VrRfidChamber_Extensions:

    def __init__(self):
        pass

    @property
    def context(self):
        return xAPI_Extensions_Context_VrRfidChamber();

    @property
    def result(self):
        return xAPI_Extensions_Result_VrRfidChamber();
