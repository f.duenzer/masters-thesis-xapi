from core import xapi_verb
from core import xapi_verbs

class xAPI_Verbs_Generic(xAPI_Verbs):
    accepted = xAPI_Verb(
        "Generic",
        "accepted",
        {
            "en-US": "accepted",
            "de-DE": "akzeptierte"
        },
        {
            "en-US": "Indicates that that the actor has accepted the object. For instance, a person accepting an award, or accepting an assignment.",
            "de-DE": "Signalisiert, dass ein Akteur ein Objekt akzeptiert hat. Zum Beispiel: Eine Person akzeptiert eine Auszeichnung, oder eine Aufgabe."
        })
    """Indicates that that the actor has accepted the object. For instance, a person accepting an award, or accepting an assignment."""

    accessed = xAPI_Verb(
        "Generic",
        "accessed",
        {
            "en-US": "accessed",
            "de-DE": "zugegriffen"
        },
        {
            "en-US": "Actor accessed/viewed a page, file, video, ...",
            "de-DE": "Akteur hat (auf) eine Seite, eine Datei, ein Video, ... zugegriffen/angesehen"
        })
    """Actor accessed/viewed a page, file, video, ..."""

    achieved = xAPI_Verb(
        "Generic",
        "achieved",
        {
            "en-US": "achieved",
            "de-DE": "erreichte"
        },
        {
            "en-US": "An actor achieved some goal or score.",
            "de-DE": "Ein Akteur erreichte ein Ziel oder ein Ergebnis."
        })
    """An actor achieved some goal or score."""

    answered = xAPI_Verb(
        "Generic",
        "answered",
        {
            "en-US": "answered",
            "de-DE": "beantwortete"
        },
        {
            "en-US": "An actor answered some question.",
            "de-DE": "Ein Akteur beantwortete eine Frage."
        })
    """An actor answered some question."""

    asked = xAPI_Verb(
        "Generic",
        "asked",
        {
            "en-US": "asked",
            "de-DE": "fragte"
        },
        {
            "en-US": "An actor asked a question. Could also asked for help or advice.",
            "de-DE": "Ein Akteur stellte eine Frage. Es kann auch nach Hilfe oder Ratschlag gefragt werden."
        })
    """An actor asked a question. Could also asked for help or advice."""

    calibrated = xAPI_Verb(
        "Generic",
        "calibrated",
        {
            "en-US": "calibrated",
            "de-DE": "kalibrierte"
        },
        {
            "en-US": "An actor calibrated something. This could be an eye tracking system for example.",
            "de-DE": "Ein Akteur kalibrierte etwas. Dies kann zum Beispiel ein Eyetracking System sein."
        })
    """An actor calibrated something. This could be an eye tracking system for example."""

    changed = xAPI_Verb(
        "Generic",
        "changed",
        {
            "en-US": "changed",
            "de-DE": "veränderte"
        },
        {
            "en-US": "An actor changed something. This could be the level or some settings.",
            "de-DE": "Ein Akteur veränderte etwas. Dies könnte das Level sein oder die Einstellungen."
        })
    """An actor changed something. This could be the level or some settings."""

    clicked = xAPI_Verb(
        "Generic",
        "clicked",
        {
            "en-US": "clicked",
            "de-DE": "klickte"
        },
        {
            "en-US": "An actor has clicked a button.",
            "de-DE": "Der Akteur klickte einen Knopf."
        })
    """An actor has clicked a button."""

    closed = xAPI_Verb(
        "Generic",
        "closed",
        {
            "en-US": "closed",
            "de-DE": "schloss"
        },
        {
            "en-US": "An actor closed something. Could for example be the settings.",
            "de-DE": "Ein Akteur schloss etwas. Könnten zum Beispiel die Einstellungen sein."
        })
    """An actor closed something. Could for example be the settings."""

    collapsed = xAPI_Verb(
        "Generic",
        "collapsed",
        {
            "en-US": "collapsed",
            "de-DE": "klappte ein"
        },
        {
            "en-US": "An actor collapsed something, a menu for example.",
            "de-DE": "Ein Akteur klappte etwas ein, ein Menü zum Beispiel ein."
        })
    """An actor collapsed something, a menu for example."""

    collected = xAPI_Verb(
        "Generic",
        "collected",
        {
            "en-US": "collected",
            "de-DE": "sammelte"
        },
        {
            "en-US": "An actor collected some item.",
            "de-DE": "Ein Akteur sammelte einen Gegenstand ein."
        })
    """An actor collected some item."""

    combined = xAPI_Verb(
        "Generic",
        "combined",
        {
            "en-US": "combined",
            "de-DE": "kombinierte"
        },
        {
            "en-US": "An actor brought two or more objects together. This can create a new object out of the combined ones. If just a logical connection is meant, use connected.",
            "de-DE": "Ein Akteur brachte zwei oder mehr Gegenstände zusammen. Aus den kombinierten Gegenständen können neue Gegenstände entstehen. Wenn eine logische Kombination der Gegenstände gemeint ist, nutzte verband."
        })
    """An actor brought two or more objects together. This can create a new object out of the combined ones. If just a logical connection is meant, use connected."""

    completed = xAPI_Verb(
        "Generic",
        "completed",
        {
            "en-US": "completed",
            "de-DE": "schloss ab"
        },
        {
            "en-US": "An actor completed some task, goal or level.",
            "de-DE": "Ein Akteur schloss eine Aufgabe, ein Ziel oder ein Level ab."
        })
    """An actor completed some task, goal or level."""

    connected = xAPI_Verb(
        "Generic",
        "connected",
        {
            "en-US": "connected",
            "de-DE": "verband"
        },
        {
            "en-US": "An actor connected two or more items with each other. This is meant in the sense of drawn a line between the objects, while combined can make something new out of the objects.",
            "de-DE": "Ein Akteur verband zwei oder mehr Gegenstände mit einander. Dies ist eher im Sinne von eine Linie zwischen den Objekten malen, während mit kombinierte auch zum Beispiel neue Gegenstände aus den verbundenen entstehen können."
        })
    """An actor connected two or more items with each other. This is meant in the sense of drawn a line between the objects, while combined can make something new out of the objects."""

    created = xAPI_Verb(
        "Generic",
        "created",
        {
            "en-US": "created",
            "de-DE": "erstellte"
        },
        {
            "en-US": "An actor created something.",
            "de-DE": "Ein Akteur erstellte etwas."
        })
    """An actor created something."""

    crossedOut = xAPI_Verb(
        "Generic",
        "crossedOut",
        {
            "en-US": "crossedOut",
            "de-DE": "streichte durch"
        },
        {
            "en-US": "A actor crossed out some text or object.",
            "de-DE": "Ein Acteur streichte einen Text oder ein Objekt durch."
        })
    """A actor crossed out some text or object."""

    deleted = xAPI_Verb(
        "Generic",
        "deleted",
        {
            "en-US": "deleted",
            "de-DE": "löschte"
        },
        {
            "en-US": "An actor deleted something.",
            "de-DE": "Ein Akteur löschte etwas."
        })
    """An actor deleted something."""

    deselected = xAPI_Verb(
        "Generic",
        "deselected",
        {
            "en-US": "deselected",
            "de-DE": "nahm Auswahl zurück"
        },
        {
            "en-US": "An actor deselected a option.",
            "de-DE": "Ein Akteur nahm die Auswahl einer Option zurück."
        })
    """An actor deselected a option."""

    downloaded = xAPI_Verb(
        "Generic",
        "downloaded",
        {
            "en-US": "downloaded",
            "de-DE": "downloadete"
        },
        {
            "en-US": "A user used a local device to download data from a different distant disk.",
            "de-DE": "Ein Benutzer nutze ein lokales Gerät um Daten von einem entfernten Speichermedium zu laden."
        })
    """A user used a local device to download data from a different distant disk."""

    drew = xAPI_Verb(
        "Generic",
        "drew",
        {
            "en-US": "drew",
            "de-DE": "malte"
        },
        {
            "en-US": "An actor drew something.",
            "de-DE": "Ein Akteur malte etwas."
        })
    """An actor drew something."""

    dropped = xAPI_Verb(
        "Generic",
        "dropped",
        {
            "en-US": "dropped",
            "de-DE": "ließ fallen"
        },
        {
            "en-US": "A actor dropped some draggable into a specific area.",
            "de-DE": "Ein Acteur ließ einen ziehbaren Gegenstand in ein spezielles Feld fallen."
        })
    """A actor dropped some draggable into a specific area."""

    edited = xAPI_Verb(
        "Generic",
        "edited",
        {
            "en-US": "edited",
            "de-DE": "editierte"
        },
        {
            "en-US": "An actor edited an object, for example their account profile. 'edited' has a strong connection to text-based input, e.g. a user edited the title. If the user uses preset values, for example realized by a dropdown menu, choose 'changed'. Also in most non-text-based input e.g. changing a color, 'changed' is the matching verb",
            "de-DE": "Ein Acteur editierte ein Object, zum Beispiel ihren Account.'editierte' hat eine startke Verbindung zu text-basiertem Input. Z.B. Ein Benutzer editierte den Titel. Wenn dem Benutzer eine Auswahl benutzt, wie bei einem Dropdown Menü, sollte 'geändert' benutzt werden. Darüberhinaus ist in den meisten nicht-text-basierten Input, z.B. beim Ändern einer Farbe, 'änderte' das passende Verb"
        })
    """An actor edited an object, for example their account profile. 'edited' has a strong connection to text-based input, e.g. a user edited the title. If the user uses preset values, for example realized by a dropdown menu, choose 'changed'. Also in most non-text-based input e.g. changing a color, 'changed' is the matching verb"""

    entered = xAPI_Verb(
        "Generic",
        "entered",
        {
            "en-US": "entered",
            "de-DE": "gab ein"
        },
        {
            "en-US": "An actor gave some input.",
            "de-DE": "Ein Akteur gab eine Eingabe."
        })
    """An actor gave some input."""

    failed = xAPI_Verb(
        "Generic",
        "failed",
        {
            "en-US": "failed",
            "de-DE": "scheiterte"
        },
        {
            "en-US": "An actor failed some task.",
            "de-DE": "Ein Akteur scheiterte an einer Aufgabe."
        })
    """An actor failed some task."""

    found = xAPI_Verb(
        "Generic",
        "found",
        {
            "en-US": "found",
            "de-DE": "fand"
        },
        {
            "en-US": "An actor found some item.",
            "de-DE": "Ein Akteur fand einen Gegenstand."
        })
    """An actor found some item."""

    left = xAPI_Verb(
        "Generic",
        "left",
        {
            "en-US": "left",
            "de-DE": "lies zurück"
        },
        {
            "en-US": "An actor left behind some object.",
            "de-DE": "Ein Akteur lies einen Gegenstand zurück."
        })
    """An actor left behind some object."""

    locked = xAPI_Verb(
        "Generic",
        "locked",
        {
            "en-US": "locked",
            "de-DE": "verschloss"
        },
        {
            "en-US": "An actor did some action to lock up some object. Could also be locked out of a progression path for example.",
            "de-DE": "Ein Akteur verschloss einen Gegenstand. Kann zum Beispiel aber auch ausschließen von einem Weg zu voran kommen sein."
        })
    """An actor did some action to lock up some object. Could also be locked out of a progression path for example."""

    lookedAt = xAPI_Verb(
        "Generic",
        "lookedAt",
        {
            "en-US": "looked at",
            "de-DE": "schaute auf"
        },
        {
            "en-US": "A actor looked at a specific object. Only use if you can determine that the actor really looked at something, thriough eye-tracking for example.",
            "de-DE": "Ein Acteur schaute auf eine spezifische Sache. Sollte nur genutzt werden wenn sichergestellt werden kann, dass der Spieler sich die Sache auch wirklich angeschaut hat. Mit Eyetracking kann das zum Beispiel gemacht werden."
        })
    """A actor looked at a specific object. Only use if you can determine that the actor really looked at something, thriough eye-tracking for example."""

    looted = xAPI_Verb(
        "Generic",
        "looted",
        {
            "en-US": "looted",
            "de-DE": "plünderte"
        },
        {
            "en-US": "An actor found some item and collected it.",
            "de-DE": "Ein Akteur fand einen Gegenstand und sammelte ihn ein."
        })
    """An actor found some item and collected it."""

    lost = xAPI_Verb(
        "Generic",
        "lost",
        {
            "en-US": "lost",
            "de-DE": "verlor"
        },
        {
            "en-US": "An actor lost the game.",
            "de-DE": "Ein Akteur verlor das Spiel."
        })
    """An actor lost the game."""

    made = xAPI_Verb(
        "Generic",
        "made",
        {
            "en-US": "made",
            "de-DE": "fällte"
        },
        {
            "en-US": "An actor made a decision.",
            "de-DE": "Ein Akteur fällte eine Entscheidung."
        })
    """An actor made a decision."""

    moved = xAPI_Verb(
        "Generic",
        "moved",
        {
            "en-US": "moved",
            "de-DE": "bewegte"
        },
        {
            "en-US": "An actor moved itself or some object in the game world.",
            "de-DE": "Ein Akteur bewegte sich oder einen Gegenstand in der Spielwelt."
        })
    """An actor moved itself or some object in the game world."""

    opened = xAPI_Verb(
        "Generic",
        "opened",
        {
            "en-US": "opened",
            "de-DE": "öffnete"
        },
        {
            "en-US": "An actor opened something. Could also be the settings for example.",
            "de-DE": "Ein Akteur öffnete etwas. Könnten zum Beispiel auch die Einstellungen sein."
        })
    """An actor opened something. Could also be the settings for example."""

    ordered = xAPI_Verb(
        "Generic",
        "ordered",
        {
            "en-US": "ordered",
            "de-DE": "ordnete"
        },
        {
            "en-US": "An actor brought some objects into order.",
            "de-DE": "Ein Akteur ordnete ein Anzahl an Gegenständen."
        })
    """An actor brought some objects into order."""

    pasted = xAPI_Verb(
        "Generic",
        "pasted",
        {
            "en-US": "pasted",
            "de-DE": "eingefügt"
        },
        {
            "en-US": "Refers to the copy-and-paste action performed by an actor. E.g. An actor pasted a text into a textbox",
            "de-DE": "Dieses Verb bezieht sich auf die kopier-und-einfüge Operation eines Akteurs. Z.B. ein Akteur fügte den (vorher kopierten) Text in ein Textfeld ein."
        })
    """Refers to the copy-and-paste action performed by an actor. E.g. An actor pasted a text into a textbox"""

    played = xAPI_Verb(
        "Generic",
        "played",
        {
            "en-US": "played",
            "de-DE": "spielte"
        },
        {
            "en-US": "An actor played a game or something specific in a game.",
            "de-DE": "Ein Akteur spielte ein Spiel oder etwas spezifisches in einem Spiel."
        })
    """An actor played a game or something specific in a game."""

    pressed = xAPI_Verb(
        "Generic",
        "pressed",
        {
            "en-US": "pressed",
            "de-DE": "drückte"
        },
        {
            "en-US": "An actor has pressed a button.",
            "de-DE": "Der Akteur drückte eine Taste."
        })
    """An actor has pressed a button."""

    pressedButton = xAPI_Verb(
        "Generic",
        "pressedButton",
        {
            "en-US": "pressedButton",
            "de-DE": "drückte Knopf"
        },
        {
            "en-US": "A actor or gorup pressed a specific button. Only use if the press of specific buttons should be tracked. Use the specific button as activity then. Otherwise use press as verb and button as activity.",
            "de-DE": "Ein Akteur drückte einen bestimmten Knopf. Sollte nur genutzt werden, wenn das Drücken eines bestimmten Knopfes aufgezeichnet werden soll. Nutze dann den speziellen Knopf als Aktivität der Aussage. Andernfalls nutze drückte als Verb und Knopf als Aktivität."
        })
    """A actor or gorup pressed a specific button. Only use if the press of specific buttons should be tracked. Use the specific button as activity then. Otherwise use press as verb and button as activity."""

    progressed = xAPI_Verb(
        "Generic",
        "progressed",
        {
            "en-US": "progressed",
            "de-DE": "erzielte Fortschritt"
        },
        {
            "en-US": "An actor progressed in some game related way(achieved something or completed something).",
            "de-DE": "Ein Akteur erzielte in einer Form einen Fortschritt im Spiel(erreichte oder vollendete etwas)."
        })
    """An actor progressed in some game related way(achieved something or completed something)."""

    read = xAPI_Verb(
        "Generic",
        "read",
        {
            "en-US": "read",
            "de-DE": "las"
        },
        {
            "en-US": "A actor read some text. Only use if it can be assured that the text is read.",
            "de-DE": "Ein Acteur las einen Text. Sollte nur genutzt werden wenn sichergestellt werden kann, dass der Text auch wirklich gelesen wurde."
        })
    """A actor read some text. Only use if it can be assured that the text is read."""

    received = xAPI_Verb(
        "Generic",
        "received",
        {
            "en-US": "received",
            "de-DE": "erhielt"
        },
        {
            "en-US": "An actor received for example an item or help.",
            "de-DE": "Ein Akteur erhielt zum Beispiel einen Gegenstand oder Hilfe."
        })
    """An actor received for example an item or help."""

    released = xAPI_Verb(
        "Generic",
        "released",
        {
            "en-US": "released",
            "de-DE": "löste"
        },
        {
            "en-US": "An actor has released a button.",
            "de-DE": "Der Akteur löste eine Taste."
        })
    """An actor has released a button."""

    saved = xAPI_Verb(
        "Generic",
        "saved",
        {
            "en-US": "saved",
            "de-DE": "speicherte"
        },
        {
            "en-US": "An actor saved the progress.",
            "de-DE": "Ein Akteur speicherte den Fortschritt."
        })
    """An actor saved the progress."""

    searched = xAPI_Verb(
        "Generic",
        "searched",
        {
            "en-US": "searched",
            "de-DE": "suchte"
        },
        {
            "en-US": "An actor searched for an item or searched a location.",
            "de-DE": "Ein Acteur oder ein Gruppe suchte nach einem Gegenstand oder zum Beispiel auch einen Ort ab."
        })
    """An actor searched for an item or searched a location."""

    selected = xAPI_Verb(
        "Generic",
        "selected",
        {
            "en-US": "selected",
            "de-DE": "wählte aus"
        },
        {
            "en-US": "An actor selected some option. This could be an answer or a level for example.",
            "de-DE": "Ein Akteur wählte eine Option. Dies könnte eine Antwort auf eine Frage oder auch ein Level sein."
        })
    """An actor selected some option. This could be an answer or a level for example."""

    shared = xAPI_Verb(
        "Generic",
        "shared",
        {
            "en-US": "shared",
            "de-DE": "teilte"
        },
        {
            "en-US": "An actor shared an object with another actor or a group of actors.",
            "de-DE": "Ein Akteur teitle eine Gegenstand mit einem anderen Acteur oder einer Gruppe."
        })
    """An actor shared an object with another actor or a group of actors."""

    solved = xAPI_Verb(
        "Generic",
        "solved",
        {
            "en-US": "solved",
            "de-DE": "löste"
        },
        {
            "en-US": "An actor solved some task.",
            "de-DE": "Ein Akteur löste eine Aufgabe."
        })
    """An actor solved some task."""

    sorted = xAPI_Verb(
        "Generic",
        "sorted",
        {
            "en-US": "sorted",
            "de-DE": "sortierte"
        },
        {
            "en-US": "An actor sorted some objects.",
            "de-DE": "Ein Akteur sortierte Objekte."
        })
    """An actor sorted some objects."""

    synchronized = xAPI_Verb(
        "Generic",
        "synchronized",
        {
            "en-US": "synchronized",
            "de-DE": "synchronisierte"
        },
        {
            "en-US": "An actor synchronized data with an LMS for example, which means, that two objects/processes/devices synchronized between A and B or with something.",
            "de-DE": "Ein Akteur synchronisierte zum Beispiel Daten mit einem LMS, d.h. zwei Objekte/Vorgänge/Geräte wurden auf einen Stand gebracht."
        })
    """An actor synchronized data with an LMS for example, which means, that two objects/processes/devices synchronized between A and B or with something."""

    toggled = xAPI_Verb(
        "Generic",
        "toggled",
        {
            "en-US": "toggled",
            "de-DE": "schaltete um"
        },
        {
            "en-US": "An actor toggled some switch.",
            "de-DE": "Ein Akteur schaltete einen Schalter um."
        })
    """An actor toggled some switch."""

    triggered = xAPI_Verb(
        "Generic",
        "triggered",
        {
            "en-US": "triggered",
            "de-DE": "löste aus"
        },
        {
            "en-US": "An actor triggered a event.",
            "de-DE": "Ein Akteur löste ein Ereignis aus."
        })
    """An actor triggered a event."""

    unfolded = xAPI_Verb(
        "Generic",
        "unfolded",
        {
            "en-US": "unfolded",
            "de-DE": "klappte aus"
        },
        {
            "en-US": "An actor unfolded something, a menu for example.",
            "de-DE": "Ein Akteur klappte etwas aus, zum Beispiel ein Menü."
        })
    """An actor unfolded something, a menu for example."""

    unlocked = xAPI_Verb(
        "Generic",
        "unlocked",
        {
            "en-US": "unlocked",
            "de-DE": "schaltete frei"
        },
        {
            "en-US": "An actor unlocked some option or item.",
            "de-DE": "Ein Akteur schaltete eine Option oder einen Gegenstand frei."
        })
    """An actor unlocked some option or item."""

    uploaded = xAPI_Verb(
        "Generic",
        "uploaded",
        {
            "en-US": "uploaded",
            "de-DE": "lud hoch"
        },
        {
            "en-US": "A user or group used some local device to uploaded data onto a different distant disk.",
            "de-DE": "Ein Benutzer oder eine Gruppe nutze ein lokales Gerät um daten auf ein entferntes Speichermedium zu laden."
        })
    """A user or group used some local device to uploaded data onto a different distant disk."""

    used = xAPI_Verb(
        "Generic",
        "used",
        {
            "en-US": "used",
            "de-DE": "benutzte"
        },
        {
            "en-US": "An actor used some item.",
            "de-DE": "Ein Akteur benutze einen Gegenstand."
        })
    """An actor used some item."""

    voted = xAPI_Verb(
        "Generic",
        "voted",
        {
            "en-US": "voted",
            "de-DE": "stimmte ab"
        },
        {
            "en-US": "A group of actors voted on something.",
            "de-DE": "Eine Gruppe von Acteuren stimmte für etwas ab."
        })
    """A group of actors voted on something."""

    watched = xAPI_Verb(
        "Generic",
        "watched",
        {
            "en-US": "watched",
            "de-DE": "schaute"
        },
        {
            "en-US": "An actor watched a video. Only use if you can assure the actors really watched the video, use eye-tracking for example.",
            "de-DE": "Ein Akteur schaute ein Video. Sollte nur genutzt werden wenn sichergestellt werden kann, dass das Video wirklich geschaut wurde. Eyetracking könnte dafür zum Beispiel benutzt werden."
        })
    """An actor watched a video. Only use if you can assure the actors really watched the video, use eye-tracking for example."""

    won = xAPI_Verb(
        "Generic",
        "won",
        {
            "en-US": "won",
            "de-DE": "gewann"
        },
        {
            "en-US": "An actor won a game.",
            "de-DE": "Ein Akteur gewann ein Spiel."
        })
    """An actor won a game."""

    wrote = xAPI_Verb(
        "Generic",
        "wrote",
        {
            "en-US": "wrote",
            "de-DE": "schrieb"
        },
        {
            "en-US": "A actor put some text into a text field.",
            "de-DE": "Ein Akteur gab einen Text in ein Textfeld ein."
        })
    """A actor put some text into a text field."""

    def __init__(self):
        super().__init__("Generic")
