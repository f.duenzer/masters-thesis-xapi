from core.xapi_extensions_result import xAPI_Extensions_Result

class xAPI_Extensions_Result_VrRfidChamber(xAPI_Extensions_Result):

    def __init__(self):
        super().__init__("VrRfidChamber")

    def duration(self, value):
        add(xAPI_Extension(
                context,
                extensionType,
                "duration",
                {
                    "en-US": "duration"
                },
                {
                    "en-US": "Value representing a length of time required to complete the experiment."
                }),
            value);
        return self;

    def endTime(self, value):
        add(xAPI_Extension(
                context,
                extensionType,
                "endTime",
                {
                    "en-US": "end time"
                },
                {
                    "en-US": "The time when the experiment was completed that is when the software was closed. This is captured from the system time of the local machine."
                }),
            value);
        return self;

    def failure(self, value):
        add(xAPI_Extension(
                context,
                extensionType,
                "failure",
                {
                    "en-US": "failure"
                },
                {
                    "en-US": "The experiment is a failure."
                }),
            value);
        return self;

    def startTime(self, value):
        add(xAPI_Extension(
                context,
                extensionType,
                "startTime",
                {
                    "en-US": "start time"
                },
                {
                    "en-US": "The time when the experiment began. This is captured from the system time of the local machine."
                }),
            value);
        return self;

    def success(self, value):
        add(xAPI_Extension(
                context,
                extensionType,
                "success",
                {
                    "en-US": "success"
                },
                {
                    "en-US": "Experiment is successful."
                }),
            value);
        return self;
