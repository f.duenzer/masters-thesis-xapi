from core.xapi_extensions_activity import xAPI_Extensions_Activity

class xAPI_Extensions_Activity_SystemControl(xAPI_Extensions_Activity):

    def __init__(self):
        super().__init__("SystemControl")

    def name(self, value):
        add(xAPI_Extension(
                context,
                extensionType,
                "name",
                {
                    "en-US": "System control activity name",
                    "de-DE": "System Control Activity Name"
                },
                {
                    "en-US": "Name of a system control activity. Must be a string.",
                    "de-DE": "Namen von einer Aktivität von des Systemkontrollers. Muss ein String sein."
                }),
            value);
        return self;
