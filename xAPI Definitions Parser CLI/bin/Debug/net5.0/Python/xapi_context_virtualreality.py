from core import xapi_context
from core.xapi_verbs_virtualreality import xAPI_Verbs_VirtualReality
from core.xapi_activities_virtualreality import xAPI_Activities_VirtualReality
from core.xapi_context_virtualreality_extensions import xAPI_Context_VirtualReality_Extensions

class xAPI_Context_VirtualReality(xAPI_Context):
    verbs = xAPI_Verbs_VirtualReality()

    activities = xAPI_Activities_VirtualReality()

    extensions = xAPI_Context_VirtualReality_Extensions()

    def __init__(self):
        super().__init__("xAPI_Definitions_Parser.Definitions.xAPIContext")
