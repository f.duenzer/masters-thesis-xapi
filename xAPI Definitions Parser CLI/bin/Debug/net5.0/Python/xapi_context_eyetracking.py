from core import xapi_context
from core.xapi_verbs_eyetracking import xAPI_Verbs_EyeTracking
from core.xapi_activities_eyetracking import xAPI_Activities_EyeTracking
from core.xapi_context_eyetracking_extensions import xAPI_Context_EyeTracking_Extensions

class xAPI_Context_EyeTracking(xAPI_Context):
    verbs = xAPI_Verbs_EyeTracking()

    activities = xAPI_Activities_EyeTracking()

    extensions = xAPI_Context_EyeTracking_Extensions()

    def __init__(self):
        super().__init__("xAPI_Definitions_Parser.Definitions.xAPIContext")
