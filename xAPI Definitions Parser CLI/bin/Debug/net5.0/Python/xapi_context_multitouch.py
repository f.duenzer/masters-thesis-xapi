from core import xapi_context
from core.xapi_verbs_multitouch import xAPI_Verbs_Multitouch
from core.xapi_activities_multitouch import xAPI_Activities_Multitouch
from core.xapi_context_multitouch_extensions import xAPI_Context_Multitouch_Extensions

class xAPI_Context_Multitouch(xAPI_Context):
    verbs = xAPI_Verbs_Multitouch()

    activities = xAPI_Activities_Multitouch()

    extensions = xAPI_Context_Multitouch_Extensions()

    def __init__(self):
        super().__init__("xAPI_Definitions_Parser.Definitions.xAPIContext")
