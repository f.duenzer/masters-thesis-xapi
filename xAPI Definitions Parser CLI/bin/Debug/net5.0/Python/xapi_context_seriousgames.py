from core import xapi_context
from core.xapi_verbs_seriousgames import xAPI_Verbs_Seriousgames
from core.xapi_activities_seriousgames import xAPI_Activities_Seriousgames
from core.xapi_context_seriousgames_extensions import xAPI_Context_Seriousgames_Extensions

class xAPI_Context_Seriousgames(xAPI_Context):
    verbs = xAPI_Verbs_Seriousgames()

    activities = xAPI_Activities_Seriousgames()

    extensions = xAPI_Context_Seriousgames_Extensions()

    def __init__(self):
        super().__init__("xAPI_Definitions_Parser.Definitions.xAPIContext")
