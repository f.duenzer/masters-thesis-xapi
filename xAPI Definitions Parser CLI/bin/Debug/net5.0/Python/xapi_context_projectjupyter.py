from core import xapi_context
from core.xapi_verbs_projectjupyter import xAPI_Verbs_ProjectJupyter
from core.xapi_activities_projectjupyter import xAPI_Activities_ProjectJupyter
from core.xapi_context_projectjupyter_extensions import xAPI_Context_ProjectJupyter_Extensions

class xAPI_Context_ProjectJupyter(xAPI_Context):
    verbs = xAPI_Verbs_ProjectJupyter()

    activities = xAPI_Activities_ProjectJupyter()

    extensions = xAPI_Context_ProjectJupyter_Extensions()

    def __init__(self):
        super().__init__("xAPI_Definitions_Parser.Definitions.xAPIContext")
