from core import xapi_context
from core.xapi_verbs_gestures import xAPI_Verbs_Gestures
from core.xapi_activities_gestures import xAPI_Activities_Gestures
from core.xapi_context_gestures_extensions import xAPI_Context_Gestures_Extensions

class xAPI_Context_Gestures(xAPI_Context):
    verbs = xAPI_Verbs_Gestures()

    activities = xAPI_Activities_Gestures()

    extensions = xAPI_Context_Gestures_Extensions()

    def __init__(self):
        super().__init__("xAPI_Definitions_Parser.Definitions.xAPIContext")
