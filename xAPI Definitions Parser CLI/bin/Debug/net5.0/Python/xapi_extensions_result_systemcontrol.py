from core.xapi_extensions_result import xAPI_Extensions_Result

class xAPI_Extensions_Result_SystemControl(xAPI_Extensions_Result):

    def __init__(self):
        super().__init__("SystemControl")

    def language(self, value):
        add(xAPI_Extension(
                context,
                extensionType,
                "language",
                {
                    "en-US": "language",
                    "de-DE": "Sprache"
                },
                {
                    "en-US": "Language of the system.",
                    "de-DE": "Sprache des Systems."
                }),
            value);
        return self;
