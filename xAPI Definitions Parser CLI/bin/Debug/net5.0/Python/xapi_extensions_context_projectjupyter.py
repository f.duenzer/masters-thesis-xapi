from core.xapi_extensions_context import xAPI_Extensions_Context

class xAPI_Extensions_Context_ProjectJupyter(xAPI_Extensions_Context):

    def __init__(self):
        super().__init__("ProjectJupyter")

    def profilename(self, value):
        add(xAPI_Extension(
                context,
                extensionType,
                "profilename",
                {
                    "en-US": "profilename",
                    "de-DE": "Profilname"
                },
                {
                    "en-US": "Name of the currently loaded JupyterLab profile (within a JupyterHub).",
                    "de-DE": "Name des aktuell geladenen JupyterLab-Profils (in einem JupyterHub)."
                }),
            value);
        return self;
