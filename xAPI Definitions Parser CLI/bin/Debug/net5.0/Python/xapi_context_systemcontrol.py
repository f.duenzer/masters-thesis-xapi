from core import xapi_context
from core.xapi_verbs_systemcontrol import xAPI_Verbs_SystemControl
from core.xapi_activities_systemcontrol import xAPI_Activities_SystemControl
from core.xapi_context_systemcontrol_extensions import xAPI_Context_SystemControl_Extensions

class xAPI_Context_SystemControl(xAPI_Context):
    verbs = xAPI_Verbs_SystemControl()

    activities = xAPI_Activities_SystemControl()

    extensions = xAPI_Context_SystemControl_Extensions()

    def __init__(self):
        super().__init__("xAPI_Definitions_Parser.Definitions.xAPIContext")
