from core.xapi_extensions_result import xAPI_Extensions_Result

class xAPI_Extensions_Result_Tagformance(xAPI_Extensions_Result):

    def __init__(self):
        super().__init__("Tagformance")

    def duration(self, value):
        add(xAPI_Extension(
                context,
                extensionType,
                "duration",
                {
                    "en-US": "duration"
                },
                {
                    "en-US": "The time taken to complete the experiment. This is captured from the system time of the local machine."
                }),
            value);
        return self;

    def empty(self, value):
        add(xAPI_Extension(
                context,
                extensionType,
                "empty",
                {
                    "en-US": "empty"
                },
                {
                    "en-US": "Boolean value which is true when something is empty."
                }),
            value);
        return self;
