from core import xapi_context
from core.xapi_verbs_generic import xAPI_Verbs_Generic
from core.xapi_activities_generic import xAPI_Activities_Generic
from core.xapi_context_generic_extensions import xAPI_Context_Generic_Extensions

class xAPI_Context_Generic(xAPI_Context):
    verbs = xAPI_Verbs_Generic()

    activities = xAPI_Activities_Generic()

    extensions = xAPI_Context_Generic_Extensions()

    def __init__(self):
        super().__init__("xAPI_Definitions_Parser.Definitions.xAPIContext")
