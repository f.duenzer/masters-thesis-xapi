from core.xapi_extensions_activity_tagformance import xAPI_Extensions_Activity_Tagformance
from core.xapi_extensions_context_tagformance import xAPI_Extensions_Context_Tagformance
from core.xapi_extensions_result_tagformance import xAPI_Extensions_Result_Tagformance

class xAPI_Context_Tagformance_Extensions:

    def __init__(self):
        pass

    @property
    def activity(self):
        return xAPI_Extensions_Activity_Tagformance();

    @property
    def context(self):
        return xAPI_Extensions_Context_Tagformance();

    @property
    def result(self):
        return xAPI_Extensions_Result_Tagformance();
