from core import xapi_activity
from core import xapi_activities

class xAPI_Activities_Tagformance(xAPI_Activities):
    button = xAPI_Activity(
        "Tagformance",
        "button",
        {
            "en-US": "button"
        },
        {
            "en-US": "A button the actor can interact with."
        })
    """A button the actor can interact with."""

    checkbox = xAPI_Activity(
        "Tagformance",
        "checkbox",
        {
            "en-US": "checkBox"
        },
        {
            "en-US": "A box which can be checked to select an already generated graph. Then it can be deleted using the cross button."
        })
    """A box which can be checked to select an already generated graph. Then it can be deleted using the cross button."""

    dropdown = xAPI_Activity(
        "Tagformance",
        "dropdown",
        {
            "en-US": "dropdown"
        },
        {
            "en-US": "An actor selects an option through a dropdown menu."
        })
    """An actor selects an option through a dropdown menu."""

    experiment = xAPI_Activity(
        "Tagformance",
        "experiment",
        {
            "en-US": "experiment"
        },
        {
            "en-US": "A scientific procedure, especially in a laboratory, to determine something."
        })
    """A scientific procedure, especially in a laboratory, to determine something."""

    graph = xAPI_Activity(
        "Tagformance",
        "graph",
        {
            "en-US": "graph"
        },
        {
            "en-US": "A graph that is generated after clicking on Start Sweep button. This can be interactible."
        })
    """A graph that is generated after clicking on Start Sweep button. This can be interactible."""

    keyboard = xAPI_Activity(
        "Tagformance",
        "keyboard",
        {
            "en-US": "keyboard"
        },
        {
            "en-US": "A keyboard used to input text or interact with the lab and enter values as per the actor's wish."
        })
    """A keyboard used to input text or interact with the lab and enter values as per the actor's wish."""

    mouse = xAPI_Activity(
        "Tagformance",
        "mouse",
        {
            "en-US": "mouse"
        },
        {
            "en-US": "A mouse used to click on buttons or drag items in the lab."
        })
    """A mouse used to click on buttons or drag items in the lab."""

    textBox = xAPI_Activity(
        "Tagformance",
        "textBox",
        {
            "en-US": "commentBox"
        },
        {
            "en-US": "A textbox, where the actor can fill text."
        })
    """A textbox, where the actor can fill text."""

    def __init__(self):
        super().__init__("Tagformance")
