from core.xapi_extensions_result import xAPI_Extensions_Result

class xAPI_Extensions_Result_Seriousgames(xAPI_Extensions_Result):

    def __init__(self):
        super().__init__("Seriousgames")

    def gamemode(self, value):
        add(xAPI_Extension(
                context,
                extensionType,
                "gamemode",
                {
                    "en-US": "gamemode",
                    "de-DE": "Spielmodus"
                },
                {
                    "en-US": "The result of a change of the gamemode. Can be a string or integer.",
                    "de-DE": "Der neue Spielmodus der aus dem Ereignis hervorgeht."
                }),
            value);
        return self;

    def health(self, value):
        add(xAPI_Extension(
                context,
                extensionType,
                "health",
                {
                    "en-US": "health",
                    "de-DE": "Leben"
                },
                {
                    "en-US": "The current health of a player character. Has to be an integer.",
                    "de-DE": "Das momentane Leben eines Spielers. Muss ein Integer sein."
                }),
            value);
        return self;

    def level(self, value):
        add(xAPI_Extension(
                context,
                extensionType,
                "level",
                {
                    "en-US": "level",
                    "de-DE": "Level"
                },
                {
                    "en-US": "The result of a level change. Has to be a string or integer.",
                    "de-DE": "Das neue Level welches das Ergebnis des Ereignisses ist."
                }),
            value);
        return self;

    def numberCommissionErrors(self, value):
        add(xAPI_Extension(
                context,
                extensionType,
                "numberCommissionErrors",
                {
                    "en-US": "number commission-errors",
                    "de-DE": "Anzahl Commissionsfehler"
                },
                {
                    "en-US": "The number of instances in which the player marked an incorrect solution as correct.",
                    "de-DE": "Die Anzahl der Male, in denen der Spielende es eine falsche Lösung als richtig markiert hat."
                }),
            value);
        return self;

    def numberCompleted(self, value):
        add(xAPI_Extension(
                context,
                extensionType,
                "numberCompleted",
                {
                    "en-US": "number completed",
                    "de-DE": "Anzahl beendet"
                },
                {
                    "en-US": "The number attempts that have been completed, either by winning or loosing.",
                    "de-DE": "Die Anzahl der Versuche, die beendet wurden, sowohl durch Gewinnen als auch Verlieren."
                }),
            value);
        return self;

    def numberErrors(self, value):
        add(xAPI_Extension(
                context,
                extensionType,
                "numberErrors",
                {
                    "en-US": "number errors",
                    "de-DE": "Anzahl Fehler"
                },
                {
                    "en-US": "The number of Errors that have been made in a gamemode or level.",
                    "de-DE": "Die Anzahl der Fehler, die in einem Spielmodus oder Level gemacht wurden."
                }),
            value);
        return self;

    def numberOmissionErrors(self, value):
        add(xAPI_Extension(
                context,
                extensionType,
                "numberOmissionErrors",
                {
                    "en-US": "number omission-errors",
                    "de-DE": "Anzahl Omissionsfehler"
                },
                {
                    "en-US": "The number of instances in which the player failed to mark a correct solution as correct.",
                    "de-DE": "Die Anzahl der Male, in denen der Spielende es versäumt hat, eine korrekte Lösung als richtig zu markieren."
                }),
            value);
        return self;

    def numberStarted(self, value):
        add(xAPI_Extension(
                context,
                extensionType,
                "numberStarted",
                {
                    "en-US": "number started",
                    "de-DE": "Anzahl Gestarted"
                },
                {
                    "en-US": "The number of levels that have been started in a gamemode.",
                    "de-DE": "Die Anzahl der Level, die in einem Spielmodus gestartet wurden."
                }),
            value);
        return self;

    def numberSucceeded(self, value):
        add(xAPI_Extension(
                context,
                extensionType,
                "numberSucceeded",
                {
                    "en-US": "number succeeded",
                    "de-DE": "Anzahl geschafft"
                },
                {
                    "en-US": "The number of instances in which the user was able to complete a level successfully.",
                    "de-DE": "Die Anzahl der Male, in denen der Nutzende es geschafft hat, ein Level erfolgreich abzuschließen."
                }),
            value);
        return self;

    def position(self, value):
        add(xAPI_Extension(
                context,
                extensionType,
                "position",
                {
                    "en-US": "position",
                    "de-DE": "Position"
                },
                {
                    "en-US": "The position of an object in the game. Can be in X, Y and Z coordinates.",
                    "de-DE": "Die Position eines Objektes im Spiel. Können X-, Y- und Z-Koordinaten sein."
                }),
            value);
        return self;

    def progress(self, value):
        add(xAPI_Extension(
                context,
                extensionType,
                "progress",
                {
                    "en-US": "progress",
                    "de-DE": "Fortschritt"
                },
                {
                    "en-US": "The progess a player or group achieved in the game or level. 1.0 Progress would mean the game or level was completed in every aspect. Has to be a double between 0 and 1.",
                    "de-DE": "Der Fortschritt welchen ein Spieler oder eine Gruppe erreichte in einem Level oder Spiel. 1.0 Fortschritt hieße das Spiel oder level wurde vollständig abgeschlossen. Muss ein double zwischen 0 und 1 sein."
                }),
            value);
        return self;

    def score(self, value):
        add(xAPI_Extension(
                context,
                extensionType,
                "score",
                {
                    "en-US": "score",
                    "de-DE": "Ergebnis"
                },
                {
                    "en-US": "Some relevant score which is tracked by the game. Can be an integer or double.",
                    "de-DE": "Ein relevantes Ergebnis welches während des Spiels augezeichnet wird. Kann ein Integer oder Double sein."
                }),
            value);
        return self;

    def sumPlaytime(self, value):
        add(xAPI_Extension(
                context,
                extensionType,
                "sumPlaytime",
                {
                    "en-US": "sum playtime",
                    "de-DE": "Summe Spielzeit"
                },
                {
                    "en-US": "The sum of the playtime that has been spent in a gamemode or level.",
                    "de-DE": "Die Summe der Spielzeit, die in einem Spielmodus oder Level verbracht wurde."
                }),
            value);
        return self;
