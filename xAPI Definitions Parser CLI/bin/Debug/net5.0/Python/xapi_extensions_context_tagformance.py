from core.xapi_extensions_context import xAPI_Extensions_Context

class xAPI_Extensions_Context_Tagformance(xAPI_Extensions_Context):

    def __init__(self):
        super().__init__("Tagformance")

    def energyMeasure(self, value):
        add(xAPI_Extension(
                context,
                extensionType,
                "energyMeasure",
                {
                    "en-US": "energy measure"
                },
                {
                    "en-US": "Energy Measure is an experiment mode as per the lab instruction."
                }),
            value);
        return self;

    def frequencyStep(self, value):
        add(xAPI_Extension(
                context,
                extensionType,
                "frequencyStep",
                {
                    "en-US": "frequency step"
                },
                {
                    "en-US": "An actor selects a frequency step for the measurement of the graph."
                }),
            value);
        return self;

    def measurementUnit(self, value):
        add(xAPI_Extension(
                context,
                extensionType,
                "measurementUnit",
                {
                    "en-US": "measurement unit dropdown"
                },
                {
                    "en-US": "An actor selects a unit in the Y-axis of the graph that is measured."
                }),
            value);
        return self;

    def orientationMeasure(self, value):
        add(xAPI_Extension(
                context,
                extensionType,
                "orientationMeasure",
                {
                    "en-US": "orientation measure"
                },
                {
                    "en-US": "This is an experiment mode as per lab instruction."
                }),
            value);
        return self;

    def powerStep(self, value):
        add(xAPI_Extension(
                context,
                extensionType,
                "powerStep",
                {
                    "en-US": "power step"
                },
                {
                    "en-US": "The power step selected for the measurement."
                }),
            value);
        return self;

    def protocol(self, value):
        add(xAPI_Extension(
                context,
                extensionType,
                "protocol",
                {
                    "en-US": "protocol"
                },
                {
                    "en-US": "The protocol used is ISO 18000-6c Query."
                }),
            value);
        return self;

    def selectTag(self, value):
        add(xAPI_Extension(
                context,
                extensionType,
                "selectTag",
                {
                    "en-US": "select tag dropdown"
                },
                {
                    "en-US": "The RFID tag selected for measurement."
                }),
            value);
        return self;

    def startFrequency(self, value):
        add(xAPI_Extension(
                context,
                extensionType,
                "startFrequency",
                {
                    "en-US": "start frequency"
                },
                {
                    "en-US": "The selected frequency to start the measurement."
                }),
            value);
        return self;

    def stopFrequency(self, value):
        add(xAPI_Extension(
                context,
                extensionType,
                "stopFrequency",
                {
                    "en-US": "stop frequency"
                },
                {
                    "en-US": "The selected frequency to stop the measurement."
                }),
            value);
        return self;

    def theoreticalReadingRange(self, value):
        add(xAPI_Extension(
                context,
                extensionType,
                "theoreticalReadingRange",
                {
                    "en-US": "theoretical reading range"
                },
                {
                    "en-US": "The Y-axis unit in the graph for measuring the reading range of transponder."
                }),
            value);
        return self;

    def transmittedPower(self, value):
        add(xAPI_Extension(
                context,
                extensionType,
                "transmittedPower",
                {
                    "en-US": "transmitted power"
                },
                {
                    "en-US": "The Y-axis unit in dBm for measurement."
                }),
            value);
        return self;
