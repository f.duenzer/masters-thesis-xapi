from core import xapi_activity
from core import xapi_activities

class xAPI_Activities_Generic(xAPI_Activities):
    answer = xAPI_Activity(
        "Generic",
        "answer",
        {
            "en-US": "answer",
            "de-DE": "Antwort"
        },
        {
            "en-US": "The response to a question a user can give or get.",
            "de-DE": "Eine Antwort kann auf eine Frage gegeben oder bekommen werden."
        })
    """The response to a question a user can give or get."""

    backwardButton = xAPI_Activity(
        "Generic",
        "backwardButton",
        {
            "en-US": "backward button",
            "de-DE": "Zurück Knopf"
        },
        {
            "en-US": "A button to go backward on a page or a screen.",
            "de-DE": "Ein Knopf um auf einer Seite oder dem Bildschirm zurück zu gehen."
        })
    """A button to go backward on a page or a screen."""

    card = xAPI_Activity(
        "Generic",
        "card",
        {
            "en-US": "card",
            "de-DE": "Karte"
        },
        {
            "en-US": "A card showning something inside the game.",
            "de-DE": "Eine Karte die etwas im Spiel anzeigt."
        })
    """A card showning something inside the game."""

    decision = xAPI_Activity(
        "Generic",
        "decision",
        {
            "en-US": "decision",
            "de-DE": "Entscheidung"
        },
        {
            "en-US": "A decision to be made which is relevant for the game outcome.",
            "de-DE": "Eine Entscheidung die getroffen werden muss und welche den Spielausgang beieinflusst."
        })
    """A decision to be made which is relevant for the game outcome."""

    document = xAPI_Activity(
        "Generic",
        "document",
        {
            "en-US": "document",
            "de-DE": "Dokument"
        },
        {
            "en-US": "A document ingame. Could be relevant for the game or not.",
            "de-DE": "Ein Dokument im Spiel."
        })
    """A document ingame. Could be relevant for the game or not."""

    eyeTrackingSystem = xAPI_Activity(
        "Generic",
        "eyeTrackingSystem",
        {
            "en-US": "eye tracking system",
            "de-DE": "Eye-Tracking System"
        },
        {
            "en-US": "A system to track the eyes of a player and use the gathered data in some way.",
            "de-DE": "Ein system um die Augenbewegungen des Spielers aufzuzeichnen und diese Daten in einer Form zu nutzen."
        })
    """A system to track the eyes of a player and use the gathered data in some way."""

    file = xAPI_Activity(
        "Generic",
        "file",
        {
            "en-US": "File",
            "de-DE": "Datei"
        },
        {
            "en-US": "A collection of in - most cases - connected data on a disk which is stored together under a particular name.",
            "de-DE": "Eine Sammlung von meist zusammenhängenden Daten auf einem Speichermedium die zusammen unter einem bestimmten Namen gespeichert werden."
        })
    """A collection of in - most cases - connected data on a disk which is stored together under a particular name."""

    forwardButton = xAPI_Activity(
        "Generic",
        "forwardButton",
        {
            "en-US": "forward button",
            "de-DE": "Weiter Knopf"
        },
        {
            "en-US": "A button to go forward on a page or a screen.",
            "de-DE": "Ein Knopf um auf einer Seite oder dem Bildschirm weiter zu gehen."
        })
    """A button to go forward on a page or a screen."""

    gamemode = xAPI_Activity(
        "Generic",
        "gamemode",
        {
            "en-US": "gamemode",
            "de-DE": "Spielmodus"
        },
        {
            "en-US": "The gamemode in which a game is played (Competitive or cooperative for example).",
            "de-DE": "Der Spielmodus in dem ein Spiel gespielt wird (Zusammen oder Gegeneinander zum Beispiel)."
        })
    """The gamemode in which a game is played (Competitive or cooperative for example)."""

    goal = xAPI_Activity(
        "Generic",
        "goal",
        {
            "en-US": "goal",
            "de-DE": "Ziel"
        },
        {
            "en-US": "The overall thing to be achieved in the game.",
            "de-DE": "Das Übergreifende Ziel des Spiels."
        })
    """The overall thing to be achieved in the game."""

    group = xAPI_Activity(
        "Generic",
        "group",
        {
            "en-US": "group",
            "de-DE": "Gruppe"
        },
        {
            "en-US": "A group of players playing a game together.",
            "de-DE": "Eine Gruppe von Spielern welche zusammen ein Spiel spielen."
        })
    """A group of players playing a game together."""

    help = xAPI_Activity(
        "Generic",
        "help",
        {
            "en-US": "help",
            "de-DE": "Hilfe"
        },
        {
            "en-US": "Something a player can ask for to find the solution to a task.",
            "de-DE": "Etwas wonach der Spieler fragen kann um eine Lösung für eine Aufgabe zu finden."
        })
    """Something a player can ask for to find the solution to a task."""

    image = xAPI_Activity(
        "Generic",
        "image",
        {
            "en-US": "image",
            "de-DE": "Bild"
        },
        {
            "en-US": "An image of something ingame. Can be an interactible.",
            "de-DE": "Ein Bild von etwas im Spiel. Mit einem Bild könnte interagiert werden."
        })
    """An image of something ingame. Can be an interactible."""

    keyboard = xAPI_Activity(
        "Generic",
        "keyboard",
        {
            "en-US": "keyboard",
            "de-DE": "Tastatur"
        },
        {
            "en-US": "A keyboard to input text or interact with the game in another way.",
            "de-DE": "Eine Tastatur um Text einzugeben oder in einer anderen Form mit dem Spiel zu interagieren."
        })
    """A keyboard to input text or interact with the game in another way."""

    link = xAPI_Activity(
        "Generic",
        "link",
        {
            "en-US": "Hyperlink",
            "de-DE": "Hyperlink"
        },
        {
            "en-US": "A reference to data that the user can follow. Links can either point to a whole document (e.g. website) or to a specific element within a document.",
            "de-DE": "Ein Querverweis auf ein elektronischces Dokument, welches dem Benutzer ermöglicht zu dem Dokument (z.B. einer Website) oder zu einem bestimmten Abschnitt in dem Dokument zu springen."
        })
    """A reference to data that the user can follow. Links can either point to a whole document (e.g. website) or to a specific element within a document."""

    menu = xAPI_Activity(
        "Generic",
        "menu",
        {
            "en-US": "menu",
            "de-DE": "Menü"
        },
        {
            "en-US": "A start menu or ingame menu for settings for example.",
            "de-DE": "Ein Startmenü oder ein Menü im Spiel für Einstellungen zum Beispiel."
        })
    """A start menu or ingame menu for settings for example."""

    mouse = xAPI_Activity(
        "Generic",
        "mouse",
        {
            "en-US": "mouse",
            "de-DE": "Maus"
        },
        {
            "en-US": "A mouse to use UI or interact with the game in another way.",
            "de-DE": "Eine Maus um die Benutzeroberfläche zu benutzen oder in einer anderen Form mit dem Spiel zu interagieren."
        })
    """A mouse to use UI or interact with the game in another way."""

    number = xAPI_Activity(
        "Generic",
        "number",
        {
            "en-US": "number",
            "de-DE": "Nummer"
        },
        {
            "en-US": "A number shown on the screen, with which for example can be interacted with in some way.",
            "de-DE": "Eine Nummer die auf dem Bildschirm angezeigt wird und mit der zum Beispiel interagiert werden kann."
        })
    """A number shown on the screen, with which for example can be interacted with in some way."""

    option = xAPI_Activity(
        "Generic",
        "option",
        {
            "en-US": "option",
            "de-DE": "Option"
        },
        {
            "en-US": "An option is something a player can decide for or against in the game.",
            "de-DE": "Ein Spieler kann sich für oder gegen eine Option entscheiden."
        })
    """An option is something a player can decide for or against in the game."""

    player = xAPI_Activity(
        "Generic",
        "player",
        {
            "en-US": "player",
            "de-DE": "Spieler"
        },
        {
            "en-US": "An actor which plays the game and interacts with it.",
            "de-DE": "Ein Akteur der das Spiel spielt und mit ihm interagiert."
        })
    """An actor which plays the game and interacts with it."""

    playstone = xAPI_Activity(
        "Generic",
        "playstone",
        {
            "en-US": "playstone",
            "de-DE": "Spielstein"
        },
        {
            "en-US": "A representation of the player ingame. Could also be an object the player can interact with.",
            "de-DE": "Eine Representation des Spielers im Spiel. Könnte auch ein Objekt sein mit welchem der Spieler im Spiel interagieren kann."
        })
    """A representation of the player ingame. Could also be an object the player can interact with."""

    question = xAPI_Activity(
        "Generic",
        "question",
        {
            "en-US": "question",
            "de-DE": "Frage"
        },
        {
            "en-US": "A question can be asked or answered.",
            "de-DE": "Eine Frage kann gestellt oder beantwortet werden."
        })
    """A question can be asked or answered."""

    remoteSystem = xAPI_Activity(
        "Generic",
        "remoteSystem",
        {
            "en-US": "remote system",
            "de-DE": "entferntes System"
        },
        {
            "en-US": "A system interacting with the game which is not running on the same machine as the game.",
            "de-DE": "Ein System welches mit dem Spiel interagiert aber nicht auf der selben Maschine wie das Spiel läuft."
        })
    """A system interacting with the game which is not running on the same machine as the game."""

    senorData = xAPI_Activity(
        "Generic",
        "senorData",
        {
            "en-US": "sensor data",
            "de-DE": "Sensorendaten"
        },
        {
            "en-US": "Data which is gathered by a sensor.",
            "de-DE": "Daten welche von Sensoren erfasst werden."
        })
    """Data which is gathered by a sensor."""

    sensorData = xAPI_Activity(
        "Generic",
        "sensorData",
        {
            "en-US": "sensor data",
            "de-DE": "Sensorendaten"
        },
        {
            "en-US": "Data which is gathered by a sensor.",
            "de-DE": "Daten, welche von einem Sensor erfasst werden."
        })
    """Data which is gathered by a sensor."""

    settings = xAPI_Activity(
        "Generic",
        "settings",
        {
            "en-US": "settings",
            "de-DE": "Einstellungen"
        },
        {
            "en-US": "The settings of a game.",
            "de-DE": "Die EInstellungen eines Spiels."
        })
    """The settings of a game."""

    solution = xAPI_Activity(
        "Generic",
        "solution",
        {
            "en-US": "solution",
            "de-DE": "Lösung"
        },
        {
            "en-US": "The solution to a task in the game.",
            "de-DE": "Die Lösung zu einer Aufgabe im Spiel."
        })
    """The solution to a task in the game."""

    solvability = xAPI_Activity(
        "Generic",
        "solvability",
        {
            "en-US": "solvability",
            "de-DE": "Lösbarkeit"
        },
        {
            "en-US": "The status of if a task can be completed in the current gamestate.",
            "de-DE": "Der Zustand ob die aktuelle Aufgabe noch gelöst werden kann mit dem aktuelle Spielzustand."
        })
    """The status of if a task can be completed in the current gamestate."""

    startVideoButton = xAPI_Activity(
        "Generic",
        "startVideoButton",
        {
            "en-US": "start video button",
            "de-DE": "Videostarten Knopf"
        },
        {
            "en-US": "A button to start a video.",
            "de-DE": "Ein Knopf um ein Video zu starten."
        })
    """A button to start a video."""

    stopVideoButton = xAPI_Activity(
        "Generic",
        "stopVideoButton",
        {
            "en-US": "stop video button",
            "de-DE": "Videostoppen Knopf"
        },
        {
            "en-US": "A button to stop a video.",
            "de-DE": "Ein Knopf um ein Video zu stoppen."
        })
    """A button to stop a video."""

    tangible = xAPI_Activity(
        "Generic",
        "tangible",
        {
            "en-US": "tangible",
            "de-DE": "Tangible"
        },
        {
            "en-US": "A device to interact with the game.",
            "de-DE": "Ein Gerät mit dem man mit dem Spiel interagieren kann."
        })
    """A device to interact with the game."""

    task = xAPI_Activity(
        "Generic",
        "task",
        {
            "en-US": "task",
            "de-DE": "Aufgabe"
        },
        {
            "en-US": "Some part of the goal or something to be completed to progress in the game or a level.",
            "de-DE": "Eine Aufgabe die zum Erreichen des Spielziels erfüllt werden muss oder zum Abschließen eines Levels."
        })
    """Some part of the goal or something to be completed to progress in the game or a level."""

    text = xAPI_Activity(
        "Generic",
        "text",
        {
            "en-US": "text",
            "de-DE": "Text"
        },
        {
            "en-US": "A text which can be read or entered.",
            "de-DE": "Ein Text der gelesen oder eingegeben werden kann."
        })
    """A text which can be read or entered."""

    tip = xAPI_Activity(
        "Generic",
        "tip",
        {
            "en-US": "tip",
            "de-DE": "Tipp"
        },
        {
            "en-US": "A tip to overcome some ingame task.",
            "de-DE": "Ein Tipp um eine Aufgabe im Spiel zu lösen."
        })
    """A tip to overcome some ingame task."""

    video = xAPI_Activity(
        "Generic",
        "video",
        {
            "en-US": "video",
            "de-DE": "Video"
        },
        {
            "en-US": "A video which can be watched inside the game.",
            "de-DE": "Ein Video welches im Spiel angeschaut werden kann."
        })
    """A video which can be watched inside the game."""

    word = xAPI_Activity(
        "Generic",
        "word",
        {
            "en-US": "word",
            "de-DE": "Wort"
        },
        {
            "en-US": "A word shown on the screen, with which for example can be interacted with in some way.",
            "de-DE": "Ein Wort welches auf dem Bildschirm angezeigt wird und mit dem man zum Beispiel interagieren kann."
        })
    """A word shown on the screen, with which for example can be interacted with in some way."""

    def __init__(self):
        super().__init__("Generic")
