from core import xapi_context
from core.xapi_verbs_vrrfidchamber import xAPI_Verbs_VrRfidChamber
from core.xapi_activities_vrrfidchamber import xAPI_Activities_VrRfidChamber
from core.xapi_context_vrrfidchamber_extensions import xAPI_Context_VrRfidChamber_Extensions

class xAPI_Context_VrRfidChamber(xAPI_Context):
    verbs = xAPI_Verbs_VrRfidChamber()

    activities = xAPI_Activities_VrRfidChamber()

    extensions = xAPI_Context_VrRfidChamber_Extensions()

    def __init__(self):
        super().__init__("xAPI_Definitions_Parser.Definitions.xAPIContext")
