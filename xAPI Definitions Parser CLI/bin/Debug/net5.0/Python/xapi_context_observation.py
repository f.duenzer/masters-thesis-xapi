from core import xapi_context
from core.xapi_verbs_observation import xAPI_Verbs_Observation
from core.xapi_activities_observation import xAPI_Activities_Observation
from core.xapi_context_observation_extensions import xAPI_Context_Observation_Extensions

class xAPI_Context_Observation(xAPI_Context):
    verbs = xAPI_Verbs_Observation()

    activities = xAPI_Activities_Observation()

    extensions = xAPI_Context_Observation_Extensions()

    def __init__(self):
        super().__init__("xAPI_Definitions_Parser.Definitions.xAPIContext")
