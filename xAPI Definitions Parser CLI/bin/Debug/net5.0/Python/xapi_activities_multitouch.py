from core import xapi_activity
from core import xapi_activities

class xAPI_Activities_Multitouch(xAPI_Activities):
    button = xAPI_Activity(
        "Multitouch",
        "button",
        {
            "en-US": "button",
            "de-DE": "Knopf"
        },
        {
            "en-US": "Something an actor can press, which then triggers an action in the game.",
            "de-DE": "Etwas was ein Akteur drücken kann, was dann eine Aktion im Spiel hervorruft."
        })
    """Something an actor can press, which then triggers an action in the game."""

    collectable = xAPI_Activity(
        "Multitouch",
        "collectable",
        {
            "en-US": "collectable",
            "de-DE": "einsammelbares Objekt"
        },
        {
            "en-US": "Something which can be collected inside the game to fullfill a task or goal.",
            "de-DE": "Ein Objekt, welches im Spiel eingesammelt werden kann um ein Spielziel zu erreichen oder eine Aufgabe zu erfüllen."
        })
    """Something which can be collected inside the game to fullfill a task or goal."""

    draggable = xAPI_Activity(
        "Multitouch",
        "draggable",
        {
            "en-US": "draggable",
            "de-DE": "ziehbares Objekt"
        },
        {
            "en-US": "Something that can be dragged across the screen in the game.",
            "de-DE": "Etwas was im Spiel über den Bildschirm gezogen werden kann."
        })
    """Something that can be dragged across the screen in the game."""

    interactable = xAPI_Activity(
        "Multitouch",
        "interactable",
        {
            "en-US": "interactable",
            "de-DE": "interagierfähiges Objekt"
        },
        {
            "en-US": "Something a player can interact with in the game.",
            "de-DE": "Etwas womit der Spieler im Spiel interagieren kann."
        })
    """Something a player can interact with in the game."""

    screen = xAPI_Activity(
        "Multitouch",
        "screen",
        {
            "en-US": "screen",
            "de-DE": "Bildschirm"
        },
        {
            "en-US": "The screen showing the game and being clicked or touched on for interaction with the game.",
            "de-DE": "Der Bildschirm der das Spiel anzeigt und auf dem geklickt werden kann oder der berührt wird um mit dem Spiel zu interagieren."
        })
    """The screen showing the game and being clicked or touched on for interaction with the game."""

    def __init__(self):
        super().__init__("Multitouch")
