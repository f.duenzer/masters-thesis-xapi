from core.xapi_extensions_activity import xAPI_Extensions_Activity

class xAPI_Extensions_Activity_ProjectJupyter(xAPI_Extensions_Activity):

    def __init__(self):
        super().__init__("ProjectJupyter")

    def content(self, value):
        add(xAPI_Extension(
                context,
                extensionType,
                "content",
                {
                    "en-US": "content",
                    "de-DE": "Inhalt"
                },
                {
                    "en-US": "The content of an Activity. In case of a file, it is the file content, in case of a Notebook cell the content is the code/text inside a notebook cell.",
                    "de-DE": "Der Inhalt einer Aktivität. Im Falle einer Datei ist der Inhalt der Dateiinhalt, im Falle einer Notebookzelle ist der Inhalt der Programmcode oder Text innerhalb einer Zelle."
                }),
            value);
        return self;

    def kernelName(self, value):
        add(xAPI_Extension(
                context,
                extensionType,
                "kernelName",
                {
                    "en-US": "kernel name",
                    "de-DE": "Kernelname"
                },
                {
                    "en-US": "The name of a Jupyter Kernels. E.g. 'Python 3.7'. A list of kernels is available at https://github.com/jupyter/jupyter/wiki/Jupyter-kernels.",
                    "de-DE": "Der Name eines Jupyter Kernels. Z.b. 'Python 3.7. Eine Liste mit Kerneln ist unter https://github.com/jupyter/jupyter/wiki/Jupyter-kernels einsehbar."
                }),
            value);
        return self;

    def tags(self, value):
        add(xAPI_Extension(
                context,
                extensionType,
                "tags",
                {
                    "en-US": "tags",
                    "de-DE": "Etikett"
                },
                {
                    "en-US": "A list of words used to describe or groupe activities.",
                    "de-DE": "Eine Liste mit von Wörtern mit denen eine Aktivität beschrieben wird oder mit denen mehrere Aktivitäten gruppiert werden können."
                }),
            value);
        return self;
