from core import xapi_context
from core.xapi_verbs_lms import xAPI_Verbs_Lms
from core.xapi_activities_lms import xAPI_Activities_Lms
from core.xapi_context_lms_extensions import xAPI_Context_Lms_Extensions

class xAPI_Context_Lms(xAPI_Context):
    verbs = xAPI_Verbs_Lms()

    activities = xAPI_Activities_Lms()

    extensions = xAPI_Context_Lms_Extensions()

    def __init__(self):
        super().__init__("xAPI_Definitions_Parser.Definitions.xAPIContext")
