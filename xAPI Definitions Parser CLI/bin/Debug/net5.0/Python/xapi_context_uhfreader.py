from core import xapi_context
from core.xapi_verbs_uhfreader import xAPI_Verbs_UhfReader
from core.xapi_activities_uhfreader import xAPI_Activities_UhfReader
from core.xapi_context_uhfreader_extensions import xAPI_Context_UhfReader_Extensions

class xAPI_Context_UhfReader(xAPI_Context):
    verbs = xAPI_Verbs_UhfReader()

    activities = xAPI_Activities_UhfReader()

    extensions = xAPI_Context_UhfReader_Extensions()

    def __init__(self):
        super().__init__("xAPI_Definitions_Parser.Definitions.xAPIContext")
