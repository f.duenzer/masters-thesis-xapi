from core.xapi_extensions_activity_projectjupyter import xAPI_Extensions_Activity_ProjectJupyter
from core.xapi_extensions_context_projectjupyter import xAPI_Extensions_Context_ProjectJupyter
from core.xapi_extensions_result_projectjupyter import xAPI_Extensions_Result_ProjectJupyter

class xAPI_Context_ProjectJupyter_Extensions:

    def __init__(self):
        pass

    @property
    def activity(self):
        return xAPI_Extensions_Activity_ProjectJupyter();

    @property
    def context(self):
        return xAPI_Extensions_Context_ProjectJupyter();

    @property
    def result(self):
        return xAPI_Extensions_Result_ProjectJupyter();
