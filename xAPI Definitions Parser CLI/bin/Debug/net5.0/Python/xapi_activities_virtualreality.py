from core import xapi_activity
from core import xapi_activities

class xAPI_Activities_VirtualReality(xAPI_Activities):
    action = xAPI_Activity(
        "VirtualReality",
        "action",
        {
            "en-US": "action",
            "de-DE": "Aktion"
        },
        {
            "en-US": "An action the actor can trigger.",
            "de-DE": "Eine Aktion, die von der Akteurin ausgelöst wird."
        })
    """An action the actor can trigger."""

    controllerButton = xAPI_Activity(
        "VirtualReality",
        "controllerButton",
        {
            "en-US": "controller button",
            "de-DE": "Controller Taste"
        },
        {
            "en-US": "A controller button or gamepad button the actor can use. On–off controls that switch between different system states.",
            "de-DE": "Eine Taste des Controllers oder des Gamepads, die die Akteurin benutzen kann. Die Betätigung der Taste führt zu einer Änderung des Systemstatus."
        })
    """A controller button or gamepad button the actor can use. On–off controls that switch between different system states."""

    pointable = xAPI_Activity(
        "VirtualReality",
        "pointable",
        {
            "en-US": "pointable object",
            "de-DE": "ein markierbares Objekt"
        },
        {
            "en-US": "An object which can be pointed and highlighted with for example a laser starting from the controller.",
            "de-DE": "Ein Objekt, welches z.B. mit einem Laser vom Controller markiert werden kann."
        })
    """An object which can be pointed and highlighted with for example a laser starting from the controller."""

    teleportPoint = xAPI_Activity(
        "VirtualReality",
        "teleportPoint",
        {
            "en-US": "teleport point",
            "de-DE": "Teleport Punkt"
        },
        {
            "en-US": "A fixed teleport point in VR environment.",
            "de-DE": "Ein fester Teleport Punkt in der VR Umgebung."
        })
    """A fixed teleport point in VR environment."""

    touchpad = xAPI_Activity(
        "VirtualReality",
        "touchpad",
        {
            "en-US": "controller touchpad",
            "de-DE": "Controller Touchpad"
        },
        {
            "en-US": "A VR controller touchpad the actor can use, appearance and possibilities depend on the specific conroller type.",
            "de-DE": "Ein VR Controller Touchpad, das die Akteurin benutzen kann. Aussehen und Möglichkeiten sind abhängig davon welcher Controller genutzt wurde."
        })
    """A VR controller touchpad the actor can use, appearance and possibilities depend on the specific conroller type."""

    uiElement = xAPI_Activity(
        "VirtualReality",
        "uiElement",
        {
            "en-US": "VR user interface element",
            "de-DE": "VR Benutzerflächenelement"
        },
        {
            "en-US": "An UI element in VR the user can interact with (e.g. Panel, Button, Slider).",
            "de-DE": "Ein Benutzerflächenelement mit dem der Benutzer interagieren kann (z.B. Panel, Button, Slider)."
        })
    """An UI element in VR the user can interact with (e.g. Panel, Button, Slider)."""

    vrObject = xAPI_Activity(
        "VirtualReality",
        "vrObject",
        {
            "en-US": "VR object",
            "de-DE": "VR Objekt"
        },
        {
            "en-US": "A virtual object in a VR environment.",
            "de-DE": "Ein virtuelles Objekt in einer VR Umgebung."
        })
    """A virtual object in a VR environment."""

    def __init__(self):
        super().__init__("VirtualReality")
