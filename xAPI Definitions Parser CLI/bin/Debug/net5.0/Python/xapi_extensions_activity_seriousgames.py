from core.xapi_extensions_activity import xAPI_Extensions_Activity

class xAPI_Extensions_Activity_Seriousgames(xAPI_Extensions_Activity):

    def __init__(self):
        super().__init__("Seriousgames")

    def buttontype(self, value):
        add(xAPI_Extension(
                context,
                extensionType,
                "buttontype",
                {
                    "en-US": "button type",
                    "de-DE": "Knopf Typ"
                },
                {
                    "en-US": "The type of button which was pressed. Has to be String.",
                    "de-DE": "Der Typ des Knopfes der gedrückt wurde. Muss ein String sein."
                }),
            value);
        return self;
