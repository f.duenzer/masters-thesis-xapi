from core.xapi_extensions_activity import xAPI_Extensions_Activity

class xAPI_Extensions_Activity_Tagformance(xAPI_Extensions_Activity):

    def __init__(self):
        super().__init__("Tagformance")

    def experimentMode(self, value):
        add(xAPI_Extension(
                context,
                extensionType,
                "experimentMode",
                {
                    "en-US": "experiment mode"
                },
                {
                    "en-US": "An experiment mode can be for example 'Energy Measurement' or 'Orientation Measurement'"
                }),
            value);
        return self;

    def graphName(self, value):
        add(xAPI_Extension(
                context,
                extensionType,
                "graphName",
                {
                    "en-US": "graphName"
                },
                {
                    "en-US": "An actor has authored the name of the graph or entered the username."
                }),
            value);
        return self;

    def mouseButton(self, value):
        add(xAPI_Extension(
                context,
                extensionType,
                "mouseButton",
                {
                    "en-US": "mouse button"
                },
                {
                    "en-US": "Name of the mouse button the actor has used (e.g. 'right', 'left')."
                }),
            value);
        return self;

    def uiElementValue(self, value):
        add(xAPI_Extension(
                context,
                extensionType,
                "uiElementValue",
                {
                    "en-US": "ui element value"
                },
                {
                    "en-US": "A value of an ui element (checkbox, dropdown, etc.)."
                }),
            value);
        return self;
