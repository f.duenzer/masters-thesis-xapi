from core import xapi_verb
from core import xapi_verbs

class xAPI_Verbs_ProjectJupyter(xAPI_Verbs):
    run = xAPI_Verb(
        "ProjectJupyter",
        "run",
        {
            "en-US": "run",
            "de-DE": "führte aus"
        },
        {
            "en-US": "Indicates, that an actor run some programming context. Typically a Jupyter Notebook Code Cell.",
            "de-DE": "Signalisiert, dass ein Akteur ein Programmierkonstrukt ausführte. Typischerweise eine Jupyter Notebook Codezelle."
        })
    """Indicates, that an actor run some programming context. Typically a Jupyter Notebook Code Cell."""

    def __init__(self):
        super().__init__("ProjectJupyter")
