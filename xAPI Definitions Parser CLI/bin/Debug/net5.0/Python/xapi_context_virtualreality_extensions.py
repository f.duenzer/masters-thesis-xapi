from core.xapi_extensions_activity_virtualreality import xAPI_Extensions_Activity_VirtualReality
from core.xapi_extensions_result_virtualreality import xAPI_Extensions_Result_VirtualReality

class xAPI_Context_VirtualReality_Extensions:

    def __init__(self):
        pass

    @property
    def activity(self):
        return xAPI_Extensions_Activity_VirtualReality();

    @property
    def result(self):
        return xAPI_Extensions_Result_VirtualReality();
