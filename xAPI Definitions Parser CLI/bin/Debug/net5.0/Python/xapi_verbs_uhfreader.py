from core import xapi_verb
from core import xapi_verbs

class xAPI_Verbs_UhfReader(xAPI_Verbs):
    clicked = xAPI_Verb(
        "UhfReader",
        "clicked",
        {
            "en-US": "clicked"
        },
        {
            "en-US": "An actor clicked on something on the UHF Reader software."
        })
    """An actor clicked on something on the UHF Reader software."""

    connected = xAPI_Verb(
        "UhfReader",
        "connected",
        {
            "en-US": "connected"
        },
        {
            "en-US": "An actor connected an device (e.g. transponder) to the physical reader."
        })
    """An actor connected an device (e.g. transponder) to the physical reader."""

    evaluate = xAPI_Verb(
        "UhfReader",
        "evaluate",
        {
            "en-US": "evaluate"
        },
        {
            "en-US": "A user evaluates a target (e.g. transponder)."
        })
    """A user evaluates a target (e.g. transponder)."""

    selected = xAPI_Verb(
        "UhfReader",
        "selected",
        {
            "en-US": "selected"
        },
        {
            "en-US": "An actor selects an activity mode or experiment mode from menu."
        })
    """An actor selects an activity mode or experiment mode from menu."""

    started = xAPI_Verb(
        "UhfReader",
        "started",
        {
            "en-US": "started"
        },
        {
            "en-US": "An actor started an activity or process."
        })
    """An actor started an activity or process."""

    tidCopied = xAPI_Verb(
        "UhfReader",
        "tidCopied",
        {
            "en-US": "tid copied"
        },
        {
            "en-US": "An actor copied the TID from the screen. Then the actor can paste it in google to research on the transponder."
        })
    """An actor copied the TID from the screen. Then the actor can paste it in google to research on the transponder."""

    def __init__(self):
        super().__init__("UhfReader")
