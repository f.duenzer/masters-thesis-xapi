from core import xapi_activity
from core import xapi_activities

class xAPI_Activities_Gestures(xAPI_Activities):
    hand = xAPI_Activity(
        "Gestures",
        "hand",
        {
            "en-US": "hand",
            "de-DE": "Hand"
        },
        {
            "en-US": "Actors physical (left or right) hand.",
            "de-DE": "Physikalische (linke oder rechte) Hand vom Akteur."
        })
    """Actors physical (left or right) hand."""

    head = xAPI_Activity(
        "Gestures",
        "head",
        {
            "en-US": "head",
            "de-DE": "Kopf"
        },
        {
            "en-US": "Actors physical head.",
            "de-DE": "Physikalische Kopf vom Akteur."
        })
    """Actors physical head."""

    def __init__(self):
        super().__init__("Gestures")
