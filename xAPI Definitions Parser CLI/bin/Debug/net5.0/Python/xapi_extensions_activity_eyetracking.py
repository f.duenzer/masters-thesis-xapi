from core.xapi_extensions_activity import xAPI_Extensions_Activity

class xAPI_Extensions_Activity_EyeTracking(xAPI_Extensions_Activity):

    def __init__(self):
        super().__init__("EyeTracking")

    def eye(self, value):
        add(xAPI_Extension(
                context,
                extensionType,
                "eye",
                {
                    "en-US": "eye",
                    "de-DE": "Auge"
                },
                {
                    "en-US": "Organ of the visual system. Actors physical (left or right) eye.",
                    "de-DE": "Sinnesorgan zur Wahrnehmung von Lichtreizen, in diesem Fall physikalisches Auge des Akteurs (links oder rechts)."
                }),
            value);
        return self;
