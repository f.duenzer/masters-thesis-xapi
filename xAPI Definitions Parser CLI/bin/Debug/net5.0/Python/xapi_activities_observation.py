from core import xapi_activity
from core import xapi_activities
from core import xapi_activities_observation_mainscores
from core import xapi_activities_observation_subscores

class xAPI_Activities_Observation(xAPI_Activities):
    mainscores = xAPI_Activities_Observation_Mainscores()

    subscores = xAPI_Activities_Observation_Subscores()

    andere = xAPI_Activity(
        "Observation",
        "andere",
        {
            "de-DE": "andere"
        },
        {
            "de-DE": "Verb bezieht sich auf andere Akteure"
        })
    """Verb bezieht sich auf andere Akteure"""

    anweisungen = xAPI_Activity(
        "Observation",
        "anweisungen",
        {
            "de-DE": "Anweisungen"
        },
        {
            "de-DE": "Verb bezieht sich auf Anweisungen"
        })
    """Verb bezieht sich auf Anweisungen"""

    aus_dem_Fenster = xAPI_Activity(
        "Observation",
        "aus_dem_Fenster",
        {
            "de-DE": "aus dem Fenster"
        },
        {
            "de-DE": "aus dem Fenster"
        })
    """aus dem Fenster"""

    erklaerung = xAPI_Activity(
        "Observation",
        "erklaerung",
        {
            "de-DE": "Erklärung"
        },
        {
            "de-DE": "Von Beobachtern durchgeführte Aufgabenerklärung"
        })
    """Von Beobachtern durchgeführte Aufgabenerklärung"""

    es_sich_genauer_an = xAPI_Activity(
        "Observation",
        "es_sich_genauer_an",
        {
            "de-DE": "es sich genauer an"
        },
        {
            "de-DE": "mit Betrachtung einiger Details"
        })
    """mit Betrachtung einiger Details"""

    fuehrung_ab = xAPI_Activity(
        "Observation",
        "fuehrung_ab",
        {
            "de-DE": "Führung ab"
        },
        {
            "de-DE": "Verb bezieht sich auf das Abgeben der Führung"
        })
    """Verb bezieht sich auf das Abgeben der Führung"""

    material_ab = xAPI_Activity(
        "Observation",
        "material_ab",
        {
            "de-DE": "Führung ab"
        },
        {
            "de-DE": "Verb bezieht sich auf das Abgeben des Materials"
        })
    """Verb bezieht sich auf das Abgeben des Materials"""

    material = xAPI_Activity(
        "Observation",
        "material",
        {
            "de-DE": "Material"
        },
        {
            "de-DE": "Gegnstände die zum Aufgabenlösen zur Verfügung gestellt werden"
        })
    """Gegnstände die zum Aufgabenlösen zur Verfügung gestellt werden"""

    mitschueler = xAPI_Activity(
        "Observation",
        "mitschueler",
        {
            "de-DE": "Mitschüler"
        },
        {
            "de-DE": "Verb bezieht sich auf andere Mitschüler"
        })
    """Verb bezieht sich auf andere Mitschüler"""

    nach_Aufgaben_Details = xAPI_Activity(
        "Observation",
        "nach_Aufgaben_Details",
        {
            "de-DE": "nach Aufgaben Details"
        },
        {
            "de-DE": "genaure Informationen über Aufgaben"
        })
    """genaure Informationen über Aufgaben"""

    schere = xAPI_Activity(
        "Observation",
        "schere",
        {
            "de-DE": "Schere"
        },
        {
            "de-DE": "Konkreter Gegenstand, der zum Aufgabenlösen zur Verfügung gestellt wurde"
        })
    """Konkreter Gegenstand, der zum Aufgabenlösen zur Verfügung gestellt wurde"""

    seine_Idee = xAPI_Activity(
        "Observation",
        "seine_Idee",
        {
            "de-DE": "seine Idee"
        },
        {
            "de-DE": "Von Akteur selbst erbrachte Denkleistung"
        })
    """Von Akteur selbst erbrachte Denkleistung"""

    sich_mit_anderen_Dingen = xAPI_Activity(
        "Observation",
        "sich_mit_anderen_Dingen",
        {
            "de-DE": "sich mit anderen Dingen"
        },
        {
            "de-DE": "Verb bezieht sich auf Objekte, die nichts mit der Aufgabenstellung zutun haben."
        })
    """Verb bezieht sich auf Objekte, die nichts mit der Aufgabenstellung zutun haben."""

    def __init__(self):
        super().__init__("Observation")
