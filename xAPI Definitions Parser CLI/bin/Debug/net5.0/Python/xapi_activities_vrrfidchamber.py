from core import xapi_activity
from core import xapi_activities

class xAPI_Activities_VrRfidChamber(xAPI_Activities):
    button = xAPI_Activity(
        "VrRfidChamber",
        "button",
        {
            "en-US": "button"
        },
        {
            "en-US": "Something an actor can press, which then triggers an action in the lab."
        })
    """Something an actor can press, which then triggers an action in the lab."""

    comment = xAPI_Activity(
        "VrRfidChamber",
        "comment",
        {
            "en-US": "comment"
        },
        {
            "en-US": "Some kind of written text to explain an action"
        })
    """Some kind of written text to explain an action"""

    device = xAPI_Activity(
        "VrRfidChamber",
        "device",
        {
            "en-US": "device "
        },
        {
            "en-US": "Represents the RFID chamber."
        })
    """Represents the RFID chamber."""

    door = xAPI_Activity(
        "VrRfidChamber",
        "door",
        {
            "en-US": "door"
        },
        {
            "en-US": "An actor opened or closed the door"
        })
    """An actor opened or closed the door"""

    draggable = xAPI_Activity(
        "VrRfidChamber",
        "draggable",
        {
            "en-US": "drag"
        },
        {
            "en-US": "Clicking an item, holding the click and moving it around the lab."
        })
    """Clicking an item, holding the click and moving it around the lab."""

    keyboard = xAPI_Activity(
        "VrRfidChamber",
        "keyboard",
        {
            "en-US": "keyboard"
        },
        {
            "en-US": "A keyboard used to input text or interact with the lab."
        })
    """A keyboard used to input text or interact with the lab."""

    labAudio = xAPI_Activity(
        "VrRfidChamber",
        "labAudio",
        {
            "en-US": "lab audio"
        },
        {
            "en-US": "Some audio content to feel the rela atmosphere of the lab. This can be switched on or off"
        })
    """Some audio content to feel the rela atmosphere of the lab. This can be switched on or off"""

    mouse = xAPI_Activity(
        "VrRfidChamber",
        "mouse",
        {
            "en-US": "mouse"
        },
        {
            "en-US": "A mouse used to click on buttons or drag items in the lab."
        })
    """A mouse used to click on buttons or drag items in the lab."""

    screen = xAPI_Activity(
        "VrRfidChamber",
        "screen",
        {
            "en-US": "screen"
        },
        {
            "en-US": "The screen showing the virtual lab and can be clicked or touched on for interaction with the game."
        })
    """The screen showing the virtual lab and can be clicked or touched on for interaction with the game."""

    selection = xAPI_Activity(
        "VrRfidChamber",
        "selection",
        {
            "en-US": "Select"
        },
        {
            "en-US": "Some kind of selection is represented by this. It is given by user."
        })
    """Some kind of selection is represented by this. It is given by user."""

    def __init__(self):
        super().__init__("VrRfidChamber")
