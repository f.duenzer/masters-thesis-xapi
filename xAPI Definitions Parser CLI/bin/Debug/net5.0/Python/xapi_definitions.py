from core.xapi_context_eyetracking import xAPI_Context_EyeTracking
from core.xapi_context_generic import xAPI_Context_Generic
from core.xapi_context_gestures import xAPI_Context_Gestures
from core.xapi_context_lms import xAPI_Context_Lms
from core.xapi_context_multitouch import xAPI_Context_Multitouch
from core.xapi_context_observation import xAPI_Context_Observation
from core.xapi_context_projectjupyter import xAPI_Context_ProjectJupyter
from core.xapi_context_seriousgames import xAPI_Context_Seriousgames
from core.xapi_context_systemcontrol import xAPI_Context_SystemControl
from core.xapi_context_tagformance import xAPI_Context_Tagformance
from core.xapi_context_uhfreader import xAPI_Context_UhfReader
from core.xapi_context_virtualreality import xAPI_Context_VirtualReality
from core.xapi_context_vrrfidchamber import xAPI_Context_VrRfidChamber

class xAPI_Definitions:
    eyeTracking = xAPI_Context_EyeTracking()

    generic = xAPI_Context_Generic()

    gestures = xAPI_Context_Gestures()

    lms = xAPI_Context_Lms()

    multitouch = xAPI_Context_Multitouch()

    observation = xAPI_Context_Observation()

    projectJupyter = xAPI_Context_ProjectJupyter()

    seriousgames = xAPI_Context_Seriousgames()

    systemControl = xAPI_Context_SystemControl()

    tagformance = xAPI_Context_Tagformance()

    uhfReader = xAPI_Context_UhfReader()

    virtualReality = xAPI_Context_VirtualReality()

    vrRfidChamber = xAPI_Context_VrRfidChamber()
