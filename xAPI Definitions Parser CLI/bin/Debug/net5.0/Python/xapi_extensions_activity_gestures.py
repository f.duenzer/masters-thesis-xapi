from core.xapi_extensions_activity import xAPI_Extensions_Activity

class xAPI_Extensions_Activity_Gestures(xAPI_Extensions_Activity):

    def __init__(self):
        super().__init__("Gestures")

    def hand(self, value):
        add(xAPI_Extension(
                context,
                extensionType,
                "hand",
                {
                    "en-US": "hand",
                    "de-DE": "Hand"
                },
                {
                    "en-US": "Hand which is currently active.",
                    "de-DE": "Die Hand, die gerade verwendet wird."
                }),
            value);
        return self;
