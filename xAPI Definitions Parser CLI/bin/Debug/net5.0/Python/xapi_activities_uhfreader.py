from core import xapi_activity
from core import xapi_activities

class xAPI_Activities_UhfReader(xAPI_Activities):
    button = xAPI_Activity(
        "UhfReader",
        "button",
        {
            "en-US": "button"
        },
        {
            "en-US": "A field which can be click to fire an action. For example the 'start button'."
        })
    """A field which can be click to fire an action. For example the 'start button'."""

    checkbox = xAPI_Activity(
        "UhfReader",
        "checkbox",
        {
            "en-US": "checkbox"
        },
        {
            "en-US": "A box which can be checked or unchecked. For example in this case Read TID should be selected."
        })
    """A box which can be checked or unchecked. For example in this case Read TID should be selected."""

    identification = xAPI_Activity(
        "UhfReader",
        "identification",
        {
            "en-US": "identification"
        },
        {
            "en-US": "A progress to identify "
        })
    """A progress to identify """

    mouse = xAPI_Activity(
        "UhfReader",
        "mouse",
        {
            "en-US": "mouse"
        },
        {
            "en-US": "A mouse used to click on buttons in the software."
        })
    """A mouse used to click on buttons in the software."""

    readerImage = xAPI_Activity(
        "UhfReader",
        "readerImage",
        {
            "en-US": "reader image"
        },
        {
            "en-US": "A image of the reader is generated which is interactible. The reading head is selected in the image."
        })
    """A image of the reader is generated which is interactible. The reading head is selected in the image."""

    rfidTag = xAPI_Activity(
        "UhfReader",
        "rfidTag",
        {
            "en-US": "rfidTag"
        },
        {
            "en-US": "Represent the RFID tag. You can drag it as per your requirements"
        })
    """Represent the RFID tag. You can drag it as per your requirements"""

    transponder = xAPI_Activity(
        "UhfReader",
        "transponder",
        {
            "en-US": "transponder"
        },
        {
            "en-US": "Represents the transpnder to the physical reader."
        })
    """Represents the transpnder to the physical reader."""

    def __init__(self):
        super().__init__("UhfReader")
