from core.xapi_extensions_activity_generic import xAPI_Extensions_Activity_Generic
from core.xapi_extensions_result_generic import xAPI_Extensions_Result_Generic

class xAPI_Context_Generic_Extensions:

    def __init__(self):
        pass

    @property
    def activity(self):
        return xAPI_Extensions_Activity_Generic();

    @property
    def result(self):
        return xAPI_Extensions_Result_Generic();
