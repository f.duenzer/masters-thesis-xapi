from core.xapi_extensions_result import xAPI_Extensions_Result

class xAPI_Extensions_Result_ProjectJupyter(xAPI_Extensions_Result):

    def __init__(self):
        super().__init__("ProjectJupyter")

    def cellOutput(self, value):
        add(xAPI_Extension(
                context,
                extensionType,
                "cellOutput",
                {
                    "en-US": "Cell Output",
                    "de-DE": "Zellenausgabe"
                },
                {
                    "en-US": "A string representing the cell outputs of a JupyterLab cell. Outputs can contain a.o. execution results or display data.",
                    "de-DE": "Ein String der die Ausgabe einer JupyterLab-Zelle beschreibt. Ausgaben können u.a. Ausführungsergebnisse oder Anzeigedaten enthalten."
                }),
            value);
        return self;

    def errorMessage(self, value):
        add(xAPI_Extension(
                context,
                extensionType,
                "errorMessage",
                {
                    "en-US": "Error message",
                    "de-DE": "Fehlermeldung"
                },
                {
                    "en-US": "A string describing an error. Used to provide the runtimeerror that might occur during the execution of ta JupyterLab notebook cell.",
                    "de-DE": "Die beschreibung eines Fehlers. Wird verwended um z.b. Laufzeitfehler während des Ausführens einer JupyterLab Notebook Zelle anzugeben."
                }),
            value);
        return self;
