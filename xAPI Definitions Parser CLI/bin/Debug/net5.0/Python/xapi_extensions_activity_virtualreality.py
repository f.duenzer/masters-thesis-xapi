from core.xapi_extensions_activity import xAPI_Extensions_Activity

class xAPI_Extensions_Activity_VirtualReality(xAPI_Extensions_Activity):

    def __init__(self):
        super().__init__("VirtualReality")

    def actionName(self, value):
        add(xAPI_Extension(
                context,
                extensionType,
                "actionName",
                {
                    "en-US": "action name",
                    "de-DE": "Aktionsname"
                },
                {
                    "en-US": "It is the name of an action.",
                    "de-DE": "Es ist der Name einer Aktion."
                }),
            value);
        return self;

    def triangle(self, value):
        add(xAPI_Extension(
                context,
                extensionType,
                "triangle",
                {
                    "en-US": "Triangle",
                    "de-DE": "Dreieck"
                },
                {
                    "en-US": "Sequence of vertices with it's index, color and position.",
                    "de-DE": "Sequenz von Knotenpunkten mit dessen Index, Farbe und Position."
                }),
            value);
        return self;

    def vrObjectName(self, value):
        add(xAPI_Extension(
                context,
                extensionType,
                "vrObjectName",
                {
                    "en-US": "VR object name",
                    "de-DE": "VR Objekt Name"
                },
                {
                    "en-US": "Name of a VR object. Must be a string.",
                    "de-DE": "Namen vom VR Objekt. Muss ein String sein."
                }),
            value);
        return self;
