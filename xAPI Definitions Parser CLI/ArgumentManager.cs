﻿using System;
using System.Collections.Generic;

namespace xAPI_Definitions_Parser_CLI {
    public class ArgumentManager {
        private IDictionary<string, string> values = new Dictionary<string, string>();
        private IDictionary<string, string> defaultValues;

        public bool HasArguments => values.Count > 0;

        public ArgumentManager(string[] args, IDictionary<string, string> defaultValues = default) {
            ParseArguments(args);
            this.defaultValues = defaultValues;
        }

        private void ParseArguments(IReadOnlyList<string> args) {
            values.Clear();
            if (args.Count % 2 != 0) {
                throw new FormatException("Not every argument has a value. Or one value has no argument identifier.");
            }
            for (int i = 0; i < args.Count; i += 2) {
                var key = args[i];
                if (!key.StartsWith('-')) {
                    throw new FormatException($"Every argument identifier should start with '-'. {key} does not start with '-'. Do you instead mean -{key}?");
                }
                var value = args[i + 1];
                values.Add(key.Replace("-", ""), value);
            }
        }

        public void SetValue(string key, string value) {
            values[key] = value;
        }

        public string GetValue(string key) {
            string value;
            if (!values.TryGetValue(key, out value)) {
                defaultValues?.TryGetValue(key, out value);
            }
            return value;
        }
    }
}