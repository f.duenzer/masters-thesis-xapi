﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using xAPI_Definitions_Parser;
using xAPI_Definitions_Parser.IO;

namespace xAPI_Definitions_Parser_CLI {
    internal class Program {
        private static ArgumentManager am;

        private static void Main(string[] args) {
            try {
                //source -> path, projectId, branch (optional), privateKey
                //          path
                //                                  -> lang -> namespace -> output
                var defaultValues = new Dictionary<string, string> {
                    ["source"] = "local",
                    ["namespace"] = "xAPI"
                };
                am = new ArgumentManager(args, defaultValues);
                ReadArguments();

                var sourceType = EnumMethods.GetSourceKeyFromString(am.GetValue("source"));
                if (sourceType == null) {
                    throw new Exception($"{am.GetValue("source")} is not a valid source key.");
                }

                /*Generator.Generate((SourceType)sourceType, am.GetValue("path"), am.GetValue("projectId"), am.GetValue("branch"),
                    am.GetValue("privateKey"), SourceType.Local, (Language)EnumMethods.GetLanguage(am.GetValue("lang")),
                    am.GetValue("namespace"), am.GetValue("output"), Log);*/

                var compile = am.GetValue("compile");
                var lang = (CodeLanguage)EnumMethods.GetLanguage(am.GetValue("lang"));

                if (string.IsNullOrEmpty(compile)) {
                    Generator.ParseAsync((SourceType)sourceType, am.GetValue("path"), am.GetValue("projectId"), am.GetValue("branch"),
                        am.GetValue("privateKey"), SourceType.Local, lang, am.GetValue("namespace"), am.GetValue("output"), Log)
                        .Wait();
                } else {
                    var compArgs = Generator.CreateCompilationArguments(lang, compile,
                        am.GetValue("dotnet"), am.GetValue("refpath"));
                    Generator.CompileAsync((SourceType)sourceType, am.GetValue("path"), am.GetValue("projectId"), am.GetValue("branch"),
                        am.GetValue("privateKey"), SourceType.Local, am.GetValue("namespace"), compArgs, Log)
                        .Wait();
                }

                /*sourceManager = Generator.CreateManager((SourceType)sourceType, am.GetValue("projectId"), am.GetValue("privateKey"), am.GetValue("branch"));
                sourceManager.OnLog += Log;

                var contexts = sourceManager.ReadContexts(am.GetValue("path"));
                var deserializedContexts = Deserializer.DeserializeContexts(contexts);

                CodeGenerator generator = null;
                Language lang = (Language)EnumMethods.GetLanguage(am.GetValue("lang"));
                generator = Generator.CreateGenerator(lang, am.GetValue("namespace"));
                generator.OnLog += Log;
                var codeFiles = generator.GenerateContexts(deserializedContexts);

                FileManager targetManager = null;
                if (sourceManager is FileManager fileManager) {
                    targetManager = fileManager;
                } else {
                    targetManager = new FileManager();
                }
                targetManager.WriteCodeFiles(am.GetValue("output"), codeFiles, lang);*/
            }
            catch (Exception ex) {
                Error(ex.Message);
                WaitForInput();
            }

            /*assets -> xapi, eduxr.xapi -> resources -> scripts xapi.cs -> controllers bsp playerwatchcontroller
            einheitliche struktur gut für multiplattform
            an xapi gitlab dranhängen + lokale hinzufügen
            im docker -> simple files ok, database auch ok, dockerisieren
            dll/package*/
        }

        private static void Error(string message) {
            Console.ForegroundColor = ConsoleColor.Red;
            Log(message);
            Console.ForegroundColor = ConsoleColor.Gray;
        }

        private static void Log(string message) {
            Console.WriteLine(message);
        }

        private static void WaitForInput() {
            Log("Press any key to close the window.");
            Console.ReadKey(true);
        }

        private static void ReadInput(string property, ref string outVal) {
            var defaultValue = string.IsNullOrEmpty(outVal) ? "" : $"({outVal})";
            string input = null;
            while (input == null) {
                Console.WriteLine("{0}{1}: ", property, defaultValue);
                input = Console.ReadLine();
                if (defaultValue != string.Empty && input == string.Empty) {
                    input = outVal;
                }
            }
            outVal = input;
        }

        private static void ReadArguments() {
            if (am == null) {
                return;
            }

            ReadSource(IOManager.GetSubClassesSourceKeys());

            if (am.GetValue("source") == FileManager.SourceKey.GetSourceKeyString()) {
                ReadPathLocal();
            } else if (am.GetValue("source") == GitLabManager.SourceKey.GetSourceKeyString()) {
                ReadPathGitLab();
                ReadProjectId();
                ReadPrivateKey();
                ReadBranch();
            }

            ReadLang();
            CodeLanguage lang = EnumMethods.GetLanguage(am.GetValue("lang"))
                ?? throw new ArgumentException($"Invalid Language {am.GetValue("language")}. Must be one of {EnumMethods.GetNames(CodeLanguage.CSharp.GetType())}");

            if (lang.UsesNamespace()) {
                ReadNamespace();
            }

            ReadOutput();
        }

        private static void ReadArgument(string identifier, string inputQuery = default) {
            var arg = am.GetValue(identifier);
            if (arg == null) {
                ReadInput($"{(inputQuery ?? identifier)}", ref arg);
                am.SetValue(identifier, arg);
            }
        }

        private static void ReadSource(List<SourceType> validSources) {
            while (!validSources.Any(s => s.GetSourceKeyString() == am.GetValue("source"))) {
                ReadArgument("source", "Source type");
            }
        }

        private static void ReadPathLocal() {
            while (!Directory.Exists(am.GetValue("path"))) {
                ReadArgument("path", "Local input path");
            }
        }

        private static void ReadPathGitLab() {
            ReadArgument("path", "GitLab input path");
        }

        private static void ReadProjectId() {
            ReadArgument("projectId", "GitLab project ID");
        }

        private static void ReadBranch() {
            ReadArgument("branch", "GitLab project branch (empty means \'master\')");
        }

        private static void ReadPrivateKey() {
            ReadArgument("privateKey", "GitLab Personal Access Token");
        }

        private static void ReadLang() {
            ReadArgument("lang", $"Language to generate for ({CodeLanguage.CSharp.GetName()}, {CodeLanguage.JavaScript.GetName()})");
        }

        private static void ReadNamespace() {
            ReadArgument("namespace", "Namespace (can be skipped for JavaScript)");
        }

        private static void ReadOutput() {
            ReadArgument("output", "Output path");
        }
    }
}